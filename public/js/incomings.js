$(document).ready(function(){
    
     $('#incomingdate').datepicker({
        format: "yyyy-mm-dd",
        startDate: "-365d",
        endDate: "1d",
        multidate: false,
        autoclose: true,
        todayHighlight: true,
        orientation: "top right"
        });
        
         $('#cancelation_date').datepicker({
        format: "yyyy-mm-dd",
        startDate: "-365d",
        endDate: "1d",
        multidate: false,
        autoclose: true,
        todayHighlight: true,
        orientation: "top right"
        });


});