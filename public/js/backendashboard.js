 $(document).ready(function(){ 
     
        $('select#operator_id').multipleSelect({ selectAll: true, width: 220,multipleWidth: 90,multiple: true});
        $('select#vendor_id').multipleSelect({ selectAll: true, width: 220,multipleWidth: 90,multiple: true});

        loadSalebifurcation();
       
        
        function loadSalebifurcation(oprids,sync_date,vendorids)
        {
           $('button#btnfiltersalebifurcation').addClass('disabled');
           
           oprids = (oprids == undefined)?"":oprids;
           vendorids = (vendorids == undefined)?"":vendorids;
           sync_date = (sync_date == undefined)?"":sync_date;
            
            var salebifurcation=$.getJSON(HOST+'dashboard/getBackendDashBoard',{oprids:oprids,sb_sync_date:sync_date,vendorids:vendorids});

             salebifurcation.done(function(res){
                 if(res.status && res.type)
                 {
                     if(res.data!="")
                     {
                        $('div#salebifurcationreport').html(res.data);
                        $('table.salebifurcation').stickyTableHeaders({fixedOffset: 62});
                        $.loadingbar.hide("div#salebifurcationmsg");
                     }
                     else
                     {
                         alert("No data retrieved");
                         $('div#salebifurcationreport').html("");
                          $.loadingbar.hide("div#salebifurcationmsg");
                     }
                 }
                 else
                 {
                     alert("Error");
                     $.loadingbar.hide("div#salebifurcationmsg");
                 }
             }).then(function(){
                    $('button#btnfiltersalebifurcation').removeClass('disabled');
             });
        
        }
        
        $('button#btnfiltersalebifurcation').on('click',function(){
                var oprids=$("select#operator_id").multipleSelect("getSelects");
                var vendorids=$("select#vendor_id").multipleSelect("getSelects");
                var sync_date=$('input#sb_sync_date').val();
                $.loadingbar.show("div#salebifurcationmsg","Loading sale bifurcation report");
                loadSalebifurcation(oprids,sync_date,vendorids);
        });
        
         setInterval(function(){   $.loadingbar.show("div#salebifurcationmsg","Auto refresh in process"); loadSalebifurcation($("select#operator_id").multipleSelect("getSelects"),$('input#sb_sync_date').val(),$("select#vendor_id").multipleSelect("getSelects")); }, 300000);
 });
 
 function filterbackenddashboard()
{
    //var oprids=$("select#operator_id").multipleSelect("getSelects");
    //window.location.href="/dashboard/"+"?oprids="+oprids;
}


/*
 * Start of Note system
 * Start
 */
$(document).on('show.bs.modal','#addnotesdiv',function(e){
        
            var soid = $(e.relatedTarget).data('soid');
            var operator_id = $(e.relatedTarget).data('oprid');
            var type=1;
           
            $(e.currentTarget).find('#notesthread').empty();
            $(e.currentTarget).find('input#inpnotesoid').val(soid);
            $(e.currentTarget).find('button#btnrefreshcomments').attr('data-soid',soid);
            $(e.currentTarget).find('input[type=hidden][id=addnotedate]').val($('input#sb_sync_date').val());
             $(e.currentTarget).find('input#inpnoteoprid').val(operator_id);
            getNotes(soid,type,operator_id);     
});


$(document).on('click','button#btnaddnote',function(){
    
    var message=$('div#addnotesdiv').find('textarea#addnotemsg').val();
    var soid=$('div#addnotesdiv').find('input[type=hidden][id=inpnotesoid]').val();
    var note_date=$('div#addnotesdiv').find('input#addnotedate').val();
    var oprid=$('div#addnotesdiv').find('input[type=hidden][id=inpnoteoprid]').val();

    console.log({message:message,soid:soid,note_date:note_date,type:1});



    if(message=="" || soid=="" || note_date=="")
    {
        alert("Fill all details");
        return;
    }
    else
    {
        
        var savenote=$.post(HOST+'notes/save',{message:message,soid:soid,note_date:note_date,type:1,operator_id:oprid});
        
        savenote.done(function(res){
            
            res=$.parseJSON(res);
            
            if(res.status && res.error){
                alert(res.message);
                return;
            }
            if(res.status && res.type)
            {
                    $('div#addnotemsg').showSuccess("Done").done(function(){ getNotes(soid,1);});
                    $('div#addnotesdiv').find('textarea#addnotemsg').val('');
                    
                    // Increment count on DOM
                     var messagecount=parseInt($('tr#tr_'+soid+' td.tdcomments span').text())+1;
                     $('tr#tr_'+soid+' td.tdcomments span').text(messagecount);
            }
            else
            {
                  $('div#addnotemsg').showFailure("Error");
            }
        });
        
    }
});


 function getNotes(soid,type,operator_id)
{
             var getnotes=$.getJSON(HOST+'notes/fetchNotesBySoid',{soid:soid,type:type,note_date:$('input#sb_sync_date').val(),operator_id:operator_id});
            
           getnotes.done(function(res){
              if(res.status && res.data.length)
              {
                     $('#notesthread').empty();
                     $.each(res.data,function(k,v){
                         
                         var HTML="";
                         HTML="<div class='row' style='margin-bottom: 5px;'>";
                         HTML+="<small class='pull-right time'><i class='fa fa-clock-o'></i>&nbsp;"+v.note_date+" / "+v.note_timestamp+"</small>";
                         HTML+="<h5 class='media-heading notesusername'>"+v.username+"</h5>";
                         HTML+="<small class='col-lg-l0'>"+v.message+"</small>";
                         HTML+="</div>";
                         
                         $('div#addnotesdiv #notesthread').append(HTML);
                     });
                     
                      $('button#btnrefreshcomments i').removeClass('glyphicon-refresh-animate');
              }
              else
              {
                 console.log("No Comments Fetched");
                 $('button#btnrefreshcomments i').removeClass('glyphicon-refresh-animate');
              }
              
           });
           
}

$(document).on('click','#btnrefreshcomments',function(e){
        $('button#btnrefreshcomments i').addClass('glyphicon-refresh-animate');
        var soid=$(this).attr('data-soid');
        $('#notesthread').empty();
        getNotes(soid,1);
});

/*
 * End
 */
    
