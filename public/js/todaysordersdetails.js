$(document).ready(function(){
    
    $('button.btn-sendsms').on('click',function(){
        
        var paymentid=$(this).attr('data-paymentid');
        var supplierid=$(this).attr('data-supplierid');
        var ele=$(this);
        
        var sendsms=$.post(HOST+'dashboard/sendsms',{paymentid:paymentid,supplierid:supplierid});
        
        sendsms.done(function(res){
            
         res=$.parseJSON(res);
         
         if(res.status && res.type)
         {
             ele.addClass('btn-success');
             ele.addClass('disabled');
             showMessage("Sms sent to #"+paymentid);
         }
         else
         {
             ele.addClass('btn-danger');
             alert("Error : "+res.message);
             showMessage("Sms not sent to #"+paymentid,'error');
         }
         
        });
        
    });
    
       $('button.btn-sendemail').on('click',function(){
        
        var paymentid=$(this).attr('data-paymentid');
        var supplierid=$(this).attr('data-supplierid');
        var ele=$(this);
        
        var sendsms=$.post(HOST+'dashboard/sendEmail',{paymentid:paymentid,supplierid:supplierid});
        
        sendsms.done(function(res){
            
         res=$.parseJSON(res);
         
         if(res.status && res.type)
         {
             ele.addClass('btn-success');
             showMessage("Email  sent to #"+paymentid);
         }
         else
         {
             ele.addClass('btn-danger');
             alert("Error : "+res.message);
             showMessage("Email not sent to #"+paymentid,'error');
         }
         
        });
        
    });
    
    
        
    $('#addcomment').on('show.bs.modal', function(e) {

     
      var orderid = $(e.relatedTarget).data('orderid');
      var soid = $(e.relatedTarget).data('soid');
      
     $(e.currentTarget).find('input#comment_order_id').val(orderid) ;
     $(e.currentTarget).find('input#comment_so_id').val(soid) ;

     $(e.currentTarget).find('textarea[name=comment]').val('');
     $(e.currentTarget).find('div#ordercommentmsg').empty();
     $(e.currentTarget).find('div#loadcomments').empty();
     
     var comments=$.getJSON(HOST+'orderapprovals/getComments',{soid:soid});
     
     comments.done(function(res){
         
          if(res.type && res.status)
          {
              $.each(res.comments,function(k,v){
                  
                         var row="";
                         row+="<p>";
                         row+=v.comment
                         row+="........"+v.name
                         row+="</p>";
                         
                   $(e.currentTarget).find('div#loadcomments').append(row);      
              })
          }
          else
          {
              $(e.currentTarget).find('div#loadcomments').html("No Comments Yet");
          }
          
     });
     
    
    });
    
    $(document).on('click','button#addcommentbtn',function(){
    
    
       var  orderid=$('div#addcomment').find('input#comment_order_id').val();
       var  soid=$('div#addcomment').find('input#comment_so_id').val();
       var  comment=$('div#addcomment').find('textarea#comment').val();
  
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       var addComment=$.post(HOST+'orderapprovals/saveComments',{orderid:orderid,soid:soid,comment:comment});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#ordercommentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#addcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
    });
    
});


