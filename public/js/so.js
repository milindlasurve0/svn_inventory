$(function(){
  
   var oldmarginval=parseFloat($('input#commission_type_formula').val());
   var oldbaseval=parseFloat($('input#basesale').val());

  $('button#btneditsomapping').on('click',function(){
   
    var newmarginval=parseFloat($('input#commission_type_formula').val());
    var newbaseval=parseFloat($('input#basesale').val());
    
    if(oldmarginval!=newmarginval ||  oldbaseval!=newbaseval )
    {
          $('#socommentmdl').modal('show');
          
          if(oldmarginval!=newmarginval){$('div.divmarginchange').show();};
          if(oldbaseval!=newbaseval){$('div.divbasechange').show();};
    }
    else
    {
          $('form#editsomapping').submit();
    }
  
});

$('#btnsobasecomment').on('click',function(){
    
   var newmarginval=parseFloat($('input#commission_type_formula').val());
   var newbaseval=parseFloat($('input#basesale').val());
    
   if(oldmarginval!=newmarginval)
   {
    var somargincomment =$.trim($('#socommentmdl').find('textarea#txtmarginchangecomment').val());
    if(somargincomment=="")
    {
        alert("Enter comment for margin change");
        return;
    }
      
   $('<input>').attr({ type: 'hidden',id: 'somargincomment',name: 'somargincomment',value:somargincomment}).appendTo('form#editsomapping');
   
    }
    
    if(oldbaseval!=newbaseval)
    {
    var sobasecomment =$.trim($('#socommentmdl').find('textarea#txtbasechangecomment').val());
    if(sobasecomment=="")
    {
        alert("Enter comment for basesale change");
        return;
    }
   $('<input>').attr({ type: 'hidden',id: 'sobasecomment',name: 'sobasecomment',value:sobasecomment}).appendTo('form#editsomapping');
   
    }
   
    $('form#editsomapping').submit();
   
});

$(document).on('keyup','input[type=text][name=basesale]',function(e)
{
    var textval=$(this).val();
    $('#capacity').val(textval*4);
});

});

