$(document).ready(function(){
    
      $('#sandbox-container .input-daterange').datepicker({
            format: "yyyy-mm-dd",
            startDate: "-365d",
            endDate: "1d",
            todayHighlight: true,
             orientation: "top right"
           });   
});


/*
 * Start of Note system
 * Start
 */
$(document).on('show.bs.modal','#addnotesdiv',function(e){
        
            var soid = $(e.relatedTarget).data('soid');
            var oprid=$(e.relatedTarget).data('oprid');
            var type=$(e.relatedTarget).data('type');

            $(e.currentTarget).find('#notesthread').empty();
            $(e.currentTarget).find('input#inpnotesoid').val(soid);
            $(e.currentTarget).find('input#inpnoteoprid').val(oprid);
            $(e.currentTarget).find('input#inpnotetype').val(type);
            $(e.currentTarget).find('button#btnrefreshcomments').attr('data-soid',soid);
             $(e.currentTarget).find('button#btnrefreshcomments').attr('data-type',type);
              $(e.currentTarget).find('button#btnrefreshcomments').attr('data-oprid',oprid);

            $(e.currentTarget).find('input[type=hidden][id=addnotedate]').val(getUrlParameter('from_history_date'));
            getNotes(soid,type,oprid);     
});


$(document).on('click','button#btnaddnote',function(){
    
    var message=$('div#addnotesdiv').find('textarea#addnotemsg').val();
    var soid=$('div#addnotesdiv').find('input[type=hidden][id=inpnotesoid]').val();
    var note_date=$('div#addnotesdiv').find('input#addnotedate').val();
    var oprid=$('div#addnotesdiv').find('input[type=hidden][id=inpnoteoprid]').val();
    var type=$('div#addnotesdiv').find('input[type=hidden][id=inpnotetype]').val();

    console.log({message:message,soid:soid,note_date:note_date,type:type});



    if(message=="" || soid=="" || note_date=="")
    {
        alert("Fill all details");
        return;
    }
    else
    {
        
        var savenote=$.post(HOST+'notes/save',{message:message,soid:soid,note_date:note_date,type:type,operator_id:oprid});
        
        savenote.done(function(res){
            
            res=$.parseJSON(res);
            
            if(res.status && res.error){
                alert(res.message);
                return;
            }
            if(res.status && res.type)
            {
                    $('div#addnotemsg').showSuccess("Done").done(function(){ getNotes(soid,type,oprid);});
                    $('div#addnotesdiv').find('textarea#addnotemsg').val('');
                   // $('div#addnotesdiv').find('input#addnotedate').val('');
            }
            else
            {
                  $('div#addnotemsg').showFailure("Error");
            }
        });
        
    }
});


 function getNotes(soid,type,operator_id)
{
             var getnotes=$.getJSON(HOST+'notes/fetchNotesBySoid',{soid:soid,type:type,note_date:getUrlParameter('from_history_date'),operator_id:operator_id});
            
           getnotes.done(function(res){
              if(res.status && res.data.length)
              {
                     $('#notesthread').empty();
                     $.each(res.data,function(k,v){
                         
                         var HTML="";
                         HTML="<div class='row' style='margin-bottom: 5px;'>";
                         HTML+="<small class='pull-right time'><i class='fa fa-clock-o'></i>&nbsp;"+v.note_date+" / "+v.note_timestamp+"</small>";
                         HTML+="<h5 class='media-heading notesusername'>"+v.username+"</h5>";
                         HTML+="<small class='col-lg-l0'>"+v.message+"</small>";
                         HTML+="</div>";
                         
                         $('div#addnotesdiv #notesthread').append(HTML);
                     });
                     
                      $('button#btnrefreshcomments i').removeClass('glyphicon-refresh-animate');
              }
              else
              {
                 console.log("No Comments Fetched");
                 $('button#btnrefreshcomments i').removeClass('glyphicon-refresh-animate');
              }
              
           });
           
}

$(document).on('click','#btnrefreshcomments',function(e){
        $('button#btnrefreshcomments i').addClass('glyphicon-refresh-animate');
        var soid=$(this).attr('data-soid');
        var type=$(this).attr('data-type');
        var oprid=$(this).attr('data-oprid');
        $('#notesthread').empty();
        getNotes(soid,type,oprid);
});

/*
 * End
 */
