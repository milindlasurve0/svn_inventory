$(document).ready(function(){
    
   $('#incoming_date').datepicker({
    format: "yyyy-mm-dd",
    startDate: "-3d",
    endDate: "1d",
    multidate: false,
    autoclose: true,
    todayHighlight: true,
    orientation: "top right"
    });
    
    $('input[name="inpapiincoming[]"]').ForceNumericOnly();
    
    $('button#btnsaveapicoming').on('click',function(){
            
           var soid=$(this).attr('data-soid');
           var sid=$(this).attr('data-sid');
           var oid=$(this).attr('data-oid');
           var vendorid=$(this).attr('data-vendorid');
           var incoming=parseFloat($('input#inpso_'+soid).val());
           
           if(incoming<0 || incoming=="" || isNaN(incoming))
           {
               alert("Invalid Incoming");
               return;
           }
               
           var saveincoming=$.post(HOST+'Incomings/saveincoming',{sid:sid,oid:oid,soid:soid,incoming:incoming,vendor_id:vendorid,date:getUrlParameter('incoming_date')});
           
           saveincoming.done(function(res){
            
               res=$.parseJSON(res);
               
                if(res.type && res.status)
                {
                   showMessage("Incoming set successfully ");
                }
                else
                {
                    alert("Error ");
                }
           });
           
    });
    
    $('input[name="inpapiincoming[]"]').on('keyup',function(){
        
           var soid=$(this).attr('data-soid');
           var incoming=parseFloat($(this).val());
           if(incoming>0){
           $('tr#so_'+soid).find('td:eq(3)').text($(this).val());
            }else{
            $('tr#so_'+soid).find('td:eq(3)').text($('tr#so_'+soid).find('td:eq(3)').attr('data-oldincoming')); 
            }
    });
})