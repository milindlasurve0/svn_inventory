var VARIANCE_PERCENTAGE;

$(document).ready(function(){
    
    $('#getOrder').on('click',function(){
        
        var operator_id=$('select#operator_id').val();
        var order_days=$('select#order_days').val();
        order_days=parseFloat(order_days);
         $.loadingbar.show("div.loadingbar");
            /*
            * Get operatorwise last day sale
            * Start
            */
             $('div#owlds').show();
             
            var totalLastDaySale=$.getJSON(HOST+'orders/getTotalLastDaySale',{operator_id:operator_id});

            totalLastDaySale.done(function(res){

                    if(res.type && res.status && res.lastdaysale!=null)
                    {
                        $('div#owlds span').text(ReplaceNumberWithCommas(res.lastdaysale));
                        
                        // Set Expected Sale based on variance
                     
                        expectedsale=parseFloat(res.lastdaysale)+((parseFloat(VARIANCE_PERCENTAGE)*parseFloat(res.lastdaysale))/100);
                        $('div#owes span').text(ReplaceNumberWithCommas(expectedsale.toFixed(2)));
                        
                        // set Attr
                        $('div#owes span').attr('data-owes',expectedsale.toFixed(2));
                        
                        //Set cookie
                       
                         $.cookie('owlds_'+operator_id,res.lastdaysale,{expires:cookieexpirationDate,path:'/'});
                         $.cookie('owes_'+operator_id,expectedsale,{expires:cookieexpirationDate,path:'/'});
                    }
                    else
                    {
                        $('div#owlds span').text('N.A');
                    }
                    
            });
            /*
            * End
            */
           
           /*
            * Last Day Failiure percentage
            * Start
            */
            $('div#owfp').show();
            
           var totalfailurepercentage=$.getJSON(HOST+'orders/getTotalFailurePercentage',{operator_id:operator_id});

           totalfailurepercentage.done(function(res){

              if(res.type && res.status && (res.failurepercentage>0))
                   {
                       $('div#owfp span').text(res.failurepercentage.toFixed(2)+'%');
                       
                        //Set cookie
                         $.cookie('owfp_'+operator_id,res.failurepercentage,{expires:cookieexpirationDate,path:'/'});
                   }
                   else
                   {
                       $('div#owfp span').text('N.A');
                   }

           });


           /*
            * End
            */
           
           /*
            * Current Balance
            * Start
            */
           
              $('div#owcb').show();
            
           var getCurrentBalance=$.getJSON(HOST+'orders/getCurrentBalance',{operator_id:operator_id});

           getCurrentBalance.done(function(res){

              if(res.type && res.status && (res.balance>0))
                   {
                       $('div#owcb span').text(res.balance);
                       
                       //Set cookie
                         $.cookie('owcb_'+operator_id,res.balance,{expires:cookieexpirationDate,path:'/'});
                   }
                   else
                   {
                       $('div#owcb span').text('N.A');
                   }

           });
           
           /*
            * End
            */
           
           /*
            * Total Api Sale
            * Start
            */
           
          $('div#owldsfa').show();
            
           var getTotalapiSale=$.getJSON(HOST+'orders/getTotalapiSale',{operator_id:operator_id});

           getTotalapiSale.done(function(res){

              if(res.type && res.status && (res.sale>0))
                   {
                       $('div#owldsfa span').text(res.sale);
                       
                       //Set cookie
                         $.cookie('owldsfa_'+operator_id,res.sale,{expires:cookieexpirationDate,path:'/'});
                         
                   }
                   else
                   {
                       $('div#owldsfa span').text('N.A');
                   }

           });
           
           /*
            * End
            */
           
           
           /*
            * Avg Total Api Sale
            * Start
            */
           
          $('div#owaas').show();
            
           var getTotalapiSale=$.getJSON(HOST+'orders/getTotalapiSale',{operator_id:operator_id,avg:'1'});

           getTotalapiSale.done(function(res){

              if(res.type && res.status && (res.sale>0))
                   {
                       $('div#owaas span').text(res.sale);
                       
                       //Set cookie
                         $.cookie('owaas_'+operator_id,res.sale,{expires:cookieexpirationDate,path:'/'});
                   }
                   else
                   {
                       $('div#owaas span').text('N.A');
                   }

           });
           
           /*
            * End
            */
           
           /*
            *Total Operator Pending
            * Start
            */
           
          $('div#owp').show();
            
           var getTotalapiSale=$.getJSON(HOST+'orders/operatorWiseTotalPending',{operator_id:operator_id});

           getTotalapiSale.done(function(res){

              if(res.type && res.status)
                   {
                       $('div#owp span').text(res.pending);
                   }
                   else
                   {
                       $('div#owp span').text('N.A');
                   }

           });
           
           /*
            * End
            */
           
           
          
            
         
           
           /*
            * Total Order
            */
           $('div#owes').show();
           $('div#owes').find('span').text('?');
           /*
            * Total Order
            */
           $('div#owto').show();
           $('div#owto').find('span').text('?');
           
           /*
            * Shortage for the day 
            */
            $('div#owsftd').show();
            $('div#owsftd').find('span').text('?');
            
          
            
           
        
          /*
           * Check if Order Exists for particular operator 
           */
          var existsFlag=false;
          
           var checkorderexists=$.getJSON(HOST+'orders/checkifOrderexists',{operator_id:operator_id});
           
           checkorderexists.then(function(res){
            
            /*
             * Get Supplierslist if Order already  exists
             */
            
               if(res.type==true)
               {
                   existsFlag=true;
                   
                     var supplierlist=$.getJSON(HOST+'orders/getSuppliersForOrders',{operator_id:operator_id,existsFlag:'1'});
                   
                   supplierlist.done(function(response){
                         
                            $('div#existsmessage').html('<div class="alert alert-info" role="alert">Order already exists for this operator <a style="display:none" href="/orders/reset/'+operator_id+'">Reset</a></div>');
                            
                           $('select#order_days').val(res.no_of_days);
                           
                            if(typeof(Storage) !== "undefined") { localStorage.removeItem("operator_"+operator_id);localStorage.setItem("operator_"+operator_id,JSON.stringify(response.supplierlist));}
                           
                            createSupplierHTML(response.supplierlist);
                            
                            $.loadingbar.hide("div.loadingbar");
                         
                     });
                   
               }
               else
               {
                   $('div#existsmessage').empty();
               }
               
           }).then(function(){
        
                            /*
                             * Get Supplierslist if Order doesnot exists
                             */
                            if(order_days>0  && existsFlag===false)
                            {
                                var supplierlist=$.getJSON(HOST+'orders/getSuppliersForOrders',{operator_id:operator_id,order_days:order_days,existsFlag:'0'});

                              
                                supplierlist.done(function(response){

                                     if(typeof(Storage) !== "undefined") { localStorage.removeItem("operator_"+operator_id);localStorage.setItem("operator_"+operator_id,JSON.stringify(response.supplierlist));}
                                      
                                      createSupplierHTML(response.supplierlist);
                                      
                                       $.loadingbar.hide("div.loadingbar");

                                });
                            }
          
            });
          
          
          
          
          // End
          
    });
    
    
    



    
    
});

  
$(document).on('click','button.btn-movetoorder',function(){
    
    $('div#occasionalsupplierlistdiv input[type=checkbox][name="supplierlistcheckbox[]"]:checked').each(function(){
       
       
        var  soid=$(this).val();
        var  operator_id=$('select#operator_id').val();  
        
        movetoorder(soid,operator_id);
       
    });
    
});

function movetoorder(soid,operator_id)
{
     if($('table#supplierlist > tbody > tr#tr_'+soid).length=="1")
            {
                alert("Supplier exists in  order");
                return;
            }
       
             var in_list='1';
             
             setInListFlag(operator_id,soid,in_list);
             
                var HTML='';

                //Show Edit button
                $('tr#tr_'+soid).find('a').removeClass('hide');
                $('tr#tr_'+soid).find('a').addClass('show');
                
                 // change order amt 0 to actual  hidden value
                $('tr#tr_'+soid+' td#so_'+soid).find('p').text($('tr#tr_'+soid+' td#so_'+soid).find('a').attr('data-orderamount'));

                // get selected tr HTML
                HTML=$('tr#tr_'+soid).html();

                //append HTML to uppertable
                $('table#supplierlist > tbody').append("<tr id='tr_"+soid+"'>"+HTML+"</tr>");

                //remove HTML from lower table
                $('table#occasionalsupplierlist > tbody > tr#tr_'+soid).remove();

                // update total order
                 order=parseFloat($('table#supplierlist').find('tr#tr_'+soid+' td#so_'+soid).find('a').attr('data-orderamount'));
                 totalorderamt=parseFloat($('div#owto').find('span').attr('data-totalorder'));
                 newtotalorderamt=parseFloat(totalorderamt+order);
                 $('div#owto').find('span').attr('data-totalorder',newtotalorderamt);
                 $('div#owto').find('span').text(newtotalorderamt);
                 
                 console.log(newtotalorderamt);
                   $('tr.summations th:eq(7)').text(ReplaceNumberWithCommas(newtotalorderamt,2));

                setShortfortheday(newtotalorderamt);
                setthTotalbaseSale(soid,'add');
                
                checkForEmptyTable();
}

$(document).on('click','button.btn-removefromorder',function(){
    
    $('div#occasionalsupplierlistdiv').show();
    
    $('div#regularsupplierlist input[type=checkbox][name="supplierlistcheckbox[]"]:checked').each(function(){
       
        var  soid=$(this).val();
        var  operator_id=$('select#operator_id').val();  
        
        removefromorder(soid,operator_id);
    });
    
});


function removefromorder(soid,operator_id)
{
            if($('table#occasionalsupplierlist > tbody > tr#tr_'+soid).length=="1")
            {
                alert("Supplier already remove from order");
                return;
            }
       
             var in_list='0';
             
             setInListFlag(operator_id,soid,in_list);
    
                    var HTML='';

                    // change order amt 0 to actual  hidden value
                    $('tr#tr_'+soid).find('p').text('0');

                    //Show Edit button
                    $('tr#tr_'+soid).find('a.editorderlink').removeClass('show');
                    $('tr#tr_'+soid).find('a.editorderlink').addClass('hide');


                    // get selected tr HTML
                    HTML=$('tr#tr_'+soid).html();

                    //append HTML to Lower
                    $('div#occasionalsupplierlistdiv').show();
                    $('table#occasionalsupplierlist > tbody').append("<tr id='tr_"+soid+"'>"+HTML+"</tr>");

                    //remove HTML from upper table
                    $('table#supplierlist > tbody > tr#tr_'+soid).remove();

                    // update total order
                     order=parseFloat($('table#occasionalsupplierlist').find('td#so_'+soid).find('a.editorderlink').attr('data-orderamount'));
                     totalorderamt=parseFloat($('div#owto').find('span').attr('data-totalorder'));
                     newtotalorderamt=parseFloat(totalorderamt-order);
                     $('div#owto').find('span').attr('data-totalorder',newtotalorderamt);
                     $('div#owto').find('span').text(newtotalorderamt);

                    $('tr.summations th:eq(7)').text(ReplaceNumberWithCommas(newtotalorderamt,2));
                       
                    setShortfortheday(newtotalorderamt);
                    setthTotalbaseSale(soid,'subract');

                    checkForEmptyTable();
                    
}
function setInListFlag(operator_id,soid,in_list)
{
    var orders=JSON.parse(localStorage.getItem('operator_'+operator_id));
     
     $.each(orders,function(k,v){
     
         if(v.supplier_operator_id==soid)
         {
             orders[k].in_list=in_list;
         }
         
     });
     
     if(typeof(Storage) !== "undefined") 
     {
         localStorage.setItem("operator_"+operator_id,JSON.stringify(orders));
         return true;
     }
     else
     {
         alert("No Local Storage Enabled");
         return false;
     };
     
}



$(document).on('click','button#sendforapproval',function(){
    
    
    if($('table#supplierlist > tbody').find('tr:not(.emptytr)').length<=0)
    {
        alert('No order to submit for approval');
        
        return;
    }
    
    // Set orders 
    var orders=JSON.stringify(localStorage.getItem('operator_'+$('select#operator_id').val()));
    
     var saveOrder=$.post(HOST+"orders/saveOrder",{operator_id:$('select#operator_id').val(),orders:orders});
    
     saveOrder.done(function(res){
        
        
        res=$.parseJSON(res);
        
        if(res.type)
        {
          
            showMessage(res.message);
            
            $('select#order_days').val("");
            $('select#operator_id').val("");
            $('table#supplierlist > tbody').empty();
            $('div#occasionalsupplierlistdiv').hide();
            $('div#disapprovedsupplierlistdiv').hide();
          
            $('div#owf').hide();
            $('div#owlds').hide();
            $('div#owes').hide();
            $('div#owcb').hide();
            $('div#owto').hide();
            $('div#owsftd').hide();
            $('div#owldsfa').hide();
            $('div#owaas').hide();
            $('div#owfp').hide();
            $('div#existsmessage').empty();
            $('table#supplierlist tr.summations').remove();
            location.reload();
            
          
        
        }
        else
        {
             showMessage(res.message,'error');
        }
        
    });
    
});


function createSupplierHTML(supplierlist)
{
            var  totalOrder=0;
            var totalBasesale=0;
            var totalOpening=0;
            var totalPending=0;
            var totalSimcount=0;
            var totalLastdaysale=0;
            var totalBlockedbalance=0;
            var totalIncoming=0;
            
                   $('table#supplierlist > tbody').empty();
                   $('table#occasionalsupplierlist > tbody').empty();
                   $('table#disapprovedsupplierlist > tbody').empty();
                   
                   $('tr.summations').remove();
                   
                  $.each(supplierlist,function(k,v){
                      
                                    var row="";
                                    var enableedit="show";
                                    
                                    row+="<tr id='tr_"+v.supplier_operator_id+"'>";
                                    if( v.in_list!="2")
                                    {
                                        if(!v.is_freezed){
                                          row+="<td><input type='checkbox'  name='supplierlistcheckbox[]' value='"+v.supplier_operator_id+"' /></td>";
                                        }else{
                                            row+="<td><span class='glyphicon glyphicon-ok'></span></td>"
                                        }
                                    }else{
                                    //row+="<td></td>";    
                                    row+="<td><button data-soid='"+v.supplier_operator_id+"'  class='btn btn-default btn-sm resend_disapproved_order'><span class='glyphicon glyphicon-repeat'></span></button></td>";    
                                    }
                                    row+="<td><a  target='_blank' href='/suppliers/get/"+v.id+"'>"+v.name+"</a><span onclick='movetoorder("+v.supplier_operator_id+","+v.operator_id+")'  class='glyphicon  glyphicon glyphicon-arrow-up cursorpointer'></span> <span onclick='removefromorder("+v.supplier_operator_id+","+v.operator_id+")' class='glyphicon  glyphicon glyphicon-arrow-down cursorpointer'></span> <br/><button class='btn btn-default btn-info btn-xs'>"+v.frequency+"</button>&nbsp;<div  style='overflow:hidden;width:95px;' title='"+v.modemnames+"' class='btn btn-default btn-warning btn-xs'>"+v.modemnames+"</div></td>";
                                    var marginiconhtml="";
                                    if(v.commission_type=="1"){marginiconhtml="<span class='glyphicon glyphicon-arrow-down text-success'></span>"}else{marginiconhtml="<span class='text-success glyphicon glyphicon-arrow-up'></span>"};
                                    row+="<td>"+v.commission_type_formula+""+marginiconhtml+"  </td>";
                                    row+="<td>"+v.base_amount+"</td>";
                                    row+="<td><a target='_blank' href='/orders/viewlastdayincoming/"+v.supplier_operator_id+"\/"+encodeURIComponent(v.modemids)+"\/"+v.operator_id+"'>"+v.lastdayincoming+"</a></td>";
                                    row+="<td>"+v.opening+"</td>";
                                    row+="<td><a target='_blank' href='/orderhistorys/pendinghistory/"+v.supplier_operator_id+"'>"+v.pending+"</a></td>";
                                    
                                    row+="<td id='so_"+v.supplier_operator_id+"'>";
                                    
                                    if(v.in_list=="1" || v.in_list=="2"){
                                        row+="<p>"+v.order+"</p>";
                                    }else if(v.in_list=="0"){
                                         row+="<p>0.00</p>";
                                    }
                                    
                                    if(!v.in_list || v.is_freezed){
                                     enableedit="hide"   
                                    }
                                    
                                    //row+="<button class='btn btn-default btn-xs editorder "+enableedit+" '  data-soid='"+v.supplier_operator_id+"'   data-operatorid='"+v.operator_id+"' >Edit</button>";
                                    //row+="<div class='outputhtml'></div>";
                                    row+="<a class='"+enableedit+"  editorderlink' data-operatorid='"+v.operator_id+"'  data-soid='"+v.supplier_operator_id+"' data-orderamount='"+v.order+"' data-toggle='modal' href='#neworderamount'>Edit</a>";
                                    row+="</td>";
                                    
                                    row+="<td>"+v.persimsale+"</td>";
                                    row+="<td>"+v.totalSims+"</td>";
                                    row+="<td>"+v.lastdaysale+"</td>";
                                    row+="<td>"+v.blockedBalance+"</td>";
                                    row+="<td>";
                                    row+="<a data-soid='"+v.supplier_operator_id+"'   data-operatorid='"+v.operator_id+"'   data-toggle='modal' href='#addcomment'>Comm</a>";
                                    row+="</td>";
                                    if(!v.is_freezed){
                                    row+="<td><input type='text' class='form-control squaretextbox changeorderbydays'  value='"+v.no_of_days+"'  data-soid='"+v.supplier_operator_id+"'  data-params='"+JSON.stringify(v)+"' /></td>";
                                    }else{
                                    row+="<td>"+v.no_of_days+"</td>";    
                                    }
                                    var remaingorderamt=(v.capacity_per_month-v.order_amount_till_now);
                                    
                                    var monthlyusagehighlight="";
                                   
                                    if(v.order_amount_till_now > ((v.capacity_per_month/4)*(v.weekfactor)))
                                    {
                                        monthlyusagehighlight="style='background-color:red' ";
                                    }
                                    
                                    row+="<td "+monthlyusagehighlight+">"+ReplaceNumberWithCommas(remaingorderamt,2)+"</td>";
                                    
                                    row+="<td>"+ReplaceNumberWithCommas(v.to_pay,2)+"</td>";
                                   // row+="<td>"+parseFloat(v.pending)*(-1)+"</td>";
                                    //row+="<td data-toi='"+parseFloat(v.pending*-1)+"'>"+(parseFloat(v.pending*-1)+parseFloat(v.order))+"</td>";
                                    row+="</tr>";
                         
                          if(v.in_list=="1")
                          {
                             // Code for approved & regular 
                             $('table#supplierlist > tbody').append(row);
                                totalOrder+=parseFloat(v.order);
                                
                                totalBasesale +=parseFloat(v.base_amount);
                                totalOpening +=parseFloat(v.opening);
                                totalPending += parseFloat(v.pending);
                                totalSimcount += parseFloat(v.totalSims);
                                totalLastdaysale += parseFloat(v.lastdaysale);
                                totalBlockedbalance += parseFloat(v.blockedBalance);
                                totalIncoming += parseFloat(v.lastdayincoming);
                          }
                          else
                          {
                              if(v.in_list=="0")
                              {
                             // code for occasional     
                              $('table#occasionalsupplierlist > tbody').append(row);
                              }
                              else if(v.in_list=="2")
                              {
                              // Code for disapproved section
                              $('table#disapprovedsupplierlist > tbody').append(row);
                              }
                          }
                  });
                  
                  // Set Total Order
                  $('div#owto').find('span').attr('data-totalorder',totalOrder);
                  
                  $('div#owto').find('span').text(ReplaceNumberWithCommas(totalOrder));
                  
                  //Set Summation of other columns
                  var summations = '<tr class="summations"><th></th>\n<th></th>\n<th></th>\n<th data-thtotalbasesale="'+totalBasesale+'" >'+ReplaceNumberWithCommas(totalBasesale,2)+'</th><th data-thtotalbasesale="'+totalIncoming+'" >'+ReplaceNumberWithCommas(totalIncoming,2)+'</th>\n<th>'+ReplaceNumberWithCommas(totalOpening,2)+'</th>\n<th>'+ReplaceNumberWithCommas(totalPending,2)+'</th>\n<th>'+ReplaceNumberWithCommas(totalOrder,2)+'</th>\n<th></th>\n<th>'+ReplaceNumberWithCommas(totalSimcount,2)+'</th>\n<th>'+ReplaceNumberWithCommas(totalLastdaysale,2)+'</th>\n<th>'+ReplaceNumberWithCommas(totalBlockedbalance,2)+'</th>\n<th></th></tr>';
                  
                  $(summations).insertAfter('tr#recommededsuppliertr');
                  
                  setShortfortheday(totalOrder);
                  
                  // If occasional supplier are present then show table 
                  if($('table#occasionalsupplierlist > tbody').find('tr').length>0)
                  {
                      $('div#occasionalsupplierlistdiv').show();
                  }
                  // Code for disapproved section
                  if($('table#disapprovedsupplierlist > tbody').find('tr').length>0)
                  {
                      $('div#disapprovedsupplierlistdiv').show();
                  }
                  
                 checkForEmptyTable();
                 
                 
                 highlightbasesale(totalBasesale);
}


/*
 * Get Last Day Sale
 * Start
 */

var totalLastDaySale=$.getJSON(HOST+'orders/getTotalLastDaySale');

totalLastDaySale.done(function(res){

        if(res.type && res.status && res.lastdaysale!=null)
        {
            $('div#tlda span').text(ReplaceNumberWithCommas(res.lastdaysale));
            
            $.cookie('tlds',res.lastdaysale,{expires:cookieexpirationDate,path:'/'})
        }
        else
        {
            $('div#tlda span').text('N.A');
        }
});
/*
 * End
 */

/*
 * Last Day Failiure percentage
 * Start
 */
var totalfailurepercentage=$.getJSON(HOST+'orders/getTotalFailurePercentage');

totalfailurepercentage.done(function(res){
    
   if(res.type && res.status && (res.failurepercentage>0))
        {
             $.cookie('tldfp',res.failurepercentage.toFixed(2),{expires:cookieexpirationDate,path:'/'});
             
            $('div#tldfp span').text(res.failurepercentage.toFixed(2)+'%');
        }
        else
        {
            $('div#tldfp span').text('N.A');
        }
        
});


/*
 * End
 */


        /*
         * Avg Total Api Sale
         * Start
         */
           
           var getTotalexpectedsale=$.getJSON(HOST+'orders/getExpectedSale');

           getTotalexpectedsale.done(function(res){

              if(res.type && res.status && (res.expectedSale>0))
                   {
                       VARIANCE_PERCENTAGE=res.variance_percentage
                       
                         $.cookie('tes',res.expectedSale,{expires:cookieexpirationDate,path:'/'});
                         
                       $('div#tes span').text(ReplaceNumberWithCommas(res.expectedSale));
                   }
                   else
                   {
                       $('div#tes span').text('N.A');
                   }

           });
           
           /*
            * End
            */
           
           
           /*
            * Set Short for the day
            * Start
            */
           function setShortfortheday(totalOrder)
           {
           
              //  short = totalorder-(expected sale*no days)+(current balance)
              var currentbalance=parseFloat($('div#owcb').find('span').text());
              var expectedsale=parseFloat($('div#owes span').data('owes'));
              var nooddays=2;
              var pending=parseFloat($('div#owp span').text());
              
              var  short=parseFloat(totalOrder)+currentbalance-pending-parseFloat(expectedsale*2);
               
               // $.cookie('owsftd_'+$('select#operator_id').val(),short,{expires:cookieexpirationDate,path:'/'});
                
               $('div#owsftd span').text(short.toFixed(2));
               
               // if short for the day > 10% of expected sale  highlight
               // used to determine excess order doesnot cross more than 10% of ES
               // short should be between -10% and +10% of expected sale
               var a=(short/nooddays);
               var b=(expectedsale*0.10);
               
             if(a<0)
             {
                 b=-(b);
                
                console.log(a);
                console.log(b);
                 if(a>=b)
                 {
                     $('div#owsftd').css({'background-color':'transparent'});
                     
                 }
                 else
                 {
                       $('div#owsftd').css({'background-color':'red'});
                 }
             }
             else
             {
                console.log(a);
                console.log(b);
                 if(a<=b)
                 {
                     $('div#owsftd').css({'background-color':'transparent'});
                      
                 }
                    else
                 {
                      $('div#owsftd').css({'background-color':'red'});
                 }
             }
               
//               if(a > b)
//               {
//                   $('div#owsftd').css({'background-color':'red'});
//               }
               
           }
           /*
            * End
            */
           
           /*
            * Set Total Base sale in TH.summations
            * Start
            */
           function setthTotalbaseSale(trid,mode)
           {
               var oldbasesale=parseFloat($('tr.summations th:eq(3)').attr('data-thtotalbasesale'));
               var amt=parseFloat($('tr#tr_'+trid+' td:eq(3)').text());
               if(mode=="subract")
               {
                     var newbasesale=parseFloat(oldbasesale-amt);
               }
               else
               {
                     var newbasesale=parseFloat(oldbasesale+amt);
               }
             
               
               $('tr.summations th:eq(3)').attr('data-thtotalbasesale',newbasesale);
               $('tr.summations th:eq(3)').text(ReplaceNumberWithCommas(newbasesale,2));
               
               
               highlightbasesale(newbasesale);
           }
           /*
            * End
            */
           
           // Highlight base sale if SUm(BS)-ES < 5% of ES
             
           function highlightbasesale(newbasesale)
           {
             
               var expectedsale=parseFloat($('div#owes span').attr('data-owes'));
               
               if( (expectedsale-newbasesale) >  (expectedsale*0.05) )
               {
                   $('tr.summations th:eq(3)').css({'background-color':'red'});
               }
               else
               {
                    $('tr.summations th:eq(3)').css({'background-color':'transparent'});
               }
           }
           
           /*
            * Sets valid HTML if no table tr's are empty
            * Start
            */
           function checkForEmptyTable()
           {
                 if(!$('table#supplierlist > tbody').find('tr').length)
                  {
                      $('table#supplierlist > tbody').append("<tr class='emptytr'><td colspan='12'>No Supplier Detected</td></tr>");
                  }
                  else
                  {
                      $('table#supplierlist > tbody').find('tr.emptytr').remove();
                  }
           }
           /*
            * End
            */
           
    $(document).ready(function(){         
        
    $('#neworderamount').on('show.bs.modal', function(e) {

      var orderamount = $(e.relatedTarget).data('orderamount');
      var so_id = $(e.relatedTarget).data('soid');
      var operator_id = $(e.relatedTarget).data('operatorid');

     $(e.currentTarget).find('span.oldorderamt').text(orderamount) ;
     $(e.currentTarget).find('input#so_id').val(so_id) ;
     $(e.currentTarget).find('input#order_operator_id').val(operator_id) ;

     $(e.currentTarget).find('input[name=order_amount]').val('');
     $(e.currentTarget).find('div#orderamtmsg').empty();
     $(e.currentTarget).find('span.words').empty();
     
    });
    
    $(document).on('keyup','input[name=order_amount]',function(){
        
        $('span.words').text(toWords($(this).val()));
        
    });
    
    $(document).on('click','button#updateorderamount',function(){
    
       var  soid=$('div#neworderamount').find('input#so_id').val();
       var  operatorid=$('div#neworderamount').find('input#order_operator_id').val();
       var  order=parseFloat($('div#neworderamount').find('input[name=order_amount]').val());
       
        if(order=="" || isNaN(order))
        {
            alert('Enter Valid number');
            return ;
        }
        
         if(isMaxCapExceeded(soid,order))
         {
            alert('Error :  Max Cap Exceeded');
            return;
         }
      
        if(updateOrderAmt(operatorid,soid,order))
        {
      
         oldamt=0;totalorderamt=0;newtotalorderamt=0;

        oldamt=parseFloat($('span.oldorderamt').text());
        totalorderamt=parseFloat($('div#owto').find('span').attr('data-totalorder'));
        newtotalorderamt=parseFloat(totalorderamt-oldamt+order);

        var HTML="<p>"+order+"</p><a href='#neworderamount' data-toggle='modal'  data-operatorid='"+operatorid+"' data-orderamount='"+order+"'   data-soid='"+soid+"'>Edit</a>";
        $('td#so_'+soid).html(HTML);       
       
        // Set TO
         setTotalOrderamt(order);
            
         // Set TEO
         //setTEO(soid,order);
         
         updatetopay(soid,null,null,order,operatorid);
       
         $('div#orderamtmsg').showSuccess("Order amount updated");
         
         setTimeout(function(){$('#neworderamount').modal('hide');},250);
         
         
            
       }
});

function setTotalOrderamt(order,oldorderamt)
{
        var oldamt=0; var totalorderamt=0;var newtotalorderamt=0;

        if($('span.oldorderamt').is(":visible"))
        {
            oldamt=parseFloat($('span.oldorderamt').text());
        }
        else
        {
             oldamt=oldorderamt;    
        }
        
        if(isNaN(oldamt))
        {
            oldamt=0;
        }
        
        totalorderamt=parseFloat($('div#owto').find('span').attr('data-totalorder'));
        newtotalorderamt=parseFloat(totalorderamt-oldamt+order);

         // Set SFTD
         setShortfortheday(newtotalorderamt);
         
        console.log("OLD Amount : "+oldamt);
        console.log("Total Order Amount OLD : "+totalorderamt);
        console.log("New Total Order : "+newtotalorderamt);


       $('div#owto').find('span').attr('data-totalorder',newtotalorderamt);
       $('div#owto').find('span').text(newtotalorderamt);
       $('tr.summations th:eq(7)').text(ReplaceNumberWithCommas(newtotalorderamt,2));
       
}
function setTEO(soid,order)
{
          var oldtoi=parseFloat($('tr#tr_'+soid+' td ').eq('16').attr('data-toi'));
           
         $('tr#tr_'+soid+' td ').eq('16').text(ReplaceNumberWithCommas(oldtoi+order,2));    
}

function updateOrderAmt(operator_id,soid,amt)
{
        
     var orders=JSON.parse(localStorage.getItem('operator_'+operator_id));
     
     $.each(orders,function(k,v){
     
         if(v.supplier_operator_id==soid)
         {
             orders[k].order=amt;
             orders[k].manual=true;
         }
         
     });
     
     if(typeof(Storage) !== "undefined") 
     {
         localStorage.setItem("operator_"+operator_id,JSON.stringify(orders));
         return true;
     }
     else
     {
         alert("No Local Storage Enabled");
         return false;
     };
     
     
}

function isMaxCapExceeded(soid,order)
{
   $.ajaxSetup({async: false}); 
   
    var maxcap=$.getJSON(HOST+'orders/getMaxCapPerOrder',{soid:soid});
    
    var flag=true;
    
    maxcap.done(function(res){
        
         if(res.type && res.status)
          {
//              var threshold=parseFloat(res.data.capacity)*2;
              var threshold=parseFloat(res.data.capacity);
                
              if(order<=threshold)
              {
                  flag=false;
               }
           
          }
          else
          {
               flag=true;
         }
        
    });

    $.ajaxSetup({async: true}); 
 
    return flag;
}

function updateNoofDays(operator_id,soid,days)
{
    var orders=JSON.parse(localStorage.getItem('operator_'+operator_id));
     
     $.each(orders,function(k,v){
     
         if(v.supplier_operator_id==soid)
         {
             orders[k].no_of_days=days;
             orders[k].manual=true;
         }
         
     });
     
     if(typeof(Storage) !== "undefined") 
     {
         localStorage.setItem("operator_"+operator_id,JSON.stringify(orders));
         return true;
     }
     else
     {
         alert("No Local Storage Enabled");
         return false;
     };
}

function updatetopay(soid,boundtype,margin,order,operatorid)
{
       
    var topay=0;
    
            if(boundtype === null)
            {
               
                    var orderjson=JSON.parse(localStorage.getItem('operator_'+operatorid));
                    $.each(orderjson,function(k,v){

                     if(v.supplier_operator_id==soid)
                     {
                            margin=v.commission_type_formula;
                            boundtype=parseInt(v.commission_type);
                         
                     }

                 });
            }
            else
            {
                 margin=parseFloat(margin);
                 boundtype=parseInt(boundtype);
            }
    
    var order=parseFloat(order);
      
            if(boundtype==1)
            {
                topay=order*((100-margin)/100);
            }
            else
            {
                topay=order;
            }
            
    $('tr#tr_'+soid+' td ').eq('15').text(ReplaceNumberWithCommas(topay,2));    
       
    return topay;
    
}

$('#addcomment').on('show.bs.modal', function(e) {

      var so_id = $(e.relatedTarget).data('soid');
      var operator_id = $(e.relatedTarget).data('operatorid');

     $(e.currentTarget).find('input#comment_so_id').val(so_id) ;
     $(e.currentTarget).find('input#comment_order_operator_id').val(operator_id) ;

     $(e.currentTarget).find('textarea[name=comment]').val('');
     $(e.currentTarget).find('div#ordercommentmsg').empty();
     $(e.currentTarget).find('div#loadcomments').empty();
     
      var comments=$.getJSON(HOST+'orderapprovals/getComments',{soid:so_id});
     
     comments.done(function(res){
         
          if(res.type && res.status)
          {
              $.each(res.comments,function(k,v){
                  
                         var row="";
                         row+="<p>";
                         row+=v.comment
                         row+="........"+v.name
                         row+="</p>";
                         
                   $(e.currentTarget).find('div#loadcomments').append(row);      
              })
          }
          else
          {
              $(e.currentTarget).find('div#loadcomments').html("No Comments Yet");
          }
          
     });
     
    });
 
        $(document).on('click','button#addcommentbtn',function(){
    
       var  soid=$('div#addcomment').find('input#comment_so_id').val();
       var  operatorid=$('div#addcomment').find('input#comment_order_operator_id').val();
       var  comment=$('div#addcomment').find('textarea#comment').val();
       
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       
       if(addComment(operatorid,soid,comment))
       {
           $('div#ordercommentmsg').showSuccess("Comment Saved");
         
          setTimeout(function(){$('div#addcomment').modal('hide');},1000);
       }
       else
       {
           alert("Error");
       }
     
        });
        
        function addComment(operator_id,soid,msg)
{
     var orders=JSON.parse(localStorage.getItem('operator_'+operator_id));
     
     $.each(orders,function(k,v){
     
         if(v.supplier_operator_id==soid)
         {
             orders[k].comments=msg;
            
         }
         
     });
     
     // Save comment in DB is order is already approved & Present 
     //START
        
           var  comment=msg;

           var addComment=$.post(HOST+'orders/saveComments',{soid:soid,comment:comment});

           addComment.done(function(res){

                 res=$.parseJSON(res);

                 if(res.type && res.status)
                 {
                        $('div#ordercommentmsg').showSuccess("Comment Saved");
                        setTimeout(function(){$('div#addcomment').modal('hide');},1000);
                 }
                 else
                 {
                         alert("Error");
                 }

           });
           
     //END
     
     if(typeof(Storage) !== "undefined") 
     {
         localStorage.setItem("operator_"+operator_id,JSON.stringify(orders));
         return true;
     }
     else
     {
         alert("No Local Storage Enabled");
         return false;
     };
     
     
}


// Resend disapproved order with new order value 
$(document).on('click','.resend_disapproved_order',function(){
    
   var soid=$(this).attr('data-soid');
   var orders=JSON.parse(localStorage.getItem('operator_'+$('select#operator_id').val()));
   var ordertoberesend="";
   
    $.each(orders,function(k,v){
     
       if(v.supplier_operator_id==soid)
       {
         ordertoberesend=v;
       }
        
     });
     
     
    // In order to use same ajax call that saved order originally  we will set inlist flag =1 
      ordertoberesend['in_list']="1";

     // Set orders 
     var o=JSON.stringify(ordertoberesend);
    
     var resenddisapprovedorder=$.post(HOST+"orders/saveOrder/1",{operator_id:$('select#operator_id').val(),orders:o});
    
     resenddisapprovedorder.done(function(res){
        
        res=$.parseJSON(res);
        
        if(res.type)
        {
            showMessage(res.message);
        }
        else
        {
             showMessage(res.message,'error');
        }
        
    });

});
    
 // Change orderbased on noofdays entered
     
     $(document).on('keypress','.changeorderbydays',function(e){
     
            if (e.keyCode == 13) {
               
                    showMessage("Please wait ..... ");
                    
                    var days=parseFloat($(this).val());
                     
                    if(days<=0 || days>5)
                    {
                        alert('Error : Invalid Days');
                        return;
                    }

                    var params=$(this).attr('data-params');
                    var soid=$(this).attr('data-soid');
                    var operator_id=$('select#operator_id').val();
                   
                   var paramsjson=$.parseJSON(params);
                 
                    var getorderamt=$.getJSON(HOST+'orders/getOrderForSupplier',{params:params,days:days,operator_id:operator_id,isAjax:1});

                    getorderamt.done(function(res){

                         if(res.status && res.type)
                         {
                             if(parseFloat(res.order)>0)
                             {
                                    updateOrderAmt(operator_id,soid,parseFloat(res.order));
                                    updateNoofDays(operator_id,soid,parseFloat(days));
                                    updatetopay(soid,paramsjson.commission_type,paramsjson.commission_type_formula,res.order,operator_id);

                                    var oldorderamt=parseFloat($('td#so_'+soid).find('p').text());

                                    // Set TO
                                    setTotalOrderamt(parseFloat(res.order),oldorderamt);

                                    $('td#so_'+soid).find('p').text(res.order);
                                    $('td#so_'+soid).find('a').attr('data-orderamount',res.order);

                                      showMessage("Done");
                              }
                              else
                              {
                                  alert("Error");
                              }
                         }
                         else
                         {
                             alert("Error calculating order by days");
                         }
                    });
               
            }
            
     });
     
 
    });
