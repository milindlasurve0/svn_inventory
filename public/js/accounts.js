$(document).ready(function(){
    
    $('input[type=checkbox][id="is_single"]').click(function(){
    var checked = $(this).is(':checked');
    if(checked) {
        if(!confirm('Are you sure you want to download single payments ?')){         
            $(this).removeAttr('checked');
        }
        else
        {
           unchecknormalpayments();
        }
    } else if(!confirm('Are you sure you want to discontinue dowloading single payments ?')){
        $(this).attr("checked", "checked");
     
       
    }
    });

    function unchecknormalpayments()
    {
        
        $('input[type=checkbox][name="suppliername"]:checked').each(function(k,v){
         if($(this).attr('data-singlepayment')=="0")
         {
             $(this).trigger('click');
         }
        });
    }
    
    $('button#downloadexcel').on('click',function(){
    
        var is_single=$('input[type=checkbox][name="is_single"]:checked').val();
        
        // Validation for from bank
        var from_bank=$('input[type=radio][name="frombank"]:checked').val();
        if(from_bank=='' || typeof from_bank=='undefined'){ alert("Error : No From Bank selected");return;}
        
        var idsarray=[];
        
        var selected=$('input[type=checkbox][name="suppliername"]:checked').map(function(){
            if(is_single=="on")
            {
                if($(this).attr('data-singlepayment')=="1")
                {
                    idsarray.push($(this).val());
                    return $(this).val();
                }
            }
            else
            {
               if($(this).attr('data-singlepayment')=="0")
               {
                    idsarray.push($(this).val());
                    return $(this).val();
                }
            }
        }).get().join(',');
        
        if(selected!=""){
                        var downloaded=$.post(HOST+'accounts/setDownloadStatus',{ids:selected,type:'approve',is_single:is_single,from_bank:from_bank});

                        downloaded.done(function(res){

                            res=$.parseJSON(res);

                            if(res.status && res.type)
                            {
                                
                                showMessage(res.message);

                                var ids=selected.split(',');
                             
                              // add tick to indiviual row
                              $.each(ids,function(k,v){
                                    $('td#order_'+v).next().html("<span class='glyphicon glyphicon-ok'></span>");
                              });
                              
                              
                            // Remove Big Supplier  Checkbox 
                              $.each(idsarray,function(k,v){
                                  // check if prev span
                                 // console.log($("span").find("input[type=checkbox][value='"+v+"']").prev());
                                  $("span").find("input[type=checkbox][value='"+v+"']").remove();
                                });
                                
                                setTimeout(function(){
                                     window.location.href=window.location.origin+'/accounts/downloadexcel/?orderids='+selected+'&fb='+from_bank; 
                                },1000);
                                
                               // window.location.reload();
                            }
                            else
                            {
                                showMessage(res.message,'error');
                            }

                        });
          }else{
              alert("Nothing to download");
              return;
          }
            
        
    });
    $('button#disapproveexcel').on('click',function(){
    
        var selected=$('input[type=checkbox][name="suppliername"]:checked').map(function(){
            return $(this).val();
        }).get().join(',');
        
       var downloaded=$.post(HOST+'accounts/setDownloadStatus',{ids:selected,type:'disapprove'});
       
       downloaded.done(function(res){
           
           res=$.parseJSON(res);
           
           if(res.status && res.type)
           {
               showMessage(res.message);
           }
           else
           {
               showMessage(res.message,'error');
           }
           
       });
        
    });
    
    
    $('input[type=checkbox][name="suppliername"]').on('click',function(){
        
        
        if($(this).is(':checked'))
        {
            var originalamt=parseFloat($('span.orderamt').attr('data-amt'));
           // var selectedamt=parseFloat($(this).parent().parent().find('td:eq(7)').text());
            var selectedamt=0;
            
            $(this).parent().parent().next().find('tr.info').each(function(){
                if($(this).find('td:eq(8)').attr('data-tobeconsidered')=="1"){
                 selectedamt+=parseFloat($(this).find('td:eq(8)').attr('data-topay'));
                }
            });
        
           $('span.orderamt').text(ReplaceNumberWithCommas(parseFloat(originalamt+selectedamt),2));
           $('span.orderamt').attr('data-amt',parseFloat(originalamt+selectedamt));
        }
        else
        {
            var originalamt=parseFloat($('span.orderamt').attr('data-amt'));
           // var selectedamt=parseFloat($(this).parent().parent().find('td:eq(7)').text());
            var selectedamt=0;
          
            $(this).parent().parent().next().find('tr.info').each(function(){
                 if($(this).find('td:eq(8)').attr('data-tobeconsidered')=="1"){
                 selectedamt+=parseFloat($(this).find('td:eq(8)').attr('data-topay'));
                  }
            });
            
           $('span.orderamt').text(ReplaceNumberWithCommas(parseFloat(originalamt-selectedamt),2));
           $('span.orderamt').attr('data-amt',parseFloat(originalamt-selectedamt));
        }
    });
    
    
    /*
     * Comment Start
     */
        $('#addcomment').on('show.bs.modal', function(e) {

     
      var orderid = $(e.relatedTarget).data('orderid');
     var soid = $(e.relatedTarget).data('soid');
     
     $(e.currentTarget).find('input#comment_order_id').val(orderid) ;
      $(e.currentTarget).find('input#comment_so_id').val(soid) ;
      
     $(e.currentTarget).find('textarea[name=comment]').val('');
     $(e.currentTarget).find('div#ordercommentmsg').empty();
     $(e.currentTarget).find('div#loadcomments').empty();
     
     var comments=$.getJSON(HOST+'orderapprovals/getComments',{soid:soid});
     
     comments.done(function(res){
         
          if(res.type && res.status)
          {
              $.each(res.comments,function(k,v){
                  
                         var row="";
                         row+="<p>";
                         row+=v.comment
                         row+="........"+v.name
                         row+="</p>";
                         
                   $(e.currentTarget).find('div#loadcomments').append(row);      
              })
          }
          else
          {
              $(e.currentTarget).find('div#loadcomments').html("No Comments Yet");
          }
          
     });
     
    
    });
    
    $(document).on('click','button#addcommentbtn',function(){
    
    
       var  orderid=$('div#addcomment').find('input#comment_order_id').val();
       var  comment=$('div#addcomment').find('textarea#comment').val();
       var  soid=$('div#addcomment').find('input#comment_so_id').val();
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       var addComment=$.post(HOST+'orderapprovals/saveComments',{orderid:orderid,comment:comment,soid:soid});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#ordercommentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#addcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
    });
    
    /*
     * Comment End
     */
    
 var fixmeTop = $('.fixme').offset().top;

$(window).scroll(function() {
    var currentScroll = $(window).scrollTop()+30;
    if (currentScroll >= fixmeTop) {
        $('.fixme').css({
            position: 'fixed',
            top: '60px',
            left: '71%',
            
        });
    } else {
        $('.fixme').css({
            position: 'static'
        });
    }
});

$('input[type=checkbox][name="checkallbatch"]').on('click',function(){
    

   var batch=$(this).val(); 
       
        $('div.batch'+batch).find('input[type=checkbox][name="suppliername"]').each(function(){
            
                 $(this).trigger('click');
             
        });

    
});


$('input[type=radio][name="frombank"]').on("click",function(){
    
     var from_bank=$(this).val();
    
      if(from_bank=='axis')
      {
          alert("You have selected axis bank ");
      }
      
});

});