$(document).ready(function(){
    
    $('button.updatetxn').on('click',function(){
    
        var txnid=$(this).prev('input[name=updatetxntext]').val();
        var order_id=$(this).attr('data-orderid');
        
        if(txnid!="" &&  $.trim(txnid)!="")
        {
            var updatetxn=$.post(HOST+'accounts/savetxnid',{order_id:order_id,txnid:txnid});
            
            updatetxn.done(function(res){
                
                res=$.parseJSON(res);
                
               if(res.status && res.type)
                {
                    $('td.edit_'+order_id).empty();
                    $('td.edit_'+order_id).text(txnid);
                    $('td#order_'+order_id).find('span').addClass('text-success');
                     showMessage(res.message);
                }
                else
                {
                     showMessage(res.message,'error');
                }
                
            });
            
        }
        else
        {
            alert("Invalid Transaction id");
        }
        
    });
    
    $('button.rejecttxn').on('click',function(){
    
     
        var txnid=$(this).prev().prev('input[name=updatetxntext]').val();
        var order_id=$(this).attr('data-orderid');
        
        var r=confirm('Are you sure ? ');
        
        if(!r)
        {
            return;
        }
        
        if(txnid=="")
        {
            var updatetxn=$.post(HOST+'accounts/rejecttxnid',{order_id:order_id,txnid:txnid});
            
            updatetxn.done(function(res){
                
                res=$.parseJSON(res);
                
               if(res.status && res.type)
                {
                    $('td.edit_'+order_id).empty();
                    $('td.edit_'+order_id).text("Rejected");
                    $('td#order_'+order_id).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove').addClass('text-danger');
                    showMessage(res.message);
                }
                else
                {
                     showMessage(res.message,'error');
                }
                
            });
            
        }
        else
        {
            alert("Error : Clear txn id to mark transaction as rejected");
        }
        
    });
    
    
    $(document).on('show.bs.modal','div#editutrdiv',function(e) {
        
         var orderid = $(e.relatedTarget).attr('data-orderid');
         var utr = $(e.relatedTarget).attr('data-utr');
         $(e.currentTarget).find('input#utr_order_id').val(orderid) ;
         $(e.currentTarget).find('input#newutr').val('') ;
         $(e.currentTarget).find('span#oldutr').text(utr) ;
         
    });
    
    $(document).on('click','#updateutrbtn',function(){
        
        var orderid=$('div#editutrdiv input#utr_order_id').val();
        var utr=$('div#editutrdiv input#newutr').val();
       
        if(utr!="" &&  $.trim(utr)!="")
        {
            var updatetxn=$.post(HOST+'accounts/savetxnid',{order_id:orderid,txnid:utr});
            
              updatetxn.done(function(res){
                
                res=$.parseJSON(res);
                
                $('tr#tr_'+orderid+'  td').eq(9).find('span').text(utr);
                $('tr#tr_'+orderid+'  td').eq(9).find('button').attr('data-utr',utr);
                
                $('#editutrdiv').modal('hide');
             });
        }
        else
        {
            alert("Enter Valid Utr");
        }
    });
    
      /*
     * Comment Start
     */
        $('#addcomment').on('show.bs.modal', function(e) {

     
      var orderid = $(e.relatedTarget).data('orderid');
      var soid = $(e.relatedTarget).data('soid');
      
     $(e.currentTarget).find('input#comment_order_id').val(orderid) ;
     $(e.currentTarget).find('input#comment_so_id').val(soid) ;

     $(e.currentTarget).find('textarea[name=comment]').val('');
     $(e.currentTarget).find('div#ordercommentmsg').empty();
     $(e.currentTarget).find('div#loadcomments').empty();
     
     var comments=$.getJSON(HOST+'orderapprovals/getComments',{soid:soid});
     
     comments.done(function(res){
         
          if(res.type && res.status)
          {
              $.each(res.comments,function(k,v){
                  
                         var row="";
                         row+="<p>";
                         row+=v.comment
                         row+="........"+v.name
                         row+="</p>";
                         
                   $(e.currentTarget).find('div#loadcomments').append(row);      
              })
          }
          else
          {
              $(e.currentTarget).find('div#loadcomments').html("No Comments Yet");
          }
          
     });
     
    
    });
    
    $(document).on('click','button#addcommentbtn',function(){
    
    
       var  orderid=$('div#addcomment').find('input#comment_order_id').val();
       var  soid=$('div#addcomment').find('input#comment_so_id').val();
       var  comment=$('div#addcomment').find('textarea#comment').val();
       
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       
       var addComment=$.post(HOST+'orderapprovals/saveComments',{orderid:orderid,soid:soid,comment:comment});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#ordercommentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#addcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
    });
    
    /*
     * Comment End
     */
    
});
