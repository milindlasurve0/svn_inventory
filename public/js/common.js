

function setSmsContact(status,id,supplier_id)
{

    
   $.ajax({
                            method:"POST",
                            url:HOST+'suppliers/setSmsContact/',
                            data:{status:status.checked,id:id},
                            dataType:"json",
                            success:function(data){
                             if(data.message=="success")
                            {
                                  $('div#div_success').show().delay(1000).fadeOut();;
                            }
                            else
                            {
                                   $('div#div_error').hide().delay(1000).fadeOut();;   
                            }
                                                                    }
               });
}



function checkRm(rm)
{
    console.log(rm);
    if(rm=="0")
    {
        //$('div#other_rm_div').show();
        $('div#other_rm_div input[name=other_rm]').prop("disabled", false);
    }
    else
    {
         //$('div#other_rm_div').hide();
          $('div#other_rm_div input[name=other_rm]').prop("disabled", true)
    }
}

$.fn.showSuccess = function (message) {

   var deferred = new $.Deferred();
    
    var SUCCESS_HTML=$('<div class="alert alert-success successhtml">\n<a href="#" class="close" data-dismiss="alert">&times;</a>\n<strong>Success ! </strong><span></span>\n</div>');
    
    SUCCESS_HTML.find('span').text(message);
    
    $(this).html(SUCCESS_HTML);
    
    $(this).find('div.successhtml').fadeOut(1000,function(){$(this).remove();deferred.resolve();});
       
    return deferred.promise();
    
  
};
$.fn.showFailure = function (message) {

      var FAILURE_HTML=$('<div class="alert alert-danger dangerhtml">\n<a href="#" class="close" data-dismiss="alert">&times;</a>\n<p><strong>Error ! </strong><span></span></p>\n</div>');
    
    FAILURE_HTML.find('span').text(message);
    
    $(this).html(FAILURE_HTML);
    
    $(this).find('div.dangerhtml').fadeOut(4000,function(){$(this).remove()});
       
    return this;
    
  
};

$.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

function showMessage(msg,type)
{
    var interval;
    
    classname="alert alert-success";
    
    if(type=="error")
    {
        classname="alert alert-danger";
    }
    
    $('#smsg').remove();
    
    clearInterval(interval);

    if (!$('#smsg').is(':visible'))
    {
        if (!$('#smsg').length)
        {
            $('<div id="smsg" class="'+classname+'"><strong>Success ! ! &nbsp;&nbsp;</strong><span>'+msg+'</span></div>').appendTo($('body')).css({
                    'font-size': '12px',
                    'height': '24px',
                    'left': '0',
                    'line-height': '26px',
                    'padding': '0',
                    'position': 'fixed',
                    'text-align': 'center',
                    'top': '60px',
                    'width': '100%',
                    'z - index': '1000'
                       
            }).slideDown('slow');


            interval = setTimeout(function(){
                // $('#smsg').animate({'width':'hide'}, function(){ $('#smsg').remove();});
                $('#smsg').slideUp('slow', function(){
                    $('#smsg').remove();
                });
            }, 2000);
            
            
            return;
            
        }
    }
}




//   function ReplaceNumberWithCommas(yourNumber) {
//    //Seperates the components of the number
//    var n= yourNumber.toString().split(".");
//    //Comma-fies the first part
//    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//    //Combines the two sections
//    return n.join(".");
//}

 function ReplaceNumberWithCommas(number, decimals, dec_point, thousands_sep)
 {
            number = (number + '')
          .replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
          prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
          sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
          dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
          s = '',
          toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k)
              .toFixed(prec);
          };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
          .split('.');
        if (s[0].length > 3) {
          s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
          .length < prec) {
          s[1] = s[1] || '';
          s[1] += new Array(prec - s[1].length + 1)
            .join('0');
        }
        return s.join(dec);
 }
 
 
// jQuery.Showalert=function(a)
// {
//     alert(a);
//     
// }

$.notify=function(){
    
    var success=function()
    {
        alert("gegegeg");
    }
    
    return {
        success:success
    }
}

$.loadingbar = {
    show : function(ele,msg){ 
        msg = (msg == undefined)?"Plz wait ... ..":msg;
        $(ele).html("<i class='fa fa-refresh fa-spin-custom fa-2x fa-fw'></i>"+msg);
        return this; 
    },
    hide : function(ele){ 
        $(ele).html("");
        return this; 
    }
}



$(function(){
    
    $('button#slidetodaysorder').on('click',function(){
        $('div#todaysorderlisting').slideToggle();
    });
    
})


 function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

 
 
 
 