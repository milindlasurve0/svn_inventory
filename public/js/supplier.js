$(document).ready(function(){


var suppliers = new Bloodhound({
//    datumTokenizer: function (datum) {
//        return Bloodhound.tokenizers.obj.whitespace(datum.id);
//    },
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
       url: HOST+'searches/searchSupplier?q=%QUERY',
       wildcard: '%QUERY',
       filter:function(response){
                                                            return $.map(response.results,function(supplier){
                                                                                    return {value:supplier.id,name:supplier.name}
                                                                            });
                                                       }
    }
    
});

suppliers.initialize();

$('#query').typeahead(
        {
              hint: true,
              highlight: true,
              minLength: 1,
         
        },
        {
            name: 'supplierlist',
            display: 'name',
            source: suppliers,
            limit:15
          
        }
        );

$('#query').bind('typeahead:select', function(ev, suggestion) {
  //console.log('Selection: ' + suggestion);
        console.log(suggestion);
        
        $('input#supplier_id').remove();
        
        $('<input>').attr({
            name:'supplier_id',
            id:'supplier_id',
            value:suggestion.value,
            type:'hidden'
        }).insertAfter('button#searchsupplier')
});

$('button#searchsupplier').on('click',function(){
    
   if($('input#supplier_id').val())
   {
    window.location.href="/suppliers/get/"+$('input#supplier_id').val();
   }
   else
   {
       alert('Please select supplier !');
   }
});
        
});

