$(document).ready(function(){
    
    
    $('#neworderamount').on('show.bs.modal', function(e) {
    
    var orderamount = $(e.relatedTarget).data('orderamount');
    var order_id = $(e.relatedTarget).data('orderid');
    var batch_id= $(e.relatedTarget).data('batchid');
    var soid= $(e.relatedTarget).data('soid');
    
   $(e.currentTarget).find('span').text(orderamount) ;
   $(e.currentTarget).find('input#order_id').val(order_id) ;
   $(e.currentTarget).find('input#batch_id').val(batch_id) ;
   $(e.currentTarget).find('input#so_id').val(soid) ;
   
   $(e.currentTarget).find('div#orderamtmsg').empty();
   $(e.currentTarget).find('i').empty();
   
   $(e.currentTarget).find('input#order_amount').val('');
//    $(e.currentTarget).find('input[name="bookId"]').val(bookId);
});

 $(document).on('keyup','input#order_amount',function(){
        
        $('i.words').text(toWords($(this).val()));
        
    });
    
    $('button#updateorderamount').on('click',function(){
    
    var order_id=$('input#order_id').val();
    var so_id=$('input#so_id').val();
    
    var orderamt=parseFloat($('input#order_amount').val());
    
    if(orderamt!="" && !isNaN(orderamt))
    {
       var updateorderamt= $.post(HOST+'orderapprovals/updateorderamt',{order_id:order_id,order_amount:orderamt,so_id:so_id});
       
       updateorderamt.done(function(res){
            
                 res=$.parseJSON(res);
                  
                if(res.type && res.status)
                {
                // Update total order span     
                   
                var oldorderamt=$('td#amount_'+order_id).find('p').text().replace(/,/g, "");    
              
                 var totalorder=parseFloat($('tfoot tr th:eq(9)').attr('data-orderamt'))-parseFloat(oldorderamt)+parseFloat(orderamt);
                 
                 //Update edit html
                var HTML="<p>"+ReplaceNumberWithCommas(orderamt,2)+"</p><a href='#neworderamount' data-toggle='modal'  data-soid='"+so_id+"'   data-orderamount='"+orderamt+"'   data-orderid='"+order_id+"'>Edit</a>";
                
                 $('td#amount_'+order_id).html(HTML);
                 
                 //Update to pay
                 var oldtopayamt=parseFloat($('td#topay_'+order_id).attr('data-topay'));
                 
                 $('td#topay_'+order_id).text(ReplaceNumberWithCommas(res.to_pay,2));
                 $('td#topay_'+order_id).attr('data-topay',res.to_pay);
                 
                 // Update tfoot orderamt sum
                 var oldtfootamt=parseFloat($('tfoot tr th:eq(9)').attr('data-orderamt'));
                 var oldorderamt=parseFloat($('div#neworderamount').find('span').text());
                 
                 $('tfoot tr th:eq(9)').text(ReplaceNumberWithCommas(oldtfootamt-oldorderamt+orderamt,2));
                 $('tfoot tr th:eq(9)').attr('data-orderamt',(oldtfootamt-oldorderamt+orderamt));
                 
                  // Update tfoot topay sum
                 var oldtfoottopayamt=parseFloat($('tfoot tr th:eq(10)').attr('data-topay'));
             
                 $('tfoot tr th:eq(10)').text(ReplaceNumberWithCommas(oldtfoottopayamt-oldtopayamt+res.to_pay,2));
                    console.log(oldtfoottopayamt);
                    console.log(oldtopayamt);
                    console.log(res.to_pay);
                 $('tfoot tr th:eq(10)').attr('data-topay',(oldtfoottopayamt-oldtopayamt+res.to_pay));
                 
                  // show succes message
                 $('div#orderamtmsg').showSuccess("Order amount updated");
         
                 setTimeout(function(){$('#neworderamount').modal('hide');},1000);
                 
                }
                else
                {
                 $('div#orderamtmsg').showFailure(res.message);
                }
       });
    }
    else
    {
        alert("Invalid Order Amount");
    }
});
    
    
        $('#addcomment').on('show.bs.modal', function(e) {

     
      var orderid = $(e.relatedTarget).data('orderid');
         var soid = $(e.relatedTarget).data('soid');
         
     $(e.currentTarget).find('input#comment_order_id').val(orderid) ;
      $(e.currentTarget).find('input#comment_so_id').val(soid) ;
     $(e.currentTarget).find('textarea[name=comment]').val('');
     $(e.currentTarget).find('div#ordercommentmsg').empty();
     $(e.currentTarget).find('div#loadcomments').empty();
     
     var comments=$.getJSON(HOST+'orderapprovals/getComments',{soid:soid});
     
     comments.done(function(res){
         
          if(res.type && res.status)
          {
              $.each(res.comments,function(k,v){
                  
                         var row="";
                         row+="<p>";
                         row+=v.comment
                         row+="........"+v.name
                         row+="</p>";
                         
                   $(e.currentTarget).find('div#loadcomments').append(row);      
              })
          }
          else
          {
              $(e.currentTarget).find('div#loadcomments').html("No Comments Yet");
          }
          
     });
     
    
    });
    
    $(document).on('click','button#addcommentbtn',function(){
    
    
       var  orderid=$('div#addcomment').find('input#comment_order_id').val();
         var  soid=$('div#addcomment').find('input#comment_so_id').val();
       var  comment=$('div#addcomment').find('textarea#comment').val();
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       var addComment=$.post(HOST+'orderapprovals/saveComments',{orderid:orderid,comment:comment,soid:soid});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#ordercommentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#addcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
    });
    
   
     $('input#checkall').on('click',function(){
        
        if(this.checked)
        {
            $(this).parent().parent().parent().next().find('input[type=checkbox][name="orderids[]"]').prop('checked',true);
        }
        else
        {
             $(this).parent().parent().parent().next().find('input[type=checkbox][name="orderids[]"]').prop('checked',false);
        }
        
    });
    
    $('button#approveorders').on('click',function(){
    
   var flag=1;
   
    var approved=$('input[type=checkbox][name="orderids[]"]:checked').map(function(){
 
      var bankid=$('select#bank_'+this.value).val();   
     
     if(bankid==""){ flag=flag*0; }
     
     return this.value+"|"+bankid;
    }).get().join(',');
    
    console.log("FLAG : "+flag);

    if(approved=="")
    {
        alert("No Options Selected");
        return ;
        
    }
    
    if(flag==0)
    {
        alert("Error : Supplier with no banks detected");
        return;
    }
    
    var approveorder=$.post(HOST+'orderapprovals/approveorder',{approved:approved});
    
    approveorder.done(function(res){
        
            res=$.parseJSON(res);
                  
                if(res.type && res.status)
                {
                     showMessage(res.message);
                     location.reload();
                }
                else
                {
                   showMessage(res.message,'error');
                }
        
    });

});


$('button#disapproveorders').on('click',function(){
    
    var disapproved=$('input[type=checkbox][name="orderids[]"]:checked').map(function(){
     var bankid=$('select#bank_'+this.value).val();   
     return this.value+"|"+bankid;
    }).get().join(',');
    
    var disapproved=$.post(HOST+'orderapprovals/disapproveorder',{disapproved:disapproved});
    
    disapproved.done(function(res){
        
           res=$.parseJSON(res);
                  
                 if(res.type && res.status)
                {
                      showMessage(res.message);
                      
                        if($.isArray(res.error))
                      {
                          var errormsgids=$(res.error).map(function(k,v){return v}).get().join(',');
                          alert("Order with Ids  "+errormsgids+"  cannot be rejected since its already downloaded");
                      }
                }
                else
                {
                     showMessage(res.message,'error');
                }
        
    });

});

});

function updatebank(bank_id,order_id,supplier_id)
{
    if(bank_id!="addmorebanks") 
    {
        if(bank_id!="")
        {
            var updatebank=$.post(HOST+'orderapprovals/updatebank',{bank_id:bank_id,order_id:order_id});

            updatebank.done(function(res){

                 res=$.parseJSON(res);

                  if(res.type && res.status)
                  {
                      alert('Updated Success');
                  }
                  else
                  {
                        alert('Error');
                  }

            });
        }
        else
        {
            alert("Select bank");
        }
    }
    else
    {
        window.location.href=window.location.origin+'/suppliers/addBankdetails/'+supplier_id; 
    }
    
}

function updatebatch(batch_id,order_id)
{
    var updatebatch=$.post(HOST+'orderapprovals/updatebatch',{batch_id:batch_id,order_id:order_id});
    
    updatebatch.done(function(res){
        
         res=$.parseJSON(res);
         
          if(res.type && res.status)
          {
              alert('Updated Success');
              
          }
          else
          {
                alert('Error');
          }
        
    });
    
}