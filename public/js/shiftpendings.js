$(document).ready(function(){
          
    $('#shiftpendingdialog').on('show.bs.modal', function(e) {
           var pending_date  = $(e.relatedTarget).data('pending_date');
           var soid  = $(e.relatedTarget).data('soid');
           var supplier_id  = $(e.relatedTarget).data('supplier_id');
           var vendorid  = $(e.relatedTarget).data('vendorid');
           var pending=$.getJSON(HOST+'shiftpendings/getPendings',{soid:soid,pending_date:pending_date});
            pending.done(function(res){
             
                if(res.status && res.type)
                {
                    var pending=parseFloat(res.data.pending);
                    var margin=parseFloat(res.data.margin);
                    var commission_type=parseFloat(res.data.commission_type);
                    var availableamt=((commission_type==2)?(pending/((100+margin)/100)):(pending*((100-margin)/100))).toFixed(2);
                    $(e.currentTarget).find('input#parentpendingid').val(res.data.pending_id);
                    $(e.currentTarget).find('input#parentsupplier_id').val(res.data.supplier_id);
                    $(e.currentTarget).find('input#parentoperator_id').val(res.data.operator_id);
                    $(e.currentTarget).find('input#pendingamt').val(pending);
                    $(e.currentTarget).find('input#operatorname').val(res.data.oprname);
                    $(e.currentTarget).find('input#availableamt').val(availableamt);
                    $(e.currentTarget).find('input#parentmargin').val(margin);
                    $(e.currentTarget).find('input#pcommission_type').val(commission_type);
                    $(e.currentTarget).find('h5#hremainingpending span').text(pending);
                    $(e.currentTarget).find('h5#hremainingpendingamt span').text(availableamt);

                     $(e.currentTarget).find('input#pcommission_type').text(commission_type);
                     $(e.currentTarget).find('td#operatorname').text(res.data.oprname);
                     $(e.currentTarget).find('td#availableamt').text(availableamt);
                     $(e.currentTarget).find('td#pendingamt').text(pending);
                     $(e.currentTarget).find('td#parentmargin').text(margin);
                     $(e.currentTarget).find('input#parentsoid').val(res.data.soid);

                   }
                   else
                   {
                       alert('Error');
                   }
               });
           var operatorlist=$.getJSON(HOST+"shiftpendings/getOperatorListBySupplierIdJSON",{supplier_id:supplier_id,vendorid:vendorid,soid:soid});
           
           operatorlist.done(function(res){
                 if(res.status && res.type){
            $(e.currentTarget).find('#tblshiftpending > tbody').empty();
            
            $.each(res.operators,function(k,v){
                var commission_type=parseFloat(v.commission_type);
                var pending=parseFloat(v.pending);
                var margin=parseFloat(v.margin);
                var actualamt=((commission_type==2)?(pending/((100+margin)/100)):(pending*((100-margin)/100))).toFixed(2);
                var HTML="<tr id='row_"+v.supplier_operator_id+"'>";
                HTML+="<td>"+v.operator_name+"</td>";
                HTML+="<td>"+margin+"</td>";
                HTML+="<td data-pending='"+pending+"'>"+pending+"</td>";
                HTML+="<td data-actualamt='"+actualamt+"'>"+actualamt+"</td>";
                HTML+="<td><input type='text'  style='width: 135px;' class='form-control input-md input-sm' name=txtadjustpending[]  data-margin='"+margin+"' data-commission_type='"+commission_type+"'  data-id='"+v.pending_id+"'   data-soid='"+v.supplier_operator_id+"'  data-sid='"+supplier_id+"'  data-oprid='"+v.opr_id+"'  data-pendingdate='"+v.pending_date+"'  /></td>";
                HTML+="</tr>";
                $(e.currentTarget).find('#tblshiftpending').append(HTML);

            });
            var HTML="";
                  HTML="<tr><td></td><td></td><td></td><th>Total : </th><td id='tdentersum'>0.00</td></tr>";
                   $(e.currentTarget).find('#tblshiftpending').append(HTML);
        }
        else
        {
            alert("No other operators detected");
            $('#shiftpendingdialog').modal('hide');
        }
        });
    });
    
$(document).on('keyup','input[name="txtadjustpending[]"]',function(e){
      
        if(e.keyCode == 13)
        {
           if(!$.isNumeric($(this).val())){  $(this).val('0');$(this).trigger(e);return;}
            
           var textboxvalue=parseFloat($(this).val()); 
           var pendingid=$(this).attr('data-id');
           var parentmargin=parseFloat($('td#parentmargin').text());
           var p_commission_type=parseFloat($('input#pcommission_type').val());
           var parenttopay=parseFloat($('td#availableamt').text());
           var c_commission_type=$(this).attr('data-commission_type');
           var childmargin=parseFloat($(this).attr('data-margin'));
           var totaltextboxvalue=0;
           var cfactor=(c_commission_type==2)?((100+childmargin)/100):((100-childmargin)/100);
           var pfactor=(p_commission_type==2)?((100+parentmargin)/100):((100-parentmargin)/100);
         
           // get sum of all textbox
            $('input[name="txtadjustpending[]"]').each(function(){
                
                    var temp=0;
                    if($(this).val()!="" &&  !isNaN($(this).val()) && $.isNumeric($(this).val()))
                    {temp=parseFloat($(this).val());}
                    else
                    {temp=0};
                    
                    totaltextboxvalue+=temp;

                     // Check individual topay
                    if(parenttopay>0){
                        if(temp>parenttopay || temp<0 ){alert('Value should be between 0-'+parenttopay);$(this).val('0'); return;}
                     }
                    else{ 
                        if(temp<parenttopay || temp>0   ){alert('Value should be between '+parenttopay+' -0 ');$(this).val('0'); return;}
                    }
          });
          
           totaltextboxvalue=totaltextboxvalue.toFixed(2);
          
            console.log("Parent "+parenttopay);
            console.log("textBoxSum "+totaltextboxvalue);
          // condition to check if total sum i not greater than orginal topay
          if(parenttopay>0){
          if(totaltextboxvalue>parenttopay){alert('Value cannot exceed '+parenttopay);return;}
         }else
         {
              if(totaltextboxvalue<parenttopay || totaltextboxvalue>0   ){alert('Value cannot deceed '+parenttopay);return;}
         }
         
           //reset td's to affected
            $(this).parent().prev('td').text('');
            $(this).parent().prev().prev('td').text('');
                      
           // Adjust Child pending amt
            var oldactualamt=parseFloat($(this).parent().prev('td').attr('data-actualamt'));
            $(this).parent().prev('td').text((oldactualamt+textboxvalue));
                      
           // Adjust actual pending         
          var actualpending= parseFloat((c_commission_type==2)?(textboxvalue*cfactor):(textboxvalue/cfactor));
          
          var oldpending=parseFloat($(this).parent().prev().prev('td').attr('data-pending'));
        
          $(this).parent().prev().prev('td').text((oldpending+actualpending).toFixed(2));
          
          //Change remaining pending h tag
          var og=parseFloat($('td#pendingamt').text());

          var totaladjustedvalue=parseFloat((p_commission_type==2)?(totaltextboxvalue*pfactor):(totaltextboxvalue/pfactor));

          $('h5#hremainingpending span').text((og-totaladjustedvalue).toFixed(2));
         
         // add totalenter value
         $('td#tdentersum').text(totaltextboxvalue);
         
         // Change remaining pending amt tag
        var hremainingamt=(parenttopay-totaltextboxvalue).toFixed(2);
            console.log(hremainingamt);
          $('h5#hremainingpendingamt span').text(hremainingamt);
        }
          
  });    
   $('button#btnshiftpending').on('click',function(){

            var r=confirm('Are you sure ? ');
            
            if(r===true)
            {
                var targetpending=[];
                var textboxsum=0;
                
                 $('input[name="txtadjustpending[]"]').each(function(){
                   
                            if($(this).val()!="" && $.isNumeric($(this).val())){  

                                  var temp={
                                                 id:$(this).attr('data-id'),
                                                 textboxsum:$(this).val()*(-1),
                                                 pending:$(this).parent().prev().prev('td').text(),
                                                 pendingamt:$(this).parent().prev('td').text(),
                                                 soid:$(this).attr('data-soid'),
                                                 supplier_id:$(this).attr('data-sid'),
                                                 operator_id:$(this).attr('data-oprid'),
                                                 margin:$(this).attr('data-margin'),    
                                                 pending_date:$(this).attr('data-pendingdate')    
                                             };

                             targetpending.push(temp);
                             textboxsum+=parseFloat($(this).val());
                          }
                
                 });
                 
            var   parentpendingamt=$('input#availableamt').val();
            if((textboxsum!==0) && (textboxsum >= parentpendingamt))
            {
                var parentpending=[];

                    parentpending.push({
                                                            id:$('input#parentpendingid').val(),
                                                            pending:$('h5#hremainingpending span').text(),
                                                            pendingamt:$('h5#hremainingpendingamt span').text(),
                                                            textboxsum:textboxsum,
                                                            supplier_id:$('input#parentsupplier_id').val(),
                                                            operator_id:$('input#parentoperator_id').val(),        
                                                            soid:$('input#parentsoid').val(),
                                                            margin:$('input#parentmargin').val(),
                                                            parentogpending:$('input#pendingamt').val(),
                                                            pending_date:$('input#parentpendingdate').val()
                                                            });


                 var dataString={parentpendings:parentpending,targetpendings:targetpending};

                 var adjustPendings=$.post(HOST+'shiftpendings/adjustPending',{data:JSON.stringify(dataString)});

                 adjustPendings.done(function(res){

                    res=$.parseJSON(res);

                     if(res.status && res.type)
                        {
                            alert(res.message);
                            location.reload();
                        }
                  else
                        {
                            alert("Error");
                        }

                 });
             }
         else
            {
                alert("Something went wrong");
                return;
            }
       }
            
    });
});
