$(document).ready(function(){
    $('#addcomment').on('show.bs.modal', function(e) {

      var soid = $(e.relatedTarget).data('soid');
      var oprid = $(e.relatedTarget).data('oprid');
      var blockid = $(e.relatedTarget).data('param');
      
     $(e.currentTarget).find('input#so_id').val(soid) ;
     $(e.currentTarget).find('input#opr_id').val(oprid) ;
     $(e.currentTarget).find('input#block_id').val(blockid);

     $(e.currentTarget).find('textarea[name=comment]').val('');
     $(e.currentTarget).find('div#blockcommentmsg').empty();
     $(e.currentTarget).find('div#loadcomments').empty();
     
     var comments=$.getJSON(HOST+'newblocksims/getBlockSimsComments',{soid:soid,oprid:oprid,blockid:blockid});
     
     comments.done(function(res){
         
          if(res.type && res.status)
          {
              $.each(res.comments,function(k,v){
                                                                    
                         var HTML="";
                         HTML="<div class='row' style='margin-bottom: 5px;'>";
                         HTML+="<small class='pull-right time'><i class='fa fa-clock-o'></i>&nbsp;"+v.note_date+" / "+v.note_timestamp+"</small>";
                         HTML+="<h5 class='media-heading notesusername'>"+v.username+"</h5>";
                         HTML+="<small class='col-lg-l0'>"+v.message+"</small>";
                         HTML+="</div>";
                         
                         $('div#addcomment #loadcomments').append(HTML);
              })
          }
          else
          {
              $(e.currentTarget).find('div#loadcomments').html("No Comments Yet");
          }
          
     });
     
    
    });
    
    $(document).on('click','button#addcommentbtn',function(){
                    
        var soid=$('div#addcomment').find('input#so_id').val() ;
        var oprid=$('div#addcomment').find('input#opr_id').val() ;
        var blockid=$('div#addcomment').find('input#block_id').val();
        var  comment=$.trim($('div#addcomment').find('textarea#comment').val());
  
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       var addComment=$.post(HOST+'newblocksims/saveComments',{soid:soid,oprid:oprid,blockid:blockid,comment:comment});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#blockcommentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#addcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
    });
    
       
});