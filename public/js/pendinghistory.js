$(document).ready(function(){
       window.scrollTo(0,document.body.scrollHeight);
       $('#refunddialog').on('show.bs.modal', function(e) {
           
           var avaliableamt = $(e.relatedTarget).data('avaliableamt');
           var pendingid=      $(e.relatedTarget).data('pendingid');
           
           $(e.currentTarget).find('input#avaliableamt').val(avaliableamt);
           $(e.currentTarget).find('input#inpendingid').val(pendingid);
           $(e.currentTarget).find('span.spnavalibleamt').text(avaliableamt.toFixed(2));
//           $(e.currentTarget).find('input#inprefundamt').ForceNumericOnly();

           $('input#inprefundamt').val('');
       });
       
       $(document).on('click','#btnrefund',function(){
           
           var avaliableamt=parseFloat($('input#avaliableamt').val());
           var enteredamt=parseFloat($('input#inprefundamt').val());
           var pendingid=parseFloat($('input#inpendingid').val());
           var comment=$('#refundmessage').val();
           var refundType=$('#refund_type').val();
           avaliableamt=(avaliableamt>0)?(Math.ceil(avaliableamt.toFixed(2))):(Math.floor(avaliableamt.toFixed(2)));
           enteredamt=enteredamt.toFixed(2);
           
           console.log("Avaliable : "+avaliableamt);
           console.log("Entered : "+enteredamt);
           if(avaliableamt>0)
           {
                if((enteredamt>(avaliableamt+5000)) || enteredamt<=0 || enteredamt=="")
             //   if(enteredamt<=0)
                     { 
                         alert('Error : Entered value should be between 0 to '+(avaliableamt+5000));
                         return;
                     }
            }
           else if(avaliableamt<0)
           {
              if((enteredamt*-1)>(avaliableamt*-1) || enteredamt>=0 || enteredamt=="")
        //   if(enteredamt<=0)
                    { 
                        alert('Error : Entered value should be between  '+avaliableamt+' to 0');
                        return;
                    } 
           }
           
           if($.trim(comment)==""){alert("Enter Comment "); return };
           
           var r=confirm('Are you sure ?');
           
           if(r)
           {
               $('#btnrefund').addClass('disabled');
               $.loadingbar.show('div#refundmsg');
                
               var refundpending=$.post(HOST+'orderhistorys/setrefund',{pendingid:pendingid,enteredamt:enteredamt,comment:comment,refundType:refundType});
            
               refundpending.done(function(res){
                   
                   res=$.parseJSON(res);
                   
                  if(res.type && res.status)
                  {
                      alert('Done');
                     location.reload();
                  }
                  else
                  {
                      alert('Error : '+res.message);
                      location.reload();
                  }
                          
                   
               });
               
           }
           
           
           
               
       });
       
});

