$(document).ready(function(){
    
    $('button#btnregenerateorder').on('click',function(){
        
        $(this).attr('disabled',true);
        
        if($('input[type=checkbox][name="chkregenerate[]"]:checked').length==0){
            alert("Error : No orders selected");
            return;
        }
        
         var days = prompt("Enter no of days ", "");
         
         if(isNaN(days) || days<=0 || days=="" || days > 4)
         {
             alert("Error : Enter valid number (upto 4 days only) ");
             $(this).attr('disabled',false);
             return;
         }
         else
         {
            var orderids=$('input[type=checkbox][name="chkregenerate[]"]:checked').map(function(){
                return this.value;
             }).get().join(",");
             
            var dataString={orderids:orderids};
            
            var order=$.getJSON(HOST+'orders/regenerateorder',dataString);
            
            order.done(function(res){
                
               $('#divregenerateorder').modal('show');
               
                var HTML="";
                HTML="<table class='table'><thead><tr>";
                HTML+="<th>Name</th><th>Margin</th><th>Basesale</th><th>Previous Order</th><th>Order</th><th>Topay</th><th>Days</th>";
                HTML+="</tr></thead><tbody>";
                
                 $.each(res.orders,function(k,v){
                     
                     HTML+="<tr id='tr_"+v.orderid+"'>";
                     HTML+="<td>"+v.supplier+"<br/><div class='btn btn-default btn-warning btn-xs' title='"+v.modemnames+"' style='overflow:hidden;width:95px;'>"+v.modemnames+"</div></td>";
                     HTML+="<td>"+v.margin+"</td>";
                     HTML+="<td id='tdbase_"+v.orderid+"'>"+v.basesale+"</td>";
                     HTML+="<td>"+v.oldorder+"</td>";
                     var regeneratedOrderamt=parseFloat(v.basesale)*parseFloat(days);
                     HTML+="<td><input id='inpreg_"+v.orderid+"' name='inpreg[]'  style='width: 55%;' type='text' class='form-control'  data-ctype='"+v.commission_type+"'   data-orderid='"+v.orderid+"'  data-soid='"+v.supplier_operator_id+"'   value='"+regeneratedOrderamt+"'   /><span class='words'/></td>";
                    
                     var topay=regeneratedOrderamt;
                     if(v.commission_type=="1")
                     {
                         topay=((100-v.margin)/100)*regeneratedOrderamt;
                     }
                     
                      HTML+="<td>"+topay+"</td>";
                      HTML+="<td><input type='text' class='form-control squaretextbox changeorderbydays' value='"+days+"' data-orderid='"+v.orderid+"'   data-ctype='"+v.commission_type+"'    /></td>";
                    HTML+="</tr>";
                 });
                
                HTML+="</tbody></table>";
                
                $('div#regeneratehtml').html(HTML);
                
                $('input[type=text][name="inpreg[]"]').ForceNumericOnly();
                $('.changeorderbydays').ForceNumericOnly();
            });
            
         }
        
        
    });
   
    
     $(document).on('keypress','.changeorderbydays',function(e){
     
            if (e.keyCode == 13) {
                
                 var days=parseFloat($(this).val());
                 
                  if(days<=0 || days>4)
                    {
                        alert('Error : Invalid Days');
                        return;
                    }
                    
                   var orderid=$(this).attr('data-orderid');
                   var basesale=parseFloat($('#tdbase_'+orderid).text());
                   var ctype=$(this).attr('data-ctype');
                   var margin=parseFloat($('tr#tr_'+orderid+'  td:eq(1)').text());
                   
                   var neworder=basesale*days;
                   
                    // set input value       
                   $('input#inpreg_'+orderid).val(neworder);
                   
                   // set to pay
                   var topay=neworder;
                   if(ctype==1)
                   {
                        topay=((100-margin)/100)*(neworder);
                   }
                   
                  $('tr#tr_'+orderid+'  td:eq(5)').text(topay);
                   
                   
            }
            
     });
     
     
     $('#btnconfirmregenerate').on('click',function(){
         $.ajaxSetup({async: false});
         var flag=true;
         var r=confirm('Are you sure ? ');
         
         if(r)
         {
            $('#btnconfirmregenerate').attr('disabled',true);
             
             var datastring=[];
             var soidarr=[];
             var maxcaparray=[];
             
             $('input[type=text][name="inpreg[]"]').each(function(){
                 if($(this).val()>0){
                     
                    var amount = $(this).val();
                    var soid =$(this).attr('data-soid');
                    var so_amnt=soid+"_"+amount;
                    
                 }
                 soidarr.push(so_amnt);
             });
             console.log(soidarr);
             
             var isMaxCapExceeded=$.post(HOST+'orders/isMaxCapExceeded',{data:JSON.stringify(soidarr)});
             
             isMaxCapExceeded.done(function(res){
                    
                      res=$.parseJSON(res);
                      
                         if(res.status && res.type)
                         {
                            flag= true;
                         }
                         else
                         {
                            $.each(res.data,function(k,v){
                            var somaxcap=v.supplier+"="+v.capacity;
                            maxcaparray.push(somaxcap);
                            });
                            
                            alert("Max order capacity exceeded. Max cap limit is as follows  : "+maxcaparray);
                            flag=false;
                            $('#btnconfirmregenerate').removeAttr('disabled');
                         }
             });
             
             if(flag==true){
             $('input[type=text][name="inpreg[]"]').each(function(){
                 
               if($(this).val()>0){  
                   
                        var  item = {}

                        item ["orderid"] = $(this).attr('data-orderid');
                        item ["amount"] = $(this).val();
                        item ["soid"] =$(this).attr('data-soid');
                        item["days"]=$(this).parent().next().next().find('input.changeorderbydays').val();

                         datastring.push(item);
                                              }
             });
             
             console.log(JSON.stringify(datastring));
             
             var saveorder=$.post(HOST+'orders/saveRegeneratedOrder',{data:JSON.stringify(datastring)});
             
             saveorder.done(function(res){
                    
                      res=$.parseJSON(res);
                      
                         if(res.status && res.type)
                         {
                             alert('Done');
                             window.location.reload();
                         }
                         else
                         {
                             alert("Error");
                             window.location.reload();
                         }
             });
         }
     }
    $.ajaxSetup({async: true});   
         
     });
     
     
     $(document).on('keyup','input[type=text][name="inpreg[]"]',function(e){
        
            var orderid=$(this).attr('data-orderid');
            var basesale=parseFloat($('#tdbase_'+orderid).text());
            var ctype=$(this).attr('data-ctype');
            var margin=parseFloat($('tr#tr_'+orderid+'  td:eq(1)').text());
            var textval=$(this).val();
            $('tr#tr_'+orderid+'  span.words').text(toWords($(this).val()));
            
             if(ctype==1)
                   {
                        topay=((100-margin)/100)*(textval);
                   }
                   else
                   {
                       topay=textval;
                   }
                   
             $('tr#tr_'+orderid+'  td:eq(5)').text(ReplaceNumberWithCommas(topay,2));
     });
    
});
