$(document).ready(function(){
    
    $('#target_date').datepicker({
    format: "yyyy-mm-dd",
    startDate: "-365d",
    endDate: "1d",
    multidate: false,
    autoclose: true,
    todayHighlight: true,
    orientation: "top right"
    });
    
    $('button#loadsuppliersforplanning').on('click',function(){
        
      
         $('div#planningsheet table tbody').empty();
         $('div#planningsheet table tfoot').empty();
       
        var operator_id=$('select#operator_id').val();
        var target_date=$('input#target_date').val();
        
        if(operator_id=="" || target_date==""){
            alert("Select Date and operator id");
            return;
        }
        $.loadingbar.show('div#loadsuppliermsg');
        
        var supplierlist=$.getJSON(HOST+"plannings/getSupplierListByOperatorIdJSON",{operator_id:operator_id,target_date:target_date});
        
        supplierlist.done(function(response){
          
            if(response.type===false)
            {
                alert("No Supplier fetched");
                return ;
            }
            
            var totalAvg=0;
            var totallastdaySale=0;
            var totalcurrentSale=0;
            var totalOpening=0;
            var totalBaseamount=0;
            var totalTarget=0;
            var totalModemTarget=0;
            var totalOrder=0;
            var totalPending=0;
            var totalMaxsalecap=0;
            
            $.each(response.suppliers,function(k,v){
          
                totalAvg+=parseFloat(v.avg_sale);
                totallastdaySale+=parseFloat(v.last_day_sale);
                totalcurrentSale+=parseFloat(v.current_sale);
                totalOpening+=parseFloat(v.opening);
                totalBaseamount+=parseFloat(v.base_amount);
                totalTarget+=parseFloat(v.planned_sale);
                var amt=v.totalorderamt || 0;
                totalOrder+=parseFloat(amt);
                totalPending+=parseFloat(v.pending);
                totalMaxsalecap+=parseFloat(v.max_sale_capacity);
                
                var isPrev=(moment(target_date).isSame(moment().format('YYYY-MM-DD'))?"":"disabled");
                HTML="<tr id='row_"+v.supplier_operator_id+"'>";
                HTML+="<td>"+v.name+"</td>";
                 var marginiconhtml="";
                 if(v.commission_type=="1"){marginiconhtml="<span class='glyphicon glyphicon-arrow-down text-success'></span>"}else{marginiconhtml="<span class='text-success glyphicon glyphicon-arrow-up'></span>"};
                HTML+="<td>"+v.margin+marginiconhtml+"</td>";
                HTML+="<td>"+ReplaceNumberWithCommas(v.avg_sale)+"</td>";
                HTML+="<td>"+ReplaceNumberWithCommas(v.last_day_sale)+"</td>";
                HTML+="<td>"+ReplaceNumberWithCommas(v.current_sale)+"</td>";
                HTML+="<td><a title='Blocked Bal : "+v.blockedbalance+" '  target='_blank' href='/orders/viewlastdayincoming/"+v.supplier_operator_id+"/"+v.vendorids+"/"+operator_id+"/"+v.ddate+"'>"+ReplaceNumberWithCommas(parseFloat(v.opening)-parseFloat(v.blockedbalance))+"</a></td>";
                HTML+="<td>"+v.activesims+"/"+v.totalsims+"</td>";
                HTML+="<td>"+ReplaceNumberWithCommas(amt)+"</td>";
                HTML+="<td>"+ReplaceNumberWithCommas(v.pending)+"</td>";
                var tobedisabled=(GROUP_ID=='13' || GROUP_ID=='12' )?"":"disabled";
//                HTML+="<td><input value='"+v.base_amount+"'  type='text' class='form-control inputwidth95' name='base_amount'  "+tobedisabled+"  "+isPrev+" /></td>";
                HTML+="<td><input value='"+v.base_amount+"'  type='text' class='form-control inputwidth95' name='base_amount'  disabled /></td>";
                HTML+="<td><input data-oldval='"+v.planned_sale+"' data-soid='"+v.supplier_operator_id+"'  value='"+v.planned_sale+"'  type='text' class='form-control inputwidth95' name='planned_sale'  "+tobedisabled+"  "+isPrev+"   /></td>";
                
                var vendorshtml="";
                
                if(!$.isEmptyObject(v.vendors))
                {
                    $.each(v.vendors,function(k1,v1){
                            var hflag = (v1.hflag==1)?"bg-lightblue":"";
                            vendorshtml+='<div class="row marginbt5 marginlt10 marginrt10 '+hflag+'">';                            
                            vendorshtml+='<div class="col-lg-5 font10" >'+v1.company+'</div>';
                            vendorshtml+='<div class="col-lg-7  modemtargets"><input type="text" name="modem_target[]" data-vendorid="'+v1.vendor_id+'" class="form-control smallheight"  value='+v1.target+'   '+isPrev+'   /></div>';
                            vendorshtml+='</div>';
                            totalModemTarget+=parseFloat(v1.target);
                    });
                   
                }
                HTML+="<td class='modemtargets'>"+vendorshtml+"</td>";
                HTML+="<td><input type='text' name='msc' id='msc'  class='form-control smallheight inputwidth95'  value='"+v.max_sale_capacity+"'  "+isPrev+" "+tobedisabled+"    /></td>";
                HTML+="<td><a data-soid='"+v.supplier_operator_id+"'  data-toggle='modal' href='#addnotesdiv' title='"+v.message.messages+"'><i class='fa fa-comments-o fa-2'></i></a>&nbsp;("+v.message.totalmessages+")</td>";
                HTML+="<td><button  "+isPrev+"  class='btn btn-default btn-success btn-sm' onclick='SavePlanning("+v.supplier_operator_id+");'>Save</button></td>";
                HTML+="</tr>";
                
                $('div#planningsheet table tbody').append(HTML); 
                
                HTML="";
                
            });
            
           var FOOTER="<tr><th>Totals : </th><th>&nbsp;</th><th>"+ReplaceNumberWithCommas(totalAvg,2)+"</th><th>"+ReplaceNumberWithCommas(totallastdaySale,2)+"</th><th>"+ReplaceNumberWithCommas(totalcurrentSale,2)+"</th><th>"+ReplaceNumberWithCommas(totalOpening,2)+"</th><th>&nbsp;&nbsp;</th><th>"+ReplaceNumberWithCommas(totalOrder,2)+"</th><th>"+ReplaceNumberWithCommas(totalPending,2)+"</th><th>"+ReplaceNumberWithCommas(totalBaseamount,2)+"</th><th data-target='"+totalTarget+"'>"+ReplaceNumberWithCommas(totalTarget,2)+"</th><th>"+ReplaceNumberWithCommas(totalModemTarget,2)+"</th><th>"+ReplaceNumberWithCommas(totalMaxsalecap,2)+"</th></tr>";
           $('div#planningsheet table tfoot').append(FOOTER); 
           
           $('div.sftdbs').html("<strong>Short for the day [BASESALE] : </strong><span>"+ReplaceNumberWithCommas(totalAvg-totalBaseamount,2)+"</span>");
           $('div.sftdta').html("<strong>Short for the day [TARGET] : </strong><span data-total='"+(totalAvg-totalTarget)+"'>"+ReplaceNumberWithCommas(totalAvg-totalTarget,2)+"</span>");
            
//            $('input[name="base_amount"]').ForceNumericOnly();
            $('input[name="target"]').ForceNumericOnly();
            $('input[name="msc"]').ForceNumericOnly();
            $('input[name="modem_target[]"]').ForceNumericOnly();
            
              $.loadingbar.hide('div#loadsuppliermsg');
            
        });
        
    })
    

$(document).on('keyup','input[name="planned_sale"]',function(e){
     if(e.keyCode == 13)
    {
            var soid=$(this).attr('data-soid');
            var oldshorttargertotal=parseFloat($('div.sftdta span').attr('data-total'));
            var oldtarget=parseFloat($(this).attr('data-oldval'));
            var newtarget=parseFloat($(this).val());
            var finalshort=(oldshorttargertotal+oldtarget-newtarget);
            $('div.sftdta span').text(ReplaceNumberWithCommas(finalshort,2));
            $('div.sftdta span').attr('data-total',finalshort);
            $(this).attr('value',newtarget);
            $(this).attr('data-oldval',newtarget);
            //Update bottom tfoot total
            var oldtfoottotal=parseFloat($('table#tblsetarget tfoot tr th:eq(10)').attr('data-target'));
            $('table#tblsetarget tfoot tr th:eq(10)').text(ReplaceNumberWithCommas(oldtfoottotal-oldtarget+newtarget,2));
            $('table#tblsetarget tfoot tr th:eq(10)').attr('data-target',oldtfoottotal-oldtarget+newtarget);
            
            showMessage("Working . . . .");
            SavePlanning(soid);
    }
});
    
 /*
  * Start of Note
  */
$('#addnotedate').datepicker({
    format: "yyyy-mm-dd",
    startDate: "-365d",
    endDate: "1d",
    multidate: false,
    autoclose: true,
    todayHighlight: true,
    orientation: "top right"
    }).on('show.bs.modal', function (event) { event.stopPropagation();});
            
$('#addnotesdiv').on('show.bs.modal',function(e){
        
            var soid = $(e.relatedTarget).data('soid');
            var type=1;
            
            $(e.currentTarget).find('#notesthread').empty();
            $(e.currentTarget).find('input#inpnotesoid').val(soid);
            $(e.currentTarget).find('input#addnotedate').val($('#target_date').val());
            $(e.currentTarget).find('button#btnrefreshcomments').attr('data-soid',soid);
           getNotes(soid,type);     
});

$(document).on('click','#btnrefreshcomments',function(e){
        $('button#btnrefreshcomments i').addClass('glyphicon-refresh-animate');
        var soid=$(this).attr('data-soid');
        $('#notesthread').empty();
        getNotes(soid,1);
});

function getNotes(soid,type)
{
             var getnotes=$.getJSON(HOST+'notes/fetchNotesBySoid',{soid:soid,type:type,note_date:$('input#target_date').val()});
            
           getnotes.done(function(res){
              if(res.status && res.data.length)
              {
                     $('#notesthread').empty();
                     $.each(res.data,function(k,v){
                         
                         var HTML="";
                         HTML="<div class='row' style='margin-bottom: 5px;'>";
                         HTML+="<small class='pull-right time'><i class='fa fa-clock-o'></i>&nbsp;"+v.note_date+" / "+v.note_timestamp+"</small>";
                         HTML+="<h5 class='media-heading notesusername'>"+v.username+"</h5>";
                         HTML+="<small class='col-lg-l0'>"+v.message+"</small>";
                         HTML+="</div>";
                         
                         $('div#addnotesdiv #notesthread').append(HTML);
                     });
                     
                      $('button#btnrefreshcomments i').removeClass('glyphicon-refresh-animate');
              }
              else
              {
                 console.log("No Comments Fetched");
                 $('button#btnrefreshcomments i').removeClass('glyphicon-refresh-animate');
              }
              
           });
           
}
$(document).on('click','button#btnaddnote',function(){
    
    var message=$('div#addnotesdiv').find('textarea#addnotemsg').val();
    var soid=$('div#addnotesdiv').find('input[type=hidden][id=inpnotesoid]').val();
    var note_date=$('div#addnotesdiv').find('input#addnotedate').val();
    
    if(message=="" || soid=="" || note_date=="")
    {
        alert("Fill all details");
        return;
    }else{
        
        var savenote=$.post(HOST+'notes/save',{message:message,soid:soid,note_date:note_date,type:1,operator_id:$('select#operator_id').val()});
        
        savenote.done(function(res){
            
            res=$.parseJSON(res);
            
            if(res.status && res.error){
                alert(res.message);
                return;
            }
            if(res.status && res.type)
            {
                    $('div#addnotemsg').showSuccess("Done").done(function(){ getNotes(soid,1);});
                    $('div#addnotesdiv').find('textarea#addnotemsg').val('');
                    $('div#addnotesdiv').find('input#addnotedate').val('');
            }
            else
            {
                  $('div#addnotemsg').showFailure("Error");
            }
        });
        
    }
    
    
    
        
});

    /*
     * End of notes
     */
});

    
function SavePlanning(supplier_operator_id)
{
//      var base_amount=$('tr#row_'+supplier_operator_id).find('input[name=base_amount]').val();
      var planned_sale=$('tr#row_'+supplier_operator_id).find('input[name=planned_sale]').val();
      var max_sale_capacity=$('tr#row_'+supplier_operator_id).find('input[name=msc]').val();
      var modemtarget=[];
         
    if($('tr#row_'+supplier_operator_id+' input[name="modem_target[]"]').length){
            $('tr#row_'+supplier_operator_id+' input[name="modem_target[]"]').map(function(){
                modemtarget.push({supplier_operator_id:supplier_operator_id,vendor_id:$(this).attr('data-vendorid'),target:$(this).val()});
            });
        }      
      
//       var formdata={supplier_operator_id:supplier_operator_id,planned_sale:planned_sale,base_amount:base_amount,modem_targets:modemtarget,max_sale_capacity:max_sale_capacity};
       var formdata={supplier_operator_id:supplier_operator_id,planned_sale:planned_sale,modem_targets:modemtarget,max_sale_capacity:max_sale_capacity};
       
       var request=$.post(HOST+"plannings/setTarget",{targets:JSON.stringify(formdata)});
          
        request.done(function(response){

                   response=$.parseJSON(response);

                  if(response.type)
                  {
                      showMessage("Done ..... ");
//                     $('div#output_message').showSuccess(response.message);
//                      setTimeout(function(){$('div#output_message').empty()},1000);
                  }
                  else
                  {
                       showMessage("Error : "+response.message,error);
                     // $('div#output_message').showFailure(response.message);
                  }
       });
          
    console.log(formdata);
    return ;
    //--------------------//
 

  
    

}

