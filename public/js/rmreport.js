 $(document).ready(function(){ 
     
        $('select[name="modems[]"]').multipleSelect({ selectAll: true, width: 312,multipleWidth: 140,multiple: true});
        
        $("form[name='frmfiltersupplier']").on('submit',function(e) {       
            
               e.preventDefault();
               var  modemsIds=$('select[name="modems[]"]').multipleSelect("getSelects");
               
               var values = {};
              
                $.each($(this).serializeArray(), function(i, field) {
                values[field.name] = field.value;
                });
              
              var showallrecords=values.showall=="1"?"1":"0"; 
              window.location.href="?modems_ids="+modemsIds+"&operator_id="+values.operator_id+"&is_active="+values.is_active+"&showall="+showallrecords+"&orderby=&ordertype=";
        });
 });

