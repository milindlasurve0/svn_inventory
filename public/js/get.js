$(document).ready(function(){
          
     $('input[type=radio][name="options"]').change(function(){
         var soid=$(this).attr('data-soid');
         var flag=$(this).val();
         var basesale=$(this).attr('data-baseamt');
         var margin=$(this).attr('data-margin');
         
         $("#comment_so_id").val(soid);
         $("#inpflag").val(flag);
         $("#baseamt").val(basesale);
         $("#margin").val(margin);
     
         $('#addcomment').modal('show');
        
         
     });
     
     $('button#addcommentbtn').on('click',function(){
        var soid = $("#comment_so_id").val();
        var flag = $("#inpflag").val();
        var ele=$('input[name="options"]:checked');
        var  comment=$.trim($('div#addcomment').find('textarea#comment').val());
        var basesale = $("#baseamt").val();
        var margin = $("#margin").val();
        
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       
       var addComment=$.post(HOST+'suppliers/saveComments',{soid:soid,basesale:basesale,margin:margin,flag:flag,comment:comment});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#addcomment').find('textarea#comment').val(' ');
                    $('div#commentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#addcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
       
    
        var isactive=$.post(HOST+'operators/setisactive',{soid:soid,flag:flag});
         
            isactive.done(function(res){
                   
                    res=$.parseJSON(res);
                 
                    if(res.status==true && res.type==true)
                    {
                         if(flag=="off")
                         {
                             ele.parent().removeClass('btn-success disabled').addClass('btn-danger disabled');
                             ele.parent().prev().removeClass('btn-success disabled');
                         }
                         else
                         {
                             ele.parent().removeClass('btn-danger disabled').addClass('btn-success disabled');
                             ele.parent().next().removeClass('btn-danger disabled');
                         };
                         
                         showMessage(res.message);
                    }
                    else
                    {
                         showMessage("No Data received",'error');
                    }
            });
   });
   
        $('button#resetbtn').on('click',function(){
             var ele=$('input[name="options"]:checked');
             ele.parent().removeClass("active");
         });
   
          $('input[type=radio][name="bank_onoff_flag"]').change(function(){
         
         var sbid=$(this).attr('data-sbid');
         var flag=$(this).val();
         var ele=$(this);
         
         var isactive=$.post(HOST+'suppliers/setDefaultBank',{sb_id:sbid,flag:flag});
         
            isactive.done(function(res){
                   
                    res=$.parseJSON(res);
                 
                    if(res.status==true && res.message=="success")
                    {
                      console.log(flag);
                      
                         if(flag=="0")
                         {
                             ele.parent().removeClass('btn-success').addClass('btn-danger');
                            ele.parent().next().removeClass('btn-success');
                         }
                         else
                         {
                             ele.parent().removeClass('btn-danger').addClass('btn-success');
                             ele.parent().prev().removeClass('btn-danger');  
                         };
                         
                         showMessage(res.message);
                    }
                    else
                    {
                         showMessage("No Data received",'error');
                    }
            });
         
     });
     
     $('input[type=checkbox][name="is_onhold"]').click(function(){
        var checked = $(this).is(':checked');
        var soid=$(this).attr('data-soid');
        var basesale=$(this).attr('data-baseamt');
        var margin=$(this).attr('data-margin');
        var flag=0;
        
        if(checked) 
        {
            flag=1;
        }
        
        $("#comment_soid").val(soid);
        $("#flag").val(flag);
        $("#baseamt").val(basesale);
        $("#margin").val(margin);
        $('#onholdcomment').modal('show');
    });
    
    $('button#onholdcommentbtn').on('click',function(){
        var soid = $('#comment_soid').val();
        var flag = $('#flag').val();
        var  comment=$.trim($('div#onholdcomment').find('textarea#comment').val());
        var basesale = $("#baseamt").val();
        var margin = $("#margin").val();
        
        if(comment==""){
           alert("Invalid Comment");
           return;
       }
       
       var addComment=$.post(HOST+'suppliers/saveHoldStatusComments',{soid:soid,basesale:basesale,margin:margin,flag:flag,comment:comment});
       
       addComment.done(function(res){
           
             res=$.parseJSON(res);
             
             if(res.type && res.status)
             {
                    $('div#onholdcomment').find('textarea#comment').val(' ');
                    $('div#commentmsg').showSuccess("Comment Saved");
                    setTimeout(function(){$('div#onholdcomment').modal('hide');},1000);
             }
             else
             {
                     alert("Error");
             }
           
       });
        
        var isonhold=$.post(HOST+'suppliers/setOnholdFlag',{soid:soid,flag:flag});

            isonhold.done(function(res){

                   res=$.parseJSON(res);

                   if(res.status==true && res.type==true)
                   {                                            
                        showMessage(res.message);
                   }
                   else
                   {
                        showMessage("No Data received",'error');
                   }
           });
    });
                
    function setDefaultBank(id,supplier_id)
   {
       $.ajax({
                     method:"POST",
                     url:HOST+'suppliers/setDefaultBank/',
                     data:{sb_id:id,supplier_id:supplier_id},
                     dataType:"json",
                     success:function(data){
                           if(data.message=="success")
                           {
                                   $('div#div_success').show().delay(1000).fadeOut();
                           }
                           else
                           {
                                    $('div#div_error').hide().delay(1000).fadeOut();;   
                           }
                     }
                  });
   }
   
   
   
   



});


$(function(){
    
    $('input[type=radio][name=bank_change_flag]').on('change',function(){
      
          $.ajax({
                            method:"POST",
                            url:HOST+'suppliers/setBankSwitchMode/',
                            data:{supplier_id:$(this).attr('data-supplier'),mode:$(this).val()},
                            dataType:"json",
                            success:function(data){
                            if(data.message=="success")
                            {
                                  $('div#div_success').show().delay(1000).fadeOut();;
                            }
                            else
                            {
                                   $('div#div_error').hide().delay(1000).fadeOut();;   
                            }
                                                                    }
               });
      
    })
});