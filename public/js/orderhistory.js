$(document).ready(function(){
    
     $('#sandbox-container .input-daterange').datepicker({
        format: "yyyy-mm-dd",
        startDate: "-365d",
        endDate: "1d",
        todayHighlight: true,
         orientation: "top right"
        });

    $('#handlexceptionalcase').on('show.bs.modal', function(e) {
        
         var supplierid = $(e.relatedTarget).data('sid');
         var oprid = $(e.relatedTarget).data('oprid');
         var soid = $(e.relatedTarget).data('soid');
         
         var pending=$.getJSON(HOST+'orderhistorys/getPendingBySoid',{soid:soid});
         
         pending.done(function(res){
             
                if(res.status && res.type)
              {
                      $(e.currentTarget).find('td#tdtxtpending').text(res.data.pending);
                      $(e.currentTarget).find('td#tdmargin').text(res.data.commission_type_formula);
                      var pendingamt=res.data.pending*((100-res.data.commission_type_formula)/100);
                      $(e.currentTarget).find('td#tdpendingamt').text(pendingamt);
                      $(e.currentTarget).find('td#tdoprname').text(res.data.oprname);
                      $(e.currentTarget).find('h5#hremainingpending span').text(res.data.pending);
                      $(e.currentTarget).find('h5#hremainingpendingamt span').text(pendingamt);
                      
                      
                      $(e.currentTarget).find('input#parentpendingid').val(res.data.id);
                      $(e.currentTarget).find('input#parentsupplier_id').val(res.data.supplier_id);
                      $(e.currentTarget).find('input#parentoperator_id').val(res.data.operator_id);
                      $(e.currentTarget).find('input#parentmargin').val(res.data.commission_type_formula);
                      $(e.currentTarget).find('input#parentsoid').val(res.data.soid);
                      $(e.currentTarget).find('input#parentogpendingamt').val(res.data.pending);
                      $(e.currentTarget).find('input#parentpendingdate').val(res.data.pending_date);
                    
                      
              }
              else
              {
                  alert('Error');
              }
         });
         
         
           var remainingpendings=$.getJSON(HOST+'orderhistorys/getpendingbysupplierid',{supplierid:supplierid,operatorid:oprid});
           
           
           remainingpendings.done(function(res){
               
                  if(res.status && res.type)
              {
                 $(e.currentTarget).find('table#tblotherpendings tbody').empty();
                    
                  $.each(res.data,function(k,v){
                      
                      var row="";
                      var actualamt=v.pending-((v.pending*v.commission_type_formula)/(100));
                      row+="<tr id='tr_"+v.id+"'>";
                      row+="<td>"+v.name+"</td>";
                      row+="<td>"+v.commission_type_formula+"</td>";
                      row+="<td data-pending='"+v.pending+"'>"+v.pending+"</td>";
                      row+="<td data-actualamt='"+actualamt+"'>"+actualamt+"</td>";
                      row+="<td><input type='text'  style='width: 135px;' class='form-control input-md input-sm' name=txtadjustpending[]  data-margin='"+v.commission_type_formula+"'  data-id='"+v.id+"'   data-soid='"+v.supplier_operator_id+"'  data-sid='"+v.supplier_id+"'  data-oprid='"+v.operator_id+"'  data-pendingdate='"+v.pending_date+"'  /></td>";
                      row+="</tr>";
                      
                      $(e.currentTarget).find('table#tblotherpendings').append(row);
                  });
                  
                   var row="";
                  row="<tr><td></td><td></td><td></td><th>Total : </th><td id='tdentersum'>0.00</td></tr>";
                  $(e.currentTarget).find('table#tblotherpendings').append(row);
              }
              else
              {
                  alert('No Target Pendings Detected');
              }
              
           });

      
         
         
    });
    
    
  $(document).on('keyup','input[name="txtadjustpending[]"]',function(e){
      
        if(e.keyCode == 13)
        {
           if(!$.isNumeric($(this).val())){ alert('Error'); return;}
            
          var textboxvalue=parseFloat($(this).val());
           var pendingid=$(this).attr('data-id');
          var parentmargin=parseFloat($('td#tdmargin').text());
          var parenttopay=parseFloat($('td#tdpendingamt').text());
          var childmargin=parseFloat($(this).attr('data-margin'));
          var totaltextboxvalue=0.00;
          
         
           // get sum of all textbox
            $('input[name="txtadjustpending[]"]').each(function(){
                
                    var temp=0;
                    if($(this).val()!="" &&  !isNaN($(this).val()) && $.isNumeric($(this).val()))
                    {temp=$(this).val()}
                    else
                    {temp=0};
              totaltextboxvalue+=parseFloat(temp);
              
              // Check individual topay
                    //var ev = $.Event("keypress");
                    if(parenttopay>0){
                        if(temp>parenttopay || temp<0 ){alert('Value should be between 0-'+parenttopay);$(this).val('0'); ev.keyCode = 13;$(this).trigger(ev);return;}
                     }
                    else{ 
                        if(temp<parenttopay || temp>0   ){alert('Value should be between '+parenttopay+' -0 ');$(this).val('0'); ev.keyCode = 13;$(this).trigger(ev);  return;}
                    }
          });
          
          // condition to check if total sum i not greater than orginal topay
          if(parenttopay>0){
          if(totaltextboxvalue>parenttopay){alert('Value cannot exceed '+parenttopay);return;}
         }else
         {
              if(totaltextboxvalue<parenttopay || totaltextboxvalue>0   ){alert('Value cannot deceed '+parenttopay);return;}
         }
         
           //reset td's to affected
            $(this).parent().prev('td').text('');
            $(this).parent().prev().prev('td').text('');
           
           
           // Adjust Child pending amt by formula oldchildpending+x*((100-parentmargin)/100)
          // var actualamt=textboxvalue*((100-parentmargin)/100);
           var oldactualamt=parseFloat($(this).parent().prev('td').attr('data-actualamt'));
           $(this).parent().prev('td').text(oldactualamt+textboxvalue);
           
           
           // Adjust pending by formula oldvalue+(actualamt/(100-childmargin)/100)
          var actualpending= textboxvalue/((100-childmargin)/100);
          var oldpending=parseFloat($(this).parent().prev().prev('td').attr('data-pending'));
          $(this).parent().prev().prev('td').text((oldpending+actualpending).toFixed(2));
          
          //Change remaining pending h tag
          var og=parseFloat($('td#tdtxtpending').text());
          var totalccvalue=parseFloat(totaltextboxvalue/((100-parentmargin)/100))
          $('h5#hremainingpending span').text((og-totalccvalue).toFixed(2));
         
         // add totalenter value
         $('td#tdentersum').text(totaltextboxvalue.toFixed(3));
         
         // Change remaining pending amt tag
          $('h5#hremainingpendingamt span').text((parenttopay-totaltextboxvalue).toFixed(2));
        }
      
    
  });
    
    
    $('button#btnadjustpendings').on('click',function(){

            var r=confirm('Are you sure ? ');
            
            if(r===true)
            {
                var targetpending=[];
                var textboxsum=0;
                
                 $('input[name="txtadjustpending[]"]').each(function(){
                   
                            if($(this).val()!="" && $.isNumeric($(this).val())){  

                                  var temp={
                                                 id:$(this).attr('data-id'),
                                                 textboxsum:$(this).val(),
                                                 pending:$(this).parent().prev().prev('td').text(),
                                                 pendingamt:$(this).parent().prev('td').text(),
                                                 soid:$(this).attr('data-soid'),
                                                 supplier_id:$(this).attr('data-sid'),
                                                 operator_id:$(this).attr('data-oprid'),
                                                 margin:$(this).attr('data-margin'),    
                                                 pending_date:$(this).attr('data-pendingdate')    
                                             };

                             targetpending.push(temp);
                             textboxsum+=parseFloat($(this).val());
                          }
                
                 });
                 
               
            var parentpending=[];
            
                parentpending.push({
                                                        id:$('input#parentpendingid').val(),
                                                        pending:$('h5#hremainingpending span').text(),
                                                        pendingamt:$('h5#hremainingpendingamt span').text(),
                                                        textboxsum:textboxsum,
                                                        supplier_id:$('input#parentsupplier_id').val(),
                                                        operator_id:$('input#parentoperator_id').val(),        
                                                        soid:$('input#parentsoid').val(),
                                                        margin:$('input#parentmargin').val(),
                                                        parentogpending:$('input#parentogpendingamt').val(),
                                                        pending_date:$('input#parentpendingdate').val()
                                                        });
              
            
             var dataString={parentpendings:parentpending,targetpendings:targetpending};
             
             var adjustPendings=$.post(HOST+'pendings/adjustPending',{data:JSON.stringify(dataString)});
             
             adjustPendings.done(function(res){
                 
                res=$.parseJSON(res);
                
                 if(res.status && res.type)
                    {
                        alert(res.message);
                        //location.reload();
                    }
              else
                    {
                        alert("Error");
                    }
                 
             });
             
             
            }
            
    });
    
    
});
