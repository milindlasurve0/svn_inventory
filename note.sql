More sophisticted fix fix Investment query

Select id,if(a.devices_incoming is NULL,0,a.devices_incoming) as devices_incoming ,if(b.pending_incoming is null,0,b.pending_incoming) as pending_incoming,CEIL(  IF( so.commission_type =1, (
                    a.devices_incoming
                    ) * ( ( 100 - so.commission_type_formula ) /100 ) , (
                    a.devices_incoming 
                    ) * ( 100 / ( 100 + so.commission_type_formula ) ) )  ) AS investment , (if(a.devices_incoming is NULL,0,a.devices_incoming) -if(b.pending_incoming is null,0,b.pending_incoming)) as diffincoming
from inv_supplier_operator so
LEFT JOIN 
(
        Select supplier_operator_id,SUM(tfr) as devices_incoming
        from devices_data 
        where sync_date='2016-09-13'
        group by supplier_operator_id
) as a ON 
        so.id=a.supplier_operator_id
JOIN
(
        Select supplier_operator_id,SUM(incoming) as pending_incoming 
        from inv_pendings 
        where pending_date='2016-09-13'
        group by supplier_operator_id
) as b ON 
        so.id=b.supplier_operator_id
where 
a.devices_incoming > 0 OR b.pending_incoming > 0 







ALTER TABLE `inv_pendings` ADD `refund` DECIMAL( 10, 2 ) NOT NULL AFTER `incoming` 
ALTER TABLE `inv_pendings` ADD `comment` TEXT NOT NULL AFTER `refund` 


Sample data for update pending if incoming edited


INSERT INTO `inv_orders` (`id`, `supplier_operator_id`, `margin`, `amount`, `to_pay`, `amount_by_bounds`, `paid`, `bank_id`, `created_by`, `is_approved`, `is_downloaded`, `is_payment_done`, `txnid`, `checked_by`, `payed_by`, `supplier_id`, `operator_id`, `manual_order_flag`, `no_of_days`, `batch`, `order_date`) VALUES
(11, 13, 4.75, 100000.00, 90000.00, 100000.00, 90000.00, 1, 1, '1', '1', '1', 'asdfghjkl', 1, 1, 7, 9, '1', 2.00, 1, '2015-07-16'),
(12, 13, 4.75, 200000.00, 190000.00, 200000.00, 190000.00, 1, 1, '1', '1', '1', 'asdfghjkl', 1, 1, 7, 9, '1', 2.00, 1, '2015-07-17'),
(13, 13, 4.75, 100000.00, 90000.00, 100000.00, 90000.00, 1, 1, '1', '1', '1', 'asdfghjkl', 1, 1, 7, 9, '1', 2.00, 1, '2015-07-18'),
(14, 13, 4.75, 300000.00, 290000.00, 300000.00, 290000.00, 1, 1, '1', '1', '1', 'asdfghjkl', 1, 1, 7, 9, '1', 2.00, 1, '2015-07-19');



INSERT INTO `inv_pendings` (`id`, `supplier_operator_id`, `order_id`, `pending`, `incoming`, `pending_date`) VALUES
(1, 13, 10, 0.00, 0.00, '2015-07-15'),
(2, 13, 11, 0.00, 100000.00, '2015-07-16'),
(3, 13, 12, -50000.00, 150000.00, '2015-07-17'),
(4, 13, 13, -50000.00, 100000.00, '2015-07-18'),
(5, 13, 14, -100000.00, 250000.00, '2015-07-19');



ALTER TABLE `inv_pendings` ADD `operator_id` INT NOT NULL AFTER `supplier_operator_id` 
update inv_pendings set operator_id=9

ALTER TABLE `inv_orders` CHANGE `bank_id` `supplier_bank_id` INT( 11 ) NOT NULL 


Exception Changed Step;

ALTER TABLE `inv_orders` ADD `is_adjusted` ENUM( '1', '0' ) NOT NULL DEFAULT '0' AFTER `manual_order_flag` 
ALTER TABLE `inv_orders` ADD `comment` TEXT NOT NULL AFTER `is_adjusted` 
ALTER TABLE `inv_orders` DROP `paid`

Svn stat -u
svn up and copy

Add that update incoming line of devices data even if 


ALTER TABLE `inv_supplier_operator` ADD `disputeamt` DECIMAL( 10, 2 ) NOT NULL AFTER `is_active` 


ALTER TABLE `inv_comments` ADD `supplier_operator_id` INT NOT NULL AFTER `user_id` 
ALTER TABLE `inv_comments` CHANGE `comment_timestamp` `comment_timestamp` DATETIME NOT NULL 

ALTER TABLE `inv_supplier_operator` CHANGE `frequency` `frequency` TINYINT NOT NULL DEFAULT '0' COMMENT '''0''=>''Not Defined'',''1''=>''Daily'',''2''=>''Weekly'',''3''=>''Monthly'',''4''=>''[Tue,Thu,Sat]'',''5''=>''[Mon,Wed,Fri]'',''6''=>''M-F'''


ALTER TABLE `inv_suppliers` ADD `single_payment` ENUM( '1', '0' ) NOT NULL DEFAULT '0' AFTER `handled_by` 


ALTER TABLE `inv_orders` CHANGE `regenerated_amount` `regenerated_amount` DECIMAL( 12, 2 ) NOT NULL DEFAULT '0'

SELECT so.id AS supplier_operator_id, s.name, p.name, GROUP_CONCAT( IF( pending <0, 1, 0 ) ) AS commaseperated, GROUP_CONCAT( IF( pending <0, 1, 0 )
SEPARATOR '' ) AS factorstring, LOCATE( '0', GROUP_CONCAT( IF( pending <0, 1, 0 )
SEPARATOR '' ) ) AS factor
FROM inv_suppliers s
JOIN inv_supplier_operator so ON s.id = so.supplier_id
JOIN products p ON p.id = so.operator_id
JOIN inv_pendings pe ON pe.supplier_operator_id = so.id
WHERE pending_date > '2015-10-25'
AND pending_date <= '2015-10-30'
GROUP BY 1
HAVING factorstring != "00000"
ORDER BY pending_date ASC

ALTER TABLE `inv_supplier_operator` ADD `max_sale_capacity` DECIMAL( 12, 2 ) NOT NULL AFTER `capacity_per_month`  


ALTER TABLE `inv_set_target_history` ADD `baseweight` DECIMAL( 6, 4 ) NOT NULL AFTER `pending` ,
ADD `targetweight` DECIMAL( 6, 4 ) NOT NULL AFTER `baseweight` ,
ADD `actualweight` DECIMAL( 6, 4 ) NOT NULL AFTER `targetweight` 



Alert System

Modem |  operator | count 


Select company,operatorname,vendor_id,product_id,COUNT(id) as totaltransaction,SUM(is_benchmark_exceeded) as benchmark_exceeded_count
FROM (
SELECT v.company,p.name as operatorname,va.id, va.vendor_id, va.product_id, va.ref_code, va.shop_transaction_id, va.timestamp AS start_time, vt.processing_time, TIMESTAMPDIFF(
SECOND , va.timestamp, vt.processing_time ) AS diff,benchmark_processtime,if( TIMESTAMPDIFF(
SECOND , va.timestamp, vt.processing_time ) > benchmark_processtime,1,0) as is_benchmark_exceeded
FROM vendors_activations va
JOIN vendors_transactions vt ON ( va.vendor_id = vt.vendor_id
AND va.ref_code = vt.ref_id
AND va.date = '2015-12-21' )
JOIN vendors v ON va.vendor_id = v.id
JOIN products p ON va.product_id=p.id
WHERE 1
AND v.update_flag != '0'
AND v.machine_id != '0'
AND va.timestamp >= '2015-12-21 18:00:00'
AND va.timestamp <= '2015-12-21 18:00:05'
AND vt.status IN ('0','1')
) as a
group by vendor_id,product_id



Cross check margin in cc and inv

 select s.name as suppliername,p.name as operator,v.company,so.commission_type_formula as invmargin,discount_commission as margin,v.id as vendorid 
 from inv_suppliers s 
JOIN inv_supplier_vendor_mapping svm
ON s.id=svm.supplier_id 
JOIN vendors v 
ON svm.vendor_id=v.id 
 JOIN inv_supplier_operator so 
ON s.id=so.supplier_id
 JOIN products p
 ON so.operator_id=p.id 
LEFT JOIN vendors_commissions vc 
ON (v.id=vc.vendor_id AND p.id=vc.product_id)  
 WHere s.is_api='1'  AND so.commission_type_formula <> discount_commission ;

              $sql="Select o.id,SUM(o.to_pay) as Amount ,o.supplier_bank_id,o.supplier_id,GROUP_CONCAT(pa.id) as paymentid  "
                     . "from inv_orders o  JOIN inv_payments pa ON  o.id=pa.order_id  "
                     . "where o.id IN ({$ids}) "
                     . "and is_payment_done IN ('0','2') and pa.status='0' "
                     . " group by supplier_bank_id ";


http://inventory.pay1.com/accounts/downloadexcel/?orderids=24158,24164,24159,24161,24162,24165,24163


UTR change accounts.php,acccount.php,updatetxnid.php

select * from 

(SELECT supplier_operator_id, SUM( sale ) AS devices_sale, GROUP_CONCAT( DISTINCT (
vendor_id
) ) AS vendorids
FROM devices_data
WHERE sync_date = '2016-03-02'
GROUP BY supplier_operator_id) as a

LEFT JOIN 

(SELECT supplier_operator_id, SUM( currentsale ) AS sale, GROUP_CONCAT( vendor_id ) AS target_vids
FROM inv_set_target_history
WHERE history_date = '2016-03-02'
GROUP BY supplier_operator_id ) as b

ON a.supplier_operator_id=b.supplier_operator_id
where a.devices_sale <> b.sale 	



-------------------------------------------------------------
select *,(a.devices_sale-b.set_target_sale) as diff 
from ( SELECT supplier_operator_id, SUM( sale ) AS devices_sale,vendor_id,opr_id,operator
 FROM devices_data
 WHERE sync_date = '2016-03-25' 
GROUP BY supplier_operator_id,vendor_id ) 
as a JOIN
 (SELECT supplier_operator_id, SUM( currentsale ) AS set_target_sale,vendor_id 
FROM inv_set_target_history 
WHERE history_date = '2016-03-25' 
GROUP BY supplier_operator_id,vendor_id ) as b 
ON (a.supplier_operator_id=b.supplier_operator_id AND a.vendor_id=b.vendor_id) 
where a.devices_sale <> b.set_target_sale 
order by a.vendor_id 

Updated 'public/js/backendashboard.js' to r1273.
  Updated 'public/css/style.css' to r1273.
  Updated 'application/views/dashboard/backenddashboardcontainer.php' to r1273.
  Updated 'application/models/planning.php' to r1273.
  Updated 'application/controllers/dashboard.php' to r1273.	


select *,(a.devices_sale-b.set_target_sale) as diff 
from ( SELECT supplier_operator_id, SUM( sale ) AS devices_sale,vendor_id,opr_id,operator 
            FROM devices_data 
            WHERE sync_date = '2016-04-03' 
            GROUP BY supplier_operator_id,vendor_id ) as a 
JOIN (SELECT supplier_operator_id, SUM( currentsale ) AS set_target_sale,vendor_id 
           FROM inv_set_target_history 
           WHERE history_date = '2016-04-03' 
          GROUP BY supplier_operator_id,vendor_id ) as b 
ON (a.supplier_operator_id=b.supplier_operator_id AND a.vendor_id=b.vendor_id) 
where a.devices_sale <> b.set_target_sale 
order by a.vendor_id 


refund report
order cancel so adjust pending
Adjust APi incoming in earning report


SELECT count( order_id ) AS orderids, GROUP_CONCAT( order_id )
FROM `inv_payments`
WHERE `payment_date` = '2016-04-02'
GROUP BY order_id
HAVING orderids >1

update inv_pendings set pending=pending-160000 where supplier_operator_id=433 and pending_date >= '2016-04-02'


select s.name,pending_date,SUM(incoming) as inc
from inv_pendings p
JOIN inv_supplier_operator so
ON p.supplier_operator_id=so.id AND so.is_api='1'
JOIN inv_suppliers s 
ON so.supplier_id=s.id
Where p.pending_date>='2015-09-28'  AND p.pending_date<='2016-04-11'
group by p.supplier_operator_id,p.pending_date
having inc>0

SELECT pe.pending_date AS date, s.name, SUM( to_pay ) AS topay, SUM( incoming ) AS incoming, CEIL( SUM( IF( so.commission_type =1, pe.incoming * ( ( 100 - pe.margin ) /100 ) , pe.incoming * ( 100 / ( 100 + pe.margin ) ) ) ) ) AS invested
FROM inv_pendings pe
LEFT JOIN inv_orders o ON ( pe.order_id = o.id
AND o.is_payment_done = '1' )
JOIN inv_supplier_operator so ON ( so.id = pe.supplier_operator_id )
JOIN inv_suppliers s ON s.id = so.supplier_id
WHERE pe.pending_date >= '2015-09-28'
AND pe.pending_date <= '2016-03-31'
GROUP BY so.id, pe.pending_date
HAVING invested >0


ALTER TABLE `inv_pendings` ADD `refund_type` ENUM( '0', '1', '2' ) NOT NULL DEFAULT '0' COMMENT '1=>bank 2=>Adjustments' AFTER `refund` 

Select pending_date,p.margin,p.supplier_operator_id,pending,SUM( IF( so.commission_type =1, (
pending) * ( ( 100 - p.margin ) /100 ) , (pending) * ( 100 / ( 100 + p.margin ) ) ) )  as pendingamt ,if(to_pay is null,0,to_pay) as to_pay,if(refund_type=1,refund,0) as inbank,if(refund_type=2,refund,0) as adjustments,SUM( if( tfr IS NULL , 0, tfr ) ) AS Incoming,CEIL( SUM( IF( so.commission_type =1, (
tfr) * ( ( 100 - p.margin ) /100 ) , (tfr) * ( 100 / ( 100 + p.margin ) ) ) ) ) as IncomingInvested
from inv_pendings p
JOIN inv_supplier_operator so
ON p.supplier_operator_id=so.id 
LEFT JOIN inv_orders  o
ON (so.id=o.supplier_operator_id AND o.order_date='2016-04-13' AND o.is_payment_done='1')
LEFT JOIN devices_data dd
ON (dd.supplier_operator_id=so.id AND dd.sync_date='2016-04-13')
where p.pending_date='2016-04-13'
GROUP by supplier_operator_id

CEIL( SUM( IF( so.commission_type =1, (
tfr) * ( ( 100 - p.margin ) /100 ) , (tfr) * ( 100 / ( 100 + p.margin ) ) ) ) ) as IncomingInvested




---A+B
SELECT a.*,if(b.incoming is null,0,b.incoming) as incoming,if(b.incominginvested is null,0,b.incominginvested) as incominginvested  FROM

(Select p.pending_date,p.margin,p.supplier_operator_id,p.pending,
CEIL(SUM( IF( so.commission_type =1, (p.pending) * ( ( 100 - p.margin ) /100 ) , (p.pending) * ( 100 / ( 100 + p.margin ) ) ) ) )  as pendingclosing,pprev.pending as previouspending,
CEIL(SUM( IF( so.commission_type =1, (pprev.pending) * ( ( 100 - pprev.margin ) /100 ) , (pprev.pending) * ( 100 / ( 100 +pprev.margin ) ) ) ) )  as pendingopening,
if(to_pay is null,0,to_pay) as to_pay,if(p.refund_type=1,p.refund,0) as inbank,
if(p.refund_type=2,p.refund,0) as adjustments
from inv_supplier_operator so
LEFT JOIN  inv_pendings p 
ON (p.supplier_operator_id=so.id AND  p.pending_date='2016-04-13')
LEFT JOIN inv_orders  o
ON (so.id=o.supplier_operator_id AND o.order_date='2016-04-13' AND o.is_payment_done='1')
LEFT JOIN inv_pendings pprev 
ON (pprev.supplier_operator_id=so.id AND  pprev.pending_date='2016-04-12')
GROUP by supplier_operator_id  ) as a

LEFT JOIN 

(
select so.id as supplier_operator_id,SUM(tfr) as incoming,CEIL( SUM( IF( so.commission_type =1, (
tfr) * ( ( 100 - pe.margin ) /100 ) , (tfr) * ( 100 / ( 100 + pe.margin ) ) ) ) ) as IncomingInvested 
from inv_supplier_operator so
JOIN devices_data dd
ON (so.id=dd.supplier_operator_id)
JOIN inv_pendings pe
ON (pe.supplier_operator_id=so.id AND pe.pending_date='2016-04-13')
Where dd.sync_date='2016-04-13'
group by so.id
having incoming > 0
)
as b
ON a.supplier_operator_id=b.supplier_operator_id


--A+b little bit modified

SELECT SUM(todaypending) as currentdaypending,SUM(pendingclosing) as pendingclosing,SUM(previouspending) as previouspending,SUM(pendingopening) as pendingopening,SUM(to_pay) as to_pay,SUM(adjustments) as adjustments,SUM(incoming) as incoming,SUM(if(b.incoming is null,0,b.incoming)) as incoming,SUM(if(b.incominginvested is null,0,b.incominginvested)) as incominginvested  FROM

(Select p.pending_date,p.margin,p.supplier_operator_id,p.pending as todaypending,
CEIL(SUM( IF( so.commission_type =1, (p.pending) * ( ( 100 - p.margin ) /100 ) , (p.pending) * ( 100 / ( 100 + p.margin ) ) ) ) )  as pendingclosing,pprev.pending as previouspending,
CEIL(SUM( IF( so.commission_type =1, (pprev.pending) * ( ( 100 - pprev.margin ) /100 ) , (pprev.pending) * ( 100 / ( 100 +pprev.margin ) ) ) ) )  as pendingopening,
if(to_pay is null,0,to_pay) as to_pay,if(p.refund_type=1,p.refund,0) as inbank,
if(p.refund_type=2,p.refund,0) as adjustments
from inv_supplier_operator so
LEFT JOIN  inv_pendings p 
ON (p.supplier_operator_id=so.id AND  p.pending_date='2016-04-13')
LEFT JOIN inv_orders  o
ON (so.id=o.supplier_operator_id AND o.order_date='2016-04-13' AND o.is_payment_done='1')
LEFT JOIN inv_pendings pprev 
ON (pprev.supplier_operator_id=so.id AND  pprev.pending_date='2016-04-12')
GROUP by supplier_operator_id  ) as a

LEFT JOIN 

(
select so.id as supplier_operator_id,SUM(tfr) as incoming,CEIL( SUM( IF( so.commission_type =1, (
tfr) * ( ( 100 - pe.margin ) /100 ) , (tfr) * ( 100 / ( 100 + pe.margin ) ) ) ) ) as IncomingInvested 
from inv_supplier_operator so
JOIN devices_data dd
ON (so.id=dd.supplier_operator_id)
JOIN inv_pendings pe
ON (pe.supplier_operator_id=so.id AND pe.pending_date='2016-04-13')
Where dd.sync_date='2016-04-13'
group by so.id
having incoming > 0
)
as b
ON a.supplier_operator_id=b.supplier_operator_id


ALTER TABLE  `devices_data` ADD  `recharge_method` ENUM(  '1',  '0',  '2',  '3',  '4' ) NOT NULL DEFAULT  '0' COMMENT  '0=>default 1=>App 2=>SMS 3=>Ussd 4=>NA' AFTER  `last`

SELECT order_date, COUNT( id ) AS tt, GROUP_CONCAT( id ) 
FROM inv_orders
GROUP BY supplier_operator_id, order_date
HAVING tt >1


SELECT s.name, pa.amount_by_bounds AS todaysorder, pa.to_pay, pa.utr AS txnid, p.name AS operator, s.email_id AS email, account_no
FROM inv_payments pa
JOIN inv_supplier_operator so ON pa.so_id = so.id
JOIN inv_suppliers s ON so.supplier_id = s.id
JOIN products p ON p.id = so.operator_id
JOIN inv_orders o ON pa.order_id = o.id
JOIN inv_supplier_banks sb ON sb.id = o.supplier_bank_id
WHERE pa.id = '21950'

