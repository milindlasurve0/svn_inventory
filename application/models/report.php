<?php

class Report extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getAvgSaleByOperatorId($operator_id)
    {
         $date=  date('Y-m-d',  strtotime('-1 day'));
        $past7day=date('Y-m-d',  strtotime("-8 days"));
        
        $sql="Select SUM(sale)/7 AS avgsale from devices_data where opr_id='{$operator_id}' AND sync_date>='{$past7day}'  AND sync_date<='{$date}' ";
        
          $query=$this->slaveDB->query($sql);        
          
          if($query->num_rows()):
                    $data=$query->row_array();
                    return $data['avgsale'];
          endif;
    }
    
    public function getTotalBaseSaleByOperatorid($operator_id)
    {
        $sql="SELECT SUM( base_amount ) AS sumbasesale "
                . " FROM inv_planning_sheet ps "
                . " JOIN inv_supplier_operator so "
                . " ON ps.supplier_operator_id = so.id "
                . " AND so.operator_id = '{$operator_id}' ";
          
                
       $query=$this->slaveDB->query($sql);      
       
         if($query->num_rows()):
                    $data=$query->row_array();
                    return $data['sumbasesale'];
          endif;
          
          return 0;
    }
    
    public function getPendingReport()
    {
        $date=  date('Y-m-d',  strtotime(' -1 day'));
        $past5day=date('Y-m-d',  strtotime(" -6 days"));
       
        
         $sql="SELECT so.id AS supplier_operator_id, s.name as supplier, p.name  as operator, GROUP_CONCAT( IF( pe.pending <0, 1, 0 ) ) AS commaseperated, GROUP_CONCAT( IF( pe.pending <0, 1, 0 ) SEPARATOR '' ) AS factorstring, LOCATE( '0', GROUP_CONCAT( IF( pe.pending <0, 1, 0 ) SEPARATOR '' ) ) AS factor,pee.pending as currentpending "
                . " FROM inv_suppliers s "
                . " JOIN inv_supplier_operator so "
                . " ON s.id = so.supplier_id "
                . " JOIN products p "
                . " ON p.id = so.operator_id "
                . " JOIN inv_pendings pe "
                . " ON pe.supplier_operator_id = so.id  JOIN inv_pendings pee  ON (pee.supplier_operator_id=so.id AND pee.pending_date='{$date}') "
                . " WHERE pe.pending_date >= '{$past5day}' "
                . " AND pe.pending_date <= '{$date}' "
                . " GROUP BY 1 "
                . " ORDER BY pee.pending Asc";
                
           $query=$this->slaveDB->query($sql);               
           
            if($query->num_rows()):
                    
                return $query->result_array();
            
            endif;
            
            return false;
    }
    
    function getPendingHistory($from,$to)
    {
        $sql="Select ph.*,s.name as supplier, p.name  as operator,ph.pending as currentpending,ph.history_date  "
                . " from inv_pending_history ph "
                . " JOIN inv_supplier_operator so "
                . " ON ph.supplier_operator_id=so.id "
                . " JOIN inv_suppliers s "
                . " ON so.supplier_id=s.id "
                . " JOIN products p "
                . " ON so.operator_id=p.id WHERE ph.history_date>='{$from}' AND ph.history_date<='{$to}' ORDER BY ph.history_date ASC ";

        $query=$this->slaveDB->query($sql);               
           
            if($query->num_rows()):
                    
                return $query->result_array();
            
            endif;
            
            return false;         
    }
    
    public function getDisputeAmt()
    {
        $sql="SELECT s.name AS supplier, p.name AS operator, so.id AS supplier_operator_id, so.disputeamt "
                . " FROM inv_supplier_operator so "
                . " JOIN inv_suppliers s "
                . " ON so.supplier_id = s.id "
                . " JOIN products p "
                . " ON so.operator_id = p.id "
                . " WHERE so.disputeamt <>0 ORDER BY so.disputeamt DESC ";
        
           $query=$this->slaveDB->query($sql);               
           
            if($query->num_rows()):
                    
                return $query->result_array();
            
            endif;
            
            return false; 
    }
    
    public function setPendingReportToDb($data)
    {
        $insert_arr=array();
        
        foreach($data as $row):
                $insert_arr[]=array(
                                                    'supplier_operator_id'=>$row['supplier_operator_id'],
                                                    'longterm'=>$row['longterm'],
                                                    'shorterm'=>$row['shorterm'],
                                                    'goodbooks'=>$row['goodbooks'],
                                                    'pending'=>$row['currentpending'],
                                                    'disputeamt'=>$row['disputeamt'],
                                                    'history_date'=>date('Y-m-d')
                                                   );
        endforeach;
        
        $insert_arr=  createBatch($insert_arr,250);
        
        foreach ($insert_arr as $batch):
                 $this->db->insert_batch('inv_pending_history', $batch);
                if($errormsg=$this->db->_error_message()):
                                         $sql = $this->db->last_query();
                                         logerror("Query Failed :  {$this->db->_error_message()} ","pendinghistory");
                                         logerror("Sql query : {$sql}","pendinghistory");
                                         send_email('vibhas@mindsarray.com','Store Pending History',"Mysql Query for Pending cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
                 endif;
        endforeach;
       
       
      }
}