<?php

class Orderapproval extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getOrderByOperatorId($operator_id)
    {
        $date=date('Y-m-d');
       
        
           $sql="select s.id as supplierid,s.name,p.name as operator_name, ps.base_amount,o.*,s.parent_id,GROUP_CONCAT( v.company) as vendornames,so.commission_type "
                 . " from inv_orders o "
                 . " JOIN inv_planning_sheet ps "
                 . " ON o.supplier_operator_id = ps.supplier_operator_id "
                 . " LEFT JOIN inv_supplier_operator so "
                 . " ON so.id=o.supplier_operator_id  "
                 . " LEFT JOIN products p "
                 . " ON o.operator_id=p.id  "
                 . " LEFT JOIN inv_suppliers s  "
                 . " ON s.id = o.supplier_id  "
                 . " JOIN inv_supplier_vendor_mapping svm "
                 . " ON svm.supplier_id = o.supplier_id "
                 . " JOIN vendors v "
                 . " ON svm.vendor_id = v.id "
                 . " where o.operator_id='{$operator_id}' "
                 . " and o.order_date='{$date}' "
                 . " group by o.supplier_operator_id ";
        
         $query=$this->slaveDB->query($sql);
        
           if($query->num_rows()):
               
               return $query->result();
           
           endif;
           
           return false;
           
    }
    
    function getClosingByOperatorID($operator_id)
    {
         $dateusedinsimdata=date('Y-m-d',strtotime('-1 days'));
         $sql="Select supplier_operator_id,SUM(if(block=1,0,closing)) AS opening "
                 . " from devices_data "
                 . " where sync_date='{$dateusedinsimdata}' "
                 . " and opr_id='{$operator_id}' "
                 . " group by supplier_operator_id ";
                 
         $query=$this->slaveDB->query($sql);   
         
         if($query->num_rows()):
               
                $tobeReturned=array();
         
                $data= $query->result();
               
                foreach($data as $value):
                    $tobeReturned[$value->supplier_operator_id]=$value->opening;
                endforeach;
           
                return $tobeReturned;
                
           endif;
           
           return false;
                 
    }
    
    function getClosingBySupplierID($supplier_id)
    {
         $dateusedinsimdata=date('Y-m-d',strtotime('-1 days'));
          $childstr=$this->getPaymentChildByParentid($supplier_id);
          
         $sql="Select supplier_operator_id,SUM(if(block=1,0,closing)) AS opening "
                 . " from devices_data "
                 . " where sync_date='{$dateusedinsimdata}' "
                 . " and inv_supplier_id IN ({$supplier_id},{$childstr}) "
                 . " group by supplier_operator_id ";
                 
         $query=$this->slaveDB->query($sql);   
         
         if($query->num_rows()):
               
                $tobeReturned=array();
         
                $data= $query->result();
               
                foreach($data as $value):
                    $tobeReturned[$value->supplier_operator_id]=$value->opening;
                endforeach;
           
                return $tobeReturned;
                
           endif;
           
           return false;
                 
    }
    
    function getOrderBySupplierId($supplier_id) 
    {
        $date=date('Y-m-d');
        $dateusedinsimdata=date('Y-m-d',strtotime('-1 days'));
        
        //Get all child suppliers of given suppliers
        $childstr=$this->getPaymentChildByParentid($supplier_id);
        
         $sql="select s.name,p.name as operator_name, ps.base_amount,SUM( sd.closing ) AS opening,o.*,s.parent_id,so.commission_type  "
                 . " from inv_orders o "
                 . " JOIN inv_planning_sheet ps "
                 . " ON o.supplier_operator_id = ps.supplier_operator_id  "
                 . " LEFT JOIN inv_supplier_operator so "
                 . " ON so.id=o.supplier_operator_id  "
                 . " LEFT JOIN products p "
                 . " ON o.operator_id=p.id  "
                 . " LEFT JOIN inv_suppliers s  "
                 . " ON s.id = o.supplier_id"
                 . "  LEFT JOIN devices_data sd  "
                 . " ON o.supplier_operator_id=sd.supplier_operator_id "
                 . " where o.supplier_id IN ({$supplier_id},{$childstr}) "
                 . " and o.order_date='{$date}' and sd.sync_date='{$dateusedinsimdata}' "
                 . " group by o.supplier_operator_id order by field(o.supplier_id,{$supplier_id}) desc ";
        
         $query=$this->slaveDB->query($sql);
        
           if($query->num_rows()):
               
               return $query->result();
           
           endif;
           
           return false;
    }
    
    function getdefaultBankid($supplier_id)
    {
        $sql="Select id from inv_supplier_banks where supplier_id='{$supplier_id}' and  default_bank='1' ";
        
         $query=$this->slaveDB->query($sql);
        
           if($query->num_rows()):
               
               return $query->row();
           
           endif;
           
           return false;
    }
    
    function updateorderamt($data)
    {
       $sql="Select margin from inv_orders where id='{$data['order_id']}' ";
       $query=$this->db->query($sql);
       $result= $query->row();
       
       
        $to_pay=isUpperBoundH($data['so_id'])?($data['order_amount']):floor(($data['order_amount']-($data['order_amount']*($result->margin/100))));
        $order_amount_by_bounds=  getHOrderAmountByBounds($data['order_amount'],$data['so_id']);
        
       if($this->db->update('inv_orders',array('amount'=>$data['order_amount'],'to_pay'=>$to_pay,'manual_order_flag'=>'1','amount_by_bounds'=>$order_amount_by_bounds),array('id'=>$data['order_id'])))
       {
           // Also Update inv_payments where status is zero because there would always be one unpaid payment belonging to same orderid 
           $this->db->update('inv_payments',array('amount'=>$data['order_amount'],'to_pay'=>$to_pay,'amount_by_bounds'=>$order_amount_by_bounds),array('order_id'=>$data['order_id'],'status'=>'0'));
           

           return $to_pay;
       }
       
       return false;
       
    }
    
    function updateexcessamt($data)
    {
       $sql="Select margin from inv_orders where id='{$data['order_id']}' ";
       $query=$this->db->query($sql);
       $result= $query->row();
       
       
        $to_pay=$data['excess_amount']-($data['excess_amount']*($result->margin/100));
        $order_amount_by_bounds=  getHOrderAmountByBounds($data['excess_amount'],$data['so_id']);
        
        
        $sql="Update inv_orders Set amount=amount+{$data['excess_amount']},"
                                                        . " to_pay=to_pay+{$to_pay},"
                                                        . " manual_order_flag=1,"
                                                        . " amount_by_bounds=amount_by_bounds+{$order_amount_by_bounds},"
                                                        . " excess_amount={$data['excess_amount']} "
                                                        . " where id={$data['order_id']}";
                                                        
          $query=$this->db->query($sql);
        
           if($this->db->affected_rows()>=0):
              
                // Returned new topay
                $sql2="Select to_pay from inv_orders where id='{$data['order_id']}'";
                $query2=$this->db->query($sql2);
                $result2=$query2->row();
                return $result2->to_pay;
               
           endif;
 
        return false;
    }
    
    function updatebankid($data)
    {
        if($this->db->update('inv_orders',array('supplier_bank_id'=>$data['bank_id']),array('id'=>$data['order_id']))):
            return true;
        endif;
        
        return false;
    }
    function updatebatchid($data)
    {
        if($this->db->update('inv_orders',array('batch'=>$data['batch_id']),array('id'=>$data['order_id']))):
            return true;
        endif;
        
        return false;
    }
    
    function approve($data)
    {
        //$this->db->where("id IN ({$data['approved']})");
        
        //Since data comes in format of order_id|bank_id
        //use array_filter to remove any empty elements
        
        $approved=strpos($data['approved'],",")?explode(",",$data['approved']):array_filter(array($data['approved']));
        
        if(!empty($approved)):
             
                  foreach($approved as $approve):

                    $temp=explode("|",$approve);

                    $this->db->update('inv_orders',array('is_approved'=>'1','supplier_bank_id'=>$temp[1],'checked_by'=>getLoggedInUserId()),"id ='{$temp[0]}' ");  

                endforeach;
       
         endif;
       
        return true;
    }
    
    public function disapprove($data)
    {
          $disapproved=strpos($data['disapproved'],",")?explode(",",$data['disapproved']):array_filter(array($data['disapproved']));
          
          $errormsg=array();
          
            if(!empty($disapproved)):
           
                 foreach($disapproved as $disapprove):

                    $temp=explode("|",$disapprove);

                    if(!$this->isDownloaded($temp[0])):
                           $this->db->update('inv_orders',array('is_approved'=>'0','supplier_bank_id'=>$temp[1],'checked_by'=>getLoggedInUserId()),"id ='{$temp[0]}' ");  
                    else:
                        $errormsg[]="#{$temp[0]}";
                    endif;   
                    
                endforeach;
       
         endif;
         
         return !empty($errormsg)?$errormsg:true;
    }
    
    public function isDownloaded($orderid)
    {
        $this->slaveDB->select('is_downloaded');
        
        $query=$this->slaveDB->get_where('inv_orders',array('id'=>$orderid));
        
        $data=$query->row_array();
        
        if(in_array($data['is_downloaded'],array('1'))):
                return true;
        endif;
        
        return false;
    }
    
    function getCommentsBySOID($soid,$date=null,$orderid=0)
    {
        $date=  is_null($date)?date('Y-m-d'):$date;
        
        
        $sql="Select comment,u.name from inv_comments  i JOIN users  u ON  i.user_id=u.id   where i.supplier_operator_id='{$soid}' and DATE(comment_timestamp)='{$date}' ";
        
        $query=$this->slaveDB->query($sql);
        
          if($query->num_rows()):
               
               return $query->result();
           
          elseif($orderid>0):
              
                  $sql="Select comment,u.name  from inv_comments  i JOIN users  u ON  i.user_id=u.id   where i.order_id='{$orderid}'  ";
                  
                  $query=$this->slaveDB->query($sql);
                  
                   return $query->result();
                  
          endif;
           
          return array();
        
    }
    
   
    function saveCommentsByOrderID($order_id,$comment,$soid=0)
    {
        if($this->db->insert('inv_comments',array('comment_timestamp'=>date('Y-m-d H:i:s'),'order_id'=>$order_id,'supplier_operator_id'=>$soid,'comment'=>$comment,'user_id'=>  getLoggedInUserId()))):
             return true;
        endif;
        
        return false;
    }
    
    function getLastComment($soid,$date=null)
    {
        $date=  is_null($date)?date('Y-m-d'):$date;
        
        $sql="Select comment from inv_comments where supplier_operator_id='{$soid}'  AND DATE(comment_timestamp)='{$date}' order by id desc limit 1";
        
        $query=$this->slaveDB->query($sql);
        
          if($query->num_rows()):
               
               $data=$query->row();
          
               return $data->comment;
           
           endif;
           
           return "";
           
    }
    
    public function getGroupedBanksBySupplierId($supplier_id)
    {
        $sql=" SELECT id,supplier_id,bank_id,account_no,account_holder_name,default_bank "
                . " FROM inv_supplier_banks "
                . " Where supplier_id = '{$supplier_id}' AND default_bank='1'  ";
        
        $query=$this->slaveDB->query($sql);
        
        $temp=array();
         if($query->num_rows()):
            
              $data=$query->result();
         
              foreach($data as $value):
                    
                            $temp[]=array('supplier_mapped_bank_id'=>$value->id,'bank_id'=>$value->bank_id,'account_no'=>$value->account_no,'account_holder_name'=>$value->account_holder_name,'default_bank'=>$value->default_bank);
                  
              endforeach;
              
              return $temp;
         
         endif;
         
         return ;
    }
    
    public function getPaymentChildByParentid($supplier_id)
    {
        $sql="Select id from inv_suppliers where parent_id='{$supplier_id}' ";
        
       $query=$this->slaveDB->query($sql);
       
        if($query->num_rows()):
            
                     $data=$query->result_array();
                     $temp=array();
                     foreach($data as $value):
                            $temp[]=$value['id'];
                     endforeach;
                    
                     if(count($temp)>0):
                         return implode(",", $temp);
                     else:
                         return $temp[0];
                     endif;
        endif;
        
        return "0";
        
    }
}
