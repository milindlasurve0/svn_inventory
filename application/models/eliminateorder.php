<?php

class Eliminateorder extends CI_Model
{
    public function eliminateByOrderId($params)
    {
        $sql="Select o.id, supplier_operator_id, amount_by_bounds, to_pay, amount, order_date, s.name AS suppliername, p.name AS operator  "
                . "  from inv_orders o "
                . " JOIN inv_suppliers s ON o.supplier_id = s.id "
                . " JOIN products p ON o.operator_id = p.id "
                . "  where o.id='{$params['orderid']}' "; 
              
        $query=$this->db->query($sql);
        
        if($query->num_rows()):
            
                    $orderinfo=$query->row_array();
        
                     $this->db->trans_begin();
                        
                    // Set appropiate flags in orders table         
                    $this->db->update('inv_orders',array('is_approved'=>'0','is_downloaded'=>'0','is_payment_done'=>'0'),array('id'=>$params['orderid']));
                     
                    // Set status false in payments table for same orderid
                    $this->db->update('inv_payments',array('status'=>'0'),array('order_id'=>$params['orderid']));
                     
                    // Set order id to 0 for that date only 
                    $this->db->update('inv_pendings',array('order_id'=>'0'),array('order_id'=>$params['orderid'],'supplier_operator_id'=>$orderinfo['supplier_operator_id'],'pending_date ' => $orderinfo['order_date']));
                     
                    // Fix Successive Pending after that date
                    $this->db->set('pending', "pending+{$orderinfo['amount_by_bounds']}", FALSE);
                    $this->db->where(array('supplier_operator_id' => $orderinfo['supplier_operator_id'], 'pending_date >= ' => $orderinfo['order_date']));
                    $this->db->update('inv_pendings');
                    
                    // Add an entry in order logs table 
                    $this->db->insert('inv_ordercancel_logs',array('order_id'=>$params['orderid'],'comment'=>$params['comment'],'cancelation_date'=>date('Y-m-d')));
                     
                  if($this->db->trans_status() === FALSE):
                           $this->db->trans_rollback();
                           return array('type'=>false,'msg'=>"Db Issue",'info'=>$orderinfo);
                  else:
                          $this->db->trans_commit();
                          return array('type'=>true,'msg'=>"Done");
                  endif;
                    
        else:
            return array('type'=>false,'msg'=>"Order with #{$params['orderid']} doesnot exisits");
        endif;
    }
    
        public function getCanceledOrderByDate($params)
        {
            $sql = "select ioc.*,io.amount,io.to_pay,s.name as supp_name,p.name as opr_name"
                    . " from inv_ordercancel_logs ioc"
                    . " join inv_orders  io"
                    . " on ioc.order_id=io.id"
                    . " join inv_suppliers s"
                    . " on io.supplier_id=s.id"
                    . " join products p"
                    . " on io.operator_id=p.id"
                    . " where cancelation_date='{$params}'";

                    
            $query=  $this->db->query($sql);

            if($query->num_rows()):
            return $query->result();
            endif;
        
            return false;
        }
}

