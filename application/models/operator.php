<?php

class Operator extends CI_Model
{
    
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getOperatorsForDropdown()
    {
         $query=$this->slaveDB->select('id,name as operator_name')->get('products');
       
        return $query->result();
    }
    
    public function saveOperatorMapping($formdata)
    {
          $this->db->set('created_at',date("Y-m-d H:i:s"));
          
        if(strpos($formdata['commission_type_formula'],'=') && $formdata['commission_type']=="3"):
             $formdata['commission_type_expression']=$formdata['commission_type_formula'];
             $parts=  explode('=', trim($formdata['commission_type_formula']));
             $parts=  array_map('trim', $parts);
             $percentage=(($parts[1]-$parts[0])*100)/$parts[0];
             $formdata['commission_type_formula']=$percentage;
         endif;
         
        
         if($this->db->insert('inv_supplier_operator',$formdata)):
            
                return true;
        
        endif;
        
        return false;    
    }
    
    public function getOperatorsDetail($supplier_id)
    {
        
        $this->slaveDB->select('o.name as operator_name,ps.base_amount,so.*')
                          ->from('inv_supplier_operator so')
                          ->join('products o','so.operator_id=o.id','left')
                          ->join('inv_planning_sheet ps','ps.supplier_operator_id = so.id','left')
                          ->where('so.supplier_id',$supplier_id);
        
        $query = $this->slaveDB->get();
        
        if($query->num_rows()):
             
                 return $query->result();
         
         endif;
         
         return false;
                          
    }
    
    public function getSupplierOperatorMappingById($id)
    {
        //$query=  $this->db->get_where('inv_supplier_operator',array('id'=>$id),1);
        
        $sql="Select so.*,p.base_amount as basesale from inv_supplier_operator so  LEFT JOIN inv_planning_sheet p  ON  so.id=p.supplier_operator_id where id='{$id}' ";
         
        $query=$this->slaveDB->query($sql);
         
        if($query->num_rows()):
             
                 return $query->row();
         
         endif;
         
        return false;
    }
    
    public function updateOperatorMapping($id,$formdata)
    {
        
        if(strpos($formdata['commission_type_formula'],'=') && $formdata['commission_type']=="3"):
             $formdata['commission_type_expression']=$formdata['commission_type_formula'];
             $parts=  explode('=', trim($formdata['commission_type_formula']));
             $parts=  array_map('trim', $parts);
             $percentage=(($parts[1]-$parts[0])*100)/$parts[0];
             $formdata['commission_type_formula']=$percentage;
         endif;
                       
        $basesale=$formdata['basesale'];
        unset($formdata['basesale']);
         
        // if is_api checbox is not checked set is_api=0
        $formdata['is_api']=isset($formdata['is_api'])?1:0;
        
        //changed params comment
         $changed_params_comment=array('somargincomment'=>$formdata['somargincomment'],'sobasecomment'=>@$formdata['sobasecomment'],'margin'=> $formdata['commission_type_formula'],'basesale'=>$basesale);
         unset($formdata['somargincomment']);unset($formdata['sobasecomment']);
         
         $this->logsoparamhistory($changed_params_comment,$id);
         
         $this->db->where('id', $id);
        
        $this->db->set('updated_at',date("Y-m-d H:i:s"));
         
         if($this->db->update('inv_supplier_operator',$formdata)):
            
             // vendors commission table will be always updated from inv panel for API vendors only
             if($formdata['is_api']=='1'): $this->UpdateVendorCommissionTable($id,$formdata['commission_type_formula']); endif;
               
               $CI =& get_instance();
               $CI->load->model('planning');
               
                                            if(!$CI->planning->checkIfPlanningAlreadyExists($id)):

                                                  $this->db->insert('inv_planning_sheet',array(
                                                                                                                             'supplier_operator_id'=>$id,
                                                                                                                             'base_amount'=>$basesale,
                                                                                                                             'last_planned_date'=>date('Y-m-d H:i:s')
                                                                                                                              )
                                                                            );

                                            else:

                                                 $this->db->update('inv_planning_sheet',array(
                                                                                                                             'supplier_operator_id'=>$id,
                                                                                                                             'base_amount'=>$basesale,
                                                                                                                             'last_planned_date'=>date('Y-m-d H:i:s')
                                                                                                                              ), array('supplier_operator_id'=>$id));

                                            endif;

                $this->db->insert('inv_planning_sheet_history',array(
                                                                                              'supplier_operator_id'=>$id,
                                                                                              'base_amount'=>$basesale,
                                                                                              'planned_by'=>  getLoggedInUserId()
                                                                                               ));
               
//                $this->logsoparamhistory($changed_params_comment,$id);

                 return true;
                                   
                                   
                
        endif;
        
    
        return false;    
    }
    
    
    public function UpdateVendorCommissionTable($soid,$margin)
    {
        
        $sql="SELECT so.supplier_id, so.operator_id, svm.vendor_id"
                . "  FROM inv_supplier_operator so "
                . " JOIN inv_supplier_vendor_mapping svm "
                . " ON so.supplier_id = svm.supplier_id "
                . " WHERE so.id ={$soid} AND so.is_api = '1'  limit 1 ";
                
        $query=  $this->slaveDB->query($sql);
        
          if($query->num_rows()):
                    $data=$query->row_array();
                    $this->db->update('vendors_commissions',array('discount_commission'=>$margin),array('product_id'=>$data['operator_id'],'vendor_id'=>$data['vendor_id']));    
         endif;
    }
    
    public function deleteSupplierOperatorMapping($id)
    {
          if($this->db->delete('inv_supplier_operator', array('id' => $id))):
            return true;
        endif;
        
        return false;
    }
    
    public function getSupplierIdFromOperatorMapping($id)
    {
        $this->slaveDB->select('supplier_id');
        
        $query=  $this->slaveDB->get_where('inv_supplier_operator',array('id'=>$id),1);
        
        if($query->num_rows()):
             
                 $result =$query->row();
                    
                  return $result->supplier_id;
         
         endif;
         
        return false;
    }
    
    public function getSupplierOperatorDetailsBySOId($id)
    {
          $this->slaveDB->select('s.name,o.operator_name')
                          ->from('inv_supplier_operator so')
                          ->join('products o','so.operator_id=o.id','left')
                          ->join('inv_suppliers s','so.supplier_id=s.id','left')
                          ->where('so.id',$id);
        
        $query = $this->slaveDB->get();
                 
         if($query->num_rows()):
             
                 return $query->row();
         
         endif;
         
         return false;
    }
    
    public function checkifAlreadyExists($supplier_id,$operator_id)
    {
        $this->slaveDB->select('id');
        
        $query=  $this->slaveDB->get_where('inv_supplier_operator',array('supplier_id'=>$supplier_id,'operator_id'=>$operator_id));
  
        if($query->num_rows()):
             
               return true;
         
         endif;
         
        return false;
    }
    
    public function insertSOID($supplier_id,$operator_id)
    {
        
        $this->db->insert('inv_supplier_operator',array('supplier_id'=>$supplier_id,'operator_id'=>$operator_id));
        echo $this->db->last_query();
        echo "<br/>";     
    }
    
    public function setisactive($data)
    {
        $val=$data['flag']=="on"?"1":"0";
        
        if(!$this->db->update('inv_supplier_operator',array('is_active'=>$val),array('id'=>$data['soid'])))
        {
             return false;
        }
        // Set target & Basesale as ZERO  if off
        if($val=='0'):
            $this->db->update('inv_planning_sheet',array('base_amount'=>'0','planned_sale'=>'0','max_sale_capacity'=>'0'),array('supplier_operator_id'=>$data['soid']));
            $this->db->update('inv_modem_planning_sheet',array('target'=>'0'),array('supplier_operator_id'=>$data['soid']));
        endif;
        
        return true;
    }
    
    /*
     * Storing margin & base sale history in table with value as current value bt we can old value from previous entry of this soid
     */
    public function logsoparamhistory($data,$soid)
    {
        $marginarr=array();
        $basearr=array();
         
        $sql="Select so.commission_type_formula as old_margin,p.base_amount as old_basesale from inv_supplier_operator so  LEFT JOIN inv_planning_sheet p  ON  so.id=p.supplier_operator_id where so.id='{$soid}' ";
        
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            $result= $query->row_array();
        endif;

        if(isset($data['somargincomment'])):$marginarr=array('margin'=>array('oldmargin'=>$result['old_margin'],'newmargin'=>$data['margin'],'comment'=>$data['somargincomment'])); endif;
        if(isset($data['sobasecomment'])): $basearr=array('basesale'=>array('oldbasesale'=>$result['old_basesale'],'newbasesale'=>$data['basesale'],'comment'=>$data['sobasecomment'])); endif;
        
        $changed_params=  array_merge($marginarr,$basearr);

        //if no change in margin or basesale field,dont insert record in sohistory
        if(!empty($changed_params)):
        $this->db->insert('inv_so_paramhistory',array('so_id'=>$soid,'changed_params'=>  json_encode($changed_params),'user_id'=>  getLoggedInUserId(),'updated_time'=>date('Y-m-d H:i:s'),'updated_date'=>date('Y-m-d')));
        endif;
    }
}