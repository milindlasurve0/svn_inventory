<?php

/*
 * Created By Swapnil Tiwhane on 4 Feb 2016
 * Get data about b2b transaction for generting alert system
 * And store this data in alert_system db
 */

class Alerttransaction extends CI_Model {
    private $slaveDB;
    private $alertDB;
    public function __construct() {
        $this->slaveDB = $this->load->database('slavedb',TRUE);
        $this->alertDB = $this->load->database('alertDb',TRUE);
        $this->load->library('Memcachedlib');
    }

    /*
      Get operator wise total sales,inprocess count,average processtime,% of failure
     *      */

    public function getOperatorReport($start, $end) {
        $date = date('Y-m-d');
        //$date = '2016-01-06';
        $processTime = $this->config->item('processing_time');
        //modem query
        $sql = "SELECT VA.product_id, 99 AS api_flag,SUM(CASE WHEN VA.status NOT IN (2,3) THEN VA.amount ELSE 0 END ) AS sales,"
                . "(SUM( CASE WHEN VA.status IN(2,3) THEN 1 ELSE 0 END ) ) AS failure,"
                . "count(*) as trans_count,"
                . "(SUM( CASE WHEN VA.status NOT IN (2,3) THEN 1 ELSE 0 END ) ) AS success,"
                . "SUM(CASE WHEN VA.status NOT IN (2,3) THEN TIME_TO_SEC( TIMEDIFF( VT.processing_time, VA.timestamp ) ) ELSE 0 END) as processtime, "
                . "SUM(CASE WHEN VA.status NOT IN (2,3) THEN TIME_TO_SEC( TIMEDIFF( VT.processing_time, VA.timestamp ) ) ELSE 0 END) as modemprocesstime "
                . "FROM vendors_activations AS VA "
                . "INNER JOIN vendors_transactions AS VT ON ( VA.`ref_code` = VT.`ref_id` AND VA.vendor_id=VT.vendor_id) "
                . "INNER JOIN vendors AS V ON ( V.id = VA.vendor_id ) "
                ."  WHERE VA.`date` =  '$date' "
                ."  AND VT.`processing_time` is not NULL "
                ."  AND  VA.`timestamp` >  '$start' "
                ."  AND  VA.`timestamp` <=  '$end' "
                . "AND V.update_flag =1 GROUP BY VA.product_id"; 
         $query = $this->slaveDB->query($sql);
        
         //API query
          $sql2 = "SELECT d.product_id, 99 AS api_flag, SUM(CASE WHEN d.status NOT IN (2,3) THEN d.amount ELSE 0 END ) AS sales,"
                . "SUM( CASE WHEN d.status NOT IN (2,3) THEN 1 ELSE 0 END ) AS success,"
                . "SUM(CASE WHEN d.status NOT IN (2,3) THEN d.processtime ELSE 0 END) as processtime,"
                . "SUM(CASE WHEN d.status NOT IN (2,3) THEN d.processtime ELSE 0 END) as apiprocesstime,"
                . "SUM( CASE WHEN d.status IN (2,3) THEN 1 ELSE 0 END ) AS failure, "
                . "count(d.ref_code) as trans_count"
                . " FROM ( SELECT ref_code, VA.product_id, VA.amount, VA.status, "
                . "MAX( TIME_TO_SEC( TIMEDIFF( VM.timestamp, VA.timestamp ) ) ) AS processtime "
                . "FROM vendors_activations AS VA "
                . "INNER JOIN vendors_messages AS VM "
                . "ON ( VA.`ref_code` = VM.`shop_tran_id`  AND VA.vendor_id=VM.service_vendor_id) "
                . "INNER JOIN vendors AS V ON ( V.id = VA.vendor_id ) "
                ."  WHERE VA.`date` =  '$date' "
                ."  AND  VA.`timestamp` >  '$start' "
                ."  AND  VA.`timestamp` <=  '$end' "
                ."  AND V.update_flag =0 "
                 ."  AND VM.status !='pending' "
                . "GROUP BY ref_code ) AS d "
                . "GROUP BY d.product_id"; 
        
        $query2 = $this->slaveDB->query($sql2);
        $result1 = $query->result_array();
        $result2 = $query2->result_array();
        
        logerror('getOperatorReport '.$sql,"alertquery");
        logerror('getOperatorReport sql2 '.$sql2,"alertquery");
        
        if ($query->num_rows() && $query2->num_rows()){
            $result = sum_array($result1,$result2,'product_id');
            return $result;
        }
        elseif($query->num_rows()){
            return $query->result_array();
        }elseif($query2->num_rows()){
            return $query2->result_array();
        }

        return array();
    }

    public function getApiReport(){
        $date = date('Y-m-d');
        $processTime = $this->config->item('processing_time');
        $sql = "SELECT d.product_id, 99 AS api_flag, SUM( d.amount ) AS sales, "
                . "SUM(CASE WHEN d.status =0 AND d.processtime >=$processTime THEN 1 ELSE 0 END ) AS inprocess,"
                . " (CASE WHEN d.status =1 THEN ROUND( AVG( d.processtime ) ) ELSE 0 END) AS processtime,"
                . "(SUM( CASE WHEN d.status =2 OR d.status =3 THEN 1 ELSE 0 END ) ) AS failure,"
                . "count(d.ref_code) as trans_count "
                . "FROM "
                . "( "
                . "SELECT ref_code, VA.product_id, VA.amount, VA.status, "
                . "MAX( TIME_TO_SEC( TIMEDIFF( VM.timestamp, VA.timestamp ) ) ) AS processtime "
                . "FROM vendors_activations AS VA "
                . "INNER JOIN vendors_messages AS VM ON ( VA.`ref_code` = VM.`shop_tran_id` ) "
                . "INNER JOIN vendors AS V ON (V.id=VA.vendor_id) "
                . "WHERE VA.date=? AND VA.timestamp>=? AND VA.timestamp<=? AND V.update_flag=0 GROUP BY ref_code ) AS d "
                . "GROUP BY d.product_id";
        $query = $this->slaveDB->query($sql, array($date, $start, $end));

        if ($query->num_rows()):
            return $query->result_array();
        endif;

        return array();
    }

    /*
      Get devise wise total sales,inprocess count,average processtime,% of failure
     *      */

    public function getDeviseReport($start, $end) {
        $date = date('Y-m-d');
        $processTime = $this->config->item('processing_time');
        //modem query
        $sql = "SELECT VA.api_flag,0 as product_id,SUM(CASE WHEN VA.status NOT IN (2,3) THEN VA.amount ELSE 0 END ) AS sales,"
                . "(SUM( CASE WHEN VA.status IN (2,3) THEN 1 ELSE 0 END ) ) AS failure,"
                . "count(*) as trans_count,"
                . "(SUM( CASE WHEN VA.status NOT IN (2,3) THEN 1 ELSE 0 END ) ) AS success,"
                . "SUM(CASE WHEN VA.status NOT IN (2,3) THEN TIME_TO_SEC( TIMEDIFF( VT.processing_time, VA.timestamp ) ) ELSE 0 END) as processtime, "
                . "SUM(CASE WHEN VA.status NOT IN (2,3) THEN TIME_TO_SEC( TIMEDIFF( VT.processing_time, VA.timestamp ) ) ELSE 0 END) as modemprocesstime "
                . "FROM vendors_activations AS VA "
                . "INNER JOIN vendors_transactions AS VT ON ( VA.`ref_code` = VT.`ref_id` AND VA.vendor_id=VT.vendor_id) "
                . "INNER JOIN vendors AS V ON ( V.id = VA.vendor_id ) "
                ."  WHERE VA.`date` =  '$date' "
                ."  AND VT.`processing_time` is not NULL "
                ."  AND  VA.`timestamp` >  '$start' "
                ."  AND  VA.`timestamp` <=  '$end' "
                . "AND V.update_flag =1 GROUP BY VA.api_flag";
       
        $query = $this->slaveDB->query($sql);
        //API query
         $sql2 = "SELECT d.api_flag,0 as product_id,SUM(CASE WHEN d.status NOT IN (2,3) THEN d.amount ELSE 0 END ) AS sales,"
                . "SUM( CASE WHEN d.status NOT IN (2,3) THEN 1 ELSE 0 END ) AS success,"
                . "SUM(CASE WHEN d.status NOT IN (2,3) THEN d.processtime ELSE 0 END) as processtime,"
                 . "SUM(CASE WHEN d.status NOT IN (2,3) THEN d.processtime ELSE 0 END) as apiprocesstime,"
                . "SUM( CASE WHEN d.status IN (2,3) THEN 1 ELSE 0 END ) AS failure, "
                . "count(d.ref_code) as trans_count"
                . " FROM ( SELECT VA.api_flag,ref_code, VA.product_id, VA.amount, VA.status, "
                . "MAX( TIME_TO_SEC( TIMEDIFF( VM.timestamp, VA.timestamp ) ) ) AS processtime "
                . "FROM vendors_activations AS VA "
                . "INNER JOIN vendors_messages AS VM "
                . "ON ( VA.`ref_code` = VM.`shop_tran_id`  AND VA.vendor_id=VM.service_vendor_id) "
                . "INNER JOIN vendors AS V ON ( V.id = VA.vendor_id ) "
                ."  WHERE VA.`date` =  '$date' "
                ."  AND  VA.`timestamp` >  '$start' "
                ."  AND  VA.`timestamp` <=  '$end' "
                ."  AND V.update_flag =0 "
                ."  AND VM.status !='pending' "
                . "GROUP BY ref_code ) AS d "
                . "GROUP BY d.api_flag";
        $query2 = $this->slaveDB->query($sql2);
        $result1 = $query->result_array();
        $result2 = $query2->result_array();
        
        logerror('getDeviseReport '.$sql,"alertquery");
        logerror('getDeviseReport 2'.$sql2,"alertquery");
        
        if ($query->num_rows() && $query2->num_rows()){
            $result = sum_array($result1,$result2,'api_flag');
            return $result;
        }
        elseif($query->num_rows()){
            return $result1;
        }elseif($query2->num_rows()){
            return $result2;
        }

        return array();
    }

    /*
      Get count of operator switched off and on
     *      */

    public function getSwitchedOperator($start, $end) {
        $date = date('Y-m-d');
        $sql = "SELECT operator_id,SUM(CASE WHEN flag=0 THEN 1 ELSE 0 END) as opr_down,"
                . "SUM(CASE WHEN flag=1 THEN 1 ELSE 0 END) as opr_up "
                . "FROM alert_system.oprator_managment "
                . "WHERE date='$date' AND timestamp>='" . $start . "' AND timestamp<='".$end."' "
                . "GROUP BY operator_id";
        $query = $this->slaveDB->query($sql);

        $operator = array();
        if($query->num_rows()):
            foreach ($query->result() as $row) {
                $operator[$row->operator_id]['up'] = $row->opr_up;
                $operator[$row->operator_id]['down'] = $row->opr_down;
            }
            return $operator;
        endif;

        return array();
    }

    /*
      insert report data in alert_system transaction table
     *      */

    public function setTransaction($data, $end) {
        //insert data in alert_system
        logerror("Inside Sale Transaction ".$end,"alertReport");
        $date = date('Y-m-d');
        $values = '';
        $coulmns = '';
        $operator = isset($data['switched_operator']) ? $data['switched_operator'] : array();
        
        foreach ($data['operators'] as $result) {
              if(array_key_exists('product_id',$result)){  
                $prod_id = $result['product_id'];
                $count_opr_down = isset($operator[$prod_id]['down']) ? $operator[$prod_id]['down'] : 0;
                $count_opr_up = isset($operator[$prod_id]['up']) ? $operator[$prod_id]['up'] : 0;

                $inser_data[] = array(
                                       'device'=>$result['api_flag'],
                                       'operator'=>$prod_id,
                                        'sale'=>$result['sales'],
                                        'failure'=>$result['failure'],
                                        //'inprocess'=>$result['inprocess'],
                                        'processtime'=> (array_key_exists('processtime',$result)) ? $result['processtime']:0,
                                        'opr_up'=>$count_opr_up,
                                        'opr_down'=>$count_opr_down,
                                        'date'=>$date,
                                        'trans_count'=>$result['trans_count'],
                                        'timestamp'=>$end,
                                        'success'=>$result['success'],
                                        'apiprocesstime'=>(array_key_exists('apiprocesstime',$result)) ? $result['apiprocesstime']:0,
                                        'modemprocesstime'=>(array_key_exists('modemprocesstime',$result)) ? $result['modemprocesstime']:0,
                );
            }
        }
        $result = '';

        foreach ($data['devise'] as $result):
            if(!array_key_exists('processtime',$result)){  
                 
                  logerror("processtime not available ".json_encode($result),"alertReport");
            }
            if(array_key_exists('product_id',$result)){  
                $prod_id = $result['product_id'];
                $count_opr_down = isset($operator[$prod_id]['down']) ? $operator[$prod_id]['down'] : 0;
                $count_opr_up = isset($operator[$prod_id]['up']) ? $operator[$prod_id]['up'] : 0;

                $inser_data[] = array(
                                        'device'=>$result['api_flag'],
                                        'operator'=>$prod_id,
                                        'sale'=>$result['sales'],
                                        'failure'=>$result['failure'],
                                        //'inprocess'=>$result['inprocess'],
                                        'processtime'=> (array_key_exists('processtime',$result)) ? $result['processtime']:0,
                                        'opr_up'=>$count_opr_up,
                                        'opr_down'=>$count_opr_down,
                                        'date'=>$date,
                                        'trans_count'=>$result['trans_count'],
                                        'timestamp'=>$end,
                                        'success'=>$result['success'],
                                        'apiprocesstime'=>(array_key_exists('apiprocesstime',$result)) ? $result['apiprocesstime']:0,
                                        'modemprocesstime'=>(array_key_exists('modemprocesstime',$result)) ? $result['modemprocesstime']:0,
                );
            }   
        endforeach;

        if (!empty($inser_data)) {
            logerror("setTransaction JSON data : ". json_encode($inser_data),"alertsystem");
            $insert_array=  createBatch($inser_data);
            foreach ($insert_array as $batch):
                $this->alertDB->insert_batch('transaction', $batch);
                if($errormsg=$this->alertDB->_error_message()):
                    $sql = $this->alertDB->last_query();
                    logerror("Query Failed :  {$this->alertDB->_error_message()} ","alertReport");
                    logerror("Sql query : {$sql}","alertsystem");
                    send_email('swapnil@mindsarray.com','Add Transaction for alert system',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
                endif;
            endforeach;
            $this->setLastTransTime($end);
        }else{
             logerror("empty insert data".$end,"alertReport");
        }
    }
    public function getAllProduct(){
        $cachedAllProduct=$this->memcachedlib->get('cachedAllProduct');
        
         if(!empty($cachedAllProduct)):
            // logerror("Data added of all product  to Memcache","cachedAllProduct");
            $allProduct=json_decode($cachedAllProduct,true);
      else:
             logerror("Setting all product  to Memcache","cachedAllProduct");
            $allProduct=$this->getProductList();
            $this->memcachedlib->set('cachedAllProduct',  json_encode($allProduct),strtotime('tomorrow') - time());
      endif;
      return $allProduct;
       
    }
    
    public function getLastTransTime(){
        $lastTransTime=$this->memcachedlib->get('lastTransTime');
        logerror("Inside getLastTransTime".$lastTransTime,"lastTransTime");
        if($lastTransTime){
             return $lastTransTime;
         }else{
             logerror("not get last timestamp","lastTransTime");
         }
         return false;
    }
    
    public function setLastTransTime($lasttime){
         $this->memcachedlib->set('lastTransTime', $lasttime,3600);
         logerror("Inside setLastTransTime".$lasttime,"lastTransTime");
         return true;
    }


    public function getProductList(){
         $this->slaveDB->select('id,name')
                 ->where('monitor',1)
                ->from('products');
        $query = $this->slaveDB->get()->result_array(); 
        
    }


    //generate failure alert
    public function failurealert($start) {
        $trans = $this->config->item('failure_minTrans');
        $failConst = $this->config->item('failure_const');
        $minfail = $this->config->item('min_fail');
        //$operators_Name = $this->getAllProduct();
        $operators_Name = $this->config->item('all_products');
        $date = date('Y-m-d');
        $this->slaveDB->select('id,benchmark_failure')
                ->from('products');
        $query = $this->slaveDB->get();
        $benchmark = $query->result_array();
        $failure = array();
        foreach ($benchmark as $row) {
            $failure[$row['id']] = $row['benchmark_failure'];
        }
        //$this->load->database('alertDb', true);
         $sql = "SELECT operator, (SUM( failure ) / SUM( trans_count )) *100 as fail "
                . "FROM transaction "
                . "WHERE date =  '$date' AND ( timestamp =  '$start' ) AND trans_count >$trans "
                . "GROUP BY operator";
         logerror('failurealert '.$sql,"alertquery");
        $result = $this->alertDB->query($sql);
        $records = array();
        if ($result && $result->num_rows()):
            foreach ($result->result_array() as $row) {
                if (isset($failure[$row['operator']]) && ($row['fail'] > round( $failConst * $failure[$row['operator']])) && isset($operators_Name[$row['operator']]) && (round($row['fail'])>$minfail)) {
                    $row['benchmark'] = $failure[$row['operator']];
                    $row['name'] = $operators_Name[$row['operator']];
                    $records[] = $row;
                    
                }
            }

           if(!empty($records)):
                $smsdata="";
                $i=1;
                foreach($records as $row){

                    $benchmark = $row['benchmark'];
                    $operator = $row['name'];
                    $fail = round($row['fail']);
                    $smsdata.="\n $i) $operator : $fail % ($benchmark %)";
                    $i++;
                }
                $sms=  sprintf($this->config->item('failuresms'),$smsdata);   
                return $sms;
            endif;

        endif;
    }

    //proceesstime alert
    public function processingalert($start,$end){
        $trans = $this->config->item('failure_minTrans');
        $date = date('Y-m-d');
        //$operators_Name = $this->getAllProduct();
        $operators_Name = $this->config->item('all_products');
        $procPerc = $this->config->item('processing_const');
        //api transaction count
        $sql = "SELECT d.product_id, 99 AS api_flag, "
                . "SUM( CASE WHEN d.status NOT IN (2,3) THEN 1 ELSE 0 END ) AS success"
                . " FROM ( SELECT ref_code, VA.product_id, VA.amount, VA.status, "
                . "MAX( TIME_TO_SEC( TIMEDIFF( VM.timestamp, VA.timestamp ) ) ) AS processtime "
                . "FROM vendors_activations AS VA "
                . "INNER JOIN vendors_messages AS VM "
                . "ON ( VA.`ref_code` = VM.`shop_tran_id`  AND VA.vendor_id=VM.service_vendor_id) "
                . "INNER JOIN vendors AS V ON ( V.id = VA.vendor_id ) "
                ."  WHERE VA.`date` =  '$date' "
                ."  AND  VA.`timestamp` >  '$start' "
                ."  AND  VA.`timestamp` <=  '$end' "
                ."  AND V.update_flag =0 "
                 ."  AND VM.status !='pending' "
                . "GROUP BY ref_code ) AS d "
                . "GROUP BY d.product_id"; 
        logerror('processingalert API data '.$sql,"alertquery");
        
        $query = $this->slaveDB->query($sql);
         $result = $query->result_array();
         $api = array();
         foreach($result as $row){
             $api[$row['product_id']] = $row['success'];
         }
        $this->slaveDB->select('id,benchmark_processtime')
                ->from('products')
                ->where('benchmark_processtime !=', '');
        $benchmark = $this->slaveDB->get()->result_array();
        $processtime = array();
        foreach ($benchmark as $row) {
            $val = $row['benchmark_processtime'];
            $processtime[$row['id']] = ($val);
        }
       $sql = "SELECT operator, SUM(trans_count) as count,SUM(processtime) as processtime,"
               . "SUM(success) as success,AVG(modemprocesstime) as modem,AVG(apiprocesstime) as api "
                . "FROM transaction "
                . "WHERE date =  '$date' AND timestamp>='$start' AND timestamp<='$end' AND operator > 0 AND trans_count >$trans "
                . "GROUP BY operator";
       logerror('processingalert '.$sql,"alertquery");
       
        $result = $this->alertDB->query($sql);
        $records = array();
        if ($result->num_rows()):
            $min_process_time = $this->config->item('min_proc_time');
            foreach ($result->result_array() as $row):
                $modem_proc = $row['modem'];
                $api_proc = $row['api'];
                if (isset($processtime[$row['operator']]) && $row['success']!=0 && isset($operators_Name[$row['operator']]) && ($modem_proc > $procPerc*$processtime[$row['operator']]  
                        || (isset($api[$row['operator']]) && $api[$row['operator']]>=2 && round(($row['success']/$api[$row['operator']])*100)>10 && $api_proc >$min_process_time))):
                    $row['benchmark'] = $processtime[$row['operator']];
                    $row['name'] = $operators_Name[$row['operator']];
                    $records[] = $row;
                endif;
            endforeach;
            $sms="";
        if(!empty($records)):
            $i=1;
            $smsdata="";
           foreach($records as $row){

                    $benchmark = $row['benchmark'];
                    $operator = $row['name'];
                    $processtime = round($row['processtime']/$row['success']);
                    $count = $row['count'];
                    $smsdata.="\n $i) $operator : $processtime sec ($benchmark sec) [M-".intval($row['modem']).", A-".intval($row['api'])."]";
                    $i++;
                }
             $sms=  sprintf($this->config->item('process_sms'),$smsdata);
            return $sms;
            endif;

        endif;
    }
    
    //last 7 day sale
    public function Pastsales($fifteenmin,$thirtymin,$hour){
        
        $num = $this->config->item('sales_day_Trans');
        $currentdate = new DateTime();
        $startDate = $currentdate->sub(new DateInterval('P1D'))->format('Y-m-d');
        $currentdate = new DateTime();
        $interval = $currentdate->sub(new DateInterval('P7D'));
        $endDate = $interval->format('Y-m-d');
        logerror("start date ".$startDate." End date ".$endDate,"salestrend");
        
        $data = array();
        $device_Name = $this->config->item('all_devices');
        
        $cond = "";
        logerror("start time ".$fifteenmin." End time ".$hour,"salestrend");
        //collect 7 day data
         $query = "SELECT id,sale,trans_count,timestamp,operator,device,date,DATE_FORMAT(timestamp,'%H:%i:00') as time  FROM `transaction` "
                . "WHERE `date` <= '$startDate' AND `date`>='$endDate' AND DATE_FORMAT(timestamp,'%H:%i:00')>='$hour' AND DATE_FORMAT(timestamp,'%H:%i:00')<='$fifteenmin' ";
        logerror('Pastsales '.$query,"alertquery");
        
        $result = $this->alertDB->query($query);
        
        if ($result->num_rows()):
            $pastfifteen_optTranstotal=0;
            $pastfifteen_optSaletotal = 0;
            $pastfifteen_deviceTranstotal =0;
            $pastfifteen_deviceSaletotal =0;
            
            $pastthirty_optTranstotal=0;
            $pastthirty_optSaletotal = 0;
            $pastthirty_deviceSaletotal = 0;
            $pastthirty_deviceTranstotal=0;
            
            $pasthour_optTranstotal = 0;
            $pasthour_optSaletotal = 0;
            $pasthour_deviceTranstotal = 0;
            $pasthour_deviceSaletotal = 0;
                    
            $pastfifteen = array();
            $pastthirty = array();
            $pasthour = array();
        
            foreach($result->result_array() as $row):
                if($row['time']==$fifteenmin):
                    if($row['operator']!=0):
                     $pastfifteen[$row['operator']]['sale'] = (isset($pastfifteen[$row['operator']]['sale'])) ? $pastfifteen[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                     $pastfifteen[$row['operator']]['trans_count'] = (isset($pastfifteen[$row['operator']]['trans_count'])) ? $pastfifteen[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                     $pastfifteen_optTranstotal += $row['trans_count'];
                     $pastfifteen_optSaletotal += $row['sale'];
                   endif;
                    if($row['device']!=99 && array_key_exists($row['device'], $device_Name)):
                     
                     $key = $device_Name[$row['device']];
                     $pastfifteen[$key]['sale'] = (isset($pastfifteen[$key]['sale'])) ? $pastfifteen[$key]['sale']+$row['sale'] :  $row['sale'];
                     $pastfifteen[$key]['trans_count'] = (isset($pastfifteen[$key]['trans_count'])) ? $pastfifteen[$key]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                     $pastfifteen_deviceTranstotal += $row['trans_count'];
                     $pastfifteen_deviceSaletotal += $row['sale'];
                    endif;
                    
                endif;
                
                if($row['time']>=$thirtymin):
                    if($row['operator']!=0):
                        $pastthirty[$row['operator']]['sale'] = (isset($pastthirty[$row['operator']]['sale'])) ? $pastthirty[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                        $pastthirty[$row['operator']]['trans_count'] = (isset($pastthirty[$row['operator']]['trans_count'])) ? $pastthirty[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                        $pastthirty_optTranstotal += $row['trans_count'];
                        $pastthirty_optSaletotal += $row['sale'];

                    endif;
                    if($row['device']!=99 && array_key_exists($row['device'], $device_Name)):
                            $key = $device_Name[$row['device']];
                        $pastthirty[$key]['sale'] = (isset($pastthirty[$key]['sale'])) ? $pastthirty[$key]['sale']+$row['sale'] :  $row['sale'];
                        $pastthirty[$key]['trans_count'] = (isset($pastthirty[$key]['trans_count'])) ? $pastthirty[$key]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                        $pastthirty_deviceTranstotal += $row['trans_count'];
                        $pastthirty_deviceSaletotal += $row['sale'];
                       
                    endif;
                    
                endif;
                
                if($row['operator']!=0):
                    $pasthour[$row['operator']]['sale'] = (isset($pasthour[$row['operator']]['sale'])) ? $pasthour[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                    $pasthour[$row['operator']]['trans_count'] = (isset($pasthour[$row['operator']]['trans_count'])) ? $pasthour[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                    $pasthour_optTranstotal += $row['trans_count'];
                    $pasthour_optSaletotal += $row['sale'];

                endif;
                if($row['device']!=99 && array_key_exists($row['device'], $device_Name)):
                     $key = $device_Name[$row['device']];
                    $pasthour[$key]['sale'] = (isset($pasthour[$key]['sale'])) ? $pasthour[$key]['sale']+$row['sale'] :  $row['sale'];
                    $pasthour[$key]['trans_count'] = (isset($pasthour[$key]['trans_count'])) ? $pasthour[$key]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                    $pasthour_deviceTranstotal += $row['trans_count'];
                    $pasthour_deviceSaletotal += $row['sale'];
                   
                endif;   
                
            endforeach;
            //END collect 7 day data
            
            $data['pastfifteen'] = $pastfifteen;
            $data['pastfifteen_optTranstotal'] = $pastfifteen_optTranstotal;
            $data['pastfifteen_optSaletotal'] = $pastfifteen_optSaletotal;

            $data['pastfifteen_deviceTranstotal'] = $pastfifteen_deviceTranstotal;
            $data['pastfifteen_deviceSaletotal'] = $pastfifteen_deviceSaletotal;
            
            $data['pastthirty'] = $pastthirty;
            $data['pastthirty_optTranstotal'] = $pastthirty_optTranstotal;
            $data['pastthirty_optSaletotal'] = $pastthirty_optSaletotal;

            $data['pastthirty_deviceTranstotal'] = $pastthirty_deviceTranstotal;
            $data['pastthirty_deviceSaletotal'] = $pastthirty_deviceSaletotal;
            
            $data['pasthour'] = $pasthour;
            $data['pasthour_optTranstotal'] = $pasthour_optTranstotal;
            $data['pasthour_optSaletotal'] = $pasthour_optSaletotal;

            $data['pasthour_deviceTranstotal'] = $pasthour_deviceTranstotal;
            $data['pasthour_deviceSaletotal'] = $pasthour_deviceSaletotal;
            
            endif;
            
            return $data;
    }
    
    public function TodaySale($fifteenmin,$thirtymin,$hour){
          //todays sale
            $device_Name = $this->config->item('all_devices');
            $data = array();
            $currentdate = date('Y-m-d');
            $query = "SELECT id,sale,trans_count,timestamp,operator,device,date,DATE_FORMAT(timestamp,'%H:%i:00') as time  FROM `transaction` "
            . "WHERE `date` = '$currentdate' AND DATE_FORMAT(timestamp,'%H:%i:00')>='$hour' AND DATE_FORMAT(timestamp,'%H:%i:00')<='$fifteenmin'  AND id NOT IN (SELECT trans_id FROM past_transaction WHERE `date` = '$currentdate' and type='sale') ";
            
            logerror('TodaySale '.$query,"alertquery");
             $result = $this->alertDB->query($query);
             if ($result->num_rows()):
                $fifteen_optTranstotal =0;
                $fifteen_optSaletotal =0;

                $fifteen_deviceTranstotal=0;
                $fifteen_deviceSaletotal =0;

                $thirty_optTranstotal =0;
                $thirty_optSaletotal =0;

                $thirty_deviceTranstotal =0;
                $thirty_deviceSaletotal =0;

                $hour_optTranstotal =0;
                $hour_optSaletotal =0;

                $hour_deviceTranstotal =0;
                $hour_deviceSaletotal =0;

                $fifteen = array();
                $thirty = array();
                $hour = array();

                foreach($result->result_array() as $row):
                    if($row['time']==$fifteenmin):
                        if($row['operator']!=0):
                         $fifteen[$row['operator']]['sale'] = (isset($fifteen[$row['operator']]['sale'])) ? $fifteen[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                         $fifteen[$row['operator']]['trans_count'] = (isset($fifteen[$row['operator']]['trans_count'])) ? $fifteen[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                         $fifteen_optTranstotal += $row['trans_count'];
                         $fifteen_optSaletotal += $row['sale'];
                         $fifteen[$row['operator']]['id'][]=  $row['id'];
                        endif;

                        if($row['device']!=99 && array_key_exists($row['device'], $device_Name)):

                                $key = $device_Name[$row['device']];
                                $fifteen[$key]['sale'] = (isset($fifteen[$key]['sale'])) ? $fifteen[$key]['sale']+$row['sale'] :  $row['sale'];
                                $fifteen[$key]['trans_count'] = (isset($fifteen[$key]['trans_count'])) ? $fifteen[$key]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                                $fifteen_deviceTranstotal += $row['trans_count'];
                                $fifteen_deviceSaletotal += $row['sale'];

                                $fifteen[$key]['id'][]=  $row['id'];

                         endif;

                    endif;
                      
                    if($row['time']>=$thirtymin):
                        if($row['operator']!=0):
                            $thirty[$row['operator']]['sale'] = (isset($thirty[$row['operator']]['sale'])) ? $thirty[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                            $thirty[$row['operator']]['trans_count'] = (isset($thirty[$row['operator']]['trans_count'])) ? $thirty[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                            $thirty_optTranstotal += $row['trans_count'];
                            $thirty_optSaletotal += $row['sale'];

                            $thirty[$row['operator']]['id'][]=  $row['id'];
                        endif;
                        if($row['device']!=99 && array_key_exists($row['device'], $device_Name)):

                                $key = $device_Name[$row['device']];
                                $thirty[$key]['sale'] = (isset($thirty[$key]['sale'])) ? $thirty[$key]['sale']+$row['sale'] :  $row['sale'];
                                $thirty[$key]['trans_count'] = (isset($thirty[$key]['trans_count'])) ? $thirty[$key]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                                $thirty_deviceTranstotal += $row['trans_count'];
                                $thirty_deviceSaletotal += $row['sale'];

                                $thirty[$key]['id'][]=  $row['id'];

                      endif;

                    endif;
                       
                    if($row['operator']!=0):
                        $hour[$row['operator']]['sale'] = (isset($hour[$row['operator']]['sale'])) ? $hour[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                        $hour[$row['operator']]['trans_count'] = (isset($hour[$row['operator']]['trans_count'])) ? $hour[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                        $hour_optTranstotal += $row['trans_count'];
                        $hour_optSaletotal += $row['sale'];

                        $hour[$row['operator']]['id'][]=  $row['id'];
                    endif;
                    if($row['device']!=99 && array_key_exists($row['device'], $device_Name)):

                            $key = $device_Name[$row['device']];
                            $hour[$key]['sale'] = (isset($hour[$key]['sale'])) ? $hour[$key]['sale']+$row['sale'] :  $row['sale'];
                            $hour[$key]['trans_count'] = (isset($hour[$key]['trans_count'])) ? $hour[$key]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                            $hour_deviceTranstotal += $row['trans_count'];
                            $hour_deviceSaletotal += $row['sale'];

                            $hour[$key]['id'][]= $row['id'];

                      endif;   
                 
                endforeach;
                $data['fifteen'] = $fifteen;
                $data['fifteen_optTranstotal'] = $fifteen_optTranstotal;
                $data['fifteen_optSaletotal'] = $fifteen_optSaletotal;

                $data['fifteen_deviceTranstotal'] = $fifteen_deviceTranstotal;
                $data['fifteen_deviceSaletotal'] = $fifteen_deviceSaletotal;

                $data['thirty'] = $thirty;
                $data['thirty_optTranstotal'] = $thirty_optTranstotal;
                $data['thirty_optSaletotal'] = $thirty_optSaletotal;

                $data['thirty_deviceTranstotal'] = $thirty_deviceTranstotal;
                $data['thirty_deviceSaletotal'] = $thirty_deviceSaletotal;

                $data['hour'] = $hour;
                $data['hour_optTranstotal'] = $hour_optTranstotal;
                $data['hour_optSaletotal'] = $hour_optSaletotal;

                $data['hour_deviceTranstotal'] = $hour_deviceTranstotal;
                $data['hour_deviceSaletotal'] = $hour_deviceSaletotal;
                 //END todays sale
                
      endif;
      return $data;

}
    
    public function SalesTrend($lastTime=null){
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        logerror("Sales Trend started".date('Y-m-d'),"salestrend");
        $date = date('Y-m-d');
        $sms='';
        $gettime = $this->getLastTransTime();
        if(!empty($gettime)){
            $type = 'sale';
         logerror("Get last transaction time".$gettime,"salestrend");
            $start = $gettime;
            $currentDate = new DateTime($start);
            $fifteenmin = $currentDate->format('H:i:s');
            
            $currentDate = new DateTime($start);
            $thirtymin = $currentDate->sub(new DateInterval('PT15M'))->format('H:i:s');
            
            $currentDate = new DateTime($start);
            $hour= $currentDate->sub(new DateInterval('PT45M'))->format('H:i:s');
         
            $past = $this->Pastsales($fifteenmin,$thirtymin,$hour);
            
        if(!empty($past)){
            $today_data=  $this->TodaySale($fifteenmin,$thirtymin,$hour);
            
            $operators_Name = $this->config->item('all_products');
            $device_Name =  $this->config->item('all_devices');
         
            $id = array();
            $deviceid = array();
            $i = 1;
            $smsdata = "";
            $devicesmsdata = "";
            $devicemedsmsdata = "";
            $bigdevicekeys = array();
            $bigkeys = array();
            $timestamp = $gettime;
            $curr_date = date("Y-m-d");
            $inser_data = array();
            $sale_deflation = $this->config->item('sale_deflation');
        //common condition prevtrans - trans>10 and ((prevtrans-trans)/prevtrans)*100 >20
        if(!empty($today_data['fifteen'])){
                
                $pastfifteen = $past['pastfifteen'];
                
                foreach($today_data['fifteen'] as $key=>$row){
                    
                    $avgPrevTrans = (array_key_exists($key, $pastfifteen)) ? round($pastfifteen[$key]['trans_count']/7) : 0;
                    $avgPrevSale = (array_key_exists($key, $pastfifteen)) ? round($pastfifteen[$key]['sale']/7) : 0;
                    //for operator
                    if(array_key_exists($key, $operators_Name) && ($avgPrevTrans-$row['trans_count'])>10
                            && (round(($avgPrevTrans-$row['trans_count'])/$avgPrevTrans)*100 > $sale_deflation) 
                            && (($avgPrevTrans-$row['trans_count']>100 || ($today_data['fifteen_optSaletotal']>0 && ($row['sale']/$today_data['fifteen_optSaletotal'])*100>2)) ))
                    {
                        $id =  array_merge($id,$row['id']);
                        //$id[] = $row['id'];
                        $operator = $operators_Name[$key];
                        $bigkeys[]=$key;

                        $smsdata.="\n $i) $operator : ".round(($row['trans_count']/$avgPrevTrans)*100)."% (".$row['trans_count']."/".$avgPrevTrans." ), 15 min)";
                        $i++;
                    }
                    //for devise
                    if(in_array($key, $device_Name) && ($avgPrevTrans-$row['trans_count'])>10 
                            && (round(($avgPrevTrans-$row['trans_count'])/$avgPrevTrans)*100 > $sale_deflation) 
                            && ($avgPrevTrans-$row['trans_count']>100 || ($today_data['fifteen_deviceSaletotal']>0 && ($row['sale']/$today_data['fifteen_deviceSaletotal'])*100>2) ))
                    {
                        $deviceid =array_merge($deviceid,$row['id']);
                        $device = $key;
                        $bigdevicekeys[]=$key;

                        $devicesmsdata.="\n $i) $device : ".round(($row['trans_count']/$avgPrevTrans)*100)."% (".$row['trans_count']."/".$avgPrevTrans.") , 15 min)";
                        $i++;
                    }
                
            }
        }
        
         $data = $this->insertData($smsdata,$id,$curr_date,$timestamp,$type);
         $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;

       //device
        $data = $this->insertData($devicesmsdata,$deviceid,$curr_date,$timestamp,$type);
        $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
       
       
        $mediumopr = array();
        $medsmsdata = '';
        $medium_id = array();
        $medium_deviceid = array();
        if(!empty($today_data['thirty'])){
             $pastthirty = $past['pastthirty'];
            foreach($today_data['thirty'] as $key=>$row){
                if(count($row['id'])==2){
                    $avgPrevTrans = (array_key_exists($key, $pastthirty)) ? round($pastthirty[$key]['trans_count']/7) : 0;
                     $avgPrevSale = (array_key_exists($key, $pastthirty)) ? round($pastthirty[$key]['sale']/7) : 0;

                    //for operator  
                    if(array_key_exists($key, $operators_Name) && !in_array($key, $bigkeys) && ($avgPrevTrans-$row['trans_count'])>10 
                             && (round(($avgPrevTrans-$row['trans_count'])/$avgPrevTrans)*100 > $sale_deflation) 
                             && ( ($avgPrevTrans-$row['trans_count']>50 ) || ($today_data['thirty_optSaletotal']>0 && ($row['sale']/$today_data['thirty_optSaletotal'])*100>1 && ($row['sale']/$today_data['thirty_optSaletotal'])*100<=2)))
                    {

                        $mediumopr[$key] = $row;
                        $bigkeys[]=$key;

                        $medium_id =array_merge($medium_id,$row['id']);
                        $operator = $operators_Name[$key];

                        $medsmsdata.="\n $i) $operator : ".round(($row['trans_count']/$avgPrevTrans)*100)."% (".$row['trans_count']."/".$avgPrevTrans." ) , 30 min)";
                        $i++;
                    }

                    //for device
                    if(in_array($key, $device_Name) && !in_array($key, $bigdevicekeys) && ($avgPrevTrans-$row['trans_count'])>10 
                             && (round(($avgPrevTrans-$row['trans_count'])/$avgPrevTrans)*100 > $sale_deflation) 
                             && ( ($avgPrevTrans-$row['trans_count']>50 ) || ($today_data['thirty_deviceSaletotal']>0 && ($row['sale']/$today_data['thirty_optSaletotal'])*100>1 && ($row['sale']/$today_data['thirty_optSaletotal'])*100<=2)))
                    {

                        $mediumopr[$key] = $row;
                        $bigdevicekeys[]=$key;

                        $medium_deviceid =array_merge($medium_deviceid,$row['id']);
                        $device = $key;

                        $devicemedsmsdata.="\n $i) $device : ".round(($row['trans_count']/$avgPrevTrans)*100)."% (".$row['trans_count']."/".$avgPrevTrans." ) , 30 min)";
                        $i++;
                    }
                }
            }
        }
        
        if(!empty($medsmsdata)){
            $smsdata.=$medsmsdata;
             $data = $this->insertData($medsmsdata,$medium_id,$curr_date,$timestamp,$type);
            $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
        }
        // device
     if(!empty($devicemedsmsdata)){
         $data = $this->insertData($devicemedsmsdata,$medium_deviceid,$curr_date,$timestamp,$type);
        $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
        $devicesmsdata.=$devicemedsmsdata;
    }
     
     //smallopr
    $smallopr = array();
    $smallsmsdata = '';
    $small_id = array();
    $smallsmsdevicedata = "";
    $small_deviceid = array();
    if(!empty($hour)){
        $pasthour = $past['pasthour'];
        foreach($today_data['hour'] as $key=>$row){
            if(count($row['id'])==4){
                
                $avgPrevTrans = (array_key_exists($key, $pasthour)) ? round($pasthour[$key]['trans_count']/7) : 0;
                $avgPrevSale = (array_key_exists($key, $pasthour)) ? round($pasthour[$key]['sale']/7) : 0;

               //for operator 
               if(array_key_exists($key, $operators_Name) && !in_array($key, $bigkeys) && ($avgPrevTrans-$row['trans_count'])>20
                            && (round(($avgPrevTrans-$row['trans_count'])/$avgPrevTrans)*100 > $sale_deflation) 
                 )
               {

                   $smallopr[$key] = $row;
                   $operator = $operators_Name[$key];
                   $small_id =array_merge($small_id,$row['id']);
                    $smallsmsdata.="\n $i) $operator : ".round(($row['trans_count']/$avgPrevTrans)*100)."% (".$row['trans_count']."/".$avgPrevTrans." ) , 60 min)";
                    $i++;

               }
               //for device 
               if(in_array($key, $device_Name) && !in_array($key, $bigdevicekeys) && ($avgPrevTrans-$row['trans_count'])>20
                            && (round(($avgPrevTrans-$row['trans_count'])/$avgPrevTrans)*100 > $sale_deflation) 
                 )
               {
                   $smallopr[$key] = $row;
                   $device = $key;
                   logerror($key,"smallperatorloop");
                   $small_deviceid =array_merge($small_deviceid,$row['id']);
                    $smallsmsdevicedata.="\n $i) $device : ".round(($row['trans_count']/$avgPrevTrans)*100)."% (".$row['trans_count']."/".$avgPrevTrans." ) , 60 min)";
                    $i++;

               }
            }
        
          }
        
    }
    
    //for operator
    $data = $this->insertData($smallsmsdata,$small_id,$curr_date,$timestamp,$type);
    $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
     if(!empty($smallsmsdata)){
        $smsdata.=$smallsmsdata;
    }

    //for device
    $data = $this->insertData($smallsmsdevicedata,$small_deviceid,$curr_date,$timestamp,$type);
    $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
    if(!empty($smallsmsdevicedata)){
        $devicesmsdata.=$smallsmsdevicedata;
        $smsdata .=$devicesmsdata;
    }
    
  if (!empty($inser_data)) {
    logerror("setTransaction JSON data : ". json_encode($inser_data),"alertsystem");
    $insert_array=  createBatch($inser_data);
    foreach ($insert_array as $batch):
        $this->alertDB->insert_batch('past_transaction', $batch);
        if($errormsg=$this->alertDB->_error_message()):
            $sql = $this->alertDB->last_query();
            logerror("Query Failed :  {$this->alertDB->_error_message()} ","alertsystem");
            logerror("Sql query : {$sql}","alertsystem");
            send_email('swapnil@mindsarray.com','Add Transaction for alert system',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
        endif;
    endforeach;

    }
    if(!empty($smsdata)){
         $sms=  sprintf($this->config->item('salestrend_sms'),$smsdata);
        logerror("sms for sale trend ".$sms,"salestrend");

    }
   
  }

}
    return $sms;

}
// function to create insert data array
public function insertData($sms,$id_array,$curr_date,$timestamp,$type){
    $type = (empty($type)) ? 'sale' : $type;
    $data = array();
    if(!empty($sms)){
        
        if(count($id_array)>0){

            foreach($id_array as $row){
                $data[] = array(
                    'trans_id'=> $row,
                    'sms'=>$sms,
                    'type'=>$type,
                    'timestamp'=>$timestamp,
                    'date'=>$curr_date
                );
            }
        }
    }
    return $data;
}

    public function getLastTime(){
        $sql = "SELECT transaction.timestamp "
                . "FROM  `transaction` "
                . "ORDER BY id DESC "
                . "LIMIT 0 , 1";
        $result = $this->alertDB->query($sql);
        if($result->num_rows()):
            return $result->result_array();
        endif;
             
    }
    
    public function getinprocesscount($count){
        $inporocess_cachetime = $this->config->item('inporocess_cachetime');
        $cachedinprocess=$this->memcachedlib->get('cachedinprocess');
        logerror("get inprocess : ". $count,"inprocesstxn");
         if(!empty($cachedinprocess)):
             logerror("cached inprocess : ". $cachedinprocess,"inprocesstxn");
        
             if($count> $cachedinprocess+round($cachedinprocess/10) ): 
                 $this->memcachedlib->set('cachedinprocess',  $count,$inporocess_cachetime);
                 return true; 
             endif;
      else:
           
            $this->memcachedlib->set('cachedinprocess',  $count,$inporocess_cachetime);
            return true; 
      endif;
      return false;
    }
       
    public function inprocesstxn(){
        $currentdate = new DateTime();
        $date = $currentdate->format('Y-m-d');
        $time = $this->config->item('inprocess_time');
        $interval = $currentdate->sub(new DateInterval('PT'.$time.'S'));
        $dateTime = $interval->format('Y-m-d H:i:s');
        $inprocess_count = $this->config->item('inprocess_count');
        $sql="SELECT COUNT( id ) AS inprocess_count "
                . "FROM  `vendors_activations` WHERE STATUS =0 AND DATE =  '$date' AND timestamp <'$dateTime' "
                . "HAVING COUNT(id) > $inprocess_count ";
        $query = $this->slaveDB->query($sql);
        $sms="";
            if($query->num_rows()):
                
                $data = $query->result_array();
                    logerror("set JSON data : ". json_encode($data),"inprocesstxn");
                    $insert_array=  createBatch($data);

                    foreach ($insert_array as $batch):
                        
                        $this->alertDB->insert_batch('inprocess', $batch);
                        if($errormsg=$this->alertDB->_error_message()):
                            $sql = $this->alertDB->last_query();
                            logerror("Query Failed :  {$this->alertDB->_error_message()} ","alertsystem");
                            logerror("Sql query : {$sql}","alertsystem");
                            send_email('swapnil@mindsarray.com','Add Transaction for alert system',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
                        endif;
                    endforeach;
                    $smsdata = "";
                    $check = $this->getinprocesscount($data[0]['inprocess_count']);
                    if($check):
                    //$sms.= "\n data generated at ".$dateTime;
                    $sms=  sprintf($this->config->item('inprocess_sms'),$data[0]['inprocess_count']);
                    endif;
                   
                    return $sms;
                    endif;
    }

    public function inprocessalert(){
        $currentdate = new DateTime();
        //$current = $currentdate->format('Y-m-d H:i:s');
        $interval = $currentdate->sub(new DateInterval('PT300S'));
        $dateTime = $interval->format('Y-m-d H:i:s');
        
        
    }
    
    public function newfailure_alert($start=null){
        
        $date = date('Y-m-d');
        $gettime = $this->getLastTransTime();
        if(!empty($gettime)){
            $type = 'fail';
            $start = $gettime;
            $currentDate = new DateTime($start);
            $fifteenmin = $currentDate->format('Y-m-d H:i:s');
            
            $currentDate = new DateTime($start);
            $thirtymin = $currentDate->sub(new DateInterval('PT15M'))->format('Y-m-d H:i:s');
            
            $currentDate = new DateTime($start);
            $dateTime= $currentDate->sub(new DateInterval('PT45M'))->format('Y-m-d H:i:s');
            $inser_data = array();
            $start = $fifteenmin;
            $curr_date = date("Y-m-d");
            
        $query = "SELECT id,sale,failure,operator,trans_count,timestamp FROM `transaction` "
                . "WHERE `date` = '$date' AND timestamp>='$dateTime'  AND operator>0 and id"
                . " NOT IN (SELECT trans_id FROM past_transaction WHERE `date` = '$date' and type='fail')";
        logerror('Failure '.$query,"alertquery");
        $result = $this->alertDB->query($query);
        $fifteen = array();
        $thirty = array();
        $hour = array();
      
        $i=0;
        if ($result->num_rows()):
           $fifteen_total=0;
            $thirty_total=0;
            $hour_total=0;
            foreach($result->result_array() as $row):
                if( !empty($row['operator'])):
                if(strtotime($start)==strtotime($row['timestamp'])):
                    $fifteen[$row['operator']] = $row;
                    $fifteen_total += $row['sale'];
                    $fifteen[$row['operator']]['id'] = $row['id'];
                endif;
                if(strtotime($thirtymin)<=strtotime($row['timestamp'])):
                    $thirty[$row['operator']]['sale'] = (isset($thirty[$row['operator']]['sale'])) ? $thirty[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                    $thirty[$row['operator']]['failure'] = (isset($thirty[$row['operator']]['failure'])) ? $thirty[$row['operator']]['failure']+$row['failure'] :  $row['failure'];
                    $thirty[$row['operator']]['trans_count'] = (isset($thirty[$row['operator']]['trans_count'])) ? $thirty[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                    $thirty_total += $row['sale'];
                    $thirty[$row['operator']]['id'][] = $row['id'];
                endif;
                
                
                    $hour[$row['operator']]['sale'] = (isset($hour[$row['operator']]['sale'])) ? $hour[$row['operator']]['sale']+$row['sale'] :  $row['sale'];
                    $hour[$row['operator']]['failure'] = (isset($hour[$row['operator']]['failure'])) ? $hour[$row['operator']]['failure']+$row['failure'] :  $row['failure'];
                    $hour[$row['operator']]['trans_count'] = (isset($hour[$row['operator']]['trans_count'])) ? $hour[$row['operator']]['trans_count']+$row['trans_count'] :  $row['trans_count'];
                    $hour[$row['operator']]['id'][]=  $row['id'];
                    $hour_total += $row['sale'];
                    
                    endif;
           
            endforeach;
            
//            logerror(json_encode($fifteen),"bigoperator");
//            logerror(json_encode($thirty),"medium");
//            logerror(json_encode($hour),"small");
            
            $minfail = $this->config->item('min_fail');
            //$operators_Name = $this->getAllProduct();
            $operators_Name = $this->config->item('all_products');
            $date = date('Y-m-d');
            $this->slaveDB->select('id,benchmark_failure')
                    ->from('products');
            $query = $this->slaveDB->get();
            $benchmark = $query->result_array();
            $failure = array();
            foreach ($benchmark as $row) {
                $failure[$row['id']] = $row['benchmark_failure'];
            }
            $smsdata="";
            //bigoperator
            $bigkeys = array();
            $bigopr = array();
            $i=1;
            $failConst = $this->config->item('failure_const');
            $id = array();
            if(!empty($fifteen)){
                foreach($fifteen as $key=>$row){
                     if(array_key_exists($key, $operators_Name) && $fifteen_total>0 &&
                             ((($row['sale']/$fifteen_total)*100)>2 || $row['trans_count']>100) && $row['trans_count']>0 && (($row['failure']/$row['trans_count'])*100 >  round( $failConst * $failure[$key])) && $row['failure']>$minfail && ($row['failure']/$row['trans_count'])*100>5){
                        
                        $bigopr[$key] = $row;
                        $bigkeys[]=$key;
                        $id[] = $row['id'];
                        $benchmark = $failure[$key];
                        $fail = ($row['failure']/$row['trans_count'])*100;
                        $operator = $operators_Name[$key];
                        
                        $smsdata.="\n $i) $operator : ".round($fail,1)."% (".$row['failure']."/".$row['trans_count']." , 15 min)";
                        $i++;
                    }
                }
            }
            
            $data = $this->insertData($smsdata,$id,$curr_date,$start,$type);
            $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
            //mediumopr
            $mediumopr = array();
            $medsmsdata = '';
            $medium_id = array();
            if(!empty($thirty)){
                foreach($thirty as $key=>$row){
                    if(count($row['id'])==2 && array_key_exists($key, $operators_Name) && $thirty_total>0 &&  (($row['sale']/$thirty_total*100)>=1 && (($row['sale']/$thirty_total)*100)<=2 || ($row['trans_count']>50 )) && $row['trans_count']>0 && (($row['failure']/$row['trans_count'])*100 >  round( $failConst * $failure[$key])) && $row['failure']>$minfail && !in_array($key, $bigkeys) && ($row['failure']/$row['trans_count'])*100>5  ){
                        
                        $mediumopr[$key] = $row;
                        $bigkeys[]=$key;
                        $medium_id =array_merge($medium_id,$row['id']);
                        $benchmark = $failure[$key];
                        $fail = ($row['failure']/$row['trans_count'])*100;
                        $operator = $operators_Name[$key];
                        
                        $medsmsdata.="\n $i) $operator : ".round($fail,1)."% (".$row['failure']."/".$row['trans_count']." , 30 min)";
                        $i++;
                    }
                }
            }
            if(!empty($medsmsdata)){
               
                $smsdata.=$medsmsdata;
                
                $data = $this->insertData($medsmsdata,$medium_id,$curr_date,$start,$type);
                $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
            
            }
            
            //smallopr
            $smallopr = array();
            $smallsmsdata = '';
            $small_id = array();
            if(!empty($hour)){
                foreach($hour as $key=>$row){
                    if(count($row['id'])==4 && array_key_exists($key, $operators_Name) && $hour_total>0 && (($row['sale']/$hour_total)*100)<1  && $row['trans_count']>0 && (($row['failure']/$row['trans_count'])*100 >  round( $failConst * $failure[$key])) && $row['failure']>$minfail && !in_array($key, $bigkeys) && ($row['failure']/$row['trans_count'])*100>5){
                        
                        $smallopr[$key] = $row;
                        $benchmark = $failure[$key];
                        $fail = ($row['failure']/$row['trans_count'])*100;
                        $operator = $operators_Name[$key];
                        $small_id =array_merge($small_id,$row['id']);
                         
                        $smallsmsdata.="\n $i) $operator : ".round($fail,1)."% (".$row['failure']."/".$row['trans_count']." , 60 min)";
                         $i++;
                       
                    }
                }
            }
            if(!empty($smallsmsdata)){
               
                $smsdata.=$smallsmsdata;
                
                $data = $this->insertData($smallsmsdata,$small_id,$curr_date,$start,$type);
                $inser_data= (!empty($data)) ? array_merge($inser_data,$data) : $inser_data;
                
            }
            
            $sms = '';
            
            if(!empty($smsdata)){
                $sms=  sprintf($this->config->item('failuresms'),$smsdata);
                
            }
            
            if (!empty($inser_data)) {
            logerror("setTransaction JSON data : ". json_encode($inser_data),"alertquery_check");
            $insert_array=  createBatch($inser_data);
            foreach ($insert_array as $batch):
                $this->alertDB->insert_batch('past_transaction', $batch);
                if($errormsg=$this->alertDB->_error_message()):
                    $sql = $this->alertDB->last_query();
                    logerror("Query Failed :  {$this->alertDB->_error_message()} ","alertsystem");
                    logerror("Sql query : {$sql}","alertsystem");
                    send_email('swapnil@mindsarray.com','Add Transaction for alert system',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
                endif;
            endforeach;

        } 
            return $sms;
            
        endif;
        }
        
    }
    
    public function gettime(){
        $date = date('Y-m-d');
        $sql = "SELECT distinct transaction.timestamp "
                . "FROM  `transaction` "
                . "WHERE `date` = '$date' "
                . "ORDER BY 1 DESC "
                . "LIMIT 0 , 4";
        $result = $this->alertDB->query($sql);
        if($result->num_rows()==4):
            return $result->result_array();
        endif;
        return array();
    }
    
    public function get_activerm(){
        //select active rm
        $sql = "SELECT id,mobile "
                . "FROM rm "
                . "where active_flag=1";
        $result = $this->slaveDB->query($sql);
        $rm = $result->result_array();
        $active_rm = array();
        foreach($rm as $row){
            $active_rm[$row['id']] = $row['mobile'];
        }
        return  $active_rm;
    }
    public function rm_report(){
        logerror("Inside rm_report ","rm_alertsystem");
        //get active rm
        $rm = $this->get_activerm();
        $result= array();
        logerror("active rm ","rm_alertsystem");
        //get 1 month rm wise report
        $rm_monthreport = $this->rm_monthreport();
        logerror("month report ".json_encode($rm_monthreport),"rm_alertsystem");
        $rm_wkreport = $this->rm_wkreport();
        logerror("month report ".json_encode($rm_wkreport),"rm_alertsystem");
        $rm_ydayreport = $this->rm_ydayreport();
         logerror("month report ".json_encode($rm_ydayreport),"rm_alertsystem");
        if(!empty($rm_monthreport)){
            foreach($rm_monthreport as $key=>$row){
                    if(isset($rm[$key])){
                        $result[$key]['mobile'] = $rm[$key];
                        $result[$key]['month'] = $row;
                        $result[$key]['sms'] = "Last 30 days:( D/".$row['dist']."/ Avg Rs ".round(($row['sale']/$row['dist'])/30)." / Panel Avg Rs. ".round($row['sale']/30)." & R/".$row['ret']." / Avg Rs ".round(($row['sale']/$row['ret'])/30)." )";
                    }
            }
            foreach($rm_wkreport as $key=>$row){
                if(isset($rm[$key]))
                        $result[$key]['mobile'] = $rm[$key];
                if(isset($result[$key]['month'])){
                    $result[$key]['wk'] = $row;
                    $result[$key]['sms'] .= "\n Last 7 days:( D/".$row['dist']."/ Avg Rs ".round(($row['sale']/$row['dist'])/7)." / Panel Avg Rs. ".round($row['sale']/7)." & R/".$row['ret']." / Avg Rs ".round(($row['sale']/$row['ret'])/7)." )";
                }
            }
            foreach($rm_ydayreport as $key=>$row){
                if(isset($rm[$key]))
                        $result[$key]['mobile'] = $rm[$key];
                if(isset($result[$key]['month']) && isset($result[$key]['wk'])){
                    $result[$key]['yday'] = $row;
                    $result[$key]['sms'] .= "\n Yesterdays :( D/".$row['dist']."/ Avg Rs ".round($row['sale']/$row['dist'])." / Panel Avg Rs. ".round($row['sale'])." & R/".$row['ret']." / Avg Rs ".round($row['sale']/$row['ret'])." )";
                }
            }
        }
        logerror("rm report".json_encode($result),"rm_alertsystem");
        return $result;
    }
    
    public function rm_wkreport(){
        $date = new DateTime();
        $current_dt = $date->sub(new DateInterval('P1D'))->format('Y-m-d');
        $date = new DateTime();
        $onemonth_dt = $date->sub(new DateInterval('P7D'));
        $onemonth_dt = $onemonth_dt->format('Y-m-d');
        $sql = "SELECT SUM( rlog.sale ) AS tertiery, d.rm_id, COUNT( DISTINCT d.id ) AS dist, "
                . "COUNT( DISTINCT retailer_id ) AS retailer "
                . "FROM retailers_logs AS rlog "
                . "LEFT JOIN retailers AS rt ON ( rt.id = rlog.retailer_id ) "
                . "LEFT JOIN distributors AS d ON ( d.id = rt.parent_id ) "
                . "WHERE rlog.date >=  '$onemonth_dt' "
                . "AND rlog.date <=  '$current_dt' AND d.active_flag=1 and d.rm_id>0 AND  rt.block_flag!=2 "
                . "GROUP BY d.rm_id";
        $result = $this->slaveDB->query($sql);
        if($result->num_rows()):
           $rm = array();
            $data = $result->result_array();
         foreach($data as $row){
            $rm[$row['rm_id']]['dist'] = $row['dist'];
            $rm[$row['rm_id']]['ret'] = $row['retailer'];
            $rm[$row['rm_id']]['sale'] = $row['tertiery'];
        }
            return $rm;
        endif;
        return array();
    }
    
    public function rm_ydayreport(){
        $date = new DateTime();
        $yday_dt = $date->sub(new DateInterval('P1D'))->format('Y-m-d');
        $sql = "SELECT SUM( rlog.sale ) AS tertiery, d.rm_id, COUNT( DISTINCT d.id ) AS dist, "
                . "COUNT( DISTINCT retailer_id ) AS retailer "
                . "FROM retailers_logs AS rlog "
                . "LEFT JOIN retailers AS rt ON ( rt.id = rlog.retailer_id ) "
                . "LEFT JOIN distributors AS d ON ( d.id = rt.parent_id ) "
                . "WHERE rlog.date =  '$yday_dt' "
                . " AND d.active_flag=1 and d.rm_id>0 AND  rt.block_flag!=2 "
                . "GROUP BY d.rm_id";
        $result = $this->slaveDB->query($sql);
        if($result->num_rows()):
           $rm = array();
            $data = $result->result_array();
         foreach($data as $row){
            $rm[$row['rm_id']]['dist'] = $row['dist'];
            $rm[$row['rm_id']]['ret'] = $row['retailer'];
            $rm[$row['rm_id']]['sale'] = $row['tertiery'];
        }
            return $rm;
        endif;
        return array();
    }
    
    public function rm_monthreport(){
        $date = new DateTime();
        $current_dt = $date->sub(new DateInterval('P1D'))->format('Y-m-d');
        $date = new DateTime();
        $onemonth_dt = $date->sub(new DateInterval('P30D'));
        $onemonth_dt = $onemonth_dt->format('Y-m-d');
         $sql = "SELECT SUM( rlog.sale ) AS tertiery, d.rm_id, COUNT( DISTINCT d.id ) AS dist, "
                . "COUNT( DISTINCT retailer_id ) AS retailer "
                . "FROM retailers_logs AS rlog "
                . "LEFT JOIN retailers AS rt ON ( rt.id = rlog.retailer_id ) "
                . "LEFT JOIN distributors AS d ON ( d.id = rt.parent_id ) "
                . "WHERE rlog.date >=  '$onemonth_dt' "
                . "AND rlog.date <=  '$current_dt' AND d.active_flag=1 and d.rm_id>0 AND  rt.block_flag!=2 "
                . "GROUP BY d.rm_id";
        $result = $this->slaveDB->query($sql);
        if($result->num_rows()):
            $rm = array();
            $data = $result->result_array();
         foreach($data as $row){
            $rm[$row['rm_id']]['dist'] = $row['dist'];
            $rm[$row['rm_id']]['ret'] = $row['retailer'];
            $rm[$row['rm_id']]['sale'] = $row['tertiery'];
        }
            return $rm;
        endif;
        return array();
    }
    
    public function dist_zerosale(){
       $date = new DateTime();
       $yday = $date->sub(new DateInterval('P1D'))->format('Y-m-d');
       $sql = "SELECT d.rm_id, dlog.distributor_id, d.user_id, u.mobile "
             . "FROM  `distributors_logs` AS dlog "
             . "INNER JOIN distributors AS d ON ( d.id = dlog.distributor_id ) "
             . "INNER JOIN users AS u ON ( u.id = d.user_id ) "
             . "WHERE d.rm_id !=0 AND d.active_flag =1 "
             . "AND dlog.date =  '$yday' AND dlog.distributor_id NOT IN "
                . " ( "
                . "SELECT rt.parent_id "
                . "FROM retailers_logs AS rlog "
                . "INNER JOIN retailers AS rt ON ( rt.id = rlog.retailer_id )  "
                . "WHERE rlog.date =  '$yday' "
                . ")  ORDER BY 1";
       logerror("dist_zerosale sql ".$sql,"rm_alertsystem");
        $result = $this->slaveDB->query($sql);
        if($result->num_rows()):
            $rm = array();
            $data = $result->result_array();
            logerror("dist_zerosale data 1 ","rm_alertsystem");
            $arr = array();
            foreach($data as $row){
              // $rm[$row['rm_id']]['dist'] = $row['distributor_id'];
                if(in_array($row['rm_id'], $arr)){
                    $rm[$row['rm_id']]['dist_mobile'] .=",".$row['mobile'];
                }else{
                    $rm[$row['rm_id']]['dist_mobile'] = $row['mobile'];
                }

               $arr[]=$row['rm_id'];
           }
            logerror("dist_zerosale data 2 ","rm_alertsystem");
            return $rm;
        endif;
        return array();
    }
    
    public function get_trans_retailer($date=null){
        if(empty($date)){
            $date = new DateTime();
            $second_dt = $date->sub(new DateInterval('P2D'))->format('Y-m-d');
             $date = new DateTime();
            $yday = $date->sub(new DateInterval('P1D'))->format('Y-m-d');
        }
        $sql = "SELECT d.rm_id,d.id,dlog.topup_unique,date, u.mobile "
                . "FROM distributors_logs as dlog "
                . "LEFT JOIN distributors AS d ON ( d.id = dlog.distributor_id ) "
                . "INNER JOIN users AS u ON ( u.id = d.user_id ) "
                . "WHERE d.rm_id !=0 "
                . "AND d.active_flag =1 "
                . "AND dlog.date >='$second_dt'";
        logerror("get_trans_retailer sql ".$sql,"rm_alertsystem");
        $result = $this->slaveDB->query($sql);
        if($result->num_rows()):
            $data = $result->result_array();
        logerror("get_trans_retailer data 1  ","rm_alertsystem");
            $two_day = array();
            $y_day = array();
            foreach($data as $row){
                if($second_dt==$row['date']){
                    $two_day[$row['id']] = $row;
                }
               elseif($yday==$row['date']) {
                   $y_day[$row['id']] = $row;
               }
            }
            $arr = array();
            $distributor = array();
            foreach($two_day as $key=>$row){
                if(isset($y_day[$key]) && $two_day[$key]['topup_unique']>0 && $y_day[$key]['topup_unique']/$two_day[$key]['topup_unique']<0.80){
                        if(in_array($row['rm_id'], $arr)){
                            $distributor[$row['rm_id']] .= ",".$row['mobile'];
                        }else{
                            $distributor[$row['rm_id']] = $row['mobile'];
                        }
                        $arr[]=$row['rm_id'];
                }
            }
            logerror("get_trans_retailer data 2  ","rm_alertsystem");
            return $distributor;
        endif;
    }
    
    public function addInAlert($data){
        logerror("data for insert query  ","InsideAlert");
        $this->alertDB->insert('central_alert', $data);
        if($errormsg=$this->alertDB->_error_message()):
            $sql = $this->alertDB->last_query();
            logerror("Query Failed :  {$this->alertDB->_error_message()} ","alertsystem");
            logerror("Sql query : {$sql}","alertsystem");
            send_email('swapnil@mindsarray.com','Add Transaction for alert system',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
        endif;
    }   
    
    public function addmodem($data){
        logerror("data for insert query  ","modem_log");
        $this->alertDB->insert('modem', $data);
        if($errormsg=$this->alertDB->_error_message()):
            $sql = $this->alertDB->last_query();
            logerror("Query Failed :  {$this->alertDB->_error_message()} ","modem_log");
            logerror("Sql query : {$sql}","modem_log");
            send_email('swapnil@mindsarray.com','Add Transaction for alert system',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
        endif;
    }

    public function getup_modem($lasttime){
        $query = "";
    }
}


