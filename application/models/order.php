<?php

class Order extends CI_Model
{

    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getSuppliers($operator_id)
    {
        $sql="SELECT s.id, s.name,so.frequency ,so.commission_type_formula,so.commission_type,ps.base_amount,so.operator_id,so.id as supplier_operator_id,so.batch,"
                . " so.capacity,so.min_capacity,so.capacity_per_day,so.capacity_per_month,so.riskfactor, "
                . " GROUP_CONCAT( v.company ) as modemnames ,GROUP_CONCAT( ivm.vendor_id ) as modemids "
                     . " FROM inv_suppliers s "
                     . " JOIN inv_supplier_operator so "
                     . " ON s.id = so.supplier_id "
                     . " LEFT JOIN inv_planning_sheet ps "
                     . " ON ps.supplier_operator_id = so.id   "
                     . " LEFT JOIN inv_supplier_vendor_mapping ivm "
                     . " ON s.id = ivm.supplier_id  "
                     . " LEFT JOIN vendors v "
                     . " ON ivm.vendor_id = v.id "
                     . " WHERE so.operator_id='{$operator_id}' and so.is_active='1'  ";
         
         if($operator_id!=54):
                $sql.=" AND s.is_api<>'1'  ";
         endif;           
         
          $sql .= " GROUP BY so.id";
          
         $query=$this->slaveDB->query($sql);
        
         return $query->result_array();
                     
    }
    
    public function getSupplierWiseSimdata($supplier_operator_id)
    {
        $date=  date('Y-m-d',  strtotime('-1 day'));
        // Note SUM(closing) is used as opening
        
        //Also if sync_timestamp is of previous date that means modem was stopped so in this case balance=closing
        //SUM(if(DATE(sync_timestamp)=(SUBDATE(CURDATE(),1)),balance,closing))
        $query="SELECT SUM(if(block=1,0,closing)) as closing,SUM(tfr) as lastdayincoming,count(id) as totalSims,TRUNCATE((SUM(sale)/COUNT(id)),2) as persimsale,SUM(CASE WHEN block=1 THEN balance ELSE 0 END) as blockedbalance"
                     . " FROM devices_data "
                     . " WHERE supplier_operator_id='{$supplier_operator_id}' "
                    . " AND sync_date='{$date}'";

        // Exclude B2C modem #29
        // $query.=" AND vendor_id<>29 ";           
             
           $result=$this->slaveDB->query($query);
        
           $data=$result->row();
           
           return $data;
    }
    
    public function getPerDayCapacity($so_id)
    {
        $sql="Select capacity_per_day from inv_supplier_operator where id='{$so_id}'";
        
        $query=$this->slaveDB->query($sql);
         
        $data=$query->row();
        
        return $data->capacity_per_day;
    }
    
    public function getPerDayMonth($so_id)
    {
        $sql="Select capacity_per_month from inv_supplier_operator where id='{$so_id}'";
        
        $query=$this->slaveDB->query($sql);
         
        $data=$query->row();
        
        return $data->capacity_per_month;
    }
    
    public function getPerDayOrder($so_id)
    {
        $sql="Select capacity from inv_supplier_operator where id='{$so_id}'";
        
        $query=$this->slaveDB->query($sql);
         
        $data=$query->row();
        
        if($data->capacity>0):
       
            return $data->capacity;
        
        endif;
        
        return 0;
       
    }
    
    public function getOrderAmountTillNow($so_id)
    {
        $startDate=date('Y-m-01');
        
        $endDate=date('Y-m-t');
        
        $sql="Select SUM(amount)  as amount "
                . " FROM inv_orders "
                . " WHERE DATE(order_date) BETWEEN '{$startDate}' AND '{$endDate}' "
                . " AND supplier_operator_id='{$so_id}' "
                . "  AND is_payment_done='1' AND is_downloaded='1' AND is_approved='1' ";
        
        $query=$this->slaveDB->query($sql);
         
           if($query->num_rows()):
               
                $data=$query->row();
        
                 return ($data->amount>0)?$data->amount:0;
          
           endif;
           
           return 0;
           
       
    }
    
    public function getBlockedBalance($so_id)
    {
        return 0;
        //$sql=""
    }
    
    public function getSupplierWiseLastDaySale($supplier_operator_id=0,$avg=false)
    {
        $date=  date('Y-m-d',  strtotime('-1 day'));
       
        $past7day=date('Y-m-d',  strtotime("-8 days"));
         
        $select=" Select supplier_operator_id, SUM( sale ) AS lastdaysale";
      
       if($avg):
                     $select=" Select supplier_operator_id, SUM(sale)/7 AS avgsale ";
        endif;
        
         $sql=" $select   "
                . " FROM devices_data  Where 1 ";
        
         if($avg):
            $sql.=" AND sync_date >='{$past7day}' AND sync_date <='{$date}' ";
        else:
            $sql.=" AND sync_date='{$date}' ";
        endif;
        
        if($supplier_operator_id>0):

            $sql.=" AND supplier_operator_id='{$supplier_operator_id}' ";

        endif;
         
        $sql .= " GROUP BY supplier_operator_id ";
          
         $query=$this->slaveDB->query($sql);        
          
         $data=array();
         
          if($query->num_rows()):
              
                foreach($query->result() as $key=>$value):
                    
                                $data[$value->supplier_operator_id]=$avg?$value->avgsale:$value->lastdaysale;
              
                endforeach;
              
          endif;
          
          return $data;;
          
    }
    
    public function saveOrder($orders,$operator_id)
    {
        $date=date('Y-m-d');
        
        // Delete from comments since order is generated twice
        //$deletefromcomments="Delete from inv_comments where order_id IN (Select id from inv_orders where operator_id='{$operator_id}' AND order_date='{$date}')";
        //$this->db->query($deletefromcomments);
        // Delete all previous order entries with current date
        //$this->db->delete('inv_orders',array('operator_id'=>$operator_id,'order_date'=>date('Y-m-d')));
        
                foreach($orders as $order):

                        if($order['in_list']=="1" && $order['is_freezed']!="1"):

                              if(!$order_id=$this->checkIfOrderExistsBySOID($order['supplier_operator_id'])):
                                  
                               
                                            $orderamount=$this->getOrderAmountbyBounds($order['order'],$order['supplier_operator_id']);
                                            $topay=$this->isUpperBound($order['supplier_operator_id'])?$order['order']:floor(($order['order']-($order['order']*($order['commission_type_formula']/100))));

                                            $this->db->set('order_date',date("Y-m-d"));

                                            $data=array(
                                                                       'supplier_operator_id'=>$order['supplier_operator_id'],
                                                                       'amount'=>$order['order'],
                                                                       'amount_by_bounds'=>$orderamount, 
                                                                       'to_pay'=>$topay,
                                                                       'created_by'=>  getLoggedInUserId(),
                                                                       'margin'=>$order['commission_type_formula'],
                                                                       'supplier_id'=>$order['id'],
                                                                       'operator_id'=>$order['operator_id'],
                                                                       'manual_order_flag'=>isset($order['manualorder'])?$order['manualorder']:'0',
                                                                       'no_of_days'=>$order['no_of_days'],
                                                                       'batch'=>$order['batch']
                                                                   );

                                                if($this->db->insert('inv_orders',$data)):

                                                    $insert_id=$this->db->insert_id();

                                                    // dont insert  if there is status 0 entry in inv_payments table before inserting a new zero entry
                                                    // Added for extra security check
                                                    if(!$this->ifOldStatusZeroEntryExists($insert_id)):
                                                          $this->db->insert('inv_payments',array('order_id'=>$insert_id,'so_id'=>$order['supplier_operator_id'],'amount'=>$order['order'],'to_pay'=>$topay,'amount_by_bounds'=>$orderamount,'payment_date'=>$date));
                                                    endif;
                                                    
                                                endif;
                                   
                             else:
                               
                                           $orderamount= $this->getOrderAmountbyBounds($order['order'],$order['supplier_operator_id']);
                                           $topay=$this->isUpperBound($order['supplier_operator_id'])?$order['order']:floor(($order['order']-($order['order']*($order['commission_type_formula']/100))));
                                           $this->db->set('order_date',date("Y-m-d"));
                                           $data=array(
                                                                       'amount'=>$order['order'],
                                                                       'amount_by_bounds'=>$orderamount, 
                                                                       'to_pay'=>$topay,
                                                                       'created_by'=>  getLoggedInUserId(),
                                                                       'margin'=>$order['commission_type_formula'],
                                                                       'manual_order_flag'=>isset($order['manualorder'])?$order['manualorder']:'0',
                                                                       'no_of_days'=>$order['no_of_days'],
                                                                   );
                                          
                                            $this->db->update('inv_orders',$data,array('id'=>$order_id));  
                                            
                                            $this->db->update('inv_payments',array('amount'=>$order['order'],'amount_by_bounds'=>$orderamount,'to_pay'=>$topay),array('order_id'=>$order_id,'status'=>'0'));
                                    
                             endif;     
                                  
                       endif;
                   
                endforeach;
                
        
      return true;
        

    }
    
    function ifOldStatusZeroEntryExists($order_id)
    {
        $sql="Select id from inv_payments where status='0' AND order_id='{$order_id}' ";
        
         $query = $this->db->query($sql); 
         
         if($query->num_rows()):
             return true;
         endif;
         
         return false;
         
    }
    function checkIfOrderExistsBySOID($soid)
    {
        $date=date('Y-m-d');
        
        $this->db->where(array('supplier_operator_id'=>$soid,'order_date'=>$date));
      
        $query = $this->db->get('inv_orders');
            

        if ($query->num_rows() > 0):
                
                 $data= $query->row();
                 return $data->id;
              
            
            endif;
            
       return false;
    }
    function getOrderAmountbyBounds($amount,$so_id)
    {
        $sql="Select commission_type,commission_type_formula from inv_supplier_operator where id='{$so_id}'";
        $query = $this->db->query($sql); 
        $data= $query->row();
        
        if($data->commission_type=="2"):
            
                return (($amount)+(($amount*$data->commission_type_formula)/100));
        
        endif;
        
        return $amount;
        
    }
    
    function isUpperBound($so_id)
    {
        $sql="Select commission_type  from inv_supplier_operator where id='{$so_id}'";
        $query = $this->slaveDB->query($sql); 
         $data= $query->row();
        if($data->commission_type=="2"):
                return true;
        endif;
        
        return false;
    }
    
    function getOrder($operator_id)
    {
        $date=date('Y-m-d');
        
        $sql="select id,no_of_days from inv_orders where operator_id='{$operator_id}' and order_date='$date' limit 1";
        
        $query=$this->slaveDB->query($sql);        
        
        $data=$query->row();
        
         if($query->num_rows()):
             
             return array('exists'=>true);
             
         else:
             
              return array('exists'=>false);
         
         endif;
    }
    
    function getexistingOrderAmt($operator_id)
    {
        
        $orders=array();
        
         $date=date('Y-m-d');
          
        $sql="select supplier_operator_id,amount,is_approved from inv_orders where operator_id='$operator_id' AND order_date='$date'";
        
        $query=$this->slaveDB->query($sql);        
        
        $approved=array();
        
        $disapproved=array();
        
        foreach($query->result() as $row):
            
              if($row->is_approved=="1" || $row->is_approved=="2"):
              $approved[$row->supplier_operator_id]=$row->amount;
              else:
               $disapproved[$row->supplier_operator_id]=$row->amount;   
              endif;
              
        endforeach;
        
        $orders['approved']=$approved;
        
        $orders['disapproved']=$disapproved;        
        
        return $orders;
       
    }
    
    public function getexistingOrderDays($operator_id)
    {
        $days=array();
        
        $date = date('Y-m-d');

        $sql = "select supplier_operator_id,no_of_days from inv_orders where operator_id='$operator_id' AND order_date='$date'";

        $query = $this->slaveDB->query($sql);
        
        foreach($query->result_array() as $row):
                    $days[$row['supplier_operator_id']]=$row['no_of_days'];
        endforeach;
        
        return $days;
    }
    
    
    function getTotalLastDaySale($operator_id=0)
    {
        $date=date('Y-m-d',  strtotime("-1 days"));
        
        $sql="Select SUM(amount) as total_last_day_sale "
                . "  from vendors_activations  va"
                . "  join products p"
                . "  on (p.id=va.product_id)"
                . " where  date='{$date}'  and status NOT IN (2,3) ";
        
         if(!empty($operator_id)):
         
             $sql.=" and p.parent = '{$operator_id}' ";
             
         endif;
         
        $query=$this->slaveDB->query($sql);        
        
        if($query->num_rows()):
            
                return $query->row();
        
        endif;
        
        return false;
    }
    
    
    function getTotalFailurePercentage($operator_id=0)
    {
         $date=date('Y-m-d',  strtotime("-1 days"));
        
        $sql="Select count(id) as Totaltransactions,SUM(CASE when status=2  OR status=3 then 1 else 0 END) as Totalfailed  "
                . "  from vendors_activations  "
                . " where  date='{$date}' ";
        
       if(!empty($operator_id)):
         
             $sql.=" and product_id IN ({$operator_id}) ";
             
         endif;
         
        $query=$this->slaveDB->query($sql);        
        
        if($query->num_rows()):
            
                return $query->row();
        
        endif;
        
        return false;
    }
    
    
    function getCurrentBalance($operator_id)
    {
        $date=date('Y-m-d',  strtotime("-1 days"));
        
        $sql="Select SUM(closing) as balance "
                . " from devices_data "
                . " where opr_id IN ({$operator_id}) "
                . "  And sync_date='{$date}' ";
                
        $query=$this->slaveDB->query($sql);        
        
        if($query->num_rows()):
            
                $data=$query->row();
        
                return is_null($data->balance)?"0.00":$data->balance;
        
        endif;
        
        return false; 
    }
    
    
    function getapiSale($operator_id,$avg=false)
    {
         
         $date=date('Y-m-d',  strtotime("-1 days"));
         $past7day=date('Y-m-d',  strtotime("-8 days"));
     
      $select=" SUM(va.amount) as totalAPIsale ";
      
       if($avg):
                     $select=" SUM(va.amount)/7 as totalAPIsale ";
        endif;
       
        $sql="select $select  "
                . " from vendors_activations  va  USE INDEX (idx_date) JOIN vendors v   "
                . " ON va.vendor_id=v.id  "
                . " Where v.machine_id=0  "
                . " and va.product_id IN ({$operator_id})  and status NOT IN(2,3) ";
         
        if($avg):
            $sql.=" AND va.date>='{$past7day}'  AND va.date<='{$date}' ";
        else:
            $sql.=" AND  va.date='{$date}' ";
        endif;
        
       $query=$this->slaveDB->query($sql);        
       
        if($query->num_rows()):
            
                $data=$query->row();
        
                return is_null($data->totalAPIsale)?"0.00":$data->totalAPIsale;
        
        endif;
        
        return false;          
    }
    
    function getlast7days24hrSale()
    {
        $last7thdaydate=date('Y-m-d',strtotime("-8 days"));
        
        $lastdaydate=date('Y-m-d',strtotime("-1 days"));
        
        $sql=" select date,sale,hour "
                . " from float_logs "
                . " where date >='{$last7thdaydate}' "
                . " and date <='{$lastdaydate}' "
                . " and hour=24";
                
         $query=$this->slaveDB->query($sql);                 
         
          if($query->num_rows()):
              
                return $query->result();
              
          endif;
    }
    
    public function getAB()
    {
        $day1=date('Y-m-d',strtotime('-14 days'));
        $day2=date('Y-m-d',strtotime('-7 days'));
        
        $sql="select hour, AVG(sale) as  avgsale "
                . " from float_logs "
                . " where date>='{$day1}' and date<='{$day2}' "
                . " group by hour having hour in (10,24)";
                
       $query=$this->slaveDB->query($sql);                 
         
        if($query->num_rows()):

              return $query->result();

        endif;
                
    }
    
    function getexpectedsalesbetweendates($date1,$date2)
    {
        $sql="Select date,hour,sale from float_logs where date >='{$date1}' and date<='{$date2}'  and hour IN (10,24)";
        
         $query=$this->slaveDB->query($sql);                 
         
        if($query->num_rows()):

              $date = $query->result();
              
               $result=array();
               
              foreach ($date as $value):
                  
                        if($value->hour==10):
                        $result[$value->date]['10hr']=$value->sale;
                        else:
                          $result[$value->date]['24hr']=$value->sale;   
                        endif;
                    
              endforeach;

              
              return $result;
              
        endif;
        
        return ;
        }
        
        function getLastPending($so_id)
        {
            $sql="Select SUM(pending) as pending "
                    . " from inv_pendings "
                    . " where supplier_operator_id='{$so_id}' "
                    . " and pending_date=(Select MAX(pending_date) from inv_pendings where supplier_operator_id='{$so_id}')  ";
            
           $query=$this->slaveDB->query($sql);     
           
            if($query->num_rows()):
                
                $data=$query->row();
            
                return !is_null($data->pending)?$data->pending:0.00;
                
            endif;
            
             return '0.00';
        }
        
        function deleteallorders($operator_id)
        {
            $date=date('Y-m-d');
            $this->db->query("Delete from inv_orders where operator_id='{$operator_id}' and order_date='{$date}' ");
        }
        
        function getFreezeFlag($so_id)
        {
            $date=date('Y-m-d');
            
            $sql="Select is_downloaded,is_payment_done from inv_orders where order_date='{$date}' and supplier_operator_id='{$so_id}' ";
            
            $query=$this->slaveDB->query($sql);     
            
             if($query->num_rows()):
                 
                      $data=$query->row();
             
                      if($data->is_downloaded=="1"):
                                return 1;
                      else:
                                if($data->is_downloaded=="2"  && $data->is_payment_done=="0"):
                                    return 1;
                                else:
                                    return 0;
                                endif;
                                
                      endif;
                        
             endif;
             
             return 0;
        }
        
        // Can be optimized by removind operator_id as SOID is enough
        public function getLastDayIncomingBySoidandModemids($so_id,$modemids,$operator_id,$dateparam=null)
        {
            $date=is_null($dateparam)?date('Y-m-d',strtotime('-1 days')):$dateparam;
            $modemids= urldecode($modemids);
            $opids=  combineOperator($operator_id);
            $sql="select s.name as supplier,p.name as operator,v.company as modemname,mobile,sync_date,sync_timestamp,dd.balance,opening,tfr as incoming,closing,dd.server_diff,dd.sale,dd.inc,dd.block,dd.active_flag "
                    . " from devices_data dd "
                    . " JOIN products p  "
                    . " ON dd.opr_id=p.id "
                    . " JOIN vendors  v "
                    . " ON dd.vendor_id=v.id "
                    . " JOIN inv_suppliers s  "
                    . " ON s.id=dd.inv_supplier_id "
                    . " where opr_id IN({$opids})  "
                    . " and  vendor_id IN ({$modemids}) "
                    . " and  sync_date='{$date}' "
                    . " and supplier_operator_id='{$so_id}' ";
                    
                    
         // Exclude B2C modem #29
        // $sql.=" AND vendor_id<>29 ";                     
     
         
         $query=$this->slaveDB->query($sql);               
                    
          if($query->num_rows()):
                return $query->result();
          endif;   
          
          
          return;
            
        }
        
        public function getLastdayOrderamtBySoid($so_id)
        {
            $date=date('Y-m-d',strtotime('-1 day'));
            $sql="Select amount_by_bounds from inv_orders where order_date='{$date}' and supplier_operator_id='{$so_id}'";
            $query=$this->slaveDB->query($sql);    
             if($query->num_rows()):
                   $data=$query->row();
                    return $data->amount_by_bounds;
             endif;
             return 0;
        }

       public function getLastavaliablePendingExceptCurrent($so_id)
       {
             $date=date('Y-m-d',strtotime('-2 days'));
             
             $sql="Select pending "
                . " from inv_pendings "
                . " where supplier_operator_id='{$so_id}' AND  pending_date<='{$date}'"
                . " order by pending_date desc "
                . " limit 1 ";
                
         $query = $this->slaveDB->query($sql);      
                 
         if($query->num_rows()):
             
                 $data=$query->row();
         
                 return $data->pending;
                 
         endif;    
         
         return 0;
       }
       
       public function saveCommentsBySOID($soid,$comment)
       {
            if(trim($comment)!=""):
              $this->db->insert('inv_comments',array('comment_timestamp'=>date('Y-m-d H:i:s'),'supplier_operator_id'=>$soid,'order_id'=>0,'comment'=>$comment,'user_id'=>  getLoggedInUserId()));
           endif;
           
            return true;
       }
       
       public function getvendoridsbysupplierid($supplier_id)
       {
           $sql="Select vendor_id  from inv_supplier_vendor_mapping where supplier_id='{$supplier_id}' ";
          
           $query = $this->slaveDB->query($sql);  
          
            $temp=array();
            
             if($query->num_rows()):
                       foreach($query->result_array() as $row):
                                        $temp[]=$row['vendor_id'];
                        endforeach;
             endif;
             
             return $temp;
       }
       
       public function getOperatorWiseTotalPending($operator_id)
       {
           if($operator_id):
               $date=date('Y-m-d',strtotime('-1 days'));
                $sql=" SELECT SUM( p.pending ) as pending "
                   . " FROM inv_supplier_operator so "
                   . " JOIN inv_pendings p "
                   . " ON ( so.id = p.supplier_operator_id ) "
                   . " WHERE so.operator_id ='{$operator_id}' "
                   . " AND p.pending_date = '{$date}' AND so.is_active =1 GROUP BY p.operator_id ";
                $query = $this->slaveDB->query($sql);  
                 if($query->num_rows()):
                            $pending=$query->row_array();
                            return $pending['pending'];
                 endif;
           endif;
           
           return 0;
       }
       
         function getRegeneratedOrderDetails($orderids)
       {
           $date=date('Y-m-d');
             
           $sql="Select so.id as supplier_operator_id,s.name as supplier,p.name as operator,amount as oldorder,"
                   . " ps.base_amount as basesale,o.margin,GROUP_CONCAT(svm.vendor_id) as vendorids,GROUP_CONCAT(v.company) as modemnames,o.id as orderid,so.commission_type  "
                   . " from inv_orders o  "
                   . " JOIN inv_supplier_operator so"
                   . "  ON o.supplier_operator_id=so.id "
                   . " JOIN inv_planning_sheet ps "
                   . " ON o.supplier_operator_id=ps.supplier_operator_id "
                   . " JOIN inv_suppliers s "
                   . " ON so.supplier_id=s.id "
                   . " JOIN products p "
                   . " ON p.id=so.operator_id "
                   . " JOIN inv_supplier_vendor_mapping svm  "
                   . " ON svm.supplier_id=s.id "
                   . " JOIN vendors v "
                   . " ON v.id=svm.vendor_id "
                   . " Where o.order_date='{$date}' "
                   . " and o.id IN ({$orderids}) "
                   . " group by o.supplier_operator_id ";
              
          $query = $this->slaveDB->query($sql);      
                 
         if($query->num_rows()):
             
                 return $query->result_array();
                 
         endif;    
         
         return 0;          
                   
       }
       
       function getToPayBySoid($amount,$soid)
       {
                $sql="Select commission_type,commission_type_formula from inv_supplier_operator where id='{$soid}'";
                $query = $this->slaveDB->query($sql); 
                $data= $query->row_array();
                
                if($data['commission_type']=="1"):
                        $amount=((100-$data['commission_type_formula'])/100)*$amount;
                endif;
                
                return $amount;
       }
       
       function setRegenerateOrderAmt($data)
       {
           foreach ($data as $row):
                
                $amount_by_bounds=  $this->getOrderAmountbyBounds($row['amount'],$row['soid']);
                $topay=  $this->getToPayBySoid($row['amount'],$row['soid']);
                
                $update_array=array(
                                                            'amount'=>$row['amount'],
                                                            'to_pay'=>$topay,
                                                            'amount_by_bounds'=>$amount_by_bounds,
                                                            'is_downloaded'=>'2',
                                                            'is_payment_done'=>'2',
                                                            'txnid'=>'',
                                                            'no_of_days'=>$row['days'],
                                                        );
        
                // Check if there an row with status=0 in inv_payments tables                                    

                if(!$this->ifOldStatusZeroEntryExists($row['orderid'])):   

                    $this->db->update('inv_orders',$update_array,array('id'=>$row['orderid']));  

                    $this->db->insert('inv_payments',array('order_id'=>$row['orderid'],'amount'=>$row['amount'],'to_pay'=>$topay,'amount_by_bounds'=>$amount_by_bounds,'so_id'=>$row['soid'],'payment_date'=>date('Y-m-d')));

                endif; 
          
        endforeach;
        
        
       }
       
       public function getMaxCapBySoid($soid)
       {
//           $sql="Select capacity_per_day as capacity from inv_supplier_operator where id='{$soid}' ";
           $sql="Select capacity from inv_supplier_operator where id='{$soid}' ";
           
          $query = $this->slaveDB->query($sql);      
                 
         if($query->num_rows()):
             
                 return $query->row();
                 
         endif;    
         
         return 0;     
       }
       
       public function getCapacityBySOID($soid)
       {
           $sql="Select so.id as soid,so.capacity,s.name as supplier from inv_supplier_operator so join inv_suppliers s on (s.id=so.supplier_id) where so.id in ($soid) ";
           
           $query = $this->slaveDB->query($sql);   
           
           $temp=array();
           
           if($query->num_rows()):
             
                 $data= $query->result_array();
           
                 foreach ($data as $val):
                    $temp[$val['soid']]=$val;
                 endforeach;
                 
                 return $temp;
            endif;    
         
         return false;     
       }
}

