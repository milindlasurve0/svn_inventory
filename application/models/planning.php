<?php

class Planning extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    function saveTarget($target)
    {
      
          // Set Modem targets
        if(!empty($target->modem_targets)):
                
            foreach($target->modem_targets as $row):
        
                           if(!$this->checkifModemTargetExists($row)):
                                
                               $this->db->insert('inv_modem_planning_sheet',array(
                                                                                                'supplier_operator_id'=>$row->supplier_operator_id,
                                                                                                'vendor_id'=>$row->vendor_id,
                                                                                                'target'=>!empty($row->target)?$row->target:0,
                                                                                                 )
                                               );
                           else:
                               
                                $this->db->update('inv_modem_planning_sheet',array(
                                                                                                                                     'target'=>!empty($row->target)?$row->target:0
                                                                                                                                        ),
                                                                                                                             array(
                                                                                                                                  'supplier_operator_id'=>$row->supplier_operator_id,
                                                                                                                                    'vendor_id'=>$row->vendor_id,
                                                                                                                             )           
                                               );
                           
                           endif;
                           
            endforeach;
             
        endif;
           
             if(!$this->checkIfPlanningAlreadyExists($target->supplier_operator_id)):
        
            $this->db->set('last_planned_date',date("Y-m-d"));
        
            $this->db->insert('inv_planning_sheet',array(
                                                                                                'supplier_operator_id'=>$target->supplier_operator_id,
                                                                                                'planned_sale'=>!empty($target->planned_sale)?$target->planned_sale:0,
                                                                                                'max_sale_capacity'=>$target->max_sale_capacity,
                                                                                                'last_planned_date'=>date('Y-m-d H:i:s')
                                                                                                 )
                                               );
            else:
                
              $this->db->update('inv_planning_sheet',array(
                                                                                                'planned_sale'=>!empty($target->planned_sale)?$target->planned_sale:0,
                                                                                                'max_sale_capacity'=>$target->max_sale_capacity,
                                                                                                 'last_planned_date'=>date('Y-m-d H:i:s')
                                                                                                 ),
                                                                                          array('supplier_operator_id'=>$target->supplier_operator_id)       
                                               );
            
            endif;
            
        return true;
    }
    
    
     public function checkIfPlanningAlreadyExists($supplier_operator_id)
        {
            
            $this->slaveDB->where('supplier_operator_id',$supplier_operator_id);
            $query = $this->slaveDB->get('inv_planning_sheet');
            
            if ($query->num_rows() > 0):
                
                return true;
            
            endif;
            
            return false;
        }
        
     public function checkifModemTargetExists($data)
        {
            
            $query = $this->slaveDB->get_where('inv_modem_planning_sheet',array('supplier_operator_id'=>$data->supplier_operator_id,'vendor_id'=>$data->vendor_id));
            
            if ($query->num_rows() > 0):
                
                return true;
            
            endif;
            
            return false;
        }
    
        public function getLastdaySaleByOprId($operator_id,$avg=false,$current=false,$groupbyvendor=false)
        {
           
            $oprids=combineOperator($operator_id);
            
            if(!$current):  
            $date=  date('Y-m-d',  strtotime('-1 day'));
            else:
            $date=  date('Y-m-d');   
            endif;
            
            $past7day=date('Y-m-d',  strtotime("-8 days"));
            
          if(!$current):  
          $select=" Select supplier_operator_id, SUM( sale ) AS lastdaysale,vendor_id ";
          else:
           $select=" Select supplier_operator_id, SUM( sale ) AS currentsale,vendor_id ";   
          endif;
          
           if($avg):
                         $select=" Select supplier_operator_id, SUM(sale)/7 AS avgsale,vendor_id ";
            endif;

             $sql=" $select   "
                    . " FROM devices_data  Where 1 ";
        
            if($avg):
            $sql.=" AND sync_date >='{$past7day}'  AND sync_date <='{$date}' ";
            else:
            $sql.=" AND  sync_date='{$date}' ";
            endif;  
            
            $sql.=" AND  opr_id IN ({$oprids}) ";
            
           $sql.= " GROUP BY supplier_operator_id"; 
            
           if($groupbyvendor):
               $sql.=" ,vendor_id ";
           endif;
           
          $query=$this->slaveDB->query($sql);        
           
         $data=array();
         
          if($query->num_rows()):
              
              if(!$groupbyvendor):
                foreach($query->result() as $key=>$value):
                    
                                $data[$value->supplier_operator_id]=$avg?$value->avgsale:($current?$value->currentsale:$value->lastdaysale);
              
                endforeach;
              else:
                   foreach($query->result() as $key=>$value):
                    
                                $data[$value->supplier_operator_id."_".$value->vendor_id]=$avg?$value->avgsale:($current?$value->currentsale:$value->lastdaysale);
              
                endforeach;
              endif;
          endif;
          
          return $data;        
        }
        
        public function findSupplierforPlanningByOperatorid($operator_id)
        {
            
            $date=date('Y-m-d');
            $previous_date=date('Y-m-d',strtotime('-1 day'));
            $sql="SELECT dd.supplier_operator_id, s.id, s.name AS name, p.name AS operator, (CASE WHEN ps.base_amount IS NULL then 0 else ps.base_amount END) AS base_amount, SUM( if( dd.active_flag =1, 1, 0 ) ) AS workingsims, commission_type_formula AS margin, SUM( dd.opening ) AS currentstock, SUM( dd.tfr ) AS incoming, SUM( sale ) AS sale, group_concat( DISTINCT (dd.vendor_id) ) AS vendorids,(CASE WHEN ps.planned_sale IS NULL then 0 else ps.planned_sale END) AS planned_sale,pe.pending  "
                    . " FROM devices_data dd "
                    . " JOIN inv_supplier_operator so "
                    . " ON dd.supplier_operator_id = so.id "
                    . " JOIN inv_suppliers s"
                    . "  ON s.id = so.supplier_id "
                    . " LEFT JOIN inv_planning_sheet ps "
                    . " ON ps.supplier_operator_id = so.id "
                    . " JOIN products p "
                    . " ON p.id = dd.opr_id LEFT JOIN inv_pendings pe ON (pe.supplier_operator_id=dd.supplier_operator_id AND pe.pending_date='{$previous_date}') "
                    . " WHERE dd.sync_date = '{$date}' "
                    . " AND dd.opr_id ={$operator_id} "
                    . " GROUP BY dd.supplier_operator_id "
                    . " ORDER BY p.id";
                    
                    
            $query=$this->slaveDB->query($sql);
           
            if($query->num_rows()):
                
                    return $query->result();
                
            endif;
            
            return false;
        }
        
        public function getBackendDashBoard($oprids="")
        {
           $date=date('Y-m-d');
           $pdate=date('Y-m-d',strtotime('-1 day'));
           
            $sql="select s.name as suppliername,s.id as supplierid,p.name as operatorname,p.id as operator_id,so.id as supplier_operator_id,so.disputeamt,so.is_api,"
                    . " SUM(if(block=1,0,dd.balance)) as currentstock,SUM(CASE WHEN  dd.active_flag='1' AND dd.block='0' THEN 1 ELSE 0 END) as totalsims,SUM(CASE WHEN dd.balance>0 AND dd.active_flag='1' AND dd.sale>10 AND dd.block='0' THEN 1 ELSE 0 END) as workingsims,"
                    . " o.amount_by_bounds as todaysorder,SUM(tfr) as received,pending,base_amount as basesale,planned_sale as targetsale,"
                    . " SUM(dd.sale) as todaysale,SUM(dd.opening) as opening,GROUP_CONCAT(DISTINCT(dd.vendor_id)) as vendorid,SUM(if(block=1,balance,0)) as blockedbalance,if(mps.target is NULL,0,mps.target) as modemtarget,"
                    . " COUNT(dd.id) as totalavaliablesims,SUM(CASE WHEN opr_id=4 AND balance>=1000 THEN 1 WHEN opr_id<>4 AND balance >=5000 THEN 1 ELSE 0 END) as simsforst,ps.max_sale_capacity,so.is_active,so.commission_type_formula as margin,so.commission_type as margin_type    "
                    . " from inv_supplier_operator so  "
                    . " LEFT JOIN devices_data dd "
                    . " ON so.id=dd.supplier_operator_id "
                    . " JOIN inv_suppliers s "
                    . " ON so.supplier_id=s.id "
                    . " JOIN products p "
                    . " ON so.operator_id=p.id "
                    . " LEFT JOIN inv_orders o  "
                    . " ON (so.id=o.supplier_operator_id AND o.order_date='{$date}' AND is_payment_done=1) "
                    . " LEFT JOIN inv_pendings pe "
                    . " ON (pe.supplier_operator_id=so.id AND pe.pending_date='{$pdate}')  "
                    . " LEFT JOIN inv_planning_sheet ps "
                    . " ON (ps.supplier_operator_id=so.id) "
                    . " LEFT JOIN inv_modem_planning_sheet mps "
                    . " ON (mps.supplier_operator_id=so.id AND mps.vendor_id=dd.vendor_id)  "
                    . " WHERE  1  AND  dd.sync_date='{$date}' ";
            
           if(!empty($oprids)):
               $sql.=" AND so.operator_id IN({$oprids}) "; 
            endif;
            
             $sql.= " group by so.id,dd.vendor_id ";
            
             $sql=  $this->MergeApiVendor($sql,$oprids);
            
             $query=$this->slaveDB->query($sql);
           
            if($query->num_rows()):
                
                    return $query->result_array();
                
            endif;
            
            return false;
        }
        
        public function MergeApiVendor($sql,$oprids="")
        {
            $date=date('Y-m-d');
         //   $oprids=  combineOperator($oprids);
            
       $sql1="SELECT s.name AS suppliername, s.id AS supplierid, po.name AS operatorname, po.parent AS operator_id, so.id AS supplier_operator_id, NULL AS disputeamt,so.is_api, NULL AS currentstock, NULL AS totalsims, NULL AS workingsims, NULL AS todaysorder, NULL AS received, NULL AS pending, ps.base_amount AS basesale, ps.planned_sale AS targetsale, SUM( if(amount,amount,0) ) AS todayssale, NULL AS opening, svm.vendor_id AS vendorid, NULL AS blockedbalance,if(mps.target is NULL,0,mps.target) as modemtarget,NULL as totalavaliablesims,NULL as simsforst,ps.max_sale_capacity,so.is_active,so.commission_type_formula as margin,so.commission_type as margin_type    "
                    . " FROM inv_supplier_operator so "
                    . " JOIN inv_suppliers s "
                    . " ON ( so.supplier_id = s.id  AND so.is_api = '1' )  "
                    . " JOIN products po "
                    . " ON so.operator_id = po.parent  "
                    . " JOIN inv_planning_sheet ps "
                    . " ON so.id = ps.supplier_operator_id  "
                    . " JOIN inv_supplier_vendor_mapping svm "
                    . " ON svm.supplier_id = so.supplier_id  "
                    . " JOIN vendors v ON v.id = svm.vendor_id "
                    . " LEFT JOIN inv_modem_planning_sheet mps ON (mps.supplier_operator_id=so.id AND mps.vendor_id=svm.vendor_id)  "
                    . " LEFT JOIN vendors_activations va USE INDEX (idx_vend_date)  "
                    . " ON ( va.vendor_id = svm.vendor_id  AND va.date = '{$date}'  AND va.status !=2  AND va.status !=3  AND po.id = va.product_id )  "
                    . " Where 1  ";
            
           if(!empty($oprids)):
              // $sql1.=" AND va.product_id IN({$oprids})  "; 
               $sql1.=" AND po.parent IN({$oprids})  "; 
            endif;
            
            //$sql1.=" GROUP BY svm.vendor_id ";
            $sql1.=" GROUP BY svm.vendor_id, po.parent ";
            
            $finalsql="({$sql}) UNION ({$sql1}) Order by vendorid ";         
           
           return $finalsql;
        }
        
        public function getRequestPerMin($operator_id)
        {
            $start=date('Y-m-d H:i:s',strtotime('-6 minutes'));
            $end=date('Y-m-d H:i:s:',strtotime('-1 minutes'));
            $date=date('Y-m-d');
             
            $sql="SELECT v.company, v.id AS vendor_id,if(v.update_flag=0 AND v.active_flag=1 AND v.machine_id=0,1,0) as is_api, po.name, po.id AS operator_id, po.parent AS operatorparent, s.name AS suppliername, s.id AS supplierid, dd.supplier_operator_id, SUM( if( va.status !=2  AND va.status !=3, amount, 0 ) ) AS sale, COUNT( va.id ) AS totalrequests, SUM( if( va.status !=2 AND va.status !=3, 1, 0 ) ) AS successcount, SUM( if( va.status =2 OR va.status =3, 1, 0 ) ) AS failurecount "
                    . " FROM vendors_activations va "
                    . " JOIN vendors_transactions vt "
                    . " ON ( va.ref_code = vt.ref_id AND va.vendor_id = vt.vendor_id ) "
                    . " LEFT JOIN vendors v "
                    . " ON va.vendor_id = v.id "
                    . " LEFT JOIN products po "
                    . " ON po.id = va.product_id "
                    . " LEFT JOIN devices_data dd "
                    . " ON ( dd.vendor_id = va.vendor_id AND dd.opr_id = po.parent AND dd.mobile = vt.sim_num AND dd.sync_date = '{$date}' ) "
                    . " LEFT JOIN inv_suppliers s "
                    . " ON dd.inv_supplier_id = s.id "
                    . " WHERE va.timestamp >= '{$start}' AND va.timestamp <= '{$end}' "
                    . " AND va.date = '{$date}' ";
            
            if(!empty($operator_id)):
                 $sql.= " AND po.parent = {$operator_id} ";
            endif;
                    
            $sql.= " GROUP BY va.vendor_id, po.parent, dd.supplier_operator_id ";
            
             $query=$this->slaveDB->query($sql);
           
                            if($query->num_rows()):

                                    $temp=array();

                                    $data=$query->result_array();

                                                    foreach($data as $key=>$row):

                                                                                if(!$row['is_api']):

                                                                                        $temp[$row['supplier_operator_id']."_".$row['vendor_id']]=array(
                                                                                                                                                                          'suppliername'=>$row['suppliername'],   
                                                                                                                                                                          'soid'=>$row['supplier_operator_id'],
                                                                                                                                                                          'last2minsale'=>$row['sale'],
                                                                                                                                                                           'totalreq'=>$row['totalrequests'],
                                                                                                                                                                           'successreq'=>$row['successcount'],
                                                                                                                                                                           'failurereq'=>$row['failurecount'],
                                                                                                                                                                           'is_api'=>false,
                                                                                                                                                                           'vendor_id'=>$row['vendor_id']
                                                                                                                                                                        );
                                                                                else:

                                                                                    $temp["api_".$row['vendor_id']]=  array(
                                                                                                                                                              'suppliername'=>$row['company'],   
                                                                                                                                                              'soid'=>0,
                                                                                                                                                              'last2minsale'=>$row['sale'],
                                                                                                                                                               'totalreq'=>$row['totalrequests'],
                                                                                                                                                               'successreq'=>$row['successcount'],
                                                                                                                                                               'failurereq'=>$row['failurecount'],
                                                                                                                                                               'is_api'=>true,
                                                                                                                                                               'vendor_id'=>$row['vendor_id']
                                                                                                                                                             );
                                                                                endif;

                                                    endforeach;    

                                            return $temp;

                            endif;
            
            return false;
        }
        
        public function getMultipleVendorMapping($operator_id)
        {
             $sql="SELECT s.id as supplierid,so.id as supplier_operator_id,s.name as supplier,v.company,svm.vendor_id,if(mps.target is null,0,mps.target) as target  "
                    . " FROM inv_suppliers s "
                    . " JOIN inv_supplier_operator so "
                    . " ON s.id = so.supplier_id "
                    . " JOIN inv_supplier_vendor_mapping svm "
                    . " ON svm.supplier_id = s.id "
                     . " JOIN vendors v on svm.vendor_id=v.id "
                     . " LEFT JOIN inv_modem_planning_sheet mps ON  (mps.supplier_operator_id=so.id AND mps.vendor_id=svm.vendor_id)  "
                    . " WHERE so.operator_id ='{$operator_id}'";
                    
           $query=$this->slaveDB->query($sql);
           
           if($query->num_rows()):
               
                $temp=array();
               
                $data=$query->result_array();

                $temp1=$this->getLast7daysIncoming($operator_id);

                    foreach($data as $row):

                        $x=$row;

                            if(isset($temp1[$row['supplier_operator_id']."_".$row['vendor_id']])):
                                     foreach($temp1[$row['supplier_operator_id']."_".$row['vendor_id']] as $modem):
                                                 $x['hflag']='1';
                                     endforeach;
                            else:
                                         $x['hflag']='0';
                            endif;

                        $temp[$row['supplier_operator_id']][]=$x;

                    endforeach;

                return $temp;
               
           endif;
           
           return;
        }
        
        function getLast7daysIncoming($operator_id)
        {
            $date=date('Y-m-d');
            $past7day=date('Y-m-d',  strtotime("-7 days"));
            
            $sql="select supplier_operator_id,vendor_id,sum(tfr) as tfr "
                    . "from devices_data "
                    . "where opr_id='{$operator_id}' and sync_date >='{$past7day}' and sync_date <= '{$date}' and tfr > 0 "
                    . "group by supplier_operator_id,vendor_id";
                    
            $query=$this->slaveDB->query($sql);
            
            if($query->num_rows()):
                $data=$query->result_array();
                $temp=array();
                
                foreach ($data as $row):
                   $temp[$row['supplier_operator_id']."_".$row['vendor_id']]=$row;
               endforeach;
                
               return $temp;
            
            endif;

            return;
        }
        
        function saveSaleBifurcationHistory($data)
        {
            $this->db->insert('inv_salebifurcation_history',array('sync_date'=>date('Y-m-d'),'json_data'=>$data['json_data'],'html_data'=>$data['html_data']));
        }
        
        function getSBhistoryByDate($date,$oprs=0,$vendorids=0)
        {
           // $this->db->select('html_data');
           // return $this->db->get_where('inv_salebifurcation_history',array('sync_date'=>$date))->row_array();
            
            $sql="Select s.name as suppliername,supplier_id as supplierid,operator_id,supplier_operator_id,disputeamt,sth.is_api,totalactivesims as totalsims,workingsims,basesale,target as targetsale,currentsale as todaysale,"
                    . " opening,vendor_id as vendorid,blocked_bal AS blockedbalance,modem_target as modemtarget,lastdaysale as last_day_sale,last7daysavg as avg_sale,order_amount as todaysorder,closing as currentstock,pending,0 as received,max_sale_capacity  "
                    . "  from inv_set_target_history sth "
                    . " LEFT JOIN inv_suppliers s "
                    . " ON sth.supplier_id=s.id "
                    . " LEFT JOIN vendors v "
                    . " ON v.id=sth.vendor_id "
                    . " WHERE history_date='{$date}' ";
            
            
            if($oprs):
                    $sql.=" AND  operator_id IN ({$oprs}) ";
            endif;
            
            if($vendorids):
                    $sql.=" AND  sth.vendor_id IN ({$vendorids}) ";
            endif;
            
            $query=  $this->slaveDB->query($sql);
            
            if($query->num_rows()):
                return $query->result_array();
            endif;
            
            return;
            
        }
        function getApisAVGS()
        {
            $fromdate=date('Y-m-d',strtotime('-8 days'));
            $todate=date('Y-m-d',strtotime('-1 days'));
            
            $sql="select v.id as vendor_id,v.company as vendorname,po.parent,po.id as operatorid,po.name,SUM(va.amount)/7 as sale  "
                    . " from vendors v  "
                    . " LEFT JOIN vendors_activations va  "
                    . " ON (v.id=va.vendor_id AND va.date>='{$fromdate}' and va.date<='{$todate}' ) "
                    . " JOIN products po "
                    . " ON po.id=va.product_id"
                    . "  where v.update_flag=0 and machine_id=0 and active_flag=1 and  va.status !=2  AND va.status !=3  group by v.id,po.parent";
            
             $query=$this->slaveDB->query($sql); 
             
              if($query->num_rows()):
                  
                  $data=$query->result_array();
                  $temp=array();
                  
                  foreach($data as $row):
                            $temp[$row['vendor_id']."_".$row['parent']]=$row['sale'];
                  endforeach;
                  
                  return $temp;
                  
              endif;
              
              return;
              
        }
        
        public function getApisLDS()
        {
             $date=date('Y-m-d',strtotime('-1 days'));
             
            $sql=" select  v.id as vendor_id,v.company as vendorname,po.parent,po.id as operatorid,po.name,SUM(va.amount) as sale "
                    . " from vendors v "
                    . " JOIN vendors_activations va "
                    . " ON (v.id=va.vendor_id AND va.date='{$date}') "
                    . " JOIN products po "
                    . " ON po.id=va.product_id "
                    . " where v.update_flag=0 and machine_id=0 and show_flag=1  and  va.status !=2  AND va.status !=3  "
                    . " group by v.id,po.parent";
            
            $query=$this->slaveDB->query($sql); 
            
             if($query->num_rows()):
                 
                    $data=$query->result_array();
                    $temp=array();
                    
                      foreach($data as $row):
                            $temp[$row['vendor_id']."_".$row['parent']]=$row['sale'];
                     endforeach;
                  
                         logerror("SQl Query Response : ". json_encode($temp),"salebifurcationhistory");
                         
                      return $temp;
                      
             endif;
             
              return;
        }
        
//        public function storetargethistory($data)
//        {
//            $insert_array=array();
//            
//            foreach($data as $row):
//                    $row=(array)$row;
//                    $insert_array[]=array(
//                                                                'supplier_operator_id'=>$row['supplier_operator_id'],
//                                                                'supplier_id'=>$row['id'],
//                                                                'operator_id'=>$row['opr_id'],
//                                                                'last7daysavg'=>$row['avg_sale'],
//                                                                'lastdaysale'=>$row['last_day_sale'],
//                                                                'currentsale'=>$row['current_sale'],
//                                                                'opening'=>$row['opening'],
//                                                                'activesims'=>$row['activesims'],
//                                                                'totalsims'=>$row['totalsims'],
//                                                                'basesale'=>$row['base_amount'],
//                                                                'target'=>$row['planned_sale'],
//                                                                'vendorids'=>$row['vendorids'],
//                                                                'modem_target_json'=>!empty($row['vendors'])?json_encode($row['vendors']):json_encode(array()),
//                                                                'history_date'=>date('Y-m-d')
//                                                           );
//                                                           
//            endforeach;
//            
//            $insert_array=  createBatch($insert_array,15);
//            
//            
//            foreach ($insert_array as $batch):
//                             $this->db->insert_batch('inv_set_target_history', $batch);
//                              if($errormsg=$this->db->_error_message()):
//                                  $sql = $this->db->last_query();
//                                  logerror("Query Failed :  {$this->db->_error_message()} ","settargethistory");
//                                  logerror("Sql query : {$sql}","settargethistory");
//                                  //send_email('vibhas@mindsarray.com','Store Set target History',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
//                              endif;
//            endforeach;
//       }
       
       public function getSetTargetFromHistory($params)
       {
            $sql="Select supplier_id as id,s.name,v.company,supplier_operator_id,basesale as base_amount,"
                    . " target as planned_sale,opening,lastdaysale as last_day_sale,last7daysavg as avg_sale,"
                    . " currentsale as current_sale,totalavaliablesims as totalsims,simsforst as activesims,"
                    . " history_date as ddate,vendor_id,modem_target,sth.order_amount as totalorderamt,sth.pending,sth.max_sale_capacity,sth.margin,if(sth.blocked_bal is null,0,sth.blocked_bal) as blockedbalance,sth.margin_type  as commission_type  "
                  . "from inv_set_target_history sth "
                  . " LEFT JOIN inv_suppliers s  "
                  . " ON sth.supplier_id = s.id "
                   . " LEFT JOIN vendors v "
                   . " ON v.id=sth.vendor_id "
                  . "where operator_id='{$params['operator_id']}'  "
                  . "and history_date='{$params['target_date']}' ";
                  
             $query=$this->slaveDB->query($sql);       
             
                if($query->num_rows()):
                              
                              $temp=array();  
                
                              $data=$query->result_array();
                         
                                foreach($data as $row):
                                  
                                    if(!isset($temp[$row['supplier_operator_id']])):
                                            $temp[$row['supplier_operator_id']]=$row;
                                            $temp[$row['supplier_operator_id']]['vendorids']=$row['vendor_id'];
                                            $temp[$row['supplier_operator_id']]['vendors'][]=array(
                                                                                                                                                    'supplierid'=>$row['id'],
                                                                                                                                                    'supplier_operator_id'=>$row['supplier_operator_id'],
                                                                                                                                                    'company'=>$row['company'],
                                                                                                                                                    'vendor_id'=>$row['vendor_id'],
                                                                                                                                                    'target'=>$row['modem_target']
                                                                                                                                                  );
                                  else:
                                      
                                            $temp[$row['supplier_operator_id']]['vendorids']=$temp[$row['supplier_operator_id']]['vendorids'].",".$row['vendor_id'];
                                            $temp[$row['supplier_operator_id']]['vendors'][]=array(
                                                                                                                                              'supplierid'=>$row['id'],
                                                                                                                                              'supplier_operator_id'=>$row['supplier_operator_id'],
                                                                                                                                              'company'=>$row['company'],
                                                                                                                                              'vendor_id'=>$row['vendor_id'],
                                                                                                                                              'target'=>$row['modem_target']
                                                                                                                                            );
                                            $temp[$row['supplier_operator_id']]['opening']+=$row['opening'];
                                            $temp[$row['supplier_operator_id']]['last_day_sale']+=$row['last_day_sale'];
                                            $temp[$row['supplier_operator_id']]['current_sale']+=$row['current_sale'];
                                            $temp[$row['supplier_operator_id']]['avg_sale']+=$row['avg_sale'];
                                            $temp[$row['supplier_operator_id']]['totalsims']+=$row['totalsims'];
                                            $temp[$row['supplier_operator_id']]['activesims']+=$row['activesims'];
                                  
                                  endif;
                                  
                              endforeach;
                              
                              return $temp;
                
                endif;
                
                return;
                
       }
       
       function getApiCurSale($operator_id)
       {
           $date=date('Y-m-d');
           
           $sql=" Select v.company,va.vendor_id,va.product_id,SUM(amount) as sale "
                   . "from vendors_activations  va "
                   . "JOIN vendors v "
                   . "ON va.vendor_id=v.id "
                   . "where  va.date='{$date}' "
                   . "AND va.status !=2  AND va.status !=3  "
                   . "AND  v.update_flag=0 and machine_id=0 and active_flag=1 and product_id='{$operator_id}' "
                   . "GROUP BY  vendor_id,product_id";
           
              $query=$this->slaveDB->query($sql);       
             
               $temp=array();
               
              if($query->num_rows()):
                  
                    $data=$query->result_array();
                   
                    foreach($data as $row):
                                $temp[$row['vendor_id']]=$row['sale'];
                    endforeach;
                  
              endif;       
              
              
              return $temp;
                   
       }
}