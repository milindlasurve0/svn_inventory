<?php

class Supplier extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function saveSupplier($formdata)
    {
        
        $data=array(
                                'name'=>$formdata['suppl_name'],
                                'email_id'=>$formdata['suppl_email'],
                                'contact'=>$formdata['suppl_contact'],
                                'account_no'=>$formdata['suppl_bank_account_no'],
                                'bank_name'=>$formdata['suppl_bank_name'],
                                'address'=>$formdata['suppl_address'],
                                'riskfactor'=>$formdata['suppl_risk_factor'],
                                'location'=>$formdata['suppl_location'],
                                'rating'=>$formdata['suppl_rating'],
                                'inv_rm_id'=>$formdata['suppl_rm_id'],
                                'parent_id'=>$formdata['suppl_parent_id']
                              );
        
        
                              
          $this->db->set('created_at',date("Y-m-d H:i:s"));
        
         if($this->db->insert('inv_suppliers',$data)):
            
                return true;
        
        endif;
        
        return false;
        
        }
        
         public function findAllSuppliers($params,$pageno,$perpage)
        {
             $date=date('Y-m-d');
             
             $sql=" SELECT  s.id AS supplier_id,s.name as supplier,so.created_at,po.name as operator,"
                     . " so.commission_type_formula as margin,capacity_per_month as capacity,so.commission_type,"
                     . " ps.base_amount,s.email_id,s.location,rm.name as rmname,so.is_active,GROUP_CONCAT(DISTINCT(v.company)) as vendors,"
                     . " COUNT(dd.id) as totalsims,SUM(if(dd.block='0' and dd.active_flag='1',1,0)) as activesims "
                    . " FROM inv_supplier_operator so "
                    . " JOIN inv_suppliers s "
                    . " ON so.supplier_id=s.id "
                    . " JOIN products  po "
                    . " ON so.operator_id=po.id "
                    . " JOIN inv_planning_sheet ps "
                    . " ON ps.supplier_operator_id=so.id "
                    . " LEFT JOIN inv_rm rm "
                    . " ON rm.id=s.inv_rm_id "
                    . " LEFT JOIN inv_supplier_vendor_mapping svm "
                    . " ON s.id=svm.supplier_id "
                    . " LEFT JOIN vendors v "
                    . " ON svm.vendor_id=v.id  "
                    . " LEFT JOIN devices_data dd"
                    . " ON (dd.supplier_operator_id=so.id and dd.sync_date='{$date}') "
                    . " WHERE  1 ";
             
             if(!empty($params['operator_id'])):
                    $sql.=" AND so.operator_id='{$params['operator_id']}' ";
             endif;
             
             if(!empty($params['rm'])):
                    $sql.=" AND s.inv_rm_id='{$params['rm']}' ";
             endif;
             
             if(!empty($params['handled_by'])):
                    $sql.=" AND s.handled_by='{$params['handled_by']}' ";
             endif;
             
             if(!empty($params['modems_ids'])):
                    $sql.=" AND v.id IN ({$params['modems_ids']}) ";
             endif;
             
             if(!empty($params['is_active'])):
                if(in_array($params['is_active'],array('no','yes')) && $params['is_active']!="all" ):
                       $is_active=$params['is_active']=="yes"?'1':'0';
                       $sql.=" AND so.is_active='{$is_active}' ";
                endif;
             endif;
            
             $sql.= " GROUP BY  so.id ";
             
             if(!empty($params['orderby'])):
                    $sql.=" ORDER BY {$params['orderby']} {$params['ordertype']} ";
             endif;
             
            $sql .= " LIMIT ".($pageno-1)*25 .",".$perpage;
            
            $query=  $this->slaveDB->query($sql);
            
            if($query->num_rows()):
                    return $query->result_array();
            endif;
            
            return array();
            
        }
        
        public function getTotalUniqueSuppliers()
        {
            $sql="SELECT count( 1 ) AS totalsuppliers FROM inv_suppliers ";
            $query=  $this->slaveDB->query($sql);
             if($query->num_rows()):
                        $data=$query->row_array();
                        return $data['totalsuppliers'];
             endif;
             
             return;
            
        }
        public function getTotalRows($params)
        {
            $sql=" SELECT so.id "
                    . " FROM inv_supplier_operator so "
                    . " JOIN inv_suppliers s "
                    . " ON so.supplier_id=s.id "
                    . " JOIN products  po "
                    . " ON so.operator_id=po.id "
                    . " JOIN inv_planning_sheet ps "
                    . " ON ps.supplier_operator_id=so.id "
                    . " LEFT JOIN inv_rm rm "
                    . " ON rm.id=s.inv_rm_id "
                    . " LEFT JOIN inv_supplier_vendor_mapping svm "
                    . " ON s.id=svm.supplier_id "
                    . " LEFT JOIN vendors v "
                    . " ON svm.vendor_id=v.id WHERE 1 ";
            
             if(!empty($params['operator_id'])):
                    $sql.=" AND so.operator_id='{$params['operator_id']}' ";
             endif;
             
             if(!empty($params['rm'])):
                    $sql.=" AND s.inv_rm_id='{$params['rm']}' ";
             endif;
             
             if(!empty($params['handled_by'])):
                    $sql.=" AND s.handled_by='{$params['handled_by']}' ";
             endif;
             
              if(!empty($params['modems_ids'])):
                    $sql.=" AND v.id IN ({$params['modems_ids']}) ";
             endif;
             
             if(!empty($params['is_active'])):
                if(in_array($params['is_active'],array('no','yes')) && $params['is_active']!="all" ):
                       $is_active=$params['is_active']=="yes"?'1':'0';
                       $sql.=" AND so.is_active='{$is_active}' ";
                endif;
             endif;
             
              $sql.= " GROUP BY  so.id ";
            
            $query=  $this->slaveDB->query($sql);
            
            if($query->num_rows()):
                   return $query->num_rows();
            endif;
            
            return 0;
                
        }
        
        public function findSupplierById($id)
        {
            $this->slaveDB->select('rm.id as rm_id,rm.name as rm_name,rm.mobile as rm_mobile,rm.address as rm_address,'
                                                . 'rm.email_id as rm_email ,rm.created_at as rm_created_at,rm.updated_at as rm_updated_at ,s.*')
                            ->from('inv_suppliers s') 
                            ->join('inv_rm rm','s.inv_rm_id=rm.id','left')
                            ->where('s.id',$id)
                            ->limit(1);

             

            $query = $this->slaveDB->get();
                    
            if($query->num_rows()):
                
                       return $query->row();
            
            else:
                
                      return false;
            
            endif;
        }
        
        public function UpdateSupplier($id,$formdata)
    {
        $data=array(
                                'name'=>$formdata['suppl_name'],
                                'email_id'=>$formdata['suppl_email'],
                                'address'=>$formdata['suppl_address'],
                                'riskfactor'=>$formdata['suppl_risk_factor'],
                                'location'=>$formdata['suppl_location'],
                                'rating'=>$formdata['suppl_rating'],
                                'inv_rm_id'=>$formdata['suppl_rm_id'],
                                'parent_id'=>$formdata['parent_id'],
                                'frombank'=>$formdata['frombank'],
                                'handled_by'=>$formdata['handled_by'],
                                'single_payment'=>isset($formdata['single_payment'])?$formdata['single_payment']:"0",
                                'is_api'=>isset($formdata['is_api'])?$formdata['is_api']:"0"
                              );
        
        $data['other_rm']="";
        
          if($formdata['other_rm']!="" && $formdata['suppl_rm_id']=="0"):
              
                $data['other_rm']=$formdata['other_rm'];
          
         endif; 
        
        
                if($this->db->delete('inv_supplier_vendor_mapping',array('supplier_id'=>$id))):

                           foreach($formdata['vendors'] as $vendor):

                               $this->db->insert('inv_supplier_vendor_mapping',array('supplier_id'=>$id,'vendor_id'=>$vendor));
                               
                           endforeach;
                           
                           logerror($this->input->ip_address()." | ".$id." | ".json_encode($formdata['vendors'])." | ".getLoggedInUserId() ,"inv_suppvendor_log");

        endif;

                
         $this->db->set('updated_at',date("Y-m-d H:i:s"));
         
         $this->db->where('id', $id);
         
         if($this->db->update('inv_suppliers', $data)):
              
                return true;
         
        endif;
        
        
        
          return false;
         
    }
    
    public function deleteSupplier($id)
    {
         if($this->db->delete('inv_suppliers', array('id' => $id))):
             
             if($this->db->delete('inv_supplier_vendor_mapping',array('supplier_id'=>$id))):
                  return true;
             endif;
           
        endif;
        
        return false;
    }
    
     public function getSuppliersForDropdown()
    {
        $query=$this->slaveDB->select('id,name')->get('inv_suppliers');
       
        return $query->result();
    }
    
    public function save($data)
    {
  
        $actualcount=count($data['supplier_operator']);
        
        $inserted_count=0;
        
        $this->db->trans_start();
        
          $this->db->set('created_at',date("Y-m-d H:i:s"));
        
         if($this->db->insert('inv_suppliers',$data['supplier'])):
            
             $supplier_id=$this->db->insert_id();
         
                                /*
                                 * Insert into supplier_Operator table
                                */
         
                                     

                               foreach($data['supplier_operator'] as $value):

                                              $this->db->set('supplier_id',$supplier_id);

                                              $this->db->set('created_at',date("Y-m-d H:i:s"));

                                              $base_amount=$value['base_amount'];
                                              
                                              // remove base_value since inv_supplier_operator doesnot contain that column
                                              unset($value['base_amount']);
                                              
                                             if($this->db->insert('inv_supplier_operator',$value)):
                                                 
                                                 $soid=$this->db->insert_id();
                                                 
                                                 $this->db->insert('inv_planning_sheet',array('supplier_operator_id'=>$soid,'base_amount'=>$base_amount));
                                                 
                                                 $inserted_count ++;

                                           else:
                                                  logerror($this->db->_error_message(),"sqlqueries");
                                           endif;


                               endforeach;

                                /*
                                 * Insert into supplier_vendor  table
                                 */

                               if(!empty($data['vendors'])):

                                   foreach($data['vendors'] as $vendor):

                                        if(!$this->db->insert('inv_supplier_vendor_mapping',array('supplier_id'=>$supplier_id,'vendor_id'=>$vendor))):
                                                 logerror($this->db->_error_message(),"sqlqueries");
                                         endif;

                                    endforeach;

                               endif;
                               
                               /*
                                * Insert into supplier_contacts tables
                                */
                               
                                    foreach($data['alternate_contact'] as $contact):
                                      
                                        $this->db->set('supplier_id',$supplier_id);

                                             if(!$this->db->insert('inv_supplier_contacts',array('name'=>$contact['name'],'contact'=>$contact['contact']))):
                                                 logerror($this->db->_error_message(),"sqlqueries");
                                             endif;

                               endforeach;
        
        else:
            
               logerror($this->db->_error_message(),"sqlqueries");
               return false;
        
        endif;
        
     
        $this->db->trans_complete();
        
        
        if ($this->db->trans_status() === FALSE):
            
                    logerror($this->db->_error_message(),"sqlqueries");
                    return false;
        
        endif;
        
        
        if($actualcount==$inserted_count):
            
            return $supplier_id;
        
        endif;
        
        logerror($this->db->_error_message(),"sqlqueries");
        
        return false;
        
        
      
        
        }
        
        public function getSupplierNameById($id)
        {
            $this->slaveDB->select('id,name');
            
            $query=$this->slaveDB->get_where('inv_suppliers',array('id'=>$id));
       
            return $query->row();
            
        }
        
        public function getVendorForDropdown()
        {
             $query=$this->slaveDB->select('id,company as vendor_name')->order_by("company","asc")->get('vendors');
       
             return $query->result();
        }
        
        public function getallVendorForDropdown()
        {
             $query=$this->slaveDB->select('id,company as vendor_name')->where(array('show_flag'=>'1'))->order_by('company','asc')->get('vendors');
       
             return $query->result();
        }
        
        public function getApiVendorForDropdown()
        {
             $query=$this->slaveDB->select('id,company as vendor_name')->where(array('machine_id'=>'0','update_flag'=>'0','active_flag'=>'1'))->get('vendors');
       
             return $query->result();
        }
        
        public function findVendorsSupplierById($id)
        {
            $this->slaveDB->select('vendor_id as id');
            
            $query=$this->slaveDB->get_where('inv_supplier_vendor_mapping',array('supplier_id'=>$id));
       
            if($query->num_rows()):
                
                    $data=$query->result_array();

                    foreach($data as $value):
                        $tobereturned[]=$value['id'];
                    endforeach;

                    return $tobereturned;
            
            endif;
            
            return array();
            
        }
        
        public function __getVendors()
        {
          //  $this->load->database('shops',true);
            
            $this->slaveDB->distinct();
            
            $this->slaveDB->select('vendor');
            
            $query=$this->slaveDB->get('devices');

            if($query->num_rows()):
                
                    return $query->result();
                
            endif;
            
            return false;
            
        }
        
        public function SyncVendorsFromShopsToInventory($vendors,$modem_id)
        {
            //$this->load->database('default',true);
             
           $actualcount=count($vendors);
        
           $inserted_count=0;
        
            foreach ($vendors as $value):
                
                       
                            
                            if(!$this->checkIfSupplierAlreadyExists($value)):
                                
                                            $this->db->set('created_at',date("Y-m-d H:i:s"));
                            
                                            if($this->db->insert('inv_suppliers',array('name'=> ucwords($value)))):

                                                if($this->db->insert('inv_supplier_vendor_mapping',array('supplier_id'=>  $this->db->insert_id(),'vendor_id'=>$modem_id))):

                                                         $inserted_count++;

                                                endif;

                                            endif;
                            
                            else:
                                
                                    if(!$this->checkifSupplierVendorMappingAlreadyExisits(array('supplier_id'=>$this->getSupplierIdByName($value),'vendor_id'=>$modem_id))):
                                
                                            if($this->db->insert('inv_supplier_vendor_mapping',array('supplier_id'=>$this->getSupplierIdByName($value),'vendor_id'=>$modem_id))):

                                                        $inserted_count++;

                                             endif;
                                             
                                    else:
                                         $inserted_count++;
                                    endif;        
                                
                            endif;
                            
            
            endforeach;
            
            if($inserted_count==$actualcount):
                
                return true;
            
            endif;
            
            return false;
            
            
            
        }
        
        public function checkIfSupplierAlreadyExists($name)
        {
            
            $this->slaveDB->where('LOWER(name)',strtolower($name));
            $query = $this->slaveDB->get('inv_suppliers');
            
            if ($query->num_rows() > 0):
                
                return true;
            
            endif;
            
            return false;
        }
        
        public function checkifSupplierVendorMappingAlreadyExisits($value)
        {
             $this->slaveDB->where(array('supplier_id'=>$value['supplier_id'],'vendor_id'=>$value['vendor_id']));
            $query = $this->slaveDB->get('inv_supplier_vendor_mapping');
            
            if ($query->num_rows() > 0):
                
                return true;
            
            endif;
            
            return false;
        }
        
          public function checkIfSupplierEmailAlreadyExists($email)
        {
            
            $this->slaveDB->where('LOWER(email_id)',strtolower($email));
            $query = $this->slaveDB->get('inv_suppliers');
            
            if ($query->num_rows() > 0):
                
                return true;
            
            endif;
            
            return false;
        }
        
        public function getSupplierIdByName($name)
        {
            $this->slaveDB->select('id');
            $this->slaveDB->where('name',$name);
            $query = $this->slaveDB->get('inv_suppliers');
            
            $result=$query->row();
            
            if ($query->num_rows() > 0):
                
                  return $result->id;
            
            endif;
            
            return '0';
          
        }
        
        
        public function getBanks($dropdown=false)
        {
                $query=$this->slaveDB->select('id,bank_name')->get('inv_banks');
       
                if($dropdown):
                            $banks=array();
                
                            foreach($query->result() as $bank):
                                $banks[$bank->id]=$bank->bank_name;
                            endforeach;
                            
                            return $banks;
                else:
                      return $query->result();
                endif;
        }
        
        public function saveBankDetails($id,$data)
        {
            if(!$this->checkIfBankExists($id,$data['beneficiary_code'])):
                
                    $this->db->set('supplier_id',$id, false);

                      if($this->db->insert('inv_supplier_banks',$data)):

                          return true;

                     endif;
           endif;  
           
             return false;
             
        }
        
        public function checkIfBankExists($id,$code)
        {
            $this->slaveDB->where(array('supplier_id'=>$id,'beneficiary_code'=>$code));
            $query = $this->slaveDB->get('inv_supplier_banks');
            
            if ($query->num_rows() > 0):
                
                return true;
            
            endif;
            
            return false;
        }
        
        public function updateBankDetails($id,$data)
        {
              if($this->db->update('inv_supplier_banks',$data,array('id'=>$id))):
                                
                  return true;
                            
             endif;
             
             return false;
             
        }
        
        public function getBankDetailBySupplierID($id)
        {
            $this->slaveDB->select('sb.id as sb_id,default_bank,account_holder_name,isfc_code,account_no,bank_name,beneficiary_code,supplier_id')->from('inv_supplier_banks sb')->join('inv_banks b','sb.bank_id=b.id','left')->where('sb.supplier_id',$id);
            
            $query=  $this->slaveDB->get();
        
            if($query->num_rows()):
                
                    return $query->result();
                
            endif;
            
            return false;
        }
        
        public function updateDefaultBank($data)
        {
        
                if($this->db->update('inv_supplier_banks',array('default_bank'=>$data['flag']),array('id'=>$data['sb_id']))):
                                
                             return true;
                endif;
                                
            return false;
            
          
        }
        
        public function getBankDetailsByID($id)
        {
                if($id>0):

                $this->slaveDB->where('id', $id); 

                $this->slaveDB->limit(1);

                endif;

                $query = $this->slaveDB->get('inv_supplier_banks');

                return $query->row();
        }
        
        
        public function getSupplierByVendorID($Id)
        {
               $query=$this->slaveDB->select('supplier_id,name')->from('inv_suppliers s')->join('inv_supplier_vendor_mapping sv','s.id=sv.supplier_id','left')->where('sv.vendor_id',$Id);
       
                $query=  $this->slaveDB->get();
        
            if($query->num_rows()):
                
                    return $query->result();
                
            endif;
            
            return false;
        }
        
        public function getContactsBySupplierID($id)
        {
            $query=  $this->slaveDB->get_where('inv_supplier_contacts',array('supplier_id'=>$id));
        
            if($query->num_rows()):
                
                    return $query->result();
                
            endif;
            
            return false;
        }
        public function getVendorsBySupplierID($id)
        {
            $query=$this->slaveDB->select('company')->from('vendors v')->join('inv_supplier_vendor_mapping i','v.id=i.vendor_id')->where('i.supplier_id',$id)->get();  
        
            if($query->num_rows()):
                
                    $data=$query->result_array();

                        foreach($data as $value):
                            
                            $tobereturned[]=$value['company'];
                        
                        endforeach;

                   return $tobereturned;
            
       
            endif;
            
            
            return array();
        }
        
        
        public function saveContacts($id,$data)
        {
             $this->db->set('supplier_id',$id, false);
            
              if($this->db->insert('inv_supplier_contacts',$data)):
                                
                  return true;
                            
             endif;
             
             return false;
        }
        
        public function updateSmsNo($data)
        {
            if($this->db->update('inv_supplier_contacts',array('to_send'=>$data['status']=='true'?'1':'0'),array('id'=>$data['id']))):
                                
                             return true;
                endif;
                
            return false;
        }
        
         public function getContactByID($id)
        {
                if($id>0):

                $this->slaveDB->where('id', $id); 

                $this->slaveDB->limit(1);

                endif;

                $query = $this->slaveDB->get('inv_supplier_contacts');

                return $query->row();
        }
        
        public function updateContact($id,$data)
        {
             if($this->db->update('inv_supplier_contacts',$data,array('id'=>$id))):
                                
                  return true;
                            
             endif;
             
             return false;
        }
        
        public function updateBankSwitchMode($data)
        {
            if($this->db->update('inv_suppliers',array('bank_change_flag'=>$data['mode']),array('id'=>$data['supplier_id']))):
                                
                             return true;
             endif;
             
             return false;
        }
        
        public function findSuppliersByOperatorId($operator_id)
        {
            $date=date('Y-m-d',strtotime('-1 day'));
            $current_date=date('Y-m-d');

            $sql1="SELECT s.id,s.name,so.id AS supplier_operator_id,so.operator_id as opr_id, (CASE WHEN ps.base_amount IS NULL then 0 else ps.base_amount END) AS base_amount, "
                     . "  last_planned_date, (CASE WHEN ps.planned_sale IS NULL then 0 else ps.planned_sale END) AS planned_sale,"
                     . " SUM(if(dd.closing is null,0,dd.closing)) as opening,SUM(if(dd.block=1,dd.closing,0)) as blockedbalance,SUM(if(dd.sale is null,0,dd.sale)) as last_day_sale,COUNT(dd.id) as totalsims,SUM(CASE WHEN opr_id=4 AND closing>=1000 THEN 1 WHEN opr_id<>4 AND closing >=5000 THEN 1 ELSE 0 END) as activesims,"
                      . " GROUP_CONCAT(DISTINCT(vendor_id)) as vendorids,dd.sync_date as ddate,pe.pending,if(ps.max_sale_capacity is null,0,ps.max_sale_capacity) as  max_sale_capacity,TRUNCATE(so.commission_type_formula,2) as margin,so.commission_type  "
                     . " FROM inv_suppliers s "
                     . " JOIN inv_supplier_operator so "
                     . " ON s.id=so.supplier_id "
                     . " LEFT JOIN inv_planning_sheet ps "
                     . " ON ps.supplier_operator_id=so.id "
                     . " LEFT JOIN devices_data dd "
                     . " ON (so.id=dd.supplier_operator_id and dd.sync_date='{$date}') "
                     . " LEFT JOIN inv_pendings pe "
                      . " ON (pe.supplier_operator_id=so.id AND pending_date='{$date}')   "
                     . " WHERE so.operator_id =  '{$operator_id}' and so.is_active='1'   "
                     . " group by so.id ";  
                     
            $sql2="select so_id,sum(amount_by_bounds) as totalorderamt "
                    . "from inv_payments "
                    . "where payment_date='{$current_date}' and status='1' "
                    . "group by so_id";

            $finalsql="SELECT * "
                            . "FROM ($sql1) as a "
                            . "LEFT JOIN ($sql2) as b "
                            . "ON a.supplier_operator_id=b.so_id";
            
            $query=$this->slaveDB->query($finalsql);                       

            if($query->num_rows()):
                
                  return $query->result_array();

              endif;
            
            return false;
        }
        
        public function deleteBank($sb_id,$supplier_id)
        {
           $this->db->delete('inv_supplier_banks',array('id'=>$sb_id,'supplier_id'=>$supplier_id));
          
           if($this->db->affected_rows()>=0):
                return true;
           endif;
           
           return false;
        }
        
        public function saveToggleCommentsBySoID($soid,$basesale,$margin,$flag,$comment)
        {
            if($this->db->insert('inv_so_togglehistory',array('so_id'=>$soid,'base_amount'=>$basesale,'margin'=>$margin,'is_active'=>$flag,'comments'=>$comment,'user_id'=>  getLoggedInUserId(),'updated_date'=>date("Y-m-d"),'updated_time'=>date("Y-m-d H:i:s")))):
                return true;
            endif;
                    
            return false;
        }
        
        public function getToggleHistoryByDate($start,$end,$is_active,$is_onhold,$operator_id,$supplier_id)
        {
            $sql = "select invt.*,s.name as supplier_name,p.name as operator_name,u.name as user_name "
                    . "from inv_so_togglehistory invt "
                    . "left join inv_supplier_operator so "
                    . "on (so.id=invt.so_id) "
                    . "left join inv_suppliers s "
                    . "on (s.id=so.supplier_id) "
                    . "left join products p "
                    . "on (p.id=so.operator_id) "
                    . "left join users u "
                    . "on (u.id=invt.user_id) "
                    . "where invt.updated_date between '{$start}' and '{$end}'";
                    
            if($is_active!=""):
                    $flag=($is_active=="on")?"1":"0";
                    $sql.=" AND invt.is_active='{$flag}' ";
            endif;
             
            if($is_onhold!=""):
                  $flag=($is_onhold=="yes")?"1":"0";
                  $sql.=" AND invt.is_onhold='{$flag}' ";
            endif;
            
            if($operator_id>0):
                $sql.=" AND p.id='{$operator_id}' ";
           endif;

            if($supplier_id>0):
                $sql.=" AND s.id='{$supplier_id}' ";
            endif;
                    
            $query=$this->slaveDB->query($sql);                       

            if($query->num_rows()):
                
                  return $query->result_array();

              endif;
            
            return false;
        }
        
        public function saveHoldStatusCommentsBySoID($soid,$basesale,$margin,$flag,$comment)
        {
             if($this->db->insert('inv_so_togglehistory',array('so_id'=>$soid,'base_amount'=>$basesale,'margin'=>$margin,'is_onhold'=>$flag,'comments'=>$comment,'user_id'=>  getLoggedInUserId(),'updated_date'=>date("Y-m-d"),'updated_time'=>date("Y-m-d H:i:s")))):
                return true;
            endif;
                    
            return false;
        }

        public function setOnholdFlag($data)
        {               
            if($this->db->update('inv_supplier_operator',array('is_onhold'=>$data['flag']),array('id'=>$data['soid'])))
            {
                    if($data['flag']=='1'):
                            if($this->db->update('inv_planning_sheet',array('base_amount'=>'0'),array('supplier_operator_id'=>$data['soid']))):
                                    return true;
                            else:
                                    return false;
                            endif;
                    endif;
                    
                 return true;
            }
         
            return false;
         }
         
         public function getSoHistoryByDate($start,$end,$operator_id,$supplier_id)
        {
            $sql = "select ip.*,s.name as supplier_name,p.name as operator_name,u.name as user_name "
                    . "from inv_so_paramhistory ip "
                    . "left join inv_supplier_operator so "
                    . "on (so.id=ip.so_id) "
                    . "left join inv_suppliers s "
                    . "on (s.id=so.supplier_id) "
                    . "left join products p "
                    . "on (p.id=so.operator_id) "
                    . "left join users u "
                    . "on (u.id=ip.user_id) "
                    . "where ip.updated_date between '{$start}' and '{$end}'";
                    
            if($operator_id>0):
                $sql.=" AND p.id='{$operator_id}' ";
           endif;

            if($supplier_id>0):
                $sql.=" AND s.id='{$supplier_id}' ";
            endif;
            
            $query=$this->slaveDB->query($sql);                       

            if($query->num_rows()):
                
                  return $query->result_array();

              endif;
            
            return false;
        }
}
