<?php

class Blocksim extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getBlockSimsByDate($params)
    {
        $sql="select s.name as suppliername,vendor_tag,p.name as operator,balance,mobile,scid,last_block_date"
                . "  from devices_data dd "
                . " JOIN inv_suppliers s "
                . " ON dd.inv_supplier_id=s.id "
                . " JOIN products p "
                . " ON dd.opr_id=p.id "
                . " Where dd.block=1 "
                . " and dd.sync_date='{$params['searchdate']}' order by s.id ";
        
       $query=  $this->slaveDB->query($sql);
       
       if($query->num_rows()):
            return $query->result_array();
       endif;
       
       return;
    }
}
