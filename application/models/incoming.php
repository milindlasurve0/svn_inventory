<?php

class Incoming extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getIncomingByDate($params)
    {
         $sql1="Select SUM(tfr) as incoming,SUM(server_diff) as server_diff,SUM(closing) as closing,SUM(opening) as opening,SUM(sale) as sale,SUM(balance) as currentbalance,SUM(inc) as inc,"
                 . "s.id as supplier_id,dd.opr_id,s.name as suppliername,p.name as operatorname,opr_id as operator_id,sync_date,supplier_operator_id,count(dd.id) as totalsims,SUM(if(tfr>0,1,0)) as incomingsims "
                . "  from devices_data  dd "
                . " JOIN inv_suppliers s "
                . " ON dd.inv_supplier_id=s.id "
                . " JOIN products p "
                . " ON dd.opr_id=p.id "
                 . " WHERE sync_date='{$params['incomingdate']}' ";

       if(isset($params['vendors'])):
                    if(count($params['vendors'])>1):
                        $vendorInStat=implode(',',$params['vendors']);
                    else:
                          $vendorInStat=$params['vendors'][0];
                    endif;
                    $sql1.=" AND dd.vendor_id IN($vendorInStat) ";
       endif;           
         
       if(isset($params['handled_by'])):
            if($params['handled_by']!=""):
                  $sql1.=" AND s.handled_by={$params['handled_by']} ";
            endif;         
      endif;
         
        if($params['operator_id']>0):
           $sql1.=" AND dd.opr_id={$params['operator_id']} ";
        endif;

        if($params['supplier_id']>0):
            $sql1.=" AND dd.inv_supplier_id={$params['supplier_id']} ";
        endif;
        
        // get only mapped records with soid
        $sql1.=" AND supplier_operator_id > 0 ";
        
         // actual query to get SUM
          $sql1.=" GROUP BY supplier_operator_id HAVING incoming>0 ";
         
           // Get vendorids
           $sql2="SELECT supplier_id, GROUP_CONCAT( vendor_id ) AS vendorids  FROM inv_supplier_vendor_mapping   GROUP BY supplier_id ";
           
           // Join based on supplierid
           $finalsql="Select a.*,b.vendorids from ($sql1) as a LEFT JOIN ($sql2) as b ON a.supplier_id=b.supplier_id order by a .opr_id";
         
        
        $query=  $this->slaveDB->query($finalsql);
        
        if($query->num_rows()):
                return $query->result();
        endif;
        
        return false;
    }
    
    
    public function getInternalsPersons()
    {
             $query=$this->slaveDB->select('id,name')->get('inv_internal_person');
       
              return $query->result();
    }
    
    public function getModemwiseIncomingByDate($params)
    {
        $sql="select so.id as supplier_operator_id,po.id as operator_id,s.name as supplier,po.name as operator,dd.sync_date,v.id as vendor_id,v.company,SUM( IF( tfr IS NULL , 0, tfr ) ) as incoming,SUM(if(so.commission_type =1,tfr*((100-pe.margin)/100),tfr/((100+pe.margin)/100))) as investedamount  "
                . " from inv_supplier_operator so "
                . " JOIN inv_suppliers s "
                . " ON so.supplier_id=s.id "
                . " JOIN products po "
                . " ON so.operator_id=po.id "
                . " LEFT JOIN inv_pendings pe ON  ( pe.supplier_operator_id=so.id AND pe.pending_date='{$params['incomingdate']}' ) "
                . " LEFT JOIN devices_data dd "
                . " ON (dd.supplier_operator_id=so.id AND dd.sync_date='{$params['incomingdate']}') "
                . " JOIN vendors v "
                . " ON dd.vendor_id=v.id "
                . " where  dd.tfr<>0 ";
       
        if($params['operator_id']>0):
           $sql.=" AND dd.opr_id={$params['operator_id']} ";
        endif;         
     
        if(isset($params['vendors'])):
                    if(count($params['vendors'])>1):
                        $vendorInStat=implode(',',$params['vendors']);
                    else:
                          $vendorInStat=$params['vendors'][0];
                    endif;
                    $sql.=" AND dd.vendor_id IN($vendorInStat) ";
       endif; 
       
       $sql.= " GROUP BY dd.vendor_id, so.id,dd.sync_date "
                . " ORDER BY po.id,s.id ASC ";
                

       $query=  $this->slaveDB->query($sql);
        
        if($query->num_rows()):
                return $query->result_array();
        endif;
        
        return false;         
    }
    
    public function  getAllapivendorswithoperators($date)
    {
        $sql="select s.name as supplier,p.name as operator,so.id as supplier_operator_id,s.id as supplierid,p.id as operator_id,svm.vendor_id,if(tfr is null,0,tfr) as incoming "
                . " from inv_suppliers s "
                . " JOIN inv_supplier_operator so "
                . " ON (s.id=so.supplier_id AND so.is_api='1' AND so.operator_id =54 ) "
                . " JOIN products p "
                . " ON so.operator_id=p.id  "
                . " JOIN inv_supplier_vendor_mapping svm "
                . " ON svm.supplier_id = so.supplier_id"
                . " LEFT JOIN devices_data dd "
                . " ON (dd.supplier_operator_id=so.id AND sync_date='{$date}'  AND svm.vendor_id=dd.vendor_id)  "
                . " GROUP BY so.id ORDER BY s.id  ";
        
        $query=  $this->slaveDB->query($sql);
        
        if($query->num_rows()):
                return $query->result_array();
        endif;
        
        return false;         
    }
    
    public function setApiIncoming($params)
    {
        $date=empty($params['date'])?date('Y-m-d'):$params['date'];
        
        $arr=array('supplier_operator_id'=>$params['soid'],
                            'inv_supplier_id'=>$params['sid'],
                            'opr_id'=>$params['oid'],
                            'sync_date'=>$date,
                            'vendor_id'=>$params['vendor_id'],
                            'scid'=>implode('-',array($params['soid'],$params['sid'],$params['oid']))
                            );
        
                           
        $this->db->delete('devices_data',$arr);
        
        if($this->db->insert('devices_data',$arr+array('tfr'=>$params['incoming'],'sync_timestamp'=>date('Y-m-d H:i:s')))):
            
                    // Adjust Pending & Investment Report only if date is less than current day
                    if(strtotime($date)<strtotime('today')):

                             $query= $this->db->select('incoming')->get_where('inv_pendings',array('supplier_operator_id'=>$params['soid'],'pending_date'=>$date))->row_array();

                             $oldincoming=!empty($query)?$query['incoming']:0;
                             $pendingtoadjust=$params['incoming']-$oldincoming;


                            $updatePendingofthatday="Update inv_pendings set incoming='{$params['incoming']}',pending=pending+{$pendingtoadjust} where pending_date='{$date}' and supplier_operator_id='{$params['soid']}'  ";
                            $this->db->query($updatePendingofthatday);

                            $updateLaterPending="Update inv_pendings set pending=pending+{$pendingtoadjust} where pending_date>'{$date}' and supplier_operator_id='{$params['soid']}'  ";
                            $this->db->query($updateLaterPending);

                            $datetime=date('Y-m-d H:i:s');
                            $updateInvestment="Update earnings_logs SET update_time='{$datetime}',incoming={$params['incoming']},invested={$params['incoming']}  WHERE vendor_id={$params['vendor_id']} AND date='{$date}' ";
                            $this->db->query($updateInvestment);

                    endif;
            
            return true;
        
        endif;
        
        return false;
       
    }
    
}