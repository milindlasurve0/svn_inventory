<?php

class User extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function authenticate($data)
    {
        
        $this -> slaveDB -> select('id,username,last_login,group_id');
        $this -> slaveDB -> from('internal_users');
        $this -> slaveDB -> where('username', $data['username']);
        $this -> slaveDB -> where('password', MD5($data['password']));

        $query = $this -> slaveDB -> get();
        
        if($query -> num_rows() == 1):
            $data=$query->row();
            $this->db->update('internal_users',array('last_login'=>date('Y-m-d H:i:s')),array('id'=>$data->id));
            return $data;
        else:
            return false;
        endif;
        
    }
          
        public function change_password($data)
        {   
            $uid = getLoggedInUserId();
            $this->slaveDB->select('*');
            $this -> slaveDB -> from('internal_users');
            $this->slaveDB->where('id',$uid);
            $this->slaveDB->where('password',MD5($data['old_pwd']));
     
            $query=$this->slaveDB->get();  
            
             if ($query->num_rows() == 1):
                    $new_pwd = array('password' => md5($data['new_pwd']));
                    $this->db->where('id',$uid);
                    $this->db->update('internal_users', $new_pwd);
                    return true;
    
             else:
                    return false;

             endif;
        }
        
}

