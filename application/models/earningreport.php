<?php

class Earningreport extends CI_Model
{
    
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getOperatorInfo($operator_id,$date)
    {
        
        $sql="SELECT s.name AS suppliername,sth.supplier_id,sth.supplier_operator_id, sth.margin AS oldmargin,TRUNCATE(if(margin_type='2',((margin/(100+margin))*100),margin),3) as margin , if(sth.basesale=0,sth.target,sth.basesale) as basesale, sth.target, sth.is_api, SUM( currentsale ) AS sale "
                . "FROM inv_set_target_history sth "
                . " JOIN inv_suppliers s "
                . " ON sth.supplier_id = s.id "
                //. " JOIN inv_supplier_operator so "
               // . " ON sth.supplier_operator_id = so.id "
                . " WHERE sth.history_date = '{$date}' "
                . " AND sth.operator_id ='{$operator_id}' AND (  sth.currentsale>0 OR sth.basesale >0 OR sth.target >0  )  "
            //    . " AND ( sth.basesale >0 OR sth.target >0 OR sth.currentsale >0 ) "
                . " GROUP BY sth.supplier_operator_id ";
        
        $sql2="SELECT supplier_id, GROUP_CONCAT( v.company ) AS vendoridsnames,GROUP_CONCAT(v.id) as vendorids "
                . "  FROM inv_supplier_vendor_mapping svm "
                . " JOIN vendors v "
                . " ON svm.vendor_id = v.id "
                . " GROUP BY supplier_id";
        
         $finalSql="Select a.*,b.vendoridsnames,b.vendorids  from ({$sql}) as a JOIN ({$sql2}) as b ON a.supplier_id=b.supplier_id "; 
                
        $query=  $this->db->query($finalSql);
        
        if($query->num_rows()):
                    return $query->result_array();
        endif;
                
    }
    
    public function getLastdaySaleByOprId($operator_id)
    {
        $date=date('Y-m-d',strtotime('-2 day'));
        
        $sql=" Select SUM(if(sale>0,sale,0)) as sale,GROUP_CONCAT(DISTINCT(vendor_id)) as vendorids,supplier_operator_id "
                . " from devices_data "
                . " where opr_id='{$operator_id}' and sync_date='{$date}' "
                . " group by supplier_operator_id ";
                
         $query=  $this->db->query($sql);
         
        $data=array();  
        
        if($query->num_rows()):
                  
                      foreach($query->result_array() as $key=>$value):
                        $data[$value['supplier_operator_id']]=$value['sale'];
                     endforeach;
        endif;  
        
        return $data;
    }
    
    public function getApiMarginsByOperatorId($opr_id)
    {
        $sql="Select vendor_id,product_id,discount_commission from vendors_commissions where product_id='{$opr_id}' ";
        
         $query=  $this->db->query($sql);
        
        if($query->num_rows()):
                    return $query->result_array();
        endif;
    }
    
    public function getApiActualMarginArray($opr_id)
    {
        $date=date('Y-m-d',strtotime('-1 day'));
          
        $sql=" Select vendor_id,SUM(discount_commission*amount) as weight "
                . " from vendors v "
                . " JOIN vendors_activations va  "
                . " ON v.id=va.vendor_id AND va.product_id='{$opr_id}' AND va.date='{$date}' "
                . " Where v.machine_id='0' "
                . "  AND v.update_flag='0' AND v.active_flag='1' "
                . " group by vendor_id";
                

        $query=  $this->db->query($sql);
        
        if($query->num_rows()):
                    return $query->result_array();
        endif;
    }
    
    public function getActualModemSaleVendors($opr_id,$date="")
    {
         $data=array();
        
         $date=$date==""?date('Y-m-d',strtotime('-1 day')):$date;
           
         $sql=" select supplier_operator_id,GROUP_CONCAT(DISTINCT(v.company)) as actualsalevendors,GROUP_CONCAT(DISTINCT(v.id)) as actualsalevendorsids"
                    . "  from devices_data dd "
                    . " JOIN vendors v "
                    . " ON dd.vendor_id=v.id AND dd.sync_date='{$date}' "
                    . " Where dd.opr_id='{$opr_id}' "
                    . " and dd.sale>0 "
                    . " group by dd.supplier_operator_id";
         
          $query=  $this->db->query($sql);
        
        if($query->num_rows()):
                    
                    foreach($query->result_array() as $row):
                        $data[$row['supplier_operator_id']]=array('actualsalemodemnames'=>$row['actualsalevendors'],'actualsalemodemids'=>$row['actualsalevendorsids']);
                     endforeach;
        endif;
        
        return $data;
        
    }
    
    public function getOperationalMargin($operator_id,$date)
    {
       
        $sql=" select  va.product_id,sum(va.amount*(sp.discount_comission + if(d.parent_id = 3,(d.margin*100/(100+d.margin)) ,0) + (sd.margin*100/(100+sd.margin)))) as comm, sum(va.amount) as tot,p.parent "
                . " from vendors_activations va "
                . " LEFT JOIN products p ON va.product_id=p.id "
                . " left join retailers r "
                . " ON (va.retailer_id=r.id) "
                . " left join shop_transactions sp "
                . " ON (sp.id=va.shop_transaction_id)"
                . " left join distributors d "
                . " ON (d.id=r.parent_id) "
                . " left join super_distributors sd "
                . " ON (sd.id=d.parent_id) "
                . " where va.date='{$date}' ";

      if($operator_id):
                $sql.=" AND p.parent='$operator_id'  ";
      endif;
      
        $sql.= " group by p.parent ";
        
         $query=  $this->db->query($sql);
        
        if($query->num_rows()):
                    return $query->result_array();
        endif;
        
        return false;
        
    }
    
    function updateSupplierwiseEarningReport($data,$history_date)
    {
            foreach($data as $operator_id=>$value):
                foreach($value as $row):
                        $update_arr=array(
                                                            'baseweight'=>$row['baseweight'],
                                                            'targetweight'=>$row['targetweight'],
                                                            'actualweight'=>$row['actualweight']
                                                            );
                                                            
                       $this->db->update('inv_set_target_history',$update_arr,array('supplier_operator_id'=>$row['supplier_operator_id'],'operator_id'=>$operator_id,'history_date'=>$history_date));
                     
                       if($errormsg=$this->db->_error_message()):
                                  $sql = $this->db->last_query();
                                  logerror("Query Failed :  {$this->db->_error_message()} ","operatorsearninghistory");
                                  logerror("Sql query : {$sql}","operatorsearninghistory");
                                  send_email('vibhas@mindsarray.com','Store Operational margin',"Mysql Query for update supplierwise  cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
                   endif;
                   
              endforeach;
            endforeach;
     }
     
     function dumpOperatorwiseEarningReport($data,$history_date)
     {
             foreach($data as $operator_id=>$value):
            
                    $this->db->delete('inv_operators_earning_history',array('operator_id'=>$operator_id,'history_date'=>$history_date));
                    $insert_arr=array(
                                                            'operator_id'=>$operator_id,
                                                            'operational_margin'=>$value['operationalcost'],
                                                            'history_date'=>$history_date
                                                      );
             
                   $this->db->insert('inv_operators_earning_history',$insert_arr);              
                  
                   if($errormsg=$this->db->_error_message()):
                                  $sql = $this->db->last_query();
                                  logerror("Query Failed :  {$this->db->_error_message()} ","operatorsearninghistory");
                                  logerror("Sql query : {$sql}","operatorsearninghistory");
                                  send_email('vibhas@mindsarray.com','Store Operational margin',"Mysql Query for operational margin cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");
                   endif;

             
            endforeach;
     }
     
     public function getEarningReportByDateRange($params)
     {
          $sql="Select suppliername,supplier_operator_id,margin,operator_id,is_api,SUM(basesale) as basesale,SUM(target) as target,SUM(sale) as sale,SUM(baseweight) as baseweight,SUM(targetweight) as targetweight,SUM(actualweight) as actualweight"
                 . "  From "
                 . "            ( SELECT s.name AS suppliername,sth.margin,sth.supplier_operator_id,if(basesale=0,target,basesale) as basesale, target as target,SUM( currentsale ) AS sale, sth.is_api, baseweight, targetweight, actualweight,history_date,operator_id "
                 . "              FROM inv_set_target_history sth "
                 . "              LEFT JOIN inv_suppliers s "
                 . "              ON sth.supplier_id = s.id"
                 . "              LEFT JOIN vendors v "
                 . "              ON sth.vendor_id = v.id "
                 . "              WHERE 1  ";
          
       if(isset($params['operator_id'])):
                        $sql.=" AND operator_id='{$params['operator_id']}' ";
       endif;  
       
       $sql.= "              AND history_date>='{$params['from_history_date']}'  AND history_date<='{$params['to_history_date']}'  "
           //      . "              AND sth.is_active = '1' " commented since when supplier is inactive it target & base will be 0
                 . "              AND ( sth.basesale >0 OR sth.target >0 OR sth.currentsale >0  ) "
                 . "              AND operator_id NOT IN (54,42, 46, 40, 43, 48, 45,47,41,36,49,38) " // exclude API operator 
                 . "              GROUP BY history_date,supplier_operator_id ) as a"
                 . " GROUP BY  supplier_operator_id ";
         
        $query=  $this->slaveDB->query($sql);
        
        if($query->num_rows()):
                    return $query->result_array();
        endif;
        
        return false;
     }
     
     public function getOperationalMarginByDateRange($dates)
     {
         $sql="Select operator_id,SUM(operational_margin) as operationalmargin"
                 . "  from inv_operators_earning_history where history_date>='{$dates['from_history_date']}'  AND history_date<='{$dates['to_history_date']}' "
                 . "  group by operator_id ";
         
         $query=  $this->slaveDB->query($sql);
        
         $result=array();
         
        if($query->num_rows()):
                   foreach($query->result_array() as $row):
                            $result[$row['operator_id']]=$row['operationalmargin'];
                   endforeach;
                   return $result;;
        endif;
        
        return false;
     }
     
     public function getModemNamesByOperatorid($operator_id)
     {
         $sql="select so.id as supplier_operator_id,GROUP_CONCAT(v.id) as vendorids,GROUP_CONCAT(v.company) as vendornames"
                 . "  from inv_supplier_operator so "
                 . " JOIN inv_supplier_vendor_mapping svm"
                 . "  ON so.supplier_id=svm.supplier_id "
                 . " JOIN vendors v "
                 . " ON svm.vendor_id=v.id "
                 . " Where so.operator_id='{$operator_id}' "
                 . " group by so.id";
         
        $query=  $this->slaveDB->query($sql);
        $data=array();
        
        if($query->num_rows()):
                    
                    foreach($query->result_array() as $row):
                        $data[$row['supplier_operator_id']]=array('vendornames'=>$row['vendornames'],'vendorids'=>$row['vendorids']);
                     endforeach;
        endif;
        
        return $data;
     }
     
     public function getNotesCount($date,$operator_id=0,$type=1)
     {
          $sql="SELECT supplier_operator_id,operator_id,COUNT( id ) AS totalmessages,GROUP_CONCAT(message) as messages "
                 . " FROM inv_notes "
                 . "WHERE note_date = '{$date}' ";
      
                 
        $sql.= $operator_id>0?" AND operator_id ='{$operator_id}' ":" ";
     
      $sql.= " AND type='{$type}'  ";
          
          if($type==1):       
                  $sql.= " GROUP BY supplier_operator_id ";
          elseif($type==2):
                  $sql.= " GROUP BY operator_id ";    
          endif;
          
       
        $query=  $this->slaveDB->query($sql);
        
        $data=array();
        
        if($query->num_rows()):
  
            if($type==1):
                    foreach($query->result_array() as $row):
                               $data[$row['supplier_operator_id']]=array('totalmessages'=>$row['totalmessages'],'messages'=>$row['messages']);
                    endforeach;
            elseif ($type==2):
                    foreach($query->result_array() as $row):
                               $data[$row['operator_id']]=array('totalmessages'=>$row['totalmessages'],'messages'=>$row['messages']);
                    endforeach;
            endif;
            
        endif; 
        
        
        return $data;    
     }
     
     
     public function getInvalidData($date)
     {
         $sql="select *,(a.devices_sale-b.set_target_sale) as diff from  "
                 . "  ( SELECT supplier_operator_id, SUM( sale ) AS devices_sale,vendor_id,opr_id,operator "
                 . "   FROM devices_data "
                 . "    WHERE sync_date = '{$date}' "
                 . "   GROUP BY supplier_operator_id,vendor_id ) "
                 . " as a  JOIN  "
                 . "  (SELECT supplier_operator_id, SUM( currentsale ) AS set_target_sale,vendor_id  "
                 . "   FROM inv_set_target_history "
                 . "  WHERE history_date = '{$date}' "
                 . " GROUP BY supplier_operator_id,vendor_id ) as b "
                 . "  ON (a.supplier_operator_id=b.supplier_operator_id AND a.vendor_id=b.vendor_id) "
                 . " where a.devices_sale <> b.set_target_sale order by a.vendor_id ";
         
          $query=  $this->db->query($sql);
          
           if($query->num_rows()):
               
                    return $query->result_array();
                    
           endif;
           
           return ;
           
     }
}

