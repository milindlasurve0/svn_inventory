<?php

class Sb extends CI_Model
{
    public function setSbToDb($data)
    {
          $insert_array=array();
            
            foreach($data as $row):
                  
                    $insert_array[]=array(
                                                                'supplier_operator_id'=>$row['supplier_operator_id'],
                                                                'supplier_id'=>$row['supplierid'],
                                                                'operator_id'=>$row['operator_id'],
                                                                'last7daysavg'=>$row['avg_sale'],
                                                                'lastdaysale'=>$row['last_day_sale'],
                                                                'currentsale'=>$row['todaysale'],
                                                                'opening'=>is_null($row['opening'])?0:$row['opening'],
                                                                'closing'=>$row['currentstock'],
                                                                'totalavaliablesims'=>is_null($row['totalavaliablesims'])?0:$row['totalavaliablesims'],  // total Ava sims ST
                                                                'totalactivesims'=>is_null($row['totalsims'])?0:$row['totalsims'],  //Not block and are active flag =1 SB
                                                                'simsforst'=>is_null($row['simsforst'])?0:$row['simsforst'], // Balance > 5000 OR balance > 1000 for idea ST
                                                                'workingsims'=>is_null($row['workingsims'])?0:$row['workingsims'],  // Balance > 0 AND active =1 AND sale > 10 AND block=0 SB
                                                                'basesale'=>is_null($row['basesale'])?0:$row['basesale'],
                                                                'target'=>is_null($row['targetsale'])?0:$row['targetsale'],
                                                                'modem_target'=>$row['modemtarget'],
                                                                'is_api'=>$row['is_api'],
                                                                'blocked_bal'=>is_null($row['blockedbalance'])?0:$row['blockedbalance'],
                                                                'vendor_id'=>$row['vendorid'],
                                                                'disputeamt'=>is_null($row['disputeamt'])?0:$row['disputeamt'],
                                                                'order_amount'=>is_null($row['todaysorder'])?0:$row['todaysorder'],
                                                                'pending'=>is_null($row['pending'])?0:$row['pending'],
                                                                'max_sale_capacity'=>is_null($row['max_sale_capacity'])?0:$row['max_sale_capacity'],
                                                                'margin'=>$row['margin'],
                                                                'is_active'=>$row['is_active'],
                                                                'history_date'=>date('Y-m-d'),
                                                                'margin_type'=>$row['margin_type']
                                                           );
                                                           
            endforeach;
            
            
               logerror("Salebifurcation JSON data : ". json_encode($insert_array),"salebifurcationhistory");
                
               $insert_array=  createBatch($insert_array,50);
               
                foreach ($insert_array as $batch):
                    
                             $this->db->insert_batch('inv_set_target_history', $batch);
                
                             // If Query Fails
                              if($errormsg=$this->db->_error_message()):
                                  
                                  // Try Again
                                  if($errormsg=="MySQL server has gone away"):
                                      
                                        $this->db->close();
                                        $this->db->initialize(); 
                                        $this->db->insert_batch('inv_set_target_history', $batch);
                                        
                                        // Log if Failed Again
                                        if($this->db->_error_message()): logerror("Failed Again","salebifurcationhistory"); $this->triggerAlert(); endif;
                                        
                                  else:
                                          $this->triggerAlert();
                                  endif;
                                  
                              endif;
                              
            endforeach;
            
    }
    
        public function triggerAlert()
        {
            $sql = $this->db->last_query();
            logerror("Query Failed :  {$this->db->_error_message()} ","salebifurcationhistory");
            logerror("Sql query : {$sql};","salebifurcationhistory");
            send_email('vibhas@mindsarray.com','Store Set target History',"Mysql Query for set target cron  Failed  with error {$this->db->_error_message()} :  {$sql} ");

        }
}
