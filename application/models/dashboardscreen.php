<?php

class dashboardscreen extends CI_Model
{
    
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getTodaysOrdersdetails($params)
    {
        $date=date('Y-m-d');
        $pdate=date('Y-m-d',strtotime('today -1 day'));
        $operator_id=$params['operator_id']>0?$params['operator_id']:"";
        $supplier_id=isset($params['supplier_id'])?($params['supplier_id']>0?$params['supplier_id']:""):"";
         $join=isset($params['order_exists'])?"":"LEFT";
        $devicesstr="";
        if($operator_id):
            $devicesstr=" AND dd.opr_id ='{$operator_id}' ";
        endif;
        
        $vendorfilter="";
        
        $sql="Select  so.id as supplier_operator_id,s.name AS suppliername,s.id as supplier_id,po.name AS operator,po.id AS operator_id,"
                . "pa.order_id AS orderid,so.is_active,so.commission_type_formula as margin,so.commission_type,"
                . "pa.amount AS amount,pa.amount_by_bounds as expected, pa.to_pay,pa.utr as txnid,"
                . "o.amount AS ordertableamount, is_approved, is_downloaded, is_payment_done, pa.sms,pa.email,pa.id as paymentid,"
                . "pa.status as payment_status, p.pending as till_date_pending,planned_sale  "
                . " FROM inv_supplier_operator so "
                . " JOIN inv_suppliers s "
                . " ON so.supplier_id = s.id "
                . " JOIN products po "
                . " ON po.id = so.operator_id  "
                . " LEFT JOIN inv_pendings p  "
               . "  ON (so.id=p.supplier_operator_id AND p.pending_date='{$pdate}' ) "
               . " {$join} JOIN  inv_payments pa  "
               . " ON ( pa.so_id = so.id AND pa.payment_date='{$date}' ) "
               . " LEFT JOIN inv_orders o "
               . " ON  (pa.order_id = o.id AND pa.status = '0') LEFT JOIN inv_planning_sheet ps ON ps.supplier_operator_id = so.id "
                . " WHERE  1  ";
        
        if($operator_id):
                    $sql.=" AND so.operator_id='{$operator_id}' ";
        endif;
        
        if(!empty($supplier_id)):
                    $sql.=" AND so.supplier_id='{$supplier_id}' ";
        endif;
        
        
        if(isset($params['handled_by']) && $params['handled_by']>0):
              $sql.=" AND s.handled_by={$params['handled_by']} ";
        endif;         
        
       
         $sql.= " ORDER BY pa.order_id,operator_id,so.id"; 
        
        $sql=  $this->MergeDevicesReceived($sql,$operator_id);  
         
         if(isset($params['vendors'])):
                    if(count($params['vendors'])>1):
                        $vendorInStat=implode(',',$params['vendors']);
                    else:
                          $vendorInStat=$params['vendors'][0];
                    endif;
                    $vendorfilter.=" AND vendor_id IN($vendorInStat) ";
       endif;
       
        $sql2="SELECT supplier_id, GROUP_CONCAT( vendor_id ) AS vendorids  FROM inv_supplier_vendor_mapping  Where 1 {$vendorfilter}  GROUP BY supplier_id ";
          
        $finalsql="Select a.*,b.vendorids from ($sql) as a  JOIN ($sql2) as b ON a.supplier_id=b.supplier_id order by a.is_active ASC ";
              
         //$query=  $this->db->query($finalsql);
         $query=  $this->slaveDB->query($finalsql);
        
        if($query->num_rows()):
            
                            return $query->result_array();
        
        endif;
        
        return false;
    }
    
    function MergeDevicesReceived($sql,$operator_id="")
    {
        $date=date('Y-m-d');
        
        $devicesSQL="Select  supplier_operator_id,SUM( IF( tfr IS NULL , 0, tfr )) as received,SUM(opening) as opening "
                               . " from devices_data  "
                               . " Where sync_date='{$date}'   ";
                               
        if($operator_id):
                    $devicesSQL.=" AND opr_id='{$operator_id}' ";
        endif;
        
        $devicesSQL.= " group by supplier_operator_id ";
        
        $finalSQL="SELECT a.*,b.received,b.opening  FROM  ({$sql}) as  a LEFT JOIN  ({$devicesSQL}) as b ON a.supplier_operator_id=b.supplier_operator_id ";
        
        return $finalSQL;
        
    }
    
    function getTodaysOrdersummary()
    {
        $date=date('Y-m-d');
        
        $sql="select po.id as operator_id,po.name as operator,SUM(amount) as totalorder,count(o.id) as total "
                . " from inv_orders o "
                . " JOIN inv_suppliers s "
                . " ON o.supplier_id=s.id "
                . " JOIN products po "
                . " ON po.id=o.operator_id "
                . " AND order_date = '{$date}' "
                . " group by operator_id "
                . " order by operator_id";
                
        $query=  $this->slaveDB->query($sql);
        
        if($query->num_rows()):
            
                            return $query->result_array();
        
        endif;
        
        return false;        
    }
    
    function getsmsRelatedData($params)
    {
        $data=array();
        
        $sql="select s.name,pa.amount_by_bounds as todaysorder,pa.to_pay,pa.utr as txnid,p.name as operator,s.email_id as email,sb.account_no "
                . " from inv_payments pa"
                 . " JOIN inv_supplier_operator so "
                 . " ON pa.so_id=so.id  "
                . " JOIN inv_suppliers s "
                . " ON so.supplier_id=s.id "
                 . " JOIN products p "
                 . " ON p.id=so.operator_id"
                 . " JOIN inv_orders o"
                 . " ON o.id=pa.order_id"
                 . " JOIN inv_supplier_banks sb"
                 . " ON sb.id=o.supplier_bank_id"
                . " where pa.id='{$params['paymentid']}'";
                     
        $query=  $this->db->query($sql);
        
        if($query->num_rows()):
                
                        $data['info']=$query->row_array();
                
                        $data['mobile']=  $this->getContactBySupplierId($params['supplierid']);
                        
                        return $data;
       endif;
       
       
       return false;;
    }
    
    function getContactBySupplierId($supplier_id)
    {
         $sql="select contact "
                . " from inv_supplier_contacts "
                . " where supplier_id='{$supplier_id}' "
                . " and to_send='1' ";
         
        
          $query=  $this->db->query($sql);
          
            if($query->num_rows()):
                
                        return $query->result_array();
                       
            endif;
    }
    
    function setSmsEmailFlag($paymentid,$sms=false,$email=false)
    {
        if($paymentid):
            $key=$sms?"sms":"email";

            $update_array=array($key=>'1');   
            $this->db->update('inv_payments',$update_array,array('id'=>$paymentid));
           
        endif;             
        
    }
}

