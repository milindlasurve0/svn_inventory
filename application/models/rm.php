<?php

class Rm extends CI_Model
{
    
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function saveRM($formdata)
    {
        $data=array(
                                'name'=>$formdata['rm_name'],
                                'mobile'=>$formdata['rm_contact'],
                                'email_id'=>$formdata['rm_email'],
                                'address'=>$formdata['rm_address']
                               );
        
        $this->db->set('created_at',date("Y-m-d H:i:s"));
        
        if($this->db->insert('inv_rm',$data)):
            
                return true;
        
        endif;
        
        return false;
        
    }
    
    public function findAll($pageno)
    {
        
      $query = $this->slaveDB->get('inv_rm','5',($pageno-1)*5);
        
        return $query->result();
    }
    
    public function findRmById($id=0)
    {
        if($id>0):
        
            $this->slaveDB->where('id', $id); 
        
            $this->slaveDB->limit(1);
            
        endif;
       
        $query = $this->slaveDB->get('inv_rm');
        
        return $query->row();
    }
    
    public function UpdateRM($id,$formdata)
    {
         $data=array(
                                'name'=>$formdata['rm_name'],
                                'mobile'=>$formdata['rm_contact'],
                                'email_id'=>$formdata['rm_email'],
                                'address'=>$formdata['rm_address']
                               );
         $this->db->set('updated_at',date("Y-m-d H:i:s"));
         
         $this->db->where('id', $id);
         
         if($this->db->update('inv_rm', $data)):
             
                return true;
         
        endif;
        
          return false;
         
    }
    
    public function deleteRM($id)
    {
        if($this->db->delete('inv_rm', array('id' => $id))):
            return true;
        endif;
        
        return false;
    }
    
    public function getRMForDropdown()
    {
        $query=$this->slaveDB->select('id,name')->get('inv_rm');
        
        return $query->result();
    }
    
     public function getTotalRows()
        {
           return  $this->slaveDB->count_all('inv_rm');
        }
        
        public function findSuppliersByRmId($params,$rm_id,$pageno,$perpage)
        {
            $date=date('Y-m-d');
             
            $sql=" SELECT  s.id AS supplier_id,s.name as supplier,so.id as soid,po.name as operator,ip.pending,o.amount as todaysorder,"
                    . " so.commission_type_formula as margin,capacity_per_month as capacity,so.commission_type,"
                    . " ps.base_amount,s.email_id,s.location,rm.name as rmname,so.is_active,GROUP_CONCAT(DISTINCT(v.company)) as vendors,"
                    . " COUNT(dd.id) as totalsims,SUM(if(dd.block='0' and dd.active_flag='1',1,0)) as activesims "
                   . " FROM inv_supplier_operator so "
                   . " JOIN inv_suppliers s "
                   . " ON so.supplier_id=s.id "
                   . " JOIN products  po "
                   . " ON so.operator_id=po.id "
                   . " JOIN inv_planning_sheet ps "
                   . " ON ps.supplier_operator_id=so.id "
                   . " LEFT JOIN inv_rm rm "
                   . " ON rm.id=s.inv_rm_id "
                   . " LEFT JOIN inv_supplier_vendor_mapping svm "
                   . " ON s.id=svm.supplier_id "
                   . " LEFT JOIN vendors v "
                   . " ON svm.vendor_id=v.id  "
                   . " LEFT JOIN devices_data dd"
                   . " ON (dd.supplier_operator_id=so.id and dd.sync_date='{$date}')"
                   . " LEFT JOIN inv_pendings ip"
                   . " ON (ip.supplier_operator_id=so.id and ip.pending_date='{$date}')"
                   . " LEFT JOIN inv_orders o"
                   . " ON (o.supplier_operator_id=so.id and o.order_date='{$date}')"
                   . " WHERE  1 ";
                   
            if(!empty($rm_id)):
                $sql.=" AND rm.user_id='$rm_id' ";
            endif;
            
            if(!empty($params['operator_id'])):
                   $sql.=" AND so.operator_id='{$params['operator_id']}' ";
            endif;

            if(!empty($params['modems_ids'])):
                   $sql.=" AND v.id IN ({$params['modems_ids']}) ";
            endif;

            if(!empty($params['is_active'])):
               if(in_array($params['is_active'],array('no','yes')) && $params['is_active']!="all" ):
                      $is_active=$params['is_active']=="yes"?'1':'0';
                      $sql.=" AND so.is_active='{$is_active}' ";
               endif;
            endif;

            $sql.= " GROUP BY  so.id ";

            if(!empty($params['orderby'])):
                   $sql.=" ORDER BY {$params['orderby']} {$params['ordertype']} ";
            endif;
             
            $sql .= " LIMIT ".($pageno-1)*25 .",".$perpage;
            
            $query=  $this->slaveDB->query($sql);

            if($query->num_rows()):
                    return $query->result_array();
            endif;
            
            return array();
        }

        public function getSuppliersForDropdownByRmId($rm_id)
        {
            $sql="select s.id,s.name"
                    . " from inv_suppliers s"
                    . " join inv_rm rm"
                    . " on rm.id=s.inv_rm_id"
                    . " WHERE  1 ";
                   
            if(!empty($rm_id)):
                $sql.=" AND rm.user_id='$rm_id' ";
            endif;
                    
            $query=$this->slaveDB->query($sql);
       
            return $query->result();
        }

        public function getTotalRowsByRmId($params,$rm_id)
        {
            $date=date('Y-m-d');
             
            $sql=" SELECT  s.id AS supplier_id,s.name as supplier,so.id as soid,po.name as operator,ip.pending,o.amount as todaysorder,"
                    . " so.commission_type_formula as margin,capacity_per_month as capacity,so.commission_type,"
                    . " ps.base_amount,s.email_id,s.location,rm.name as rmname,so.is_active,GROUP_CONCAT(DISTINCT(v.company)) as vendors,"
                    . " COUNT(dd.id) as totalsims,SUM(if(dd.block='0' and dd.active_flag='1',1,0)) as activesims "
                   . " FROM inv_supplier_operator so "
                   . " JOIN inv_suppliers s "
                   . " ON so.supplier_id=s.id "
                   . " JOIN products  po "
                   . " ON so.operator_id=po.id "
                   . " JOIN inv_planning_sheet ps "
                   . " ON ps.supplier_operator_id=so.id "
                   . " LEFT JOIN inv_rm rm "
                   . " ON rm.id=s.inv_rm_id "
                   . " LEFT JOIN inv_supplier_vendor_mapping svm "
                   . " ON s.id=svm.supplier_id "
                   . " LEFT JOIN vendors v "
                   . " ON svm.vendor_id=v.id  "
                   . " LEFT JOIN devices_data dd"
                   . " ON (dd.supplier_operator_id=so.id and dd.sync_date='{$date}')"
                   . " LEFT JOIN inv_pendings ip"
                   . " ON (ip.supplier_operator_id=so.id and ip.pending_date='{$date}')"
                   . " LEFT JOIN inv_orders o"
                   . " ON (o.supplier_operator_id=so.id and o.order_date='{$date}')"
                   . " WHERE  1 ";
                   
            if(!empty($rm_id)):
                $sql.=" AND rm.user_id='$rm_id' ";
            endif;

            if(!empty($params['operator_id'])):
                   $sql.=" AND so.operator_id='{$params['operator_id']}' ";
            endif;

            if(!empty($params['modems_ids'])):
                   $sql.=" AND v.id IN ({$params['modems_ids']}) ";
            endif;

            if(!empty($params['is_active'])):
               if(in_array($params['is_active'],array('no','yes')) && $params['is_active']!="all" ):
                      $is_active=$params['is_active']=="yes"?'1':'0';
                      $sql.=" AND so.is_active='{$is_active}' ";
               endif;
            endif;

            $sql.= " GROUP BY  so.id ";

            if(!empty($params['orderby'])):
                   $sql.=" ORDER BY {$params['orderby']} {$params['ordertype']} ";
            endif;
            
            $query=  $this->slaveDB->query($sql);

            if($query->num_rows()):
                   return $query->num_rows();
            endif;
            
            return 0;
                
        }
        
        public function getTotalUniqueSuppliers($rm_id)
        {
            $sql="SELECT count( 1 ) AS totalsuppliers"
                    . " FROM inv_suppliers s"
                    . " join inv_rm rm"
                    . " on rm.id=s.inv_rm_id"
                    . " WHERE  1 ";
                   
            if(!empty($rm_id)):
                $sql.=" AND rm.user_id='$rm_id' ";
            endif;

            $query=  $this->slaveDB->query($sql);
            if($query->num_rows()):
                       $data=$query->row_array();
                       return $data['totalsuppliers'];
            endif;
             
            return;
            
        }
        
        public function getOrderHistoryByDate($params,$rm_id)
        {
      
            $join=isset($params['order_exists'])?"":"LEFT";

            $sql1="SELECT o.id as order_id,o.order_date,p.margin as margin,s.id as supplierid,s.name as suppliername,po.name as operatorname,p.operator_id,p.supplier_operator_id,o.amount,o.to_pay,o.amount_by_bounds,p.incoming,p.pending,p.pending_date,is_approved,is_downloaded,is_payment_done,o.is_adjusted,o.comment,o.txnid,rm.name as rm_name "
                        . " from inv_pendings p  "
                        . "  {$join} JOIN inv_orders o  "
                        . " ON (p.order_id=o.id AND o.is_payment_done='1')  "
                        . " JOIN inv_supplier_operator so  "
                        . " ON so.id=p.supplier_operator_id "
                        . "  JOIN inv_suppliers s "
                        . "  ON s.id=so.supplier_id  "
                        . " JOIN products po  "
                        . " ON po.id=so.operator_id  "
                        . " JOIN inv_rm rm "
                        . " ON rm.id=s.inv_rm_id "
                        . " WHERE pending_date>='{$params['start']}' and pending_date<='{$params['end']}'";
                   
            if(!empty($rm_id)):
                $sql1.=" AND rm.user_id='$rm_id' ";
            endif;
            
            if($params['operator_id']>0):
                 $sql1.=" AND po.id={$params['operator_id']} ";
             endif;

             if($params['supplier_id']>0):
                  $sql1.=" AND s.id={$params['supplier_id']} ";
             endif;    

            $sql1.= "ORDER by pending_date ";

            // Get vendorids
             $sql2="SELECT supplier_id, GROUP_CONCAT( vendor_id ) AS vendorids  FROM inv_supplier_vendor_mapping   GROUP BY supplier_id ";

            // Join based on supplierid
             $finalsql="Select a.*,b.vendorids from ($sql1) as a LEFT JOIN ($sql2) as b ON a.supplierid=b.supplier_id ORDER BY  a.pending_date ";

            $query=  $this->slaveDB->query($finalsql);

             if($query->num_rows()):

                    return $query->result();

             endif;

             return false;
        
    }   
}