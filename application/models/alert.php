<?php

class Alert extends CI_Model
{
    public function getValidOperators()
    {
        $sql="SELECT id,name as operator_name  FROM `products` WHERE to_show = '1' AND monitor = '1' ";
        $query=  $this->db->query($sql);
        if($query->num_rows()):
                return $query->result_array();
        endif;
    }
    
    public function getAlerts()
    {
        $start=date('Y-m-d H:i:s',strtotime('-6 minutes'));
        $end=date('Y-m-d H:i:s:',strtotime('-1 minutes'));
        $date=date('Y-m-d');  
        
       $sql="Select company,operatorname,vendor_id,product_id,COUNT(id) as totaltransaction,SUM(is_benchmark_exceeded) as benchmark_exceeded_count "
                . " FROM ( "
                                . " SELECT v.company,p.name as operatorname,va.id, va.vendor_id, va.product_id, va.ref_code, va.shop_transaction_id, va.timestamp AS start_time, vt.processing_time, TIMESTAMPDIFF( SECOND , va.timestamp, vt.processing_time ) AS diff,benchmark_processtime,if( TIMESTAMPDIFF( SECOND , va.timestamp, vt.processing_time ) > benchmark_processtime,1,0) as is_benchmark_exceeded "
                                . " FROM vendors_activations va "
                                . " JOIN vendors_transactions vt "
                                . " ON ( va.vendor_id = vt.vendor_id AND va.ref_code = vt.ref_id AND va.date = '{$date}' ) "
                                . " JOIN vendors v "
                                . " ON va.vendor_id = v.id "
                                . " JOIN products p "
                                . " ON va.product_id=p.id "
                                . " WHERE 1 "
                                . " AND v.update_flag != '0'  AND v.machine_id != '0' "
                                . " AND va.timestamp >= '{$start}' AND va.timestamp <= '{$end}' "
                                . " AND vt.status IN ('0','1') "
                . " ) as a "
                . "  GROUP BY  vendor_id,product_id "
                . " HAVING benchmark_exceeded_count > 0  ";
        
        $query=  $this->db->query($sql);
        
         if($query->num_rows()):
                return $query->result_array();
        endif;
        
        return ;
    }
}