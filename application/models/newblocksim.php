<?php

class Newblocksim extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getBlockSimsByDate($params)
    {
        $sql="select so.id as supplier_operator_id,s.name as suppliername,p.name as operator,v.company as vendor_name,ib.*,b.name as tag,b.description,u.name as username "
                . "  from inv_blocksims_history ib "
                . " JOIN inv_suppliers s "
                . " ON ib.inv_supplier_id=s.id "
                . " JOIN inv_internal_person ip "
                . " ON (ip.id=s.handled_by) "
                . " JOIN products p "
                . " ON ib.opr_id=p.id "                
                . " JOIN inv_supplier_operator so "
                . " ON (so.supplier_id=s.id and so.operator_id=p.id)"
                . " JOIN vendors v "
                . " ON ib.vendor_id=v.id "
                . " LEFT JOIN inv_block_tags b  "
                . " ON b.id=ib.blocktag_id "
                . " LEFT JOIN users u "
                . " ON (u.id=ib.user_id) "
                . " Where "
                . " ib.block_date >= '{$params['from_date']}' and ib.block_date <= '{$params['to_date']}' ";
                
        if(!empty($params['operator_id'])):
            $sql.= "and ib.opr_id='{$params['operator_id']}'";
        endif;
        
         if(isset($params['modems'])):
                    if(count($params['modems'])>1):
                        $vendorInStat=implode(',',$params['modems']);
                    else:
                          $vendorInStat=$params['modems'][0];
                    endif;
                    $sql.=" AND ib.vendor_id IN($vendorInStat) ";
       endif;
       
       if($params['status'] != "all"):
           $status=($params['status']=="block")?"1":"0";
           $sql.=" and ib.block='{$status}' ";
       endif;
       
       if($params['handled_by']):
           $sql.=" and s.handled_by='{$params['handled_by']}' ";
       endif;
       
       $query=  $this->slaveDB->query($sql);
       
       if($query->num_rows()):
            return $query->result_array();
       endif;
       
       return;
    }
    
    public function getBlockTagsForDropdown()
    {
        $query=$this->slaveDB->select('id,name as block_tag')->get('inv_block_tags');
       
         return $query->result();
    }
    
    public function getCommentsByBlocksimId($blockid)
    {
        $sql="Select message,note_date,note_timestamp,u.name as username from inv_notes  i JOIN users  u ON  i.user_id=u.id   where i.changing_param='{$blockid}'  ";
                  
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            return $query->result();
        endif;
        
        return;
    }
    
    public function saveCommentsByBlocksimID($params)
    {
         $arr=array('supplier_operator_id'=>$params['soid'],
//                            'type'=>$params['type'],
                             'operator_id'=>isset($params['oprid'])?$params['oprid']:0,  
                             'changing_param'=>$params['blockid'],
                            'user_id'=>  getLoggedInUserId(),
                            'message'=>$params['comment'],
                            'note_date'=>date('Y-m-d'),
                            'note_timestamp'=>date('Y-m-d H:i:s')
                            );
        
        if($this->db->insert('inv_notes',$arr)):
            return true;
        endif;
        
        return false;
    }
    
    public function getMsgCountByBlocksimId($blockid)
    {
        $sql="Select changing_param,count(id) as totalmessages from inv_notes where changing_param in ($blockid) group by changing_param";
        
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            $temp=array();
        
            $result=$query->result_array();
            
            foreach ($result as $res):
                $temp[$res['changing_param']]=$res;
            endforeach;
            return $temp;
        endif;
        
        return;
    }
}
