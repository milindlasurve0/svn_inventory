<?php

class Pending extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getPendingBySupplierId($supplier_id=0,$getTotalamt=false,$excudeoperator=0)
    {
        if($supplier_id>0):
            
           $date=date('Y-m-d',strtotime('-1 day')); 
        
             $sql="SELECT p.id, supplier_operator_id, supplier_id, po.name, p.operator_id, pending, pending_date, so.commission_type_formula, so.commission_type,(pending-(pending*commission_type_formula)/100) as pendingamt "
                . " FROM inv_pendings p "
                . " JOIN inv_supplier_operator so "
                . " ON p.supplier_operator_id = so.id "
                . " JOIN products po "
                . " ON po.id = so.operator_id "
                . " AND supplier_id ='{$supplier_id}' "
                . " AND p.pending_date = '{$date}'  ";
                
           if($excudeoperator):
                $sql.=" AND  p.operator_id <> '{$excudeoperator}' ";
           endif;       
            
           $query=$this->slaveDB->query($sql);
           
           if($query->num_rows()):
                
                $result=$query->result();
           
                if($getTotalamt):
                    $total=0;
                    foreach($result as $value):
                            $total+=$value->pendingamt;
                    endforeach;
                  return $total;  
                endif;
                
                return $result;
                   
           endif;
           
        endif;
    }
    
    public function getOrderById($orderid)
    {
        $date=date('Y-m-d',strtotime('-1 day')); 
        
        $sql="SELECT po.name AS operator_name, amount ,amount, amount_by_bounds, to_pay, margin, pending "
                . " FROM inv_orders o  "
                . " LEFT JOIN inv_pendings p  "
                . " ON o.supplier_operator_id = p.supplier_operator_id "
                . " JOIN products po "
                . " ON po.id = o.operator_id "
                . " AND p.pending_date = '{$date}' "
                . " WHERE o.id ='$orderid' ";

         $query=$this->slaveDB->query($sql);         
         
         if($query->num_rows()):
             
             return $query->result();
         
         endif;
         
         return false;
    }
    
    public function getPendingBySoid($soid)
    {
        if($soid)
        {
           $sql="select p.id,pending,so.commission_type_formula,po.name as oprname,so.supplier_id,so.operator_id,so.id as soid,so.commission_type_formula,p.pending_date  "
                    . "  from inv_pendings p "
                    . " JOIN inv_supplier_operator so "
                    . " ON  p.supplier_operator_id=so.id JOIN products po ON po.id=so.operator_id"
                    . "  where supplier_operator_id='{$soid}' "
                    . "  and pending_date=(Select MAX(pending_date) from inv_pendings where supplier_operator_id='{$soid}')";
                    
           $query=$this->slaveDB->query($sql);         
         
         if($query->num_rows()):
             
             return $query->row();
         
         endif;
         
         return false;
         
        }
        
           return false;
    }
    
    public function getOrderidByPendingId($pendingid)
    {
        $sql="Select order_id from inv_pendings where id='{$pendingid}'";
        
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            
              $result=$query->row();
        
              return $result->order_id;
        
        endif;
        
        return false;
    }
    
   public function createOrder($data,$isParent=true)
   {
         $factor=$isParent?(1):(-1);
       
        $insert_array=array(
                                                'supplier_id'=>$data['supplier_id'],
                                                'operator_id'=>$data['operator_id'],
                                                'supplier_operator_id'=>$data['soid'],
                                                'margin'=>$data['margin'],
                                                'amount'=>($data['textboxsum']/((100-$data['margin'])/100))*($factor),
                                                'to_pay'=>($data['textboxsum'])*($factor),
                                                'amount_by_bounds'=>($data['textboxsum']/((100-$data['margin'])/100))*($factor),
                                                'created_by'=>'1',
                                                'is_approved'=>'1',
                                                'is_downloaded'=>'1',
                                                'is_payment_done'=>'1',
                                                'is_adjusted'=>'1', 
                                                'txnid'=>'adjusted',
                                                'supplier_bank_id'=>'0',
                                                'checked_by'=>  getLoggedInUserId(),
                                                'payed_by'=>  getLoggedInUserId(),
                                                'batch'=>'1',
                                                'order_date'=>$data['pending_date']
                                            );
        
         
        if($this->db->insert('inv_orders',$insert_array)):
            return $this->db->insert_id();
        endif;
        
         return false;
        
   }
   
   public function adjustOrder($orderid,$data,$isParent=true)
   {
         $sql="Select amount,to_pay,amount_by_bounds,txnid,margin from inv_orders where id='{$orderid}'";
        
       
       
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            
            $result=$query->row_array();
        
            $to_pay=$isParent?($result['to_pay']+$data['textboxsum']):($result['to_pay']-$data['textboxsum']);
            $amount=$to_pay/((100-$result['margin'])/100);
            $amount_by_bound=$to_pay/((100-$result['margin'])/100);        
           
            
            $update_array=array(
                                                    'amount'=>$amount,
                                                    'amount_by_bounds'=>$amount_by_bound,
                                                    'to_pay'=>$to_pay,
                                                    'is_adjusted'=>'1'
                                                );
            
            $this->db->update('inv_orders',$update_array,"id = $orderid");
            
        endif;
   }
   
   public function adjustPendingTable($orderid,$data)
   {
       $sql="Select pending,incoming from inv_pendings where id='{$data['id']}' ";
        
        $query=$this->slaveDB->query($sql);
        
         if($query->num_rows()):
             
             $result=$query->row_array();
         
             $update_array=array('pending'=>$data['pending'],'order_id'=>$orderid);
             
             $id=$data['id'];
             
             $this->db->update('inv_pendings',$update_array,"id = $id");
           
         endif;
   }
   
   public function getTransactionDetailsByOrderid($id)
   {
       $sql="Select txnid,supplier_bank_id from inv_orders where id='{$id}'";
        
          $query=$this->slaveDB->query($sql);
        
         if($query->num_rows()):
             
                    return $query->row_array();
         
         endif;
         
         return false;
   }
   
   public function setLog($orderid,$data,$str,$isParent=true)
   {
       if($isParent):
       $message="Adjusted {$data['textboxsum']} to  ";
       $logstring=count($str)>1?implode(" & ",$str):$str[0];
       $message.=$logstring;
       else:
       $message="Adjusted {$data['textboxsum']} from  ";
       $logstring=$str[0];
       $message.=$logstring;   
       endif;
       
       $update_array=array('comment'=>$message);
       $this->db->update('inv_orders',$update_array,array('id'=>$orderid));
      
      }
}
