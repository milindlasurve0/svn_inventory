<?php

class Reconcile extends CI_Model
{
    
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
  
    public function getReconcileReport($params)
    {
        $prev_date = date("Y-m-d",strtotime($params.'-1 days'));
//        echo $prev_date;
        
        $sql ="SELECT SUM(todaypending) as pendingclosing,SUM(pendingclosing) as pendingclosingamt,SUM(previouspending) as pendingopening,SUM(pendingopening) as pendingopeningamt,SUM(to_pay) as to_pay,SUM(adjustments) as adjustments,SUM(inbank) as inbank,SUM(if(b.incoming is null,0,b.incoming)) as incoming,SUM(if(b.incominginvested is null,0,b.incominginvested)) as incominginvested
                      FROM (Select p.pending_date,p.margin,p.supplier_operator_id,p.pending as todaypending, CEIL(SUM( IF( so.commission_type =1, (p.pending) * ( ( 100 - p.margin ) /100 ) , (p.pending) * ( 100 / ( 100 + p.margin ) ) ) ) ) as pendingclosing,pprev.pending as previouspending, CEIL(SUM( IF( so.commission_type =1, (pprev.pending) * ( ( 100 - pprev.margin ) /100 ) , (pprev.pending) * ( 100 / ( 100 +pprev.margin ) ) ) ) ) as pendingopening, if(to_pay is null,0,to_pay) as to_pay,if(p.refund_type='1',p.refund,0) as inbank, if(p.refund_type='2',p.refund,0) as adjustments
                      from inv_supplier_operator so
                      LEFT JOIN inv_pendings p
                      ON (p.supplier_operator_id=so.id AND p.pending_date='$params')
                      LEFT JOIN inv_orders o
                      ON (so.id=o.supplier_operator_id AND o.order_date='$params' AND o.is_payment_done='1')
                      LEFT JOIN inv_pendings pprev
                      ON (pprev.supplier_operator_id=so.id AND pprev.pending_date='$prev_date')
                      GROUP by supplier_operator_id ) as a
                      LEFT JOIN (
                      select so.id as supplier_operator_id,SUM(tfr) as incoming,CEIL( SUM( IF( so.commission_type =1, ( tfr) * ( ( 100 - pe.margin ) /100 ) , (tfr) * ( 100 / ( 100 + pe.margin ) ) ) ) ) as IncomingInvested
                      from inv_supplier_operator so
                      JOIN devices_data dd
                      ON (so.id=dd.supplier_operator_id)
                      JOIN inv_pendings pe
                      ON (pe.supplier_operator_id=so.id AND pe.pending_date='$params')
                      Where dd.sync_date='$params' group by so.id having incoming > 0 )as b
                      ON a.supplier_operator_id=b.supplier_operator_id";
        
            $query=  $this->slaveDB->query($sql);
//        print_r($this->slaveDB->last_query());
            if($query->num_rows()):
                return $query->result();
            endif;
             
        return false;
    }
    
    public function getReconcileReportByRefundType($params)
    {
        $sql = "select s.name as supplier_name,p.name as operator_name,ip.refund,ip.refund_type,ip.comment"
                . " from inv_supplier_operator so"
                . " join inv_pendings ip"
                . " on ip.supplier_operator_id = so.id"
                . "  join inv_suppliers s"
                . " on s.id = so.supplier_id"
                . " join products p"
                . " on p.id = so.operator_id"
                . " where ip.pending_date = '$params'"
                . " and ip.refund_type != '0'";

        $query = $this->slaveDB->query($sql);
        
        if($query->num_rows()):
            return $query->result_array();
        endif;
        
        return false;
    }
}
    