<?php

class Account extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getOrder($params)
    {
        $date=date('Y-m-d');
        
        $sql="SELECT s.name AS supplier_name,s.single_payment,s.frombank,p.name AS operator_name, o.id AS order_id, o.supplier_operator_id, o.amount, o.to_pay,sb.account_no ,b.id AS bank_id, b.bank_name, o.batch,is_downloaded,is_payment_done,txnid,sb.account_holder_name   "
                . " FROM `inv_orders` o "
                . " LEFT JOIN inv_suppliers s "
                . " ON o.supplier_id = s.id "
                . " LEFT JOIN products p "
                . " ON o.operator_id = p.id "
                . " LEFT JOIN inv_supplier_banks sb "
                . " ON o.supplier_bank_id = sb.id JOIN inv_banks b ON sb.bank_id=b.id  "
                . " WHERE order_date = '{$date}' AND o.is_approved='1' ";
                
          // For Batch Filter      
          if($params['batch_type']>0 && is_numeric($params['batch_type'])):
              $sql.=" AND o.batch='{$params['batch_type']}'  ";
          endif; 
          
          // For Operator filter
          if($params['operator_id']>0 && is_numeric($params['operator_id'])):
              $sql.=" AND o.operator_id='{$params['operator_id']}'  ";
          endif;      
          
          // For update txnID
          if(isset($params['updateTxnId'])):
              $sql.=" ANd (o.is_downloaded='1' OR o.is_payment_done='0' )";
          endif;
                
         // echo $sql;
         //$query=$this->db->query($sql);
           $query=$this->slaveDB->query($sql);
         
         if($query->num_rows()):
             return $query->result_array();
         endif;
         
         return false;
    }
    
    public function updateDownloadFlag($data)
    {
         $this->db->where("id IN ({$data['ids']})");
         
            if($data['type']=="approve"):
            $this->db->where_in("is_payment_done",array('2','0'));    
            $this->db->update('inv_orders',array('is_downloaded'=>'1'));
            else:
              $this->db->update('inv_orders',array('is_downloaded'=>'2','is_approved'=>'1'));    
            endif;
            
        if($this->db->affected_rows()>=0):
                    return true;
        endif;
        
        return false;
        

        }
        
        public function getExcelRelatedOrderData($ids,$from_bank)
        {
         
            $tobeReturned=array();
            
            $sql=" SELECT id,SUM(to_pay) as Amount ,supplier_bank_id,supplier_id,GROUP_CONCAT(id SEPARATOR '#') as orderids "
                    . " FROM inv_orders "
                    . " where id IN ({$ids}) "
                    . " and is_payment_done IN ('0','2') "
                    . " and is_approved='1' "
                    . " and is_downloaded='1' "
                    . " group by supplier_bank_id ";
            
             $query=$this->db->query($sql);
             
              if($query->num_rows()):
                  
                                         $orderdata=  $query->result_array();
              
                                          if($from_bank=='icici'):
                                                    $tobeReturned=$this->formatExcelDataForIcici($orderdata);
                                          elseif($from_bank=="axis"):
                                                    $tobeReturned=$this->formatExcelDataForAxis($orderdata);
                                          endif;
                           
              endif;
              
              return $tobeReturned;
        }
        
        public function updateTxnid($data)
        {
            if($data['txnid']!="" && $data['order_id']>0):
                
                $paid=  $this->getToPayByOrderid($data['order_id']);
            
                if($paid>0):
                    
                    $updatearr=array('txnid'=>$data['txnid'],'is_payment_done'=>'1','payed_by'=>getLoggedInUserId());

                   $this->db->update('inv_orders',$updatearr,array('id'=>$data['order_id']));
                   
                   $this->getDataToSendMsg($data['order_id'],$data['txnid']);
                   
                   $this->db->update('inv_payments',array('status'=>'1','utr'=>$data['txnid']),array('order_id'=>$data['order_id'],'status'=>'0'));
                   
                   $is_utr_updated=$this->db->affected_rows();
                   
                   $response=array("is_utr_updated"=>$is_utr_updated,"order_id"=>$data['order_id']);
                   
                   $this->setTargetDataByOrderId($data['order_id']);

                   return $response;
                 
                else:
                    
                    return false;
                
                endif;               
                 
            endif;
            
            return false;
           
        }
        public function rejectTxnid($data)
        {
            if($data['txnid']=="" && $data['order_id']>0):
                
                 $updatearr=array('txnid'=>'','is_payment_done'=>'0','is_downloaded'=>'2','is_approved'=>'1','payed_by'=>getLoggedInUserId());
            
                 $this->db->update('inv_orders',$updatearr,array('id'=>$data['order_id']));
                 
                 return true;
                 
            endif;
            
            return false;
           
        }
        
        public function getToPayByOrderid($order_id)
        {
            $sql="Select to_pay from inv_orders where id='{$order_id}' ";
            
            $query=$this->db->query($sql);      
           
             if($query->num_rows()):
                 
                  $data=$query->row();
             
                  return $data->to_pay;
             
             endif;
           
            return false;
           
        }
        
        
        function haszeroinfront($amount)
{
    if($amount):
        if(substr($amount,0,2)=="00"):
            return true;
        elseif(substr($amount,0,1)=="0"):
            return true;
        endif;
     endif;
     
     return false;
}


// In Order to send order MSG & Email  we require paymentid & supplier_id to call sendsms function already written
function getDataToSendMsg($orderid,$utr)
{
    $sql="Select pa.id as paymentid,supplier_id as supplierid  from inv_orders o JOIN inv_payments pa ON  o.id=pa.order_id where pa.status='0' and  o.id='{$orderid}' ";
    
     $query=$this->db->query($sql);
     
      if($query->num_rows()==1):
          
              $data=$query->row_array();
      
              sendOrderMsgEmail($data,$utr);
              
      endif;
     
}

function updateUtrFromExcel($data)
{
    $response=array();
    
    foreach($data as $row):

        if(isset($row['11']) && !empty($row['11']) && isset($row['12']) && !empty($row['12'])):

            $Utr=$row['12'];

           $orderids=  strpos($row['11'],"#")?explode("#",$row['11']):array($row['11']);
           
            if(!empty($orderids)):

                     foreach($orderids as $value):

                        $response[]=$this->updateTxnid(array('order_id' => $value, 'txnid' => $Utr));

                    endforeach;

            endif;

            endif;

        endforeach;

        return $response;
    
    }

function formatExcelDataForIcici($orderdata)
{
    
        $tobeReturned=array();

         $tobeReturned[0]['Payment Indicator']='Payment Indicator';
         $tobeReturned[0]['Beneficiary Code']='Beneficiary Code';
         $tobeReturned[0]['Beneficiary Name']='Beneficiary Name';
         $tobeReturned[0]['Amount']='Amount';
         $tobeReturned[0]['Payment Date']='Payment Date';
         $tobeReturned[0]['Debit Account Number']='Debit Account Number';
         $tobeReturned[0]['Credit Account Number']='Credit Account Number';
         $tobeReturned[0]['IFSCCode']='IFSCCode';
         $tobeReturned[0]['Beneficiary Address']='Beneficiary Address';
         $tobeReturned[0]['Print Location']='Print Location';
         $tobeReturned[0]['Payable Location']='Payable Location';
         $tobeReturned[0]['Payment Remarks']='Payment Remarks';
                                          
                            foreach($orderdata as $key=>$order):
                                
                                         $sql2="Select  beneficiary_code,account_holder_name,isfc_code,account_no,bank_id  "
                                                . " from inv_supplier_banks "
                                                . " where id='{$order['supplier_bank_id']}' ";
                                         
                                                
                                          $query2=$this->db->query($sql2);       
                                          
                                          $bankdata=  $query2->row_array();
                                          
                                          // Check if amount is > 200000 & not has two OR one prepended zero's
                                          
                                         
                                          $isRtgs=($order['Amount']>=200000 && !$this->haszeroinfront("{$bankdata['account_no']}"))?true:false;
                                          
                                          $tobeReturned[$order['id']]['Payment Indicator']=$bankdata['bank_id']=="18"?"I":($order['Amount']>=200000?'R':'N');
                                           $tobeReturned[$order['id']]['Beneficiary Code']=$bankdata['beneficiary_code'];
                                          $tobeReturned[$order['id']]['Beneficiary Name']=$bankdata['account_holder_name'];
                                          $tobeReturned[$order['id']]['Amount']=$order['Amount'];
                                          $tobeReturned[$order['id']]['Payment Date']='="'.date('d/m/Y').'"';
                                          //$tobeReturned[$order['id']]['Debit Account Number']=  "'{$this->config->item('debit_account_no')}";
                                          $tobeReturned[$order['id']]['Debit Account Number']='="'.$this->config->item('icici_debit_account_no').'"';
                                          //$tobeReturned[$order['id']]['Credit Account Number']="'{$bankdata['account_no']}";
                                          $tobeReturned[$order['id']]['Credit Account Number']=$isRtgs?$bankdata['account_no']:'="'.$bankdata['account_no'].'"';
                                          $tobeReturned[$order['id']]['IFSCCode']=$bankdata['isfc_code'];
                                          $tobeReturned[$order['id']]['Beneficiary Address']= "";
                                          $tobeReturned[$order['id']]['Print Location']= "";
                                          $tobeReturned[$order['id']]['Payable Location']= "";
                                          //$tobeReturned[$order['id']]['Payment Remarks']= $bankdata['account_holder_name'];
                                          $tobeReturned[$order['id']]['Payment Remarks']= $order['orderids'];
                                       
                                          
                            endforeach;    

          return $tobeReturned;
}

function formatExcelDataForAxis($orderdata)
{
        $tobeReturned=array();

         $tobeReturned[0]['PaymentIdentifier']='PaymentIdentifier';
         $tobeReturned[0]['PaymentAmount']='PaymentAmount';
         $tobeReturned[0]['Beneficiary Name']='Beneficiary Name';
         $tobeReturned[0]['Bene Address 1']='Bene Address 1';
         $tobeReturned[0]['Bene Address 2']='Bene Address 2';
         $tobeReturned[0]['Bene Address 3']='Bene Address 3';
         $tobeReturned[0]['Bene City']='Bene City';
         $tobeReturned[0]['Beneficiary State']='Beneficiary State';
         $tobeReturned[0]['Pin Code']='Pin Code';
         $tobeReturned[0]['Beneficiary Account No']='Beneficiary Account No';
         $tobeReturned[0]['Beneficiary Email ID']='Beneficiary Email ID';
         $tobeReturned[0]['Email Body']='Email Body';
          $tobeReturned[0]['Debit Account No']='Debit Account No';
          $tobeReturned[0]['CRN No']='CRN No';
          $tobeReturned[0]['Receiver IFSC Code']='Receiver IFSC Code';
          $tobeReturned[0]['Receiver Account Type']='Receiver Account Type';
          $tobeReturned[0]['Print Branch']='Print Branch';
          $tobeReturned[0]['Payable Location']='Payable Location';
          $tobeReturned[0]['Instrument Date']='Instrument Date';
          $tobeReturned[0]['Additional Info 1']='Additional Info 1';
          $tobeReturned[0]['Additional Info 2']='Additional Info 2';
          $tobeReturned[0]['Swift Code']='Swift Code';
      
          
             foreach($orderdata as $key=>$order):
                                
                                         $sql2="Select  beneficiary_code,account_holder_name,isfc_code,account_no,bank_id  "
                                                . " from inv_supplier_banks "
                                                . " where id='{$order['supplier_bank_id']}' ";
                                         
                                                
                                          $query2=$this->db->query($sql2);       
                                          
                                          $bankdata=  $query2->row_array();
                                          
                                          
                                          $tobeReturned[$order['id']]['PaymentIdentifier']='IMPS';
                                          $tobeReturned[$order['id']]['PaymentAmount']=$order['Amount'];
                                          $tobeReturned[$order['id']]['Beneficiary Name']=$bankdata['account_holder_name'];
                                          $tobeReturned[$order['id']]['Bene Address 1']='';
                                          $tobeReturned[$order['id']]['Bene Address 2']='';
                                          $tobeReturned[$order['id']]['Bene Address 3']='';
                                          $tobeReturned[$order['id']]['Bene City']='';
                                          $tobeReturned[$order['id']]['Beneficiary State']='';
                                          $tobeReturned[$order['id']]['Pin Code']='';
                                          $tobeReturned[$order['id']]['Beneficiary Account No']=!$this->haszeroinfront("{$bankdata['account_no']}")?$bankdata['account_no']:"'".$bankdata['account_no'];
                                          $tobeReturned[$order['id']]['Beneficiary Email ID']='';
                                          $tobeReturned[$order['id']]['Email Body']='';
                                          $tobeReturned[$order['id']]['Debit Account No']=$this->config->item('axis_debit_account_no');
                                          $tobeReturned[$order['id']]['CRN No']='';
                                          $tobeReturned[$order['id']]['Receiver IFSC Code']=$bankdata['isfc_code'];
                                          $tobeReturned[$order['id']]['Receiver Account Type']=11;
                                          $tobeReturned[$order['id']]['Print Branch']='';
                                          $tobeReturned[$order['id']]['Payable Location']='';
                                          $tobeReturned[$order['id']]['Instrument Date']='';
                                          $tobeReturned[$order['id']]['Additional Info 1']='';
                                          $tobeReturned[$order['id']]['Additional Info 2']=$order['orderids'];
                                          $tobeReturned[$order['id']]['Swift Code']=$bankdata['isfc_code'];
                                        
             endforeach;    
             
             
            return $tobeReturned;    
             
}

public function setTargetDataByOrderId($orderid) {
        $prevdate = date('Y-m-d', strtotime('-1 day'));
        $current_date = date('Y-m-d');

        $sql1 = "SELECT o.id as order_id,so.id AS supplier_operator_id,so.is_onhold, (CASE WHEN ps.base_amount IS NULL then 0 else ps.base_amount END) AS base_amount, "
                . " SUM(if(dd.closing is null,0,dd.closing)) as opening,SUM(if(dd.block=1,dd.closing,0)) as blockedbalance,"
                . " dd.sync_date as ddate,pe.pending  "
                . " FROM inv_orders o "
                . " JOIN inv_supplier_operator so "
                . " ON so.id=o.supplier_operator_id "
                . "  JOIN inv_planning_sheet ps "
                . " ON ps.supplier_operator_id=so.id "
                . "JOIN inv_pendings pe "
                . " ON (pe.supplier_operator_id=so.id AND pending_date='{$prevdate}')   "
                . " LEFT JOIN devices_data dd "
                . " ON (so.id=dd.supplier_operator_id and dd.sync_date='{$prevdate}') "
                . " WHERE o.id =  '{$orderid}' and so.is_active='1'   "
                . " group by so.id ";

        $sql2 = "select so_id,sum(amount_by_bounds) as totalorderamt "
                . "from inv_payments "
                . "where payment_date='{$current_date}' and status='1'  and order_id='{$orderid}' "
                . "group by so_id";

        $finalsql = "SELECT * "
                . "FROM ($sql1) as a "
                . " JOIN ($sql2) as b "
                . "ON a.supplier_operator_id=b.so_id";
        
        $query = $this->db->query($finalsql);
        
        if ($query->num_rows()):

            $data = $query->row_array();

            if ($this->updateTargetData($data)):
                return true;
            endif;

        endif;

        return false;
    }

//update planned sale and max sale capacity
    public function updateTargetData($params) {
        $target = 0;
        $soid = $params['supplier_operator_id'];
        $blockbal= $params['blockedbalance'];
        $order = $params['totalorderamt'];
        $opening = $params['opening']-$blockbal;
        $basesale = $params['base_amount'];
        $isonhold = $params['is_onhold'];
        $pending = $params['pending'];
        $oop = (($pending > 0) || ($isonhold == 1)) ? ($opening + $order) :($opening + $order - $pending);

        if ($basesale == 0):
            $target = $opening * 0.5;
        elseif ($oop >= $basesale):
            $target = $basesale;
        elseif (($oop < $basesale) && $pending <= 0):
            $target = $oop * 0.5;
        elseif (($oop < $basesale) && $pending > 0):
            $target = 0;
        endif;
       
        $maxsalecap = $target * 1.2;
        
        logerror("soid :{$soid} | order :{$order} | opening :{$opening} | pending :{$pending} | basesale :{$basesale} | target :{$target} | maxsalecapacity :{$maxsalecap} | blockedbalance :{$blockbal}","updateTargetdata");
       
        $updatearr=array('planned_sale'=>$target,'max_sale_capacity'=>$maxsalecap);
        
        logerror("update inv_planning_sheet set planned_sale='{$target}',max_sale_capacity='{$maxsalecap}' where supplier_operator_id='{$soid}'","updateTargetdata");
        
        if ($this->db->update('inv_planning_sheet',$updatearr,array('supplier_operator_id'=>$soid))):
            return true;
        endif;

        return false;
    }
        
}

