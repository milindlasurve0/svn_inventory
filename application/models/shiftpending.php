<?php

class Shiftpending extends CI_Model
{
    
    public function getOperatorWisePendingForSuppliers($data)
    {
        $date=date('Y-m-d',strtotime('-1 day')); 
        
        $sql=" select p.id opr_id,p.name as operator_name,so.supplier_id,ip.id as pending_id,ip.pending,ip.margin,so.commission_type,ip.pending_date,so.id as supplier_operator_id"
                . " from inv_supplier_operator so"
                . " join inv_pendings ip"
                . " on ip.supplier_operator_id=so.id"
                . " join products p"
                . " on p.id=so.operator_id"
                . " where so.supplier_id='{$data['supplier_id']}'"
                . " and so.id!='{$data['soid']}'"
                . " and ip.pending_date='$date'";
//             echo $sql;
        $query=$this->db->query($sql);
        if($query->num_rows()):
            $data=$query->result_array();
            return $data;
        endif;
        return false;
    
    }
    
    public function getpendings($params)
    {
        $sql="select p.id as pending_id,p.pending,so.commission_type,po.name as oprname,so.supplier_id,so.operator_id,so.id as soid,so.commission_type_formula as margin,p.pending_date  "
                    . "  from inv_pendings p "
                    . " JOIN inv_supplier_operator so "
                    . " ON  p.supplier_operator_id=so.id JOIN products po ON po.id=so.operator_id"
                    . "  where supplier_operator_id='{$params['soid']}' "
                    . "  and pending_date='{$params['pending_date']}'";
                    
        $query=$this->db->query($sql);         
         
         if($query->num_rows()):
             
             return $query->row();
         
         endif;
         
         return false;
    }
    
    public function adjustPendings($data)
     {
        $update_array=array('pending'=>$data['pending'],'adjusted_pending'=>$data['textboxsum']);
        
        $this->db->update('inv_pendings',$update_array,"id ={$data['id']}");
    }

}
