<?php

class Cron extends CI_Model
{
    
    var $sdiff=array();
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getAllModems($modem_id=null)
    {
        $this->slaveDB->select('id,company,shortForm,ip,bridge_ip,port,machine_id')
                           ->from('vendors')
                           //->where(array('active_flag'=>1,'update_flag'=>1));
                              ->where(array('machine_id !='=>'0')); 
        
        if(!is_null($modem_id)):
           $this->slaveDB->where('id',$modem_id);
        endif;
            
         $query =$this->slaveDB->get();
         
          if($query->num_rows()):
             
                 return $query->result();
         
         endif;
         
         return false;
        
    }
    
   public function saveSimdata($data,$modem_id,$dateparam)
   {
       $batch=array();
      
       //$date=date("Y-m-d",strtotime("{$dateparam} -1 day"));
        
       $this->sdiff=array();
       $this->setServerDiff($modem_id,$dateparam);
       $simInfo=array();  
       foreach($data as $sim):
                   
                                $tfr=!empty($sim->tfr)?$sim->tfr:0.00;
                                $opening=!empty($sim->opening)?$sim->opening:0.00;
                                $closing=isset($sim->closing)?(!empty($sim->closing)?$sim->closing:0.00):0.00;
                                $balance=!empty($sim->balance)?$sim->balance:0.00;
                                $inv_supplier_operator_id=$this->getsupplier_operator_id($sim->inv_supplier_id,$sim->opr_id);
                                $sdiffkey=$sim->opr_id."_".$sim->mobile;
                                $serverdiff=isset($this->sdiff[$sdiffkey])?$this->sdiff[$sdiffkey]:0.00;
                                
                                // check only after 7am clock && of previous day
                                if(strtotime($dateparam)==strtotime('today -1 day')):
                                 $this->crossCheckIncoming($sim,$dateparam,$inv_supplier_operator_id,$modem_id);
                                endif;
                                
                                 $simInfo[]=array(
                                                        'device_id'=>$sim->id,
                                                        'vendor_id'=>$modem_id,
                                                        'supplier_operator_id'=> $inv_supplier_operator_id,
                                                        'sync_timestamp'=>date('Y-m-d H:i:s'),
                                                        'sync_date'=>$dateparam,
                                                        'machine_id'=>$sim->machine_id,
                                                        'scid'=>$sim->scid,
                                                        'mobile'=>$sim->mobile,
                                                        'operator'=>$sim->operator,
                                                        'circle'=>$sim->circle,
                                                        'pin'=>$sim->pin,
                                                        'type'=>$sim->type,
                                                        'balance'=>$balance,
                                                        'opr_id'=>$sim->opr_id,
                                                        'commission'=>$sim->commission,
                                                        'limit'=>$sim->limit,
                                                        'limit_today'=>$sim->limit_today,
                                                        'roaming_limit'=>$sim->roaming_limit,
                                                        'roaming_today'=>$sim->roaming_today,
                                                        'vendor'=>$sim->vendor,
                                                        'inv_supplier_id'=>($sim->inv_supplier_id>0)?$sim->inv_supplier_id:0,
                                                        'last_block_date'=>$sim->last_block_date,
                                                        'vendor_tag'=>$sim->vendor_tag,
                                                        'block'=>$sim->block,
                                                        'device_num'=>$sim->device_num,
                                                        'bus'=>$sim->bus,
                                                        'bus_dev'=>$sim->bus_dev,
                                                        'par_bal'=>$sim->par_bal,
                                                        'state'=>$sim->state,
                                                        'active_flag'=>$sim->active_flag,
                                                        'recharge_flag'=>$sim->recharge_flag,
                                                        'receive_flag'=>$sim->receive_flag,
                                                        'stop_flag'=>$sim->stop_flag,
                                                        'signal'=>$sim->signal,
                                                        'tfr'=>$tfr,
                                                        'inc'=>($sim->inc>0)?$sim->inc:0,
                                                        'opening'=>$opening,
                                                         'sale'=>!empty($sim->sale)?$sim->sale:0.00,
                                                        'closing'=>$closing,
                                                        'success'=>$sim->success,
                                                        'last'=>$sim->last,
                                                        'server_diff'=>$serverdiff,
                                                        'recharge_method'=>$sim->recharge_method,
                                                        'blocktag_id'=>!empty($sim->block_tag)?$sim->block_tag:0,
                                                        'bal_range'=>!empty($sim->bal_range)?$sim->bal_range:0
                                                    );
                                
                                                    
//                                       if(!$this->db->insert('devices_data',$simInfo)):
//                                           $last_query=$this->db->last_query();
//                                           logerror("Query Failed : {$last_query} with message  {$this->db->_error_message()} ","cronqueries");
//                                       endif;
                                     
                                   endforeach;
                                   
                                    if(!empty($simInfo)):
                                        $this->db->delete('devices_data',array('vendor_id'=>$modem_id,'sync_date'=>$dateparam));
                                    endif;
                                    
                                   $batch=  $this->createSqlbatch($simInfo);
                                   
                                   if(!empty($batch)):
                                       
                                            foreach($batch as $b):
                                                        
                                                                    // Try to insert as a batch
                                                                     $this->db->insert_batch('devices_data', $b);

                                                                      // If single row in batch fails get that batch
                                                                      if($errormsg=$this->db->_error_message()):
                                                                          
                                                                                // Log that failed query
                                                                                logerror("Batch Query Failed :  {$errormsg} ","cronqueries");
                                                                                $sql = $this->db->last_query();

                                                                                // Insert that failed batch using IGNORE
                                                                                $sql = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $sql);

                                                                                //Check that failed query status again
                                                                                if(!$this->db->query($sql)):
                                                                                    logerror("Normal Query Failed :  {$this->db->_error_message()} ","cronqueries");
                                                                                else:
                                                                                     logerror("Failed Batch Success ","cronqueries");
                                                                                endif;
                                                                          
                                                                      endif;

                                            endforeach;
                                    
                                   endif;
                                   
                                   
                                              
                                  }
    
   public function crossCheckIncoming($sim,$date,$soid,$modem_id)
   {
       if($soid>0)
       {
           $this->slaveDB->select('tfr');
            
            $query=$this->slaveDB->get_where('devices_data',array('vendor_id'=>$modem_id,'opr_id'=>$sim->opr_id,'scid'=>$sim->scid,'sync_date'=>$date));
            
            if($query->num_rows())
                
            $data=$query->row_array();

            $data['tfr']=is_numeric($data['tfr'])?$data['tfr']:0;
            $sim->tfr=is_numeric($sim->tfr)?$sim->tfr:0;
            
            if($data['tfr']==$sim->tfr):
                
                return 0;
            
            else:
                
                $temp=0;
            
                $temp=($sim->tfr-$data['tfr']);
            
               $sql="Update inv_pendings set pending=pending+{$temp},incoming=incoming+{$temp} where pending_date='{$date}' and supplier_operator_id='{$soid}'  ";
             
                //if($this->db->query($sql) && strtotime('now')>strtotime('today 07:10:00')):
                        $hour=intval(date('H'));
                        if($hour>7):
                        $currenttime=date('Y-m-d H:i:s');
                        $logstr=" <br/>------- START -------<br/>Current Time : {$currenttime} Pending Fixed : Date : {$date} simdata:".json_encode($sim)." <br/> soid : {$soid} <br/> sql : {$sql}  <br/> old tfr : {$data['tfr']} <br/> new tfr : {$sim->tfr} <br/> Toadjust : {$temp} <br/> -----END-------<br/>   ";
                        logerror($logstr,"crosscheckincoming");
                        send_email('vibhas@mindsarray.com','Crosscheckpending',$logstr);
                       endif;
               //  endif;
                
            endif;
       }
   }
    public function createSqlbatch($simInfo)
    {
        $temp=array();

        if(!empty($simInfo)):
                $temp=array_chunk($simInfo,50);
        endif;

        return $temp;


    }
    public function OldsaveSimdata($data,$modem_id)
    {
            foreach($data as $sim):

                
                        $this->db->set('sync_timestamp',date("Y-m-d",strtotime("-1 day")));
                 
                        $tfr=!empty($sim->tfr)?$sim->tfr:0.00;
                        $opening=!empty($sim->opening)?$sim->opening:0.00;
                        $closing=isset($sim->closing)?(!empty($sim->closing)?$sim->closing:0.00):0.00;
                        $balance=!empty($sim->balance)?$sim->balance:0.00;
                        $inv_supplier_operator_id=$this->getsupplier_operator_id($sim->inv_supplier_id,$sim->opr_id);
                                 
                        $simInfo=array(
                                                        'sim_no'=>$sim->mobile,
                                                        'sc_id'=>$sim->scid,
                                                        'supplier_id'=>($sim->inv_supplier_id>0)?$sim->inv_supplier_id:0,
                                                        'operator_id'=>$sim->opr_id,
                                                       'supplier_operator_id'=> $inv_supplier_operator_id,
                                                        'vendor_id'=>$modem_id,
                                                        'incoming'=>$tfr,
                                                        'opening'=>$opening,
                                                        'closing'=>$closing,
                                                        'sale'=>!empty($sim->sale)?$sim->sale:0.00,
                                                        'blocked'=>$sim->block,
                                                        'active'=>$sim->active_flag,
                                                        'balance'=>$balance,
                                                        'incentive'=>!empty($sim->inc)?$sim->inc:0.00

                                                    );
                        
                    

                      if( ($tfr > 0 || $balance>0 || $opening>0 || $closing>0)  && ($inv_supplier_operator_id>0) ):   
                        
                      $this->db->insert('inv_simdata',$simInfo);
                      
                      endif;



            endforeach;
        
        return true;
    }
    
    public function Sync($modem_id,$is_updated)
    {
        // Delete Previous Entries of  modems  who were not updated to  avoid repetation when updated
        //Start
        $this->db->where('DATE(last_sync_timestamp)=CURDATE()');
        $this->db->where('is_updated','0');
        $this->db->delete('inv_sync_status',array('modem_id'=>$modem_id));
      

        //End

        $this->db->set('last_sync_timestamp',date("Y-m-d H:i:s"));
        
        $this->db->insert('inv_sync_status',array('modem_id'=>$modem_id,'is_updated'=>$is_updated));
    }
    
    
    
    public function getNonUpdatedModems()
    {
         $this->db->select('v.id as id')
                           ->from('inv_sync_status i')
                           ->join('vendors v','i.modem_id=v.id')
                           ->where(array('is_updated'=>'0'))
                           ->where('DATE(last_sync_timestamp)=CURDATE()');         
        
         $query = $this->db->get();
         
        
         if($query->num_rows()):
             
                 return $query->result();
         
         endif;
         
         return false;
    }
    
    public  function getsupplier_operator_id($supplier_id,$operator_id)
    {
        $this->slaveDB->select('id')
                           ->from('inv_supplier_operator')
                           ->where(array('supplier_id'=>$supplier_id,'operator_id'=>$operator_id));
        
       //  $query = $this->db->get();
          $query=$this->slaveDB->get();
         
        
         if($query->num_rows()):
             
                 $data=$query->row();
                 return $data->id;
         
         endif;
         
         return false;
    }
    
    /*
     * This Function will fetch incoming of all supplier belonging to particular operator  everday  , irrespective of value =0
     * That is why commented having clause so that we can track incoming even if its zero and order was placed  
     */
    public function getIncoming($operator_id)
    {
        $date=date("Y-m-d",strtotime("-1 days"));
        
//      $sql="Select SUM(tfr) as incoming,inv_supplier_id as supplier_id,opr_id as operator_id,supplier_operator_id "
//                . " from devices_data "
//                . " where sync_date='{$date}' "
//                ." AND opr_id ='{$operator_id}' "
//                . " group by supplier_operator_id ";
               // . " Having incoming >0 "; 
    
         $sql="Select SUM( if( tfr IS NULL , 0, tfr ) ) as incoming,so.supplier_id as supplier_id,so.operator_id as operator_id,so.id as supplier_operator_id,so.commission_type_formula as margin,so.commission_type  "
                . "  from inv_supplier_operator so "
                . " LEFT JOIN devices_data dd "
                . " ON ( so.id=dd.supplier_operator_id  AND  dd.sync_date='{$date}' ) "
                . " WHERE  so.operator_id='{$operator_id}' "
                . " group by so.id "; 
      
         $query = $this->db->query($sql);
            
         if($query->num_rows()):
             
                 return $query->result();
                 
         endif;    
         
         return ;
    }
    
    public function getLastavaliablePending($so_id)
    {
        $sql="Select pending "
                . " from inv_pendings "
                . " where supplier_operator_id='{$so_id}' "
                . " order by pending_date desc "
                . " limit 1 ";
                
         $query = $this->db->query($sql);      
                 
         if($query->num_rows()):
             
                 $data=$query->row();
         
                 return $data->pending;
                 
         endif;    
         
         return 0;
         
    }
    
    public function deletePreviousAvaliablePendings($operator_id=0)
    {
          $date=date("Y-m-d",strtotime("-1 day"));
           $this->db->delete('inv_pendings',array('pending_date'=>$date,'operator_id'=>$operator_id));
    }
   /*
    *  Formula : A-B+C
    * A= Total incoming on that day for particular soid
    * B= Total Order given on that day
    * C= Last Avaliable pending before that day
    * X= New Pending to be updated for that day
    * If Order was not given on that day but there was some incoming so use (A+C) else (A-B+C)
    */
    public function setPending($so_id,$lastpending,$incoming,$operator_id=0,$margin=0,$margin_type=0)
    {
          
        $date=date("Y-m-d",strtotime("-1 day"));
         $insert=array();
      
        if($data=$this->checkifOrderExists($so_id,$date)):
            
            $pending=($incoming)+($lastpending)-($data->amount_by_bounds);
            $insert=array('order_id'=>$data->id,'incoming'=>$incoming,'pending'=>$pending,'pending_date'=>$date,'supplier_operator_id'=>$so_id,'operator_id'=>$operator_id,'margin'=>$margin,'margin_type'=>$margin_type);
            
            if($this->db->insert('inv_pendings',$insert)):
                  logerror("Soid : {$so_id} |  Incoming : {$incoming} | Order : {$data->amount_by_bounds} | LastPending : {$lastpending} | Final : {$pending}","pendings");
                  logerror("Query Success  #{$this->db->insert_id()}: {$this->db->last_query()}","pendings");
            else:
                 logerror("Query Failed : {$this->db->last_query()} with message  {$this->db->_error_message()} ","pendings");
                 send_email('vibhas@mindsarray.com','Pending Query Error',"Mysql Query for pending  cron  Failed  with error {$this->db->_error_message()} :  {$this->db->last_query()} ");
            endif;
            
            echo $this->db->last_query().";";
            echo "<br/>";    

        else:
            
         
            $pending=($incoming)+($lastpending);
            $insert=array('incoming'=>$incoming,'pending'=>$pending,'pending_date'=>$date,'supplier_operator_id'=>$so_id,'operator_id'=>$operator_id,'margin'=>$margin,'margin_type'=>$margin_type);
            if($this->db->insert('inv_pendings',$insert)):
                   logerror("Soid : {$so_id} |  Incoming : {$incoming} | LastPending : {$lastpending} | Final : {$pending} ","pendings");  
                  logerror("Query Success  #{$this->db->insert_id()} : {$this->db->last_query()}  ","pendings");
            else:
                  logerror("Query Failed : {$this->db->last_query()} with message  {$this->db->_error_message()} ","pendings");
                  send_email('vibhas@mindsarray.com','Pending Query Error',"Mysql Query for pending  cron  Failed  with error {$this->db->_error_message()} :  {$this->db->last_query()} ");
            endif;
            
            
             echo $this->db->last_query().";";
            echo "<br/>";    
            
        endif;
        
    
    }
    
    public function checkifOrderExists($so_id,$date)
    {
        $sql="Select id,amount_by_bounds from inv_orders where supplier_operator_id='{$so_id}' AND order_date='{$date}' AND is_approved='1' AND is_payment_done='1' AND is_downloaded='1' ";
        
       $query = $this->db->query($sql);      
        
          if($query->num_rows()):
              
              return $query->row();
              
          endif;
          
          return false;
    }
    
    public function deleteFromPendingTable()
    {
        $this->db->query('Truncate  inv_pendings');
        
         $this->db->get();      
         
    }
    
     public function setServerDiff($modem_id,$date=null)
    {
        if($modem_id>0):
            
                        $serverdiffjson= file_get_contents("http://cc.pay1.in/recharges/getServerDiffByModemId/{$modem_id}/{$date}/1");
                        
                        if(!empty($serverdiffjson)){
                            
                                    $serverdiffjson=  json_decode($serverdiffjson);

                                    foreach($serverdiffjson as $value):

                                                              $this->sdiff[$value->operator_id."_".$value->sim_num]=$value->server_diff;

                                    endforeach;
                        
                        }

        endif;
        
        return;
        
    }
    
    public function setTest()
    {
        $data=array(
                                        array('value1'=>'a','value2'=>'b'),
                                        array('value1'=>'a','value2'=>'c'),
                                        array('value1'=>'a','value2'=>'c'),
                                        array('value1'=>'a','value2'=>'d'),
                                        array('value1'=>'a','value2'=>'e')
                               );
        
        $this->db->insert_batch('test', $data);

// if it fails resort to IGNORE
if($error=$this->db->_error_message())
{
        $sql = $this->db->last_query();
        $sql = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $sql);
        $this->db->query($sql);
        echo $this->db->last_query();
        echo $error;
    }
    }
    
    
    function getUnupdatedSoid()
    {
        $sql=" SELECT DISTINCT inv_supplier_id, opr_id "
                . " FROM `devices_data` "
                . " WHERE sync_date = '2015-07-27' "
                . " AND supplier_operator_id =0 "
                . " AND inv_supplier_id !=0";
        
         $query = $this->slaveDB->query($sql);      
        
          if($query->num_rows()):
              
              return $query->result();
              
          endif;
          
          return false;
    }
    function fireq($q)
    {
        echo "Query : ".$q;
        echo "<br/>";
        echo "<br/>";
        echo "<br/>";
          $query = $this->db->query($q);
          
          if($query):
              
                if($query->num_rows()):

                    echo "<pre>";
                    print_r($query->result());
                    echo "</pre>";

                endif;
                
           else:
               echo "Failed";
           endif;
           
    }
    
    public function getModemSyncedPastDay()
    {
        $date=date("Y-m-d",strtotime("-1 day"));
         
        $sql="select MAX(sync_timestamp) as lastsynctimestamp,v.id,v.company,v.shortForm,v.ip,v.bridge_ip,v.port,v.machine_id  "
                . " from vendors v  "
                . " JOIN devices_data dd  "
                . " ON v.id=dd.vendor_id  "
                . " where sync_date='{$date}' "
                . " group by vendor_id "
                . " having DATE(lastsynctimestamp) < CURDATE()";
        
          $query = $this->slaveDB->query($sql);      
        
          if($query->num_rows()):
              
              return $query->result();
              
          endif;
          
          return false;
    }
    
    public function setBalancetoClosing($modemid)
    {
         $date=date("Y-m-d",strtotime("-1 day"));
         
         $sql="Update devices_data set closing=balance where vendor_id='{$modemid}' and sync_date='{$date}' ";
        
         $query = $this->db->query($sql);      
         
         return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }
    
    public function setDiffs()
    {
        $date=date("Y-m-d",strtotime("-1 day"));
        
        $sql="insert into devices_highlights (company,operator,opening,closing,balance,tfr,sale,inc,sync_date,mobile,opr_id,scid,vendor_id,diff)
                    select v.company,p.name as operator,dd.opening,dd.closing,dd.balance,dd.tfr,dd.sale,dd.inc,sync_date,mobile,opr_id,scid,vendor_id,-(dd.opening-dd.closing+dd.tfr-dd.sale+dd.inc) as diff
                    from devices_data dd 
                    JOIN vendors v ON
                    dd.vendor_id=v.id
                    JOIN products p
                    ON dd.opr_id=p.id
                    where sync_date='{$date}'
                    and (opening-dd.closing+tfr-sale+inc)>500";
        
          $query = $this->db->query($sql);                 
    }
    
    public function getInvestments($date=null){
    	$sql="SELECT SUM( IF( tfr IS NULL , 0, tfr ) ) AS incoming, so.commission_type, pe.margin, so.supplier_id AS supplier_id, so.operator_id AS operator_id, so.id AS supplier_operator_id, dd.vendor_id, CEIL( SUM( IF( so.commission_type =1, tfr * ( ( 100 - pe.margin ) /100 ) , tfr * ( 100 / ( 100 + pe.margin ) ) ) ) ) AS invested
FROM devices_data dd
LEFT JOIN inv_supplier_operator so ON ( so.id = dd.supplier_operator_id ) 
LEFT JOIN inv_pendings pe ON (pe.supplier_operator_id=so.id AND pe.pending_date='{$date}' )
WHERE dd.sync_date =  '{$date}'  
GROUP BY dd.opr_id, dd.vendor_id, so.id
HAVING (
incoming >0
)";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }
    
public function setInvestments($data,$date){
		$datetime=date('Y-m-d H:i:s');
		foreach($data as $vendor=>$row){
			$earning = $row['incoming'] - $row['invested'];
			$sql="UPDATE earnings_logs SET  update_time='{$datetime}',incoming='".$row['incoming']."',invested='".$row['invested']."'  WHERE vendor_id={$vendor} AND date='{$date}'";
			$this->db->query($sql);
		}
    }
    
    public function fixInvestment($date)
    {
         $sql="SELECT a.supplier_operator_id,a.vendor_id,a.inv_supplier_id AS supplierid, a.opr_id AS operatorid, devices_incoming, pending_incoming, (
                    devices_incoming - pending_incoming
                    ) AS diffincoming,CEIL( SUM( IF( so.commission_type =1, (
                                                                                                                            devices_incoming - pending_incoming
                                                                                                                            ) * ( ( 100 - so.commission_type_formula ) /100 ) , (
                                                                                                                            devices_incoming - pending_incoming
                                                                                                                            ) * ( 100 / ( 100 + so.commission_type_formula ) ) ) ) ) AS investment
                    FROM (

                    SELECT supplier_operator_id,vendor_id, SUM( tfr ) AS devices_incoming, inv_supplier_id, opr_id
                    FROM devices_data
                    WHERE sync_date = '{$date}'
                    GROUP BY supplier_operator_id
                    ) AS a
                    JOIN (

                    SELECT supplier_operator_id, incoming AS pending_incoming
                    FROM inv_pendings
                    WHERE pending_date = '{$date}'
                    ) AS b ON a.supplier_operator_id = b.supplier_operator_id JOIN inv_supplier_operator so ON so.id = a.supplier_operator_id
                    GROUP BY supplier_operator_id
                    HAVING diffincoming <>0";
        
        $query = $this->db->query($sql);      
        
        if($query->num_rows()):
              $datetime=date('Y-m-d H:i:s');
              foreach($query->result_array() as $row):
                  echo "<pre>";
                  print_r($row);
                  echo "</pre>";
                    $earning=$row['diffincoming']-$row['investment'];
                   echo  $updateQuery="Update earnings_logs SET update_time='{$datetime}',incoming=incoming+{$row['diffincoming']},invested=invested+{$row['investment']}  WHERE vendor_id={$row['vendor_id']} AND date='{$date}' ";
                   echo "<br/>";
                   echo $sql="Update inv_pendings set pending=pending+{$row['diffincoming']},incoming=incoming+{$row['diffincoming']} Where supplier_operator_id='{$row['supplier_operator_id']}' and pending_date='{$date}' ";
                   echo "<br/>";
                   echo $latersql="Update  inv_pendings set pending=pending+{$row['diffincoming']} where supplier_operator_id='{$row['supplier_operator_id']}' and pending_date>'{$date}' ";
                   echo "<br/>";
              endforeach;
              
              
          endif;
    }
    
    public function recheckdiffs()
    {
        $sql="SELECT dh.*,-(dd.opening-dd.closing+dd.tfr-dd.sale+dd.inc) as currentdiff  "
                . " FROM `devices_highlights`  dh "
                . " JOIN devices_data dd  "
                . " ON dh.vendor_id=dd.vendor_id  "
                . " AND dh.sync_date=dd.sync_date "
                . " AND  dh.opr_id=dd.opr_id "
                . " AND dh.mobile=dd.mobile  "
                . " AND  dh.scid=dd.scid  WHERE ( dd.opening - dd.closing + dd.tfr - dd.sale + dd.inc ) < 500 ";
        
       $query = $this->db->query($sql);  
       
         if($query->num_rows()):
               foreach($query->result_array() as $row): 
                        echo $delete="Delete from devices_highlights where vendor_id='{$row['vendor_id']}' AND sync_date='{$row['sync_date']}' AND opr_id='{$row['opr_id']}' AND mobile='{$row['mobile']}'  AND scid='{$row['scid']}';";echo "<br/>";       
                endforeach;
         endif;
    }
    
    public function mergePaymentstoOrdertbl()
    {
        $date=date('Y-m-d');
        
        $sql="SELECT so_id, order_id, SUM( amount ) AS amount, SUM( to_pay ) AS to_pay, SUM( amount_by_bounds ) AS amount_by_bounds, GROUP_CONCAT( utr ) AS txnid,count( id ) AS ordercount "
                . "  FROM inv_payments "
                . " WHERE payment_date = '{$date}' and status='1'  "
                . " GROUP BY order_id ";
                
        $query = $this->db->query($sql);        
        
          if($query->num_rows()):
                        $data=$query->result_array();
                        $json_data=  json_encode($data);
                        logerror("Dump of inv payments :  {$json_data} ","mergepayments");
                        
                        foreach($data as $row):
                            
                                $update_array=array(
                                                                       'amount'=>$row['amount'],
                                                                       'to_pay'=>$row['to_pay'],
                                                                       'amount_by_bounds'=>$row['amount_by_bounds'],
                                                                        'txnid'=>$row['txnid'],
                                                                        'is_payment_done'=>'1',
                                                                        'is_approved'=>'1',
                                                                        'is_downloaded'=>'1'
                                                                       );
                        
                                $this->db->update('inv_orders',$update_array,array('id'=>$row['order_id']));            
                                
                                logerror($this->db->last_query(),"mergepayments");

                                if($errormsg=$this->db->_error_message()):
                                    
                                     logerror("Query Failed ".$errormsg,"mergepayments");
                                
                                    send_email('vibhas@mindsarray.com','Merge Payments Error',"Mysql Query for merge payments  cron  Failed  with error {$errormsg} :  {$this->db->last_query()} ");
                                
                                endif;

                            endforeach;
                            
          endif;
                
    }
    
    public function getSimBalRange($vendor_id)
    {
        return $this->slaveDB->query("SELECT so.supplier_id, so.operator_id, so.sim_bal_range FROM inv_supplier_operator so JOIN inv_supplier_vendor_mapping svm ON so.supplier_id = svm.supplier_id AND svm.vendor_id ='{$vendor_id}' AND so.operator_id =2")->result_array();
    }
}

