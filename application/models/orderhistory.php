<?php

class Orderhistory extends CI_Model
{
    
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    /*
    public function getOrderHistoryByDate($params)
    {
          $sql="select o.id as order_id,o.order_date,o.margin,pe.pending_date,s.id as supplierid,s.name as suppliername,po.name as operatorname,o.operator_id,o.supplier_operator_id,o.amount,o.to_pay,o.amount_by_bounds,pe.incoming,pe.pending,is_approved,is_downloaded,is_payment_done,group_concat(vendor_id) as vendorids "
                  . " from inv_orders o "
                  . " RIGHT JOIN inv_pendings pe "
                  . " ON  (o.id=pe.order_id AND o.is_payment_done='1')  "
                  . " LEFT JOIN inv_suppliers s "
                  . "  ON s.id=o.supplier_id "
                  . " LEFT JOIN products po "
                  . " ON po.id=o.operator_id  "
                  . "  LEFT JOIN inv_supplier_vendor_mapping vm "
                  . " ON vm.supplier_id=o.supplier_id "
                . "   where  (order_date>='{$params['start']}' AND order_date<='{$params['end']}') ";
             
          if($params['operator_id']>0):
              $sql.=" AND o.operator_id={$params['operator_id']} ";
          endif;
          
          if($params['supplier_id']>0):
               $sql.=" AND o.supplier_id={$params['supplier_id']} ";
          endif;
          
        if($params['handled_by']!=""):
              $sql.=" AND s.handled_by={$params['handled_by']} ";
        endif;         
           
            
            $sql.=" GROUP BY o.id  order by o.order_date ";   // For vendor ID
            
         $query=  $this->db->query($sql);
         
         if($query->num_rows()):
             
                return $query->result();
             
         endif;
         
         return false;
    }
    */
    
    public function getOrderHistoryByDate($params)
    {
      
        $join=isset($params['order_exists'])?"":"LEFT";
        
        $sql1="SELECT o.id as order_id,o.order_date,p.margin as margin,s.id as supplierid,s.name as suppliername,po.name as operatorname,p.operator_id,p.supplier_operator_id,o.amount,o.to_pay,o.amount_by_bounds,p.incoming,p.pending,p.pending_date,is_approved,is_downloaded,is_payment_done,o.is_adjusted,o.comment,o.txnid  "
                . " from inv_pendings p  "
                . "  {$join} JOIN inv_orders o  "
               . " ON (p.order_id=o.id AND o.is_payment_done='1')  "
             //   . " ON (p.supplier_operator_id = o.supplier_operator_id AND order_date>='{$params['start']}' AND order_date<='{$params['end']}')  "
                . " JOIN inv_supplier_operator so  "
                . " ON so.id=p.supplier_operator_id "
                . "  JOIN inv_suppliers s "
                . "  ON s.id=so.supplier_id  "
                . " JOIN products po  "
                . " ON po.id=so.operator_id  "
                . " WHERE pending_date>='{$params['start']}' and pending_date<='{$params['end']}' ";
        
        if($params['operator_id']>0):
             $sql1.=" AND po.id={$params['operator_id']} ";
         endif;

         if($params['supplier_id']>0):
              $sql1.=" AND s.id={$params['supplier_id']} ";
         endif;

        if($params['handled_by']!=""):
             $sql1.=" AND s.handled_by={$params['handled_by']} ";
        endif;     
        
        $sql1.= "ORDER by pending_date ";
        
        // Get vendorids
         $sql2="SELECT supplier_id, GROUP_CONCAT( vendor_id ) AS vendorids  FROM inv_supplier_vendor_mapping   GROUP BY supplier_id ";
         
        // Join based on supplierid
         $finalsql="Select a.*,b.vendorids from ($sql1) as a LEFT JOIN ($sql2) as b ON a.supplierid=b.supplier_id ORDER BY  a.pending_date ";
           
        $query=  $this->slaveDB->query($finalsql);
     
         if($query->num_rows()):
             
                return $query->result();
             
         endif;
         
         return false;
        
    }
    
    public function getPendinghistory($so_id,$showall)
    {
        
     $sql="SELECT p.id as pendingid,p.refund,p.comment as refundmessage,p.adjusted_pending,s.name as suppliername,p.margin as margin,po.name as operatorname,p.order_id,o.order_date,p.pending_date,o.amount,o.amount_by_bounds,p.incoming,p.pending,so.supplier_id,so.operator_id,p.supplier_operator_id,group_concat(vendor_id) as vendorids,o.comment,o.is_adjusted "
              . " from inv_pendings p "
              . " LEFT JOIN inv_orders o "
              . " ON (p.order_id=o.id AND o.is_payment_done='1') "
              . " JOIN inv_supplier_operator so "
              . " ON so.id=p.supplier_operator_id "
              . " JOIN inv_suppliers s "
              . " ON s.id=so.supplier_id "
              . " JOIN products po "
              . " ON  po.id=so.operator_id "
              . " LEFT JOIN inv_supplier_vendor_mapping vm "
              . " ON vm.supplier_id=so.supplier_id "
              . " WHERE p.supplier_operator_id={$so_id} ";
              
          $sql.=!$showall?" AND p.pending_date > DATE_SUB(now(), INTERVAL 2 MONTH) ":"";   
             
          $sql.= " GROUP by pending_date,o.supplier_id ORDER by pending_date ";        // For vendor ID we used supplierid
                
        $query=  $this->slaveDB->query($sql);
         
         if($query->num_rows()):
             
                return $query->result();
             
         endif;
         
         return false;        
    }
    
    public function getrefund($params)
    {
        $sql="Select pending,p.margin as margin,so.id as supplier_operator_id,p.pending_date "
                . " from inv_pendings p "
                . " JOIN inv_supplier_operator so "
                . " ON p.supplier_operator_id=so.id "
                . " and p.id='{$params['pendingid']}' ";
    
         $query=  $this->db->query($sql);
         
         if($query->num_rows()):
             
                    $data=$query->row_array();
         
                        $factor=  isUpperBoundH($data['supplier_operator_id'])?((100+$data['margin'])/100):((100-$data['margin'])/100);

                        $oldpendingamt=isUpperBoundH($data['supplier_operator_id'])?$data['pending']/($factor):$data['pending']*($factor);

                        $newpendingamt=$oldpendingamt+$params['enteredamt'];

                        $newpendingamt=round($newpendingamt,2);

                        logerror("Amt to be adjusted :  {$newpendingamt}","adjustments");

                        $newpendingrecharge=round(isUpperBoundH($data['supplier_operator_id'])?($newpendingamt*($factor)):($newpendingamt/($factor)),2);

                        $rechargeAdjusted=round(isUpperBoundH($data['supplier_operator_id'])?$params['enteredamt']*($factor):$params['enteredamt']/($factor),2);

                        $extraComment=" \n Recharge Adjusted : {$rechargeAdjusted} ";

                        $updatearray=array('pending'=>$newpendingrecharge,'comment'=>$params['comment'].$extraComment,'refund'=>$params['enteredamt'],'refund_type'=>$params['refundType']);

                        $this->db->update('inv_pendings',$updatearray,array('id'=>$params['pendingid']));
                    
                        $sql="Update inv_pendings set pending=pending+{$rechargeAdjusted} where supplier_operator_id='{$data['supplier_operator_id']}' and pending_date>'{$data['pending_date']}' ";

                        $this->db->query($sql);
                    
                        logerror("SQL :  {$sql}","adjustments");

                        return true;
                    
            endif;        
         
          return false;        
    }
    
    public function isValidEntry($params)
    {

        $sql="Select pending,margin,supplier_operator_id "
                . "from inv_pendings "
                . " where id='{$params['pendingid']}' ";

          $query=  $this->db->query($sql);

          if($query->num_rows()):
             
                $data=$query->row_array();

                $is_upperbound=isUpperBoundH($data['supplier_operator_id']);
                
                $tablePending=$data['pending']*-1;
                
                 if($data['pending'] > 0):
                    $params['enteredamt']=$params['enteredamt']*(-1);
                 endif;
                 
                 $avaliablePendingAmt=$is_upperbound?($tablePending)/((100+$data['margin'])/100):($tablePending)*((100-$data['margin'])/100);

                 if($data['pending'] > 0):
                     $avaliablePendingAmt=$avaliablePendingAmt*(-1);
                 else:
                     $avaliablePendingAmt=$avaliablePendingAmt+5000;
                 endif;
                
                 $avaliablePendingAmt=ceil($avaliablePendingAmt);
                 
                 if($params['enteredamt']<=$avaliablePendingAmt):
                      if($params['refundType']=="2" && $params['enteredamt']>200 && getLoggedInUserId()=="7883026"):
                          return false;
                      else:
                         return true;
                      endif;
                 else:
                     return false;
                 endif;
                 
         endif;        
         
         return false;
    }
    
    public function validateAmount($params)
    {  
        
        if($params['refundType']==1):
           return true;
       endif;
       
        $sql="Select p.pending,so.id as supplier_operator_id,p.pending_date "
                . " from inv_pendings p "
                . " JOIN inv_supplier_operator so "
                . " ON p.supplier_operator_id=so.id "
                . " and p.id='{$params['pendingid']}' ";
                
        $query = $this->slaveDB->query($sql);
        if($query->num_rows()):
            $data=$query->row_array();

            $maxrefundamt=$this->validateMaxRefundAmtForMonth($data,$params['enteredamt']);
            
            if($maxrefundamt):
                return true;
            else:
                return false;
            endif;
        endif;
        
        return false;

    }
    public function validateMaxRefundAmtForMonth($params,$enteredamt)
    {
 
       $enteredamt=$params['pending']>0?$enteredamt*(-1):$enteredamt;
      
        $sql="select sum(if(refund<0,refund*(-1),refund)) as totalrefund,supplier_operator_id"
                . " from inv_pendings"
                . " where supplier_operator_id='{$params['supplier_operator_id']}'"
                . " and month(pending_date)=month('{$params['pending_date']}')"
                . " and year(pending_date)=year('{$params['pending_date']}') and refund_type='2' ";
                 
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            
                $data=$query->row_array();             

                if($data['totalrefund'] < 100000):

                        if(($data['totalrefund'] + $enteredamt) > 100000):
                            
                            return false;
                        
                        else:
                            
                            return true;
                        
                        endif;

                else:
                    
                    return false;
                
                endif;
            
        endif;
        
        return false;
    }
    
    public function getEmailData($params)
    {
        $sql = "select ip.order_id,ip.pending,ip.refund,ip.comment,s.name as supplier_name,p.name as operator_name,refund_type,ip.pending_date"
                . " from inv_pendings ip "
                . " join inv_supplier_operator so"
                . " on so.id = ip.supplier_operator_id"
                . " join inv_suppliers s"
                . " on s.id = so.supplier_id"
                . " join products p"
                . " on p.id = so.operator_id"
                . " where ip.id='{$params['pendingid']}'";

        $query = $this->slaveDB->query($sql);
        
        if($query->num_rows()):
            
            $data = $query->row_array();
        
            return $data;
            
        endif;
        
    }
}
