<?php

class Note extends CI_Model
{
    private $slaveDB;
    
    public function __construct() {
       $this->slaveDB = $this->load->database('slavedb', TRUE);
    }
    
    public function getNotesBySoid($params)
    {        
        $note_date=isset($params['note_date'])?$params['note_date']:date('Y-m-d');
        $prevdate=date('Y-m-d',strtotime("$note_date - 10 days"));
      //  $note_startdate=date('Y-m-d',strtotime("$note_enddate -1 day"));
        
        $sql="Select n.*,iu.name as username "
                . " from inv_notes n LEFT JOIN users iu ON  n.user_id=iu.id "
                . " where 1 "
                . " and type='{$params['type']}' "
                . " and note_date>='{$prevdate}'  ";
      
        if($params['soid']):
            $sql.=" AND  supplier_operator_id='{$params['soid']}'  ";
        endif;

         if(isset($params['operator_id']) && $params['operator_id']>0):
            $sql.=" AND  operator_id='{$params['operator_id']}'  ";
        endif;
     
        $sql.=" order by note_date asc ";
        
        $query=$this->slaveDB->query($sql);
        
        if($query->num_rows()):
            return $query->result_array();
        endif;
        
        return false;
    }
    
    public function saveNote($params)
    {
        $arr=array('supplier_operator_id'=>$params['soid'],
                            'type'=>$params['type'],
                             'operator_id'=>isset($params['operator_id'])?$params['operator_id']:0,   
                            'user_id'=>  getLoggedInUserId(),
                            'message'=>$params['message'],
                            'note_date'=>$params['note_date'],
                            'note_timestamp'=>date('Y-m-d H:i:s')
                            );
        
        if($this->db->insert('inv_notes',$arr)):
            return true;
        endif;
        
        return false;
    }
}
