<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function checkAccess($controller,$action='index')
{
    
    $CI =& get_instance();
    
    $group_id=getLoggedInUserKey('group_id');

    // Check if Admin login
    if($group_id==12):
        return true;
    endif;
    
    // Check if cron
    if(isCron($controller,$action)):
        return true;
    endif;
    
    // Check if open url
    if(isOpenUrl($controller,$action)):
        return true;
    endif;
    
    $access_arr=$CI->config->item('authorizations');
    
    $group_access=isset($access_arr['groups'][$group_id])?$access_arr['groups'][$group_id]:die('Access Denied');
    
    // Check group access
     $hasAccess=false;
     
     foreach($group_access['privileges'] as $row):
         
                    if($row['controller']==$controller && ($row['action']==$action || $row['action']=="*") ):
                     
                        if($row['url']===true):
                             $hasAccess=true;
                         else:
                             $hasAccess=false;
                             break;
                         endif;        
                   
                    endif;
                    
     endforeach;

     
return $hasAccess;

}

function isCron($controller,$action)
{
    $CI =& get_instance();
    
    $crons_arr=$CI->config->item('crons');
    
    $flag=false;
    
    foreach($crons_arr as $cron):
            if($cron['controller']==$controller && $cron['action']==$action):
                $flag=true;
                break;
            endif;
    endforeach;
    
    return $flag;

    
    
}

function isOpenUrl($controller,$action)
{
    $CI =& get_instance();
    
    $crons_arr=$CI->config->item('openurls');
    
    $flag=false;
    
    foreach($crons_arr as $cron):
            if($cron['controller']==$controller && $cron['action']==$action):
                $flag=true;
                break;
            endif;
    endforeach;
    
    return $flag;

    
    
}