<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function sum_array($val1,$val2,$key_compaire){
            //check wich array is greater
           if(count($val1)>count($val2)){
               $arr2=$val1;
               $arr1=$val2;
               $count = count($val1);
           }
           else{
               $arr2 = $val2;
               $arr1=$val1;
               $count = count($val2);
           }
           
        $keys = array_keys($arr2[0]);
        $result = array();
        $k=0;
        
	for($i=0;$i<count($arr2);$i++){
            $flag=false;
            for($j=0;$j<=$i;$j++){
               if(array_key_exists($j,$arr1) && ($arr1[$j][$key_compaire]==$arr2[$i][$key_compaire]) && !empty($arr1[$j][$key_compaire])){
                    
                   foreach($keys as $value){
                            if('apiprocesstime'!=trim($value) && 'modemprocesstime'!=trim($value)){
                                if('sales'==trim($value) ||  'inprocess'==trim($value) || 'failure'==trim($value) || 'trans_count'==trim($value) ){
                                    $result[$k][$value]=  $arr2[$i][$value]+$arr1[$j][$value];
                                }else{
                                    $result[$k][$value]=  round(($arr2[$i][$value]+$arr1[$j][$value])/2);
                                }
                            }
			}
                        $flag = true;
                        break;
                    }
                   
            }
            if(!$flag){
                if(array_key_exists($i,$arr1) && ($arr1[$i][$key_compaire] < $arr2[$i][$key_compaire]) && !empty($arr1[$i][$key_compaire])){
                       
                    foreach($keys as $value){
                            if('apiprocesstime'!=trim($value) && 'modemprocesstime'!=trim($value)){
				$result[$k][$value]=  $arr1[$i][$value];
                            }
			}
                        $k=$k+1;
                    }
                
               foreach($keys as $value){
                    if('apiprocesstime'!=trim($value) && 'modemprocesstime'!=trim($value)){
                        $result[$k][$value]= $arr2[$i][$value];
                    }
                } 
            }
	  	$k++;
	}
        $result2 = array();
       
        //get elements from arr1 which are not in arr2
        for($i=0;$i<count($arr1);$i++){
            $flag=false;
            
            for($j=0;$j<count($result);$j++){
                if(array_key_exists($i,$arr1) && $arr1[$i][$key_compaire]==$result[$j][$key_compaire]){
                    $flag=true;
                    
                }
            }
            
            if(!$flag && array_key_exists($i,$arr1)){
                foreach($keys as $value){
                    if('apiprocesstime'!=trim($value) && 'modemprocesstime'!=trim($value)){
                        $result2[$i][$value]= $arr1[$i][$value];
                    }
                } 
            }
        }
        if(!empty($result2) && count($result2)>=1){
            $i = count($result);
            foreach($result2 as $row){
                $result[$i]=$row;
                
                $i++;
            }
        }
        //echo json_encode($result);die;
        //remove duplicate
        $result = remove_duplicate($result,$key_compaire);
        
        $keys ='';
        $val = get_key($arr2);
        
        foreach($arr2 as $row){
            
            if(array_key_exists($val, $row)){
                 $index = trim(find_inex($result,$row[$key_compaire],$key_compaire));
                if($index!==false && $row['success']>0)
                $result[$index][$val] = intval($row[$val]/$row['success']);
            }
        }
        
        $val =0;
        $val = get_key($arr1);
        
        foreach($arr1 as $row){
            if(array_key_exists($val, $row)){
                $index = find_inex($result,$row[$key_compaire],$key_compaire);
                if($index!==false && $row['success']>0)
                $result[$index][$val] = intval($row[$val]/$row['success']);
            }
        }
        
         
	return $result;
}

function check_duplicate($arr,$value,$index,$key_compaire){
    foreach($arr as $key=>$val){
        if($value==$val[$key_compaire] && $key>$index ) {
            return $key;
        }
    }
    return false;
}

function find_inex($arr,$value,$index){
    foreach($arr as $key=>$val){
        if($value==$val[$index]){
            return $key;
        }
    }
    return false;
}

function get_key($arr){
    $keys = array_keys($arr[0]);
    if(in_array('apiprocesstime',$keys)==true){
        $val = 'apiprocesstime';
    }else{
            $val = 'modemprocesstime';
    }
    return $val;
}

function remove_duplicate($result,$key_compaire){
    $keys = array("product_id","api_flag","sales","success","processtime","failure","trans_count","api_flag");
        
    foreach($result as $key=>$row){
           $index = check_duplicate($result,$row[$key_compaire],$key,$key_compaire);
           if($index!==false){
               foreach($keys as $value){
                    if('sales'==trim($value) ||  'inprocess'==trim($value) || 'failure'==trim($value) || 'trans_count'==trim($value) ){
                        $result[$key][$value]=  $result[$key][$value]+$result[$index][$value];
                    }else{
                        $result[$key][$value]=  round(($result[$key][$value]+$result[$index][$value])/2);
                    } 
               }
                unset($result[$index]);
           }
        }
        return $result;
}