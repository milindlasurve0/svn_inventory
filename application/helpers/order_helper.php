<?php

function getexpectedsalefortheday()
{
     $CI =& get_instance();
     
    $CI->load->model('order');
    
    $AB=  $CI->order->getAB();
           
           $A=$AB[0]->avgsale;
           $B=$AB[1]->avgsale;
                
           $variance=array();
           
           $actualsales=  $CI->order->getexpectedsalesbetweendates(date('Y-m-d',  strtotime('-6 day')),date('Y-m-d',  strtotime('-1 day')));
           
           $dates= GetDays(date('Y-m-d',  strtotime('-6 day')), date('Y-m-d',  strtotime('-1 day')));
              
           foreach($dates as $date):
               
               
              $ES=(1+(($actualsales[$date]['10hr']-$A)/$A)) *$B;
           
              $variance[]=  ($actualsales[$date]['24hr']-$ES)/$ES;
              
              $ES=0;
              
           endforeach;
           
           $lastdaysale=$actualsales[date('Y-m-d',strtotime('-1 day'))]['24hr'];
           
           $avgVariance=  array_sum($variance)/count($variance);
          
           $X= $lastdaysale+(($avgVariance*$lastdaysale)/100);
            
            return  json_encode(array('status'=>true,'type'=>true,'variance_percentage'=>$avgVariance,'expectedSale'=>round($X,2)));
}


function combineOperator($operator_id)
{
      $temp=  strpos($operator_id,",")?explode(',',$operator_id):array($operator_id);

      $tobereturned="";
  
     foreach ($temp as $opr):

                            switch ($opr):

                                case 9 :

                                            $id="9,10,27";

                                            break;
                                case 10 :

                                            $id="9,10,27";

                                            break;

                                case 27 :

                                            $id="9,10,27";

                                            break;

                                default :

                                        $id=$opr;

                            endswitch;

                    $tobereturned[]=$id;
              
                endforeach;
    
          return count($tobereturned)>1?implode(',',array_unique($tobereturned)):$tobereturned[0];
       
}

function getRoundoff($value,$operator_id)
{
    if($value>0):
    return ($operator_id==4)?(2450 * ceil($value/2450)):(5000 * ceil($value/5000));
    endif;
    
    return $value;
}

function getOrderRoundoff($value,$operator_id)
{
    if($value>0):
    return ($operator_id==4)?(2450 * ceil($value/2450)):(10000 * ceil($value/10000));
    endif;
    
    return $value;
}

function getHOrderAmountByBounds($amount,$so_id)
{
    $CI =& get_instance();
     
    $CI->load->model('order');
    
    return $CI->order->getOrderAmountbyBounds($amount,$so_id);
}

function isUpperBoundH($so_id)
{
    $CI =& get_instance();
     
    $CI->load->model('order');
    
    return $CI->order->isUpperBound($so_id);
}

function isAlternate($freq)
{
     if(strpos($freq,',')){
         
               return true;
                    
     }
          
     return false;
}

function isMF($freq)
{
     if(strpos($freq,'-')){
         
               return true;
                    
     }
          
     return false;
}

function getVendoridsBySupplieridH($supplier_id,$wantstring=false)
{
     $CI =& get_instance();
     
    $CI->load->model('order');
    
    $ids=$CI->order->getvendoridsbysupplierid($supplier_id);
    
    if(!empty($ids)):
            if($wantstring):
                    if(count($ids)>0):
                                return implode(',',$ids);
                    else:
                                return $ids[0];
                    endif;
            else:
                    return $ids;
            endif;
    endif;
    
    
    return false;
}



 function sendOrderMsgEmail($params,$utr)
 {
      $CI =& get_instance();
      
      $CI->load->config('inventory');
      
      $CI->load->model('dashboardscreen');
      
      $data=$CI->dashboardscreen->getsmsRelatedData($params);
      
       logerror("SMS START","inventorymailer");
       logerror("UTR : ".$utr,"inventorymailer");
      /* Send Message */ 
      if(!empty($data['mobile'])):
             foreach($data['mobile'] as $mobile):
                            //if($mobile!=""):
                            if($mobile['contact']!=""):
                                 $sms=  sprintf($CI->config->item('ordersmstemplate'),$data['info']['name'],$data['info']['todaysorder'],$data['info']['to_pay'],$utr,$data['info']['operator']);   
                                 $CI->curl->get("http://www.smstadka.com/redis/insertInQsms",array('mobile'=>$mobile['contact'],'root'=>'tata','sms'=>$sms));
                                 $CI->dashboardscreen->setSmsEmailFlag($params['paymentid'],true);
                                 logerror(json_encode(array('mobile'=>$mobile['contact'],'root'=>'tata','sms'=>$sms)),"inventorymailer");
                             endif;
               endforeach;
       endif;
       logerror("SMS END","inventorymailer");
       
       
       /* Send Email */
       if($data['info']['email']!=""):
           
            logerror("EMAIL START","inventorymailer");
            logerror(json_encode($params),"inventorymailer");   
            logerror(json_encode($data),"inventorymailer");   
            $CI->load->library('parser');
            
           $maildata=array('name'=>$data['info']['name'],'operator'=>$data['info']['operator'],'todaysorder'=>number_format($data['info']['todaysorder'],2),'to_pay'=>number_format($data['info']['to_pay'],2),'txnid'=>$utr,'account_no'=>$data['info']['account_no']);  
             
            logerror(json_encode(array('mail_subject'=>'Todays Order','emails'=>$data['info']['email'].",".implode(",",$CI->config->item('inventoryOrderEmailCC')),'sender_id'=>'orders@mindsarray.com')),"inventorymailer");   
            
           $CI->curl->post("http://www.smstadka.com/redis/insertInQsms",
                                         array(
                                                    'mail_subject'=>'Todays Order',
                                                   'mail_body'=>$CI->parser->parse('templates/ordermailtosupplier', $maildata, TRUE),
                                                   'emails'=>$data['info']['email'].",".implode(",",$CI->config->item('inventoryOrderEmailCC')),
                                                   'sender_id'=>'orders@mindsarray.com'
                                                   )
                                        );
           
           $CI->dashboardscreen->setSmsEmailFlag($params['paymentid'],false,true);
           
          logerror("EMAIL END","inventorymailer"); 
           
       endif;
       
 }