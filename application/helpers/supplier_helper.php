<?php

function getOperatorDropdownHTML($multiple=true)
{
    
    $CI =& get_instance();
    $CI->load->model('operator');
    $operators=$CI->operator->getOperatorsForDropdown();

    $name=$multiple?"operators[]":"operator_id";

    $html='<select name="'.$name.'"   required data-error="Select Operator">';
    $html.='<option value="">Select Operator</option>';
    foreach($operators as $value):
         $html.='<option value="'.$value->id.'">'.$value->operator_name.'</option>';
    endforeach;
    $html.='</select>';
    

    return $html;
    
}


function getFrequencyDropdownHTML()
{
      $CI =& get_instance();
      $CI->load->config('inventory');
      
     $frequency=$CI->config->item('frequency');
     
    $html='<select name="frequency[]" required data-error="Select Frequency">';
    $html.='<option value="">Select frequency</option>';
    foreach($frequency as $key=>$value):
        $html.='<option value="'.$key.'">'.$value.'</option>';
    endforeach;
    $html.='</select>';
    
      return $html;
      
}

function getCommissionTypeDropdownHTML()
{
      $CI =& get_instance();
      $CI->load->config('inventory');
      
     $commission_type=$CI->config->item('commission_type');
     
    $html='<select name="commission_type[]" required data-error="Select Commission">';
    $html.='<option value="">Select Commission Type</option>';
    foreach($commission_type as $key=>$value):
        $html.='<option value="'.$key.'">'.$value.'</option>';
    endforeach;
    $html.='</select>';
    
      return $html;
      
}
function getBatchDropdownHTML($multiple=true,$allbatch=false)
{
      $CI =& get_instance();
      $CI->load->config('inventory');
      
       $name=$multiple?"batch_type[]":"batch_type";
       
     $batches=$CI->config->item('batch');
     
    $html='<select name="'.$name.'" required data-error="Batch Required">';
    $html.='<option value="">Select Batch</option>';
    if($allbatch):$html.='<option value="all">All Batch</option>';endif;
    foreach($batches as $key=>$value):
        $html.='<option value="'.$value.'">'.$value.'</option>';
    endforeach;
    $html.='</select>';
    
      return $html;
      
}

function getSupplierDefaultBankId($supplier_id)
{
    $CI =& get_instance();
    
    $CI->load->model('orderapproval');
    
    $bank_id=$CI->orderapproval->getdefaultBankid($supplier_id);
    
    if(isset($bank_id->id)):
        
        return $bank_id->id;
    
    endif;
    
    return ;
}

function haszeroinfront($amount)
{
    if($amount):
        if(substr($amount,0,2)=="00"):
            return true;
        elseif(substr($amount,0,1)=="0"):
            return true;
        endif;
     endif;
     
     return false;
}

function getVendorsKeyValuePair()
{
     $CI =& get_instance();
    $CI->load->model('supplier');
    $vendors=$CI->supplier->getVendorForDropdown();
    $data=array();
    
    foreach($vendors as $vendor):
        $data[$vendor->id]=$vendor->vendor_name;
    endforeach;
    
    return $data;
}

function getallVendorsKeyValuePair()
{
     $CI =& get_instance();
    $CI->load->model('supplier');
    $vendors=$CI->supplier->getallVendorForDropdown();
    $data=array();
    
    foreach($vendors as $vendor):
        $data[$vendor->id]=$vendor->vendor_name;
    endforeach;
    
    return $data;
}
function getApiVendorsKeyValuePair()
{
     $CI =& get_instance();
    $CI->load->model('supplier');
    $vendors=$CI->supplier->getApiVendorForDropdown();
    $data=array();
    
    foreach($vendors as $vendor):
        $data[$vendor->id]=$vendor->vendor_name;
    endforeach;
    
    return $data;
}
function current_full_url($asarray=false)
{
    $CI =& get_instance();

    $url = $CI->config->site_url($CI->uri->uri_string());
    $urlstr=$_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
    if(!$asarray):
    return $urlstr;
    else:
            if(strpos($urlstr,"?")):
                    $level1=explode('?',$urlstr);
                    $mainurl=$level1[0];
                    parse_str($level1[1],$out);
                    $strasarray=$out;
                     return $strasarray;
                    //echo $mainurl."?".http_build_query($strasarray);
            else:
                    
           endif;
    endif;
}

function getSortUrlByColumn($column="base_amount")
{

     $CI =& get_instance();
     $getarray=$CI->input->get();
     $sorturl=  current_url();
     
     switch ($getarray['ordertype']):

                     case "asc":
                                        $getarray['orderby']=$column;
                                        $getarray['ordertype']="desc";   
                                        $sorturl=current_url()."?".http_build_query($getarray);
                                        break;
                    case "desc":
                                        $getarray['orderby']=$column;
                                        $getarray['ordertype']="asc";   
                                        $sorturl=current_url()."?".http_build_query($getarray);
                                        break;
                   default :           
                                      $getarray['orderby']=$column;
                                      $getarray['ordertype']="asc";   
                                      $sorturl=current_url()."?".http_build_query($getarray);

      endswitch;
      
      return $sorturl;
}
