<?php

function getLoggedInUsername()
{
    $CI = & get_instance();
    $data=$CI->session->userdata('user');
  

    if($CI->session->userdata('isLoggedIn')):
        return $data->username;
    endif;
    
    return ;
}

function getLoggedInUserId()
{
    $CI = & get_instance();
    $data=$CI->session->userdata('user');
  
   return $data->id;
   
}

function getLoggedInUserKey($key)
{
    $CI = & get_instance();
    $data=$CI->session->userdata('user');
  

    if($CI->session->userdata('isLoggedIn')):
        return isset($data->$key)?$data->$key:"";
    endif;
    
    return ;
}
