<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/incomings.js'); ?>"></script>
<script>$(document).ready(function(){$('#searchdate').datepicker({format: "yyyy-mm-dd",startDate: "-365d",endDate: "1d",multidate: false,autoclose: true,todayHighlight: true,orientation: "top right"});});</script>
<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search Block Sims</h3>
           </div>
        </div>
    </div>
    
           <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="search" method="get">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-4"><label>Date</label></div>
                                    <div class="col-lg-7" id="sandbox-container">
                                  <input type="text" name="searchdate" id="searchdate" class="form-control input-md input-sm"  value="<?php echo $this->input->get('searchdate'); ?>"  required />
                                    </div>
                                </div>
                            </div>
                          
                          
                             <div class="col-lg-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                          </div>
                    </div>
                </div>
            </div>

    <?php if(!empty($sims)):  ?>
    
            <div class="row">
                <div class="col-lg-12">
                       <h3>Block date : ( <?php echo $this->input->get('searchdate'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Total Count :  <?php echo count($sims); ?></div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                       <th>Supplier</th>
                                       <th>Vendor Tag</th>
                                       <th>Operator</th>
                                       <th>Balance</th>
                                       <th>Mobile</th>
                                       <th>Scid</th>
                                       <th>Block date</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php foreach($sims as $sim):  ?>
                                   <tr>
                                       <td><?php echo $sim['suppliername']; ?></td>
                                       <td><?php echo $sim['vendor_tag']; ?></td>
                                       <td><?php echo $sim['operator']; ?></td>
                                       <td><?php echo number_format($sim['balance'],2); ?></td>
                                       <td><?php echo $sim['mobile']; ?></td>
                                       <td><?php echo $sim['scid']; ?></td>
                                       <td><?php echo $sim['last_block_date']; ?></td>
                                   </tr>
                                   <?php endforeach;  ?>
                               </tbody>
                           </table>
                       </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($sims) && $this->input->get()): echo "No block sims Detected ";  endif; ?>
</div>


<?php $this->load->view('common/footer');  ?>
