<?php $this->load->view('common/header'); ?>

<?php $this->load->view('common/menu'); ?>

<script src="<?php echo base_url('public/js/todaysordersdetails.js'); ?>"></script>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Todays order</h1>
                            </div>
                        </div>
              </div>
            
               <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="/dashboard/viewtodaysorder" method="get">
                              <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Operator</label></div>
                                   <div class="col-lg-7"> <select name="operator_id" id="operator_id"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value="all"  <?php if($this->input->get('operator_id')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($operators as $operator): ?>
                                      <option value="<?php echo $operator->id;  ?>"  <?php if($this->input->get('operator_id')==$operator->id):  echo "Selected";  endif;?>><?php echo $operator->operator_name;  ?></option>
                                      <?php endforeach; ?>
                                       </select></div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                  <div class="form-group">
                                <div class="col-lg-5"><label for="supplier_id" class="col-lg-5 control-label">Supplier</label></div>
                                     <div class="col-lg-6">
                                           <select name="supplier_id" id="supplier_id"  style="width: 150px" >
                                              <option value="">Select</option>
                                               <?php foreach($suppliers as $supplier): ?>
                                              <option value="<?php echo $supplier->id;  ?>"  <?php  if($this->input->get('supplier_id')==$supplier->id): echo "selected";  endif; ?>><?php echo $supplier->name;  ?></option>
                                              <?php endforeach; ?>
                                          </select>
                            </div>
                            </div>
                            </div>
                             <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Handled</label></div>
                                   <div class="col-lg-7"> <select name="handled_by" id="handled_by"  style="width: 150px" >
                                       <option value="0"  <?php if($this->input->get('handled_by')=="0"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($internals as $internal): ?>
                                      <option value="<?php echo $internal->id;  ?>"  <?php if($this->input->get('handled_by')==$internal->id):  echo "Selected";  endif;?>><?php echo $internal->name;  ?></option>
                                      <?php endforeach; ?>
                                       </select></div>
                                </div>
                            </div>
                              <div class="col-lg-2">
                                <div class="form-group">
                                    <div class="col-lg-8"><label>Order exists</label></div>
                                    <div class="col-lg-1"> 
                                        <input type="checkbox" name="order_exists" id="order_exists"  value="1" <?php if($this->input->get('order_exists')): echo "checked"; endif; ?> />
                                   </div>
                                </div>
                            </div>
                             <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Modems</label></div>
                                   <div class="col-lg-7">  
                                       <select name="vendors[]" multiple="multiple" style="height: 111px; width: 128px;">
                                                       <?php foreach($vendors as $vendor): ?>
                                                        <option value="<?php echo $vendor->id;  ?>" <?php if($this->input->get('vendors')): if(in_array($vendor->id,$this->input->get('vendors'))):  echo "selected"; endif; endif; ?>  ><?php echo $vendor->vendor_name;  ?></option>
                                                        <?php endforeach; ?> 
                                      </select>
                                   </div>
                                </div>
                            </div>
                           
                            
                            <div class="col-lg-1"  style="float: right;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
 </div>
            
            <div class="row">
                <div class="col-lg-12">
        <?php if(!empty($todaysordersdetails)): ?>    
                    <p>Total rows : <?php echo count($todaysordersdetails); ?></p>
                     <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                       <th>#</th>
                                       <th>Apprvd</th>
                                       <th>Download</th>
                                       <th>Payed</th>
                                       <th>Supplier</th>
                                       <th>Operator</th>
                                       <th>Margin</th>
                                       <th>TD.pending</th>
                                       <th>Order Amt</th>
                                    <!--    <th>Amt.bounds</th> -->
                                       <th>Topay</th>
                                       <th>Received</th>
                                       <th>Net.Pending</th>
                                        <th>UTR</th> 
                                       <th>Actions</th>
                                       <th>comm</th>
                                       <th>&nbsp;</th>
                                       <th>isactive</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php $totalorder=0;$totaltopay=0;  foreach($todaysordersdetails as $order):  ?>
                                   <?php 
                                    if(is_null($order['ordertableamount']) && $order['payment_status']=="1"):
                                        $totalorder+=$order['amount'];$totaltopay+=$order['to_pay'];
                                   endif;
                                   ?>
                                   <tr class="<?php if($order['is_payment_done']=="0" || $order['is_payment_done']=="2"): echo "danger"; endif; ?>">
                                       <td><?php echo $order['orderid']; ?></td>
                                       
                                       <?php if(is_null($order['ordertableamount']) && $order['payment_status']=="1" ): ?>
                                            <td><span class="glyphicon glyphicon-ok text-success"></span></td>
                                           <td><span class="glyphicon glyphicon-ok text-success"></span></td>
                                           <td><span class="glyphicon glyphicon-ok text-success"></span></td>
                                       <?php else:  ?>
                                            <td><?php if($order['is_approved']=="1"): ?><span class="glyphicon glyphicon-ok text-success"></span><?php endif;  ?></td>
                                            <td><?php if($order['is_downloaded']=="1"): ?><span class="glyphicon glyphicon-ok text-success"></span><?php endif;  ?></td>
                                            <td><?php if($order['is_payment_done']=="1"): ?><span class="glyphicon glyphicon-ok text-success"></span><?php endif;  ?></td>
                                       <?php endif;  ?>
                                            
                                            <td><a  target="_blank"  href="/suppliers/get/<?php echo $order['supplier_id']; ?>"><?php echo $order['suppliername']; ?></a></td>
                                       <td><?php echo $order['operator']; ?></td>
                                       <td>
                                           <?php echo $order['margin']; ?>
                                           
                                           <?php if($order['commission_type']=="1"): ?>
                                           <span class="glyphicon glyphicon-arrow-down text-success"></span>
                                           <?php else:  ?>
                                           <span class="text-success glyphicon glyphicon-arrow-up"></span>
                                           <?php endif;  ?>
                                       </td>
                                       <td><a target="_blank" href="/orderhistorys/pendinghistory/<?php echo $order['supplier_operator_id']; ?>"><?php echo $order['till_date_pending']; ?></a></td>
                                       <td><?php echo $order['amount']; ?></td>
                                 <!--       <td><?php echo $order['expected']; ?></td> -->
                                       <td><?php echo $order['to_pay']; ?></td>
                                       <?php $vendorids=$order['vendorids']!=""?urlencode($order['vendorids']):0;  ?>
                                       <td><a href="/orders/viewlastdayincoming/<?php echo $order['supplier_operator_id']; ?>/<?php echo $vendorids; ?>/<?php echo $order['operator_id']; ?>/<?php echo date('Y-m-d'); ?>" target="_blank">
                                           <?php echo $order['received']; ?></a>
                                       </td>
                                        <td><?php 
                                        $netpending=0;
                                        if($order['payment_status']=="1"):
                                            if($order['till_date_pending']<0):
                                                echo $netpending=-($order['expected']-$order['received']-$order['till_date_pending']);
                                            else:
                                                 echo $netpending=-($order['expected']-$order['received']);
                                            endif;
                                        else:
                                            echo $netpending=-(-$order['till_date_pending']-$order['received']);
                                        endif;
                                        ?></td>
                                        <td><?php echo !empty($order['txnid'])?"`".$order['txnid']:""; ?></td> 
                                       <td>
                                           <?php if(is_null($order['ordertableamount']) && $order['payment_status']=="1"):  ?>
                                           <button class="btn btn-xs btn-default btn-sendsms  <?php if($order['sms']=="1"): echo "disabled btn-success"; endif; ?>" data-paymentid="<?php echo $order['paymentid']; ?>" data-supplierid="<?php echo $order['supplier_id']; ?>"> 
                                                <i class="glyphicon glyphicon-phone"></i>
                                               
                                            </button>
                                            <button class="btn btn-xs btn-default btn-sendemail   <?php if($order['email']=="1"): echo "disabled btn-success"; endif; ?>" data-paymentid="<?php echo $order['paymentid']; ?>" data-supplierid="<?php echo $order['supplier_id']; ?>">
                                                <i class="glyphicon glyphicon-envelope"></i>
                                               
                                            </button>
                                         <?php endif; ?>
                                         <?php if($order['planned_sale']-$order['opening']-$order['received']>0): ?> &nbsp;<i class="fa fa-phone cursorpointer" title="Target : <?php echo $order['planned_sale']; ?>"></i><?php  endif; ?>
                                        </td>
                                        <td>
                                            <?php if($order['orderid']): ?>
                                            <?php echo getLastComment($order['supplier_operator_id']);  ?>
                                        <a  data-orderid="<?php echo $order['orderid'];  ?>" data-soid="<?php echo $order['supplier_operator_id']; ?>"  data-operatorid="<?php echo $order['operator_id'];  ?>" data-toggle="modal" href="#addcomment">View</a>
                                            <?php endif;  ?>
                                       </td>
                                       <td><a class="highlightitle"  data-notedate="<?php echo date('Y-m-d'); ?>" data-toggle='modal'  href='#addnotesdiv' data-oprid="<?php echo $order['operator_id'];  ?>" data-soid="<?php echo $order['supplier_operator_id'];  ?>"  data-type="1"><i class="fa fa-comments-o fa-2"></i></a>&nbsp;(<span><?php echo $order['messagecount']; ?></span>)</td>
                                       <td><?php if($order['is_active']=="1"): ?>active<?php else: ?>inactive<?php endif; ?></td>
                                   </tr>
                                   <?php endforeach; ?>
                               </tbody>
                               <tfoot>
                                   <tr>
                                       <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                       <th><?php echo number_format($totalorder,2)  ?></th>
                            
                                       <th><?php echo number_format($totaltopay,2)  ?></th><td></td><td></td><td></td><td></td><td></td>
                                   </tr>
                               </tfoot>
                     </table>
           <?php else: echo "No orders deteted with following filters"; endif; ?>                        
                    
                </div>
            </div>
            
        </div>
    </div>
    
     <div class="modal" id="addcomment">
<div class="modal-dialog" style="width: 300px;">
<div class="modal-content">
  <div class="modal-header">
      <h4 class="modal-title">Add Comment</h4>
  </div>
  <div class="modal-body">
      <div class="row">
          <div class="col-lg-12" id="loadcomments">
              
          </div>
      </div>
      <input type="hidden" name="comment_order_id" id="comment_order_id" value="" />
      <input type="hidden" name="comment_so_id" id="comment_so_id" value="" />
      <textarea  name="comment" id="comment"></textarea>
  </div>
  <div class="modal-footer">
      <div id="ordercommentmsg" style="text-align: left"></div>
      <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Update</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
    
</div>

<?php  $this->load->view('common/notes');  ?>
<?php $this->load->view('common/footer'); ?>

<script src="<?php echo base_url('public/js/note.min.js'); ?>"></script>
