<?php if(!empty($todaysordersummary)):  ?>
<div class="panel panel-default" id="todaysorderlisting" style="display: none;">
    <div class="panel-heading">
        <i class="fa fa-bell fa-fw"></i>Todays Order's
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="list-group">
            <?php  foreach($todaysordersummary as $order): ?>
            <a  target="_blank" href="/dashboard/viewtodaysorder?operator_id=<?php echo $order['operator_id'] ?>" class="list-group-item">
                  <?php echo $order['operator'];  ?>  (<?php echo $order['total'];  ?>)
                <span class="pull-right text-muted small"><em> <?php echo $order['totalorder'];  ?></em>
                </span>
            </a>
            <?php endforeach;  ?>
        </div>

       <a target="_blank"  href="/dashboard/viewtodaysorder" class="btn btn-default btn-block">View All</a>
    </div>

</div>

<?php endif; ?>