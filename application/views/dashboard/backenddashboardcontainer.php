<script src="<?php echo base_url('public/js/fixedheader.js'); ?>"></script>
<script src="<?php echo base_url('public/js/jquery.multiple.select.js'); ?>"></script>
<script src="<?php echo base_url('public/js/backendashboard.js?'.time()); ?>"></script>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/multiple-select.css'); ?>" />
<script>$(document).ready(function(){
    
     $('#sb_sync_date').datepicker({
        format: "yyyy-mm-dd",
        startDate: "-365d",
        endDate: "1d",
        multidate: false,
        autoclose: true,
        todayHighlight: true,
        orientation: "top right"
        });
        


});</script>
<div class="row">
    <div class="col-lg-12">
        
            <form class="form-inline" role="form">
                
                    <div class="form-group">
                        <label for="operator_id" class="col-lg-3">Operators</label>
                        <select name="operator_id[]" id="operator_id"  style="width: 150px" required>
                                                            <?php foreach($operators as $oprkey=>$operator): ?>
                                                           <option value="<?php echo $oprkey;  ?>"  <?php  if($this->input->get('operator_id')==$oprkey): echo "selected";  endif; ?>><?php echo ucwords(strtolower($operator));  ?></option>
                                                           <?php endforeach; ?>
                        </select>
                    </div>
                
                   <div class="form-group">
                        <label for="vendor_id" class="col-lg-3">Vendors</label>
                        <select name="vendor_id[]" id="vendor_id"  style="width: 150px" required>
                                                            <?php foreach($vendors as $key=>$name): ?>
                                                           <option value="<?php echo $key;  ?>" ><?php echo ucwords(strtolower($name));  ?></option>
                                                           <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                    <label for="sb_sync_date">Date</label>
                     <input   type="text" name="sb_sync_date" id="sb_sync_date"  class="form-control form-inp-med" value="<?php echo date('Y-m-d'); ?>"/>
                    </div>

                    <div class="form-group">
                        <div class="col-md-1"><button class="btn btn-default btn-sm" id="btnfiltersalebifurcation" type="button" >Filter</button></div>
                    </div>     
                
                <div class="form-group">
                    <div id="salebifurcationmsg" class="col-md-12"></div>
                </div>
                
            </form>
   
        </div>
    </div>


<div id="salebifurcationreport"><div style="margin: 0 auto;text-align: center"><i class="fa fa-refresh fa-spin fa-5x"></i><h3>Loading sale bifurcation report . . . ..</h3></div></div>