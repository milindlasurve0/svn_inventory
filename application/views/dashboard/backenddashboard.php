<?php foreach($backenddashboard as $key=>$data):$totalAvgSale=0;$totalLDS=0;$totalBasesale=0;$totalTargetsale=0;$totalOpening=0;$totalBlockedbal=0;$totalSale=0;$totalOrder=0; $totalCurrentStock=0;$totalSims=0;$totalPending=0; $totalWorkingSims=0;$totalMaxSaleCapacity=0;?>
<h2>
<?php 
$oprarr=$this->config->item('operatorkeyvalue');
echo $oprarr[$key];
?>
</h2>    
<table class="table table-bordered gridtable table-hover salebifurcation">
    <thead  style="color: #18181B;background-color: yellow;">
            <th>Modems</th>
            <th>Prior</th>
            <th>Supplier</th>
            <th>A.sale</th>
            <th>LDS</th>
            <th>Max Sale Cap</th>
            <th>Modem Target</th>
            <th>Opn</th>
            <?php  if(getLoggedInUserKey('group_id')=="13"):  ?><th>BB</th><?php endif; ?>
            <th>Curr Stock</th>
             <th>Today sale</th>
            <th>Sims</th>
            <th>Order</th>
            <th>Pending</th>
<!--            <th>last5minsale</th>
            <th>Req/5min</th>-->
             <?php  if(getLoggedInUserKey('group_id')=="13"):  ?><th>Disp Amt</th><?php endif;  ?>
             <th></th>
    </thead>
                <tbody>
           <?php foreach ($data as $row):  ?>
                    <?php
                    $totalSims+=$row['totalsims'];$totalWorkingSims+=$row['workingsims']; 
                    $totalAvgSale+=$row['avg_sale'];$totalLDS+=$row['last_day_sale'];$totalBasesale+=$row['basesale'];$totalTargetsale+=$row['modemtarget'];$totalOpening+=$row['opening']-$row['blockedbalance'];$totalBlockedbal+=$row['blockedbalance'];$totalSale+=$row['todaysale'];$totalOrder+=$row['todaysorder'];
                    $totalCurrentStock+=($row['is_api']=="1")?($row['targetsale']-$row['todaysale']):$row['currentstock'];
                    $totalMaxSaleCapacity+=$row['max_sale_capacity'];
                    ?>
                    <tr id="tr_<?php echo $row['supplier_operator_id'];  ?>" class="<?php echo $row['is_api']=="1"?"apivendorbackground":""; ?>" style="background-color: <?php echo ( $row['todaysale']>= ((92*$row['modemtarget'])/100)  && $row['modemtarget']>0 )?"#B9F2A1":""; ?>" >    
                           <td>
                               <?php  echo isset($vendors[$row['vendorid']])?$vendors[$row['vendorid']]:"";   ?>
                           </td>
                           <td><?php echo isset($row['priority'])?$row['priority']:"-NA-"; ?></td>
                           <td><?php echo $row['suppliername']; ?></td>
                           <td><?php echo number_format($row['avg_sale']) ?></td>
                            <td><?php echo $row['last_day_sale'] ?></td>
                            <td><?php echo $row['max_sale_capacity']; ?></td>
                            <td><?php echo $row['modemtarget']; ?></td>
                           <td class="cursorpointer" title="Blocked Balance :  <?php echo $row['blockedbalance'] ?> "><?php echo number_format($row['opening']-$row['blockedbalance'],2); ?></td>
                            <?php  if(getLoggedInUserKey('group_id')=="13"):  ?><td><?php echo $row['blockedbalance'];  ?></td><?php endif;  ?>
                            <td class="<?php if($row['currentstock']<5000 && $row['todaysorder']): echo "info";  endif; ?>"><?php echo ($row['currentstock'])?number_format($row['currentstock'],2):0.00; ?></td>
                           <td><?php echo $row['todaysale']?$row['todaysale']:0.00; ?></td>
                           <td><?php echo $row['totalsims']?$row['workingsims']."/".$row['totalsims']:"-NA-"; ?></td>
                           <td><?php echo $row['todaysorder']; ?></td>
                           <?php  $pending=($row['pending']<0)?($row['todaysorder']-$row['received']-$row['pending']):($row['todaysorder']-$row['received']); $totalPending+=$pending; ?>
                           <td class="cursorpointer <?php if($pending): echo "danger"; endif; ?>" title="Received :  <?php echo $row['received'] ?> &#13;Pending : <?php echo $row['pending']; ?> "><?php echo $pending;  ?></td>
<!--                           <td><?php //echo isset($row['last2minsale'])?$row['last2minsale']:"0"; ?></td>
                           <td><?php //echo isset($row['totalrequest'])?$row['successreq']."/".$row['totalrequest']:"0/0"; ?></td>-->
                             <?php  if(getLoggedInUserKey('group_id')=="13"):  ?><td><?php echo $row['disputeamt']; ?></td><?php endif; ?>
                             <td class="tdcomments"><a class="highlightitle" title="<?php echo $row['messages']; ?>"  data-soid="<?php echo $row['supplier_operator_id']; ?>" data-oprid="<?php echo $key;  ?>" data-toggle='modal' href='#addnotesdiv'><i class="fa fa-comments-o fa-2"></i></a>&nbsp;(<span><?php echo $row['messagecount']; ?></span>)</td>
                       </tr>
           <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><?php echo number_format($totalAvgSale,2);  ?></th>
                        <th><?php echo number_format($totalLDS,2);  ?></th>
                        <th><?php echo number_format($totalMaxSaleCapacity,2);  ?></th>
                        <th><?php echo number_format($totalTargetsale,2);  ?></th>
                        <th><?php echo number_format($totalOpening,2);  ?></th>
                       <?php  if(getLoggedInUserKey('group_id')=="13"):  ?><th><?php echo number_format($totalBlockedbal,2);  ?></th><?php endif;  ?>
                        <th><?php echo number_format($totalCurrentStock,2);  ?></th>
                        <th><?php echo number_format($totalSale,2);  ?></th>
                        <th><?php echo $totalWorkingSims."/".$totalSims;  ?></th>
                        <th><?php echo number_format($totalOrder,2);  ?></th>
                        <th><?php echo number_format($totalPending,2);  ?></th>
                        <th></th>
                        <th></th>
                        <?php  if(getLoggedInUserKey('group_id')=="13"):  ?><th></th><?php endif;  ?>
                    </tr>
                </tfoot>
  </table>
    <?php endforeach; ?>  
<?php echo $this->load->view('common/notes');  ?>
      
