<?php $this->load->view('common/header'); ?>

<?php $this->load->view('common/menu'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
              </div>
        
        <?php if(in_array(getLoggedInUserKey('group_id'),array(13,14,10,12)) && !empty($todaysordersummary)): ?>
            <div class="dashboardsidebar">
                <div class="row">
                    <div class="col-lg-12">
                       
                        <div class="col-lg-3" style="float: right">
                            <button class="btn btn-default btn-info btn-sm" id="slidetodaysorder">View todays order</button>     
                            <?php echo $this->load->view('dashboard/todaysorders'); ?>
                        </div>
                    </div>
                </div>
            </div>
          <?php endif; ?>  
            
            <div class="row">
                <div class="col-lg-12">
                        <?php echo $this->load->view('dashboard/backenddashboardcontainer'); ?>
                </div>
            </div>
          
            
        </div>
    </div>
</div>

<?php $this->load->view('common/footer'); ?>