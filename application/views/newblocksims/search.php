<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js/blocksims.js'); ?>"></script>
<script>$(document).ready(function(){$('#sandbox-container .input-daterange').datepicker({format: "yyyy-mm-dd",startDate: "-365d",endDate: "1d",multidate: false,autoclose: true,todayHighlight: true,orientation: "top right"});});</script>
<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search Block Sims</h3>
           </div>
        </div>
    </div>
    
           <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="search" method="get">
                            
                            <div class="col-lg-4" id="sandbox-container">
                                    <label class="col-lg-4">Date range</label>
                                          <div class="input-daterange input-group" id="datepicker">
                                              <input type="text" class="form-control form-inp-med" name="from_date"  id="from_date" value="<?php echo $this->input->get('from_date')?$this->input->get('from_date'):"" ; ?>" />
                                              <span class="input-group-addon smallheight">to</span>
                                              <input type="text" class="form-control form-inp-med" name="to_date"  id="to_date"  value="<?php echo $this->input->get('to_date')?$this->input->get('to_date'):"" ; ?>"  />
                                            </div>
                            </div>
                            
                            <div class="col-lg-3">
                                    <label>Operator</label>
                                    <select   style="width: 150px" name="operator_id"  id="operator_id">
                                                       <option value="">Select</option>
                                                        <?php foreach($operators as $operator): ?>
                                                       <option value="<?php echo $operator->id;  ?>"  <?php if($this->input->get('operator_id')==$operator->id): echo "Selected"; endif; ?>><?php echo $operator->operator_name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                    <div class="col-lg-3"><label>Modems</label></div>
                                    <div class="col-lg-7">  
                                    <select  style="width: 150px"  name="modems[]" multiple="multiple">
                                                        <?php $vendorids=$this->input->get('modems_ids');  $vendorids=  strpos($vendorids,",")?explode(",",$vendorids):array($vendorids);
                                                         foreach($vendors as $vendor): ?>
                                                       <option value="<?php echo $vendor->id;  ?>"  <?php if(in_array($vendor->id,$vendorids)): echo "Selected"; endif; ?>><?php echo $vendor->vendor_name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                <div class="form-group">
                                     <div class="col-lg-3"><label>Status</label></div>
                                     <div class="col-lg-7">
                                     <select  style="width: 70px" name="status" id="status" >
                                                       <!--<option value="">Select</option>-->
                                                       <option value="all"  <?php if($this->input->get('status')=="all"): echo "Selected"; endif; ?>>All</option>
                                                       <option value="block"  <?php if($this->input->get('status')=="block"  ): echo "Selected"; endif; ?>>Block</option>
                                                       <option value="unblock"  <?php if($this->input->get('status')=="unblock"  ): echo "Selected"; endif; ?>>Unblock</option>
                                    </select>
                                     </div>
                                </div>
                                </div>
                            <div class="clearfix"></div>
                                <div class="col-lg-3 ">
                                    <div class="form-group">
                                        <div class="col-lg-5"><label>Handled by</label></div>
                                        <div class="col-lg-7">
                                    <select   name="handled_by"  id="handled_by">
                                                       <option value="">Select</option>
                                                        <?php  foreach($internals as $internal): ?>
                                                       <option value="<?php echo $internal->id;  ?>"  <?php if($this->input->get('handled_by')==$internal->id  ): echo "Selected"; endif; ?>><?php echo $internal->name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                        </div>
                                    </div>
                                </div>
                          
                          
                             <div class="col-lg-1">
                                 <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                          </div>
                    </div>
                </div>
            </div>

    <?php if(!empty($sims)): 
       $totalbalance=0;
    ?>
    
            <div class="row">
                <div class="col-lg-12">
                       <h3>History for date : ( <?php echo $this->input->get('from_date')." - ".$this->input->get('to_date'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Total Count :  <?php echo count($sims); ?></div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                       <th>Supplier</th>
                                       <th>Modem</th>
                                       <th>Operator</th>
                                       <th>Block Tag</th>
                                       <th>Balance</th>
                                       <th>Mobile</th>
                                       <th>Scid</th>
                                       <th>Block date</th>
                                       <th>Resolved date</th>
                                       <th>Comment</th>
                                       <th>Username</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php foreach($sims as $sim):  
                                       $totalbalance+=$sim['balance'];?>
                                   <tr>
                                       <td><?php echo $sim['suppliername']; ?></td>
                                       <td><?php echo $sim['vendor_name']; ?></td>
                                       <td><?php echo $sim['operator']; ?></td>
                                       <td><?php echo $sim['description']; ?></td>    
                                       <td><?php echo number_format($sim['balance'],2); ?></td>
                                       <td><?php echo $sim['mobile']; ?></td>
                                       <td><?php echo $sim['scid']; ?></td>
                                       <td><?php echo $sim['block_date']; ?></td>
                                       <td><?php echo $sim['resolved_date']; ?></td>
                                       <td class="tdcomments"><a class="highlightitle"  data-blockid="<?php echo $sim['id']; ?>" data-soid="<?php echo $sim['supplier_operator_id']; ?>" data-oprid="<?php echo $sim['opr_id'];  ?>" data-param="<?php echo $sim['id'];  ?>" data-toggle='modal' href='#addcomment'><i class="fa fa-comments-o fa-2"></i></a>&nbsp;(<span><?php echo $sim['messagecount']; ?></span>)</td>
                                       <td><?php echo $sim['username']; ?></td>
                                   </tr>
                                   <?php endforeach;  ?>
                                   <tr><th>Total</th><td></td><td></td><td></td><th><?php echo number_format($totalbalance,2); ?></th><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                               </tbody>
                           </table>
                       </div>
                    </div>
                </div>
                
                <div class="modal" id="addcomment">
                        <div class="modal-dialog" style="width: 300px;">
                        <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Add Comment</h4>
                          </div>
                          <div class="modal-body">
                              <div class="row">
                                  <div class="col-lg-12" id="loadcomments">

                                  </div>
                              </div>
                              
                              <input type="hidden" name="so_id" id="so_id" value="" />
                              <input type="hidden" name="opr_id" id="so_id" value="" />
                              <input type="hidden" name="block_id" id="block_id" value="" />
                              <textarea  name="comment" id="comment"></textarea>
                          </div>
                          <div class="modal-footer">
                              <div id="blockcommentmsg" style="text-align: left"></div>
                              <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Update</button>
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        </div>
                </div>
                
                 
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($sims) && $this->input->get()): echo "No block sims Detected ";  endif; ?>
</div>


<?php $this->load->view('common/footer');  ?>
