<div class="modal" id="addnotesdiv" style="z-index: 9999">
  <div class="modal-dialog" style="width: 500px; height: auto;">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Add Notes  <span class="text-left pull-right" >
          <button data-soid="0" data-type="0" data-oprid="0"  class="btn btn-default" id="btnrefreshcomments"><i  class="glyphicon glyphicon-refresh"></i></button></span></h4>
        
      </div>
      <div class="modal-body">
          <div id="notesthread" style="margin: 10px;">
              
          </div>
          <div>
              <input type="hidden" name="inpnotesoid" id="inpnotesoid"  autocomplete="off"  value="0" />
               <input type="hidden" name="inpnoteoprid" id="inpnoteoprid" value="0"  autocomplete="off" />
               <input type="hidden" name="inpnotetype" id="inpnotetype"  autocomplete="off"  value="0" />
              <input autocomplete="off"  type="hidden" name="addnotedate" id="addnotedate" class="form-control form-inp marginbt5" value="<?php echo date('Y-m-d'); ?>"/>
              <textarea  autocomplete="off" name="addnotemsg" id="addnotemsg" class="marginbt5 form-control" style="height: 50px; width: 350px;"></textarea>
              <button class="btn btn-sm btn-info btn-default" id="btnaddnote">Add</button>
          </div>
       </div>
      <div class="modal-footer">
          <div id="addnotemsg" style="text-align: left;float: left;width: 250px;"></div>
           <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>