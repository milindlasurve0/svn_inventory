<!DOCTYPE html>
<html class="">
    <head>
        
        <title>Pay1 Inventory</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
         <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap.min.css'); ?>">
         <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-theme.min.css'); ?>">
         
          <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-admin-theme.css'); ?>">
          <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-admin-theme-small.css'); ?>">
          
          <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-small.css'); ?>">
          
          <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/style.css'); ?>">
         
          <!-- Select Start -->
<!--          <link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/selectize.bootstrap3.css'); ?>">-->
          <!--  Select End -->
         
         <script type="text/javascript" src="<?php echo base_url('public/js/jquery-2.0.3.min.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('public/js/twitter-bootstrap-hover-dropdown.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-validator.js'); ?>"></script>
         
         <script type="text/javascript" src="<?php echo base_url('public/js/jquery.cookie.js'); ?>"></script>
         <script type="text/javascript" src="<?php echo base_url('public/js/towords.js'); ?>"></script>
          <link rel="stylesheet" media="screen" href="<?php echo base_url('public/fontawesome/css/font-awesome.min.css'); ?>">
         
         <script>var HOST="<?php echo base_url();  ?>";</script>
         <script> var currentDate = new Date();cookieexpirationDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()+1, 0, 0, 0);</script>
         <script>var GROUP_ID=<?php echo getLoggedInUserKey('group_id')?getLoggedInUserKey('group_id'):0; ?></script>
         <script src="<?php echo base_url('public/js/common.js'); ?>"></script>
       
         
    </head>
    <body class="bootstrap-admin-with-small-navbar">
        
    
