<nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar-sm" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="collapse navbar-collapse">
                            
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Link</a></li>
                            
                                <li>
                                    <a href="#">Reminders <i class="glyphicon glyphicon-bell"></i></a>
                                </li>
                                <li>
                                    <a href="#">Settings <i class="glyphicon glyphicon-cog"></i></a>
                                </li>
                                
                                <li class="dropdown">
                                    <a href="#" role="button" class="dropdown-toggle" data-hover="dropdown"> <i class="glyphicon glyphicon-user"></i> Hello <?php echo ucfirst(getLoggedInUsername());  ?> <i class="caret"></i></a>
                                    <ul class="dropdown-menu">
<!--                                 <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>-->
                                        <!--<li><a href="<?php echo base_url('users/changepwd'); ?>">Change Password</a></li>-->
                                        <!--<li role="presentation" class="divider"></li>-->
                                        <li><a href="<?php echo base_url('users/logout'); ?>">Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

<nav role="navigation" class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar bootstrap-admin-navbar-under-small">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="navbar-header">
                            <button data-target=".main-navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/dashboard" class="navbar-brand">Pay1 Inventory</a>
                        </div>
                        <div class="navbar-collapse main-navbar-collapse collapse" style="height: 1px;">
                            <ul class="nav navbar-nav">
                                
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">RM<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                          <li class="dropdown-header" role="presentation">Relationship Manager</li>
                                        <li><a href="<?php echo base_url('relationmgr/add'); ?>">Add RM</a></li>
                                        <li><a href="<?php echo base_url('relationmgr/view'); ?>">View All</a></li>
                                        <li><a href="<?php echo base_url('relationmgr/dashboard'); ?>">MyDashboard</a></li>
                                        <li><a href="<?php echo base_url('relationmgr/search'); ?>">Search</a></li>
                                    </ul>
                                </li>
                                
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">Suppliers<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-header" role="presentation">Suppliers</li>
                                        <li><a href="<?php echo base_url('suppliers/add'); ?>">Add Supplier</a></li>
                                        <li><a href="<?php echo base_url('suppliers/view'); ?>">View All Suppliers</a></li>
                                        <li class="divider" role="presentation"></li>
                                        <li class="dropdown-header" role="presentation">Search</li>
                                        <li><a href="<?php echo base_url('operators/viewmapping'); ?>">Search Supplier</a></li>
                                        <li><a href="<?php echo base_url('suppliers/search'); ?>">On-Off History</a></li>
                                        <li><a href="<?php echo base_url('suppliers/soHistory'); ?>">SO History</a></li>
                                    </ul>
                                </li>
                                
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">Orders<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                          <li class="dropdown-header" role="presentation">Planning</li>
<!--                                        <li><a href="<?php //echo base_url('operators/addmapping'); ?>">Map Operators</a></li>-->
                                        <li><a href="<?php echo base_url('plannings/configure'); ?>">Set Target</a></li>
                                         <li class="dropdown-header" role="presentation">Orders</li>
                                        <li><a href="<?php echo base_url('orders/configure'); ?>">Generate Order</a></li>
                                        <li><a href="<?php echo base_url('orderapprovals'); ?>">View Order</a></li>
                                        <li><a href="<?php echo base_url('orderapprovals/bysupppliers'); ?>">View Order Supplierwise</a></li>
                                        <li class="dropdown-header" role="presentation">History</li>
                                        <li><a href="<?php echo base_url('orderhistorys/get'); ?>">Search Order</a></li>
                                        <li><a href="<?php echo base_url('dashboard/viewtodaysorder'); ?>">Today's  Order</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">Accounts<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li class="dropdown-header" role="presentation">Orders</li>
                                        <li><a href="<?php echo base_url('accounts/download'); ?>">Download excel</a></li>
                                        <li><a href="<?php echo base_url('accounts/updateTxnId'); ?>">Update UTR</a></li>
                                         <li class="dropdown-header" role="presentation">Adjustments</li>
                                         <li><a href="<?php echo base_url('eliminateorders'); ?>">Cancel Order</a></li>
                                         <li><a href="<?php echo base_url('eliminateorders/searchbydate'); ?>">Search</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">Incomings<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                          <li class="dropdown-header" role="presentation">Search</li>
                                        <li><a href="<?php echo base_url('Incomings/search'); ?>">View incomings</a></li>
                                        <li><a href="<?php echo base_url('Incomings/modemwise'); ?>">View modemwise </a></li>
                                        <li class="dropdown-header" role="presentation">Add</li>
                                        <li><a href="<?php echo base_url('Incomings/addapincoming'); ?>">Api incomings </a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">Reports<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                          <li class="dropdown-header" role="presentation">Pending</li>
                                        <li><a href="<?php echo base_url('reports/pendingreport'); ?>">Pending Report</a></li>
                                        <li><a href="<?php echo base_url('earningreports/get?from_history_date='.date('Y-m-d',strtotime('-1 day')).'&to_history_date='.date('Y-m-d',strtotime('-1 day'))); ?>">Earning Report</a></li>
<!--                                            <li class="dropdown-header" role="presentation">Notifications</li>
                                           <li><a href="<?php //echo base_url('notifications/alerts'); ?>">Alerts</a></li>   -->
                                           <li><a href="<?php echo base_url('newblocksims/search'); ?>">Block sims Report</a></li>   
                                           <li><a href="<?php echo base_url('reconciles/report'); ?>">Reconciliation Report</a></li>   
                                    </ul>
                                </li>
                                
                                <li><a href="#">Link</a></li>
                                <li class="dropdown">
                                    <a data-hover="dropdown" class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-header" role="presentation">Dropdown header</li>
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider" role="presentation"></li>
                                        <li class="dropdown-header" role="presentation">Dropdown header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div>
            </div><!-- /.container -->
        </nav>