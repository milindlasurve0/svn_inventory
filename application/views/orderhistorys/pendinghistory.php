<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js/pendinghistory.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/shiftpendings.js'); ?>"></script>
<div class="container">
    
        <?php if(!empty($pendings)):  $last_key = end(array_keys($pendings)); ?>
            
            <div class="row">
                <div class="col-lg-12">
                       <h3>Pending history for Supplier : <?php echo $pendings[0]->suppliername; ?> ( <?php echo $pendings[0]->operatorname; ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Pendings</div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                       <th>Order Id</th>
                                        <th>Pending Date</th>
                                       <th>Supplier</th>
                                       <th>Operator</th>
                                       <th>Order Amt</th>
                                       <th>Amt by bounds</th>
                                       <th>Margin</th>
                                       <th>Incoming</th>
                                       <th>Pending</th>
                                       <th>P.Amt</th>
                                       <th>Log</th>
                                       <th>Refund</th>
                                       <th>Adjust</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php $is_upperbound=isUpperBoundH($soid);  ?>
                                   <?php foreach($pendings as $key=>$pending): ?>
                                   <tr>
                                       <td><?php echo $pending->order_id; ?></td>
                                        <td><?php echo $pending->pending_date; ?></td>
                                       <td><?php echo $pending->suppliername; ?></td>
                                       <td><?php echo $pending->operatorname; ?></td>
                                       <td><?php echo $pending->amount?$pending->amount:"NA"; ?></td>
                                       <td><?php echo $pending->amount_by_bounds?$pending->amount_by_bounds:"NA"; ?></td>
                                       <td><?php echo $pending->margin; ?></td>
                                       <td><a href="/orders/viewlastdayincoming/<?php echo $pending->supplier_operator_id; ?>/<?php echo urlencode($pending->vendorids); ?>/<?php echo $pending->operator_id ?>/<?php echo $pending->pending_date; ?>" target="_blank"><?php echo $pending->incoming; ?></a></td>
                                       <td><?php echo $pending->pending; ?></td>
                                       <td><?php  $pendingamt=  $is_upperbound?($pending->pending)/((100+$pending->margin)/100):($pending->pending)*((100-$pending->margin)/100);  echo round($pendingamt,2);?></td>
                                       <td><?php if($pending->is_adjusted): ?><button onClick="alert('<?php echo $pending->comment; ?>')">view</button><?php endif;  ?></td>
                                       <td>
                                           <?php   if($pendingamt!=0 && $pending->refund==0 && in_array(getLoggedInUserKey('group_id'),array('12','10'))): ?>
                                            <a data-toggle="modal" href="#refunddialog"  class="btn btn-default btn-info btn-xs" data-avaliableamt="<?php echo $pendingamt*-1; ?>" data-pendingid="<?php echo $pending->pendingid;  ?>">Refund</a>
                                           <?php endif; ?>
                                           <?php if($pending->refund!=0): ?>
                                           <button onClick="alert('<?php echo str_replace(array("\r", "\n"), '', $pending->refundmessage); ?>')">view</button>
                                           <?php endif; ?>
                                       </td>
                                       <td>
                                           <?php if($last_key==$key && $pendingamt < 0 && $pending->adjusted_pending==0 && in_array(getLoggedInUserId(), array(48347012,48636190))): ?>
                                           <!--<a id="modal" data-toggle="modal" href="#shiftpendingdialog"  class="btn btn-default btn-info btn-xs" data-pending_date="<?php echo $pending->pending_date; ?>" data-soid="<?php echo $pending->supplier_operator_id; ?>" data-supplier_id="<?php echo $pending->supplier_id; ?>" data-vendorid="<?php echo ($pending->vendorids); ?>" data-operatorname="<?php echo $pending->operatorname;?>" data-pendingamt="<?php echo $pending->pending;?>" data-availableamt="<?php echo $pendingamt; ?>" data-pid="<?php echo $pending->pendingid;  ?>">Shift Pending</a>-->
                                           <a id="modal" data-toggle="modal" href="#shiftpendingdialog"  class="btn btn-default btn-info btn-xs" data-pending_date="<?php echo $pending->pending_date; ?>" data-soid="<?php echo $pending->supplier_operator_id; ?>" data-supplier_id="<?php echo $pending->supplier_id; ?>" data-vendorid="<?php echo ($pending->vendorids); ?>"  >Shift Pending</a>
                                           <?php endif;?>
                                           <?php echo ($pending->adjusted_pending < 0)? number_format($pending->adjusted_pending,2)." Dr":(($pending->adjusted_pending > 0)? number_format($pending->adjusted_pending,2)." Cr":"");?>
                                        </td>
                                   </tr>
                                   <?php endforeach;  ?>
                               </tbody>
                           </table>
                           <?php if($this->uri->segment(4)!="all"): ?><a href="<?php echo  current_url(); ?>/all">Show all</a><?php endif;  ?>
                       </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($pendings) && $this->input->get()): echo "No Pendings Detected ";  endif; ?>
    
</div>    

<div class="modal" id="refunddialog">
    <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Refund</h4>
            </div>
            <div class="modal-body">
                <p>Amount avaliable to refund  : <span class="spnavalibleamt"></span></p>
               <div class="row">
                   
                   <div class="col-lg-6">
                   <label>Refund Amt : </label>
                   <input type="text" name="inprefundamt" id="inprefundamt" class="form-control form-inp-med" autocomplete="off"  style="width: 115px;" />
                    </div>
                   
                   <div class="col-lg-6">
                   <label>Select Type : </label>
                   <select name="refund_type" id="refund_type">
                       <option value="1">Bank</option>
                       <option value="2">Adjustments</option>
                   </select>
                   
                   </div>
                </div> 
                <input type="hidden" name="avaliableamt" id="avaliableamt" value="" autocomplete="off" />
                 <input type="hidden" name="inpendingid" id="inpendingid" value="" autocomplete="off" />
                <label>Comment : </label><br/>
                <textarea name="refundmessage" id="refundmessage" autocomplete="off"></textarea>
            </div>
            <div class="modal-footer">
                <div id="refundmsg" style="text-align: left;float: left"></div>
                <button class="btn btn-sm btn-default btn-primary" id="btnrefund">Refund</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="shiftpendingdialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Shift Pendings</h4>
            </div>
            <div class="modal-body">
                
                    <table class="table table-striped" id="tblGrid">
                                <input type="hidden" name="availableamt" id="availableamt" value="" autocomplete="off" />
                                <input type="hidden" name="parentpendingid" id="parentpendingid" value="" autocomplete="off" />                                
                                <input type="hidden" name="pending_date" id="pending_date" value="" autocomplete="off" />
                                <input type="hidden" name="parentsupplier_id" id="parentsupplier_id" value="" autocomplete="off" />
                                <input type="hidden" name="vendorid" id="vendorid" value="" autocomplete="off" />
                                <input type="hidden" name="pendingamt" id="pendingamt" value="" autocomplete="off" />
                                <input type="hidden" name="parentoperator_id" id="parentoperator_id" value="" autocomplete="off" />
                                <input type="hidden" name="parentsoid" id="parentsoid" value="" />
                                <input type="hidden" name="parentmargin" id="parentmargin" value="" />
                                <input type="hidden" name="pcommission_type" id="pcommission_type" value="" />
            
                                <thead id="tblHead">
                            <tr>
                              <th>Operator</th>
                              <th>Margin</th>
                              <th>Pending</th>
                              <th>Available Amt</th>
                            </tr>
                          </thead>
                          <tbody>
                        
                                    <tr>
                                       <td id="operatorname"></td>
                                       <td id="parentmargin"></td>
                                        <td id="pendingamt"></td>
                                       <td id="availableamt"></td>
                                    </tr>
                                   
                          </tbody>
                    </table>
                    <h5 id="hremainingpending">Remaining Pending : <span style="font-weight: bold;"></span></h5>
                    <h5 id="hremainingpendingamt">Remaining Pending Amt : <span style="font-weight: bold;"></span></h5>
                             
                    <table class="table table-striped" id="tblshiftpending">
                        <caption><b>Available Operators</b></caption>
                        <thead id="tblHead">
                            <tr>
                              <th>Operator</th>
                              <th>Margin</th>
                              <th>Pending</th>
                              <th>Pending Amt</th>                              
                              <th>Enter Amt</th>
                            </tr>
                          </thead>
                          <tbody>
                              

                          </tbody>
                    </table>
                                        
                 <div class="modal-footer">
                <div id="refundmsg" style="text-align: left;float: left"></div>
                <button class="btn btn-sm btn-default btn-primary" id="btnshiftpending">Submit</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
                 
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('common/footer'); ?>