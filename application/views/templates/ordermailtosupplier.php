<!DOCTYPE html>
<html class="" style="font-family: sans-serif; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-size: 10px; -webkit-tap-highlight-color: rgba(0,0,0,0);">
<head>
<title>Pay1</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.42857143; background: #fff; margin: 0;" bgcolor="#fff">
<style type="text/css">
a:active {
outline: 0;
}
a:hover {
outline: 0;
}
.carousel-inner>.item {
-webkit-transition: -webkit-transform .6s ease-in-out; -o-transition: -o-transform .6s ease-in-out; transition: transform .6s ease-in-out; -webkit-backface-visibility: hidden; backface-visibility: hidden; -webkit-perspective: 1000px; perspective: 1000px;
}
.carousel-inner>.item.active.right {
left: 0; -webkit-transform: translate3d(100%,0,0); transform: translate3d(100%,0,0);
}
.carousel-inner>.item.next {
left: 0; -webkit-transform: translate3d(100%,0,0); transform: translate3d(100%,0,0);
}
.carousel-inner>.item.active.left {
left: 0; -webkit-transform: translate3d(-100%,0,0); transform: translate3d(-100%,0,0);
}
.carousel-inner>.item.prev {
left: 0; -webkit-transform: translate3d(-100%,0,0); transform: translate3d(-100%,0,0);
}
.carousel-inner>.item.active {
left: 0; -webkit-transform: translate3d(0,0,0); transform: translate3d(0,0,0);
}
.carousel-inner>.item.next.left {
left: 0; -webkit-transform: translate3d(0,0,0); transform: translate3d(0,0,0);
}
.carousel-inner>.item.prev.right {
left: 0; -webkit-transform: translate3d(0,0,0); transform: translate3d(0,0,0);
}
@media print {
  :after {
    color: #000 !important; text-shadow: none !important; background: 0 0 !important; -webkit-box-shadow: none !important; box-shadow: none !important;
  }
  :before {
    color: #000 !important; text-shadow: none !important; background: 0 0 !important; -webkit-box-shadow: none !important; box-shadow: none !important;
  }
  a:visited {
    text-decoration: underline;
  }
  a[href]:after {
    content: " (" attr(href) ")";
  }
  abbr[title]:after {
    content: " (" attr(title) ")";
  }
  a[href^="javascript:"]:after {
    content: "";
  }
  a[href^="#"]:after {
    content: "";
  }
}
@media (min-width:768px) {
  .lead {
    font-size: 21px;
  }
  .dl-horizontal dt {
    float: left; width: 160px; overflow: hidden; clear: left; text-align: right; text-overflow: ellipsis; white-space: nowrap;
  }
  .dl-horizontal dd {
    margin-left: 180px;
  }
  .container {
    width: 750px;
  }
  .col-sm-1 {
    float: left;
  }
  .col-sm-10 {
    float: left;
  }
  .col-sm-11 {
    float: left;
  }
  .col-sm-12 {
    float: left;
  }
  .col-sm-2 {
    float: left;
  }
  .col-sm-3 {
    float: left;
  }
  .col-sm-4 {
    float: left;
  }
  .col-sm-5 {
    float: left;
  }
  .col-sm-6 {
    float: left;
  }
  .col-sm-7 {
    float: left;
  }
  .col-sm-8 {
    float: left;
  }
  .col-sm-9 {
    float: left;
  }
  .col-sm-12 {
    width: 100%;
  }
  .col-sm-11 {
    width: 91.66666667%;
  }
  .col-sm-10 {
    width: 83.33333333%;
  }
  .col-sm-9 {
    width: 75%;
  }
  .col-sm-8 {
    width: 66.66666667%;
  }
  .col-sm-7 {
    width: 58.33333333%;
  }
  .col-sm-6 {
    width: 50%;
  }
  .col-sm-5 {
    width: 41.66666667%;
  }
  .col-sm-4 {
    width: 33.33333333%;
  }
  .col-sm-3 {
    width: 25%;
  }
  .col-sm-2 {
    width: 16.66666667%;
  }
  .col-sm-1 {
    width: 8.33333333%;
  }
  .col-sm-pull-12 {
    right: 100%;
  }
  .col-sm-pull-11 {
    right: 91.66666667%;
  }
  .col-sm-pull-10 {
    right: 83.33333333%;
  }
  .col-sm-pull-9 {
    right: 75%;
  }
  .col-sm-pull-8 {
    right: 66.66666667%;
  }
  .col-sm-pull-7 {
    right: 58.33333333%;
  }
  .col-sm-pull-6 {
    right: 50%;
  }
  .col-sm-pull-5 {
    right: 41.66666667%;
  }
  .col-sm-pull-4 {
    right: 33.33333333%;
  }
  .col-sm-pull-3 {
    right: 25%;
  }
  .col-sm-pull-2 {
    right: 16.66666667%;
  }
  .col-sm-pull-1 {
    right: 8.33333333%;
  }
  .col-sm-pull-0 {
    right: auto;
  }
  .col-sm-push-12 {
    left: 100%;
  }
  .col-sm-push-11 {
    left: 91.66666667%;
  }
  .col-sm-push-10 {
    left: 83.33333333%;
  }
  .col-sm-push-9 {
    left: 75%;
  }
  .col-sm-push-8 {
    left: 66.66666667%;
  }
  .col-sm-push-7 {
    left: 58.33333333%;
  }
  .col-sm-push-6 {
    left: 50%;
  }
  .col-sm-push-5 {
    left: 41.66666667%;
  }
  .col-sm-push-4 {
    left: 33.33333333%;
  }
  .col-sm-push-3 {
    left: 25%;
  }
  .col-sm-push-2 {
    left: 16.66666667%;
  }
  .col-sm-push-1 {
    left: 8.33333333%;
  }
  .col-sm-push-0 {
    left: auto;
  }
  .col-sm-offset-12 {
    margin-left: 100%;
  }
  .col-sm-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-sm-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-sm-offset-9 {
    margin-left: 75%;
  }
  .col-sm-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-sm-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-sm-offset-6 {
    margin-left: 50%;
  }
  .col-sm-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-sm-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-sm-offset-3 {
    margin-left: 25%;
  }
  .col-sm-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-sm-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-sm-offset-0 {
    margin-left: 0;
  }
  .form-inline .form-group {
    display: inline-block; margin-bottom: 0; vertical-align: middle;
  }
  .form-inline .form-control {
    display: inline-block; width: auto; vertical-align: middle;
  }
  .form-inline .form-control-static {
    display: inline-block;
  }
  .form-inline .input-group {
    display: inline-table; vertical-align: middle;
  }
  .form-inline .input-group .form-control {
    width: auto;
  }
  .form-inline .input-group .input-group-addon {
    width: auto;
  }
  .form-inline .input-group .input-group-btn {
    width: auto;
  }
  .form-inline .input-group>.form-control {
    width: 100%;
  }
  .form-inline .control-label {
    margin-bottom: 0; vertical-align: middle;
  }
  .form-inline .checkbox {
    display: inline-block; margin-top: 0; margin-bottom: 0; vertical-align: middle;
  }
  .form-inline .radio {
    display: inline-block; margin-top: 0; margin-bottom: 0; vertical-align: middle;
  }
  .form-inline .checkbox label {
    padding-left: 0;
  }
  .form-inline .radio label {
    padding-left: 0;
  }
  .form-inline .checkbox input[type=checkbox] {
    position: relative; margin-left: 0;
  }
  .form-inline .radio input[type=radio] {
    position: relative; margin-left: 0;
  }
  .form-inline .has-feedback .form-control-feedback {
    top: 0;
  }
  .form-horizontal .control-label {
    padding-top: 7px; margin-bottom: 0; text-align: right;
  }
  .form-horizontal .form-group-lg .control-label {
    padding-top: 14.33px; font-size: 18px;
  }
  .form-horizontal .form-group-sm .control-label {
    padding-top: 6px; font-size: 12px;
  }
  .navbar-right .dropdown-menu {
    right: 0; left: auto;
  }
  .navbar-right .dropdown-menu-left {
    right: auto; left: 0;
  }
  .nav-tabs.nav-justified>li {
    display: table-cell; width: 1%;
  }
  .nav-tabs.nav-justified>li>a {
    margin-bottom: 0;
  }
  .nav-tabs.nav-justified>li>a {
    border-bottom: 1px solid #ddd; border-radius: 4px 4px 0 0;
  }
  .nav-tabs.nav-justified>.active>a {
    border-bottom-color: #fff;
  }
  .nav-tabs.nav-justified>.active>a:focus {
    border-bottom-color: #fff;
  }
  .nav-tabs.nav-justified>.active>a:hover {
    border-bottom-color: #fff;
  }
  .nav-justified>li {
    display: table-cell; width: 1%;
  }
  .nav-justified>li>a {
    margin-bottom: 0;
  }
  .nav-tabs-justified>li>a {
    border-bottom: 1px solid #ddd; border-radius: 4px 4px 0 0;
  }
  .nav-tabs-justified>.active>a {
    border-bottom-color: #fff;
  }
  .nav-tabs-justified>.active>a:focus {
    border-bottom-color: #fff;
  }
  .nav-tabs-justified>.active>a:hover {
    border-bottom-color: #fff;
  }
  .navbar {
    border-radius: 4px;
  }
  .navbar-header {
    float: left;
  }
  .navbar-collapse {
    width: auto; border-top: 0; -webkit-box-shadow: none; box-shadow: none;
  }
  .navbar-collapse.collapse {
    display: block !important; height: auto !important; padding-bottom: 0; overflow: visible !important;
  }
  .navbar-collapse.in {
    overflow-y: visible;
  }
  .navbar-fixed-bottom .navbar-collapse {
    padding-right: 0; padding-left: 0;
  }
  .navbar-fixed-top .navbar-collapse {
    padding-right: 0; padding-left: 0;
  }
  .navbar-static-top .navbar-collapse {
    padding-right: 0; padding-left: 0;
  }
  .container-fluid>.navbar-collapse {
    margin-right: 0; margin-left: 0;
  }
  .container-fluid>.navbar-header {
    margin-right: 0; margin-left: 0;
  }
  .container>.navbar-collapse {
    margin-right: 0; margin-left: 0;
  }
  .container>.navbar-header {
    margin-right: 0; margin-left: 0;
  }
  .navbar-static-top {
    border-radius: 0;
  }
  .navbar-fixed-bottom {
    border-radius: 0;
  }
  .navbar-fixed-top {
    border-radius: 0;
  }
  .navbar>.container .navbar-brand {
    margin-left: -15px;
  }
  .navbar>.container-fluid .navbar-brand {
    margin-left: -15px;
  }
  .navbar-toggle {
    display: none;
  }
  .navbar-nav {
    float: left; margin: 0;
  }
  .navbar-nav>li {
    float: left;
  }
  .navbar-nav>li>a {
    padding-top: 15px; padding-bottom: 15px;
  }
  .navbar-form .form-group {
    display: inline-block; margin-bottom: 0; vertical-align: middle;
  }
  .navbar-form .form-control {
    display: inline-block; width: auto; vertical-align: middle;
  }
  .navbar-form .form-control-static {
    display: inline-block;
  }
  .navbar-form .input-group {
    display: inline-table; vertical-align: middle;
  }
  .navbar-form .input-group .form-control {
    width: auto;
  }
  .navbar-form .input-group .input-group-addon {
    width: auto;
  }
  .navbar-form .input-group .input-group-btn {
    width: auto;
  }
  .navbar-form .input-group>.form-control {
    width: 100%;
  }
  .navbar-form .control-label {
    margin-bottom: 0; vertical-align: middle;
  }
  .navbar-form .checkbox {
    display: inline-block; margin-top: 0; margin-bottom: 0; vertical-align: middle;
  }
  .navbar-form .radio {
    display: inline-block; margin-top: 0; margin-bottom: 0; vertical-align: middle;
  }
  .navbar-form .checkbox label {
    padding-left: 0;
  }
  .navbar-form .radio label {
    padding-left: 0;
  }
  .navbar-form .checkbox input[type=checkbox] {
    position: relative; margin-left: 0;
  }
  .navbar-form .radio input[type=radio] {
    position: relative; margin-left: 0;
  }
  .navbar-form .has-feedback .form-control-feedback {
    top: 0;
  }
  .navbar-form {
    width: auto; padding-top: 0; padding-bottom: 0; margin-right: 0; margin-left: 0; border: 0; -webkit-box-shadow: none; box-shadow: none;
  }
  .navbar-text {
    float: left; margin-right: 15px; margin-left: 15px;
  }
  .navbar-left {
    float: left !important;
  }
  .navbar-right {
    float: right !important; margin-right: -15px;
  }
  .navbar-right~.navbar-right {
    margin-right: 0;
  }
  .modal-dialog {
    width: 600px; margin: 30px auto;
  }
  .modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5); box-shadow: 0 5px 15px rgba(0,0,0,.5);
  }
  .modal-sm {
    width: 300px;
  }
}
@media (min-width:992px) {
  .container {
    width: 970px;
  }
  .col-md-1 {
    float: left;
  }
  .col-md-10 {
    float: left;
  }
  .col-md-11 {
    float: left;
  }
  .col-md-12 {
    float: left;
  }
  .col-md-2 {
    float: left;
  }
  .col-md-3 {
    float: left;
  }
  .col-md-4 {
    float: left;
  }
  .col-md-5 {
    float: left;
  }
  .col-md-6 {
    float: left;
  }
  .col-md-7 {
    float: left;
  }
  .col-md-8 {
    float: left;
  }
  .col-md-9 {
    float: left;
  }
  .col-md-12 {
    width: 100%;
  }
  .col-md-11 {
    width: 91.66666667%;
  }
  .col-md-10 {
    width: 83.33333333%;
  }
  .col-md-9 {
    width: 75%;
  }
  .col-md-8 {
    width: 66.66666667%;
  }
  .col-md-7 {
    width: 58.33333333%;
  }
  .col-md-6 {
    width: 50%;
  }
  .col-md-5 {
    width: 41.66666667%;
  }
  .col-md-4 {
    width: 33.33333333%;
  }
  .col-md-3 {
    width: 25%;
  }
  .col-md-2 {
    width: 16.66666667%;
  }
  .col-md-1 {
    width: 8.33333333%;
  }
  .col-md-pull-12 {
    right: 100%;
  }
  .col-md-pull-11 {
    right: 91.66666667%;
  }
  .col-md-pull-10 {
    right: 83.33333333%;
  }
  .col-md-pull-9 {
    right: 75%;
  }
  .col-md-pull-8 {
    right: 66.66666667%;
  }
  .col-md-pull-7 {
    right: 58.33333333%;
  }
  .col-md-pull-6 {
    right: 50%;
  }
  .col-md-pull-5 {
    right: 41.66666667%;
  }
  .col-md-pull-4 {
    right: 33.33333333%;
  }
  .col-md-pull-3 {
    right: 25%;
  }
  .col-md-pull-2 {
    right: 16.66666667%;
  }
  .col-md-pull-1 {
    right: 8.33333333%;
  }
  .col-md-pull-0 {
    right: auto;
  }
  .col-md-push-12 {
    left: 100%;
  }
  .col-md-push-11 {
    left: 91.66666667%;
  }
  .col-md-push-10 {
    left: 83.33333333%;
  }
  .col-md-push-9 {
    left: 75%;
  }
  .col-md-push-8 {
    left: 66.66666667%;
  }
  .col-md-push-7 {
    left: 58.33333333%;
  }
  .col-md-push-6 {
    left: 50%;
  }
  .col-md-push-5 {
    left: 41.66666667%;
  }
  .col-md-push-4 {
    left: 33.33333333%;
  }
  .col-md-push-3 {
    left: 25%;
  }
  .col-md-push-2 {
    left: 16.66666667%;
  }
  .col-md-push-1 {
    left: 8.33333333%;
  }
  .col-md-push-0 {
    left: auto;
  }
  .col-md-offset-12 {
    margin-left: 100%;
  }
  .col-md-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-md-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-md-offset-9 {
    margin-left: 75%;
  }
  .col-md-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-md-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-md-offset-6 {
    margin-left: 50%;
  }
  .col-md-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-md-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-md-offset-3 {
    margin-left: 25%;
  }
  .col-md-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-md-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-md-offset-0 {
    margin-left: 0;
  }
  .modal-lg {
    width: 900px;
  }
}
@media (min-width:1200px) {
  .container {
    width: 1170px;
  }
  .col-lg-1 {
    float: left;
  }
  .col-lg-10 {
    float: left;
  }
  .col-lg-11 {
    float: left;
  }
  .col-lg-12 {
    float: left;
  }
  .col-lg-2 {
    float: left;
  }
  .col-lg-3 {
    float: left;
  }
  .col-lg-4 {
    float: left;
  }
  .col-lg-5 {
    float: left;
  }
  .col-lg-6 {
    float: left;
  }
  .col-lg-7 {
    float: left;
  }
  .col-lg-8 {
    float: left;
  }
  .col-lg-9 {
    float: left;
  }
  .col-lg-12 {
    width: 100%;
  }
  .col-lg-11 {
    width: 91.66666667%;
  }
  .col-lg-10 {
    width: 83.33333333%;
  }
  .col-lg-9 {
    width: 75%;
  }
  .col-lg-8 {
    width: 66.66666667%;
  }
  .col-lg-7 {
    width: 58.33333333%;
  }
  .col-lg-6 {
    width: 50%;
  }
  .col-lg-5 {
    width: 41.66666667%;
  }
  .col-lg-4 {
    width: 33.33333333%;
  }
  .col-lg-3 {
    width: 25%;
  }
  .col-lg-2 {
    width: 16.66666667%;
  }
  .col-lg-1 {
    width: 8.33333333%;
  }
  .col-lg-pull-12 {
    right: 100%;
  }
  .col-lg-pull-11 {
    right: 91.66666667%;
  }
  .col-lg-pull-10 {
    right: 83.33333333%;
  }
  .col-lg-pull-9 {
    right: 75%;
  }
  .col-lg-pull-8 {
    right: 66.66666667%;
  }
  .col-lg-pull-7 {
    right: 58.33333333%;
  }
  .col-lg-pull-6 {
    right: 50%;
  }
  .col-lg-pull-5 {
    right: 41.66666667%;
  }
  .col-lg-pull-4 {
    right: 33.33333333%;
  }
  .col-lg-pull-3 {
    right: 25%;
  }
  .col-lg-pull-2 {
    right: 16.66666667%;
  }
  .col-lg-pull-1 {
    right: 8.33333333%;
  }
  .col-lg-pull-0 {
    right: auto;
  }
  .col-lg-push-12 {
    left: 100%;
  }
  .col-lg-push-11 {
    left: 91.66666667%;
  }
  .col-lg-push-10 {
    left: 83.33333333%;
  }
  .col-lg-push-9 {
    left: 75%;
  }
  .col-lg-push-8 {
    left: 66.66666667%;
  }
  .col-lg-push-7 {
    left: 58.33333333%;
  }
  .col-lg-push-6 {
    left: 50%;
  }
  .col-lg-push-5 {
    left: 41.66666667%;
  }
  .col-lg-push-4 {
    left: 33.33333333%;
  }
  .col-lg-push-3 {
    left: 25%;
  }
  .col-lg-push-2 {
    left: 16.66666667%;
  }
  .col-lg-push-1 {
    left: 8.33333333%;
  }
  .col-lg-push-0 {
    left: auto;
  }
  .col-lg-offset-12 {
    margin-left: 100%;
  }
  .col-lg-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-lg-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-lg-offset-9 {
    margin-left: 75%;
  }
  .col-lg-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-lg-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-lg-offset-6 {
    margin-left: 50%;
  }
  .col-lg-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-lg-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-lg-offset-3 {
    margin-left: 25%;
  }
  .col-lg-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-lg-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-lg-offset-0 {
    margin-left: 0;
  }
  .visible-lg {
    display: block !important;
  }
  table.visible-lg {
    display: table !important;
  }
  tr.visible-lg {
    display: table-row !important;
  }
  td.visible-lg {
    display: table-cell !important;
  }
  th.visible-lg {
    display: table-cell !important;
  }
  .visible-lg-block {
    display: block !important;
  }
  .visible-lg-inline {
    display: inline !important;
  }
  .visible-lg-inline-block {
    display: inline-block !important;
  }
  .hidden-lg {
    display: none !important;
  }
}
@media screen and (max-width:767px) {
  .table-responsive {
    width: 100%; margin-bottom: 15px; overflow-y: hidden; -ms-overflow-style: -ms-autohiding-scrollbar; border: 1px solid #ddd;
  }
  .table-responsive>.table {
    margin-bottom: 0;
  }
  .table-responsive>.table>tbody>tr>td {
    white-space: nowrap;
  }
  .table-responsive>.table>tbody>tr>th {
    white-space: nowrap;
  }
  .table-responsive>.table>tfoot>tr>td {
    white-space: nowrap;
  }
  .table-responsive>.table>tfoot>tr>th {
    white-space: nowrap;
  }
  .table-responsive>.table>thead>tr>td {
    white-space: nowrap;
  }
  .table-responsive>.table>thead>tr>th {
    white-space: nowrap;
  }
  .table-responsive>.table-bordered {
    border: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>td:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>th:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>td:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>th:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>thead>tr>td:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>thead>tr>th:first-child {
    border-left: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>td:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tbody>tr>th:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>td:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr>th:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>thead>tr>td:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>thead>tr>th:last-child {
    border-right: 0;
  }
  .table-responsive>.table-bordered>tbody>tr:last-child>td {
    border-bottom: 0;
  }
  .table-responsive>.table-bordered>tbody>tr:last-child>th {
    border-bottom: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr:last-child>td {
    border-bottom: 0;
  }
  .table-responsive>.table-bordered>tfoot>tr:last-child>th {
    border-bottom: 0;
  }
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
  input[type=date].form-control {
    line-height: 34px;
  }
  input[type=time].form-control {
    line-height: 34px;
  }
  input[type=datetime-local].form-control {
    line-height: 34px;
  }
  input[type=month].form-control {
    line-height: 34px;
  }
  .input-group-sm input[type=date] {
    line-height: 30px;
  }
  .input-group-sm input[type=time] {
    line-height: 30px;
  }
  .input-group-sm input[type=datetime-local] {
    line-height: 30px;
  }
  .input-group-sm input[type=month] {
    line-height: 30px;
  }
  input[type=date].input-sm {
    line-height: 30px;
  }
  input[type=time].input-sm {
    line-height: 30px;
  }
  input[type=datetime-local].input-sm {
    line-height: 30px;
  }
  input[type=month].input-sm {
    line-height: 30px;
  }
  .input-group-lg input[type=date] {
    line-height: 46px;
  }
  .input-group-lg input[type=time] {
    line-height: 46px;
  }
  .input-group-lg input[type=datetime-local] {
    line-height: 46px;
  }
  .input-group-lg input[type=month] {
    line-height: 46px;
  }
  input[type=date].input-lg {
    line-height: 46px;
  }
  input[type=time].input-lg {
    line-height: 46px;
  }
  input[type=datetime-local].input-lg {
    line-height: 46px;
  }
  input[type=month].input-lg {
    line-height: 46px;
  }
}
@media (max-device-width:480px) and (orientation:landscape) {
  .navbar-fixed-bottom .navbar-collapse {
    max-height: 200px;
  }
  .navbar-fixed-top .navbar-collapse {
    max-height: 200px;
  }
}
@media (max-width:767px) {
  .navbar-nav .open .dropdown-menu {
    position: static; float: none; width: auto; margin-top: 0; background-color: transparent; border: 0; -webkit-box-shadow: none; box-shadow: none;
  }
  .navbar-nav .open .dropdown-menu .dropdown-header {
    padding: 5px 15px 5px 25px;
  }
  .navbar-nav .open .dropdown-menu>li>a {
    padding: 5px 15px 5px 25px;
  }
  .navbar-nav .open .dropdown-menu>li>a {
    line-height: 20px;
  }
  .navbar-nav .open .dropdown-menu>li>a:focus {
    background-image: none;
  }
  .navbar-nav .open .dropdown-menu>li>a:hover {
    background-image: none;
  }
  .navbar-form .form-group {
    margin-bottom: 5px;
  }
  .navbar-form .form-group:last-child {
    margin-bottom: 0;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>li>a {
    color: #777;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #333; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>li>a:hover {
    color: #333; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.active>a {
    color: #555; background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.active>a:focus {
    color: #555; background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.active>a:hover {
    color: #555; background-color: #e7e7e7;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a {
    color: #ccc; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a:focus {
    color: #ccc; background-color: transparent;
  }
  .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a:hover {
    color: #ccc; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.dropdown-header {
    border-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu .divider {
    background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
    color: #9d9d9d;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>li>a:focus {
    color: #fff; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>li>a:hover {
    color: #fff; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a {
    color: #fff; background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:focus {
    color: #fff; background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:hover {
    color: #fff; background-color: #080808;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a {
    color: #444; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a:focus {
    color: #444; background-color: transparent;
  }
  .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a:hover {
    color: #444; background-color: transparent;
  }
  .visible-xs {
    display: block !important;
  }
  table.visible-xs {
    display: table !important;
  }
  tr.visible-xs {
    display: table-row !important;
  }
  td.visible-xs {
    display: table-cell !important;
  }
  th.visible-xs {
    display: table-cell !important;
  }
  .visible-xs-block {
    display: block !important;
  }
  .visible-xs-inline {
    display: inline !important;
  }
  .visible-xs-inline-block {
    display: inline-block !important;
  }
  .hidden-xs {
    display: none !important;
  }
}
@media screen and (min-width:768px) {
  .jumbotron {
    padding-top: 48px; padding-bottom: 48px;
  }
  .container .jumbotron {
    padding-right: 60px; padding-left: 60px;
  }
  .container-fluid .jumbotron {
    padding-right: 60px; padding-left: 60px;
  }
  .jumbotron .h1 {
    font-size: 63px;
  }
  .jumbotron h1 {
    font-size: 63px;
  }
  .carousel-control .glyphicon-chevron-left {
    width: 30px; height: 30px; margin-top: -15px; font-size: 30px;
  }
  .carousel-control .glyphicon-chevron-right {
    width: 30px; height: 30px; margin-top: -15px; font-size: 30px;
  }
  .carousel-control .icon-next {
    width: 30px; height: 30px; margin-top: -15px; font-size: 30px;
  }
  .carousel-control .icon-prev {
    width: 30px; height: 30px; margin-top: -15px; font-size: 30px;
  }
  .carousel-control .glyphicon-chevron-left {
    margin-left: -15px;
  }
  .carousel-control .icon-prev {
    margin-left: -15px;
  }
  .carousel-control .glyphicon-chevron-right {
    margin-right: -15px;
  }
  .carousel-control .icon-next {
    margin-right: -15px;
  }
  .carousel-caption {
    right: 20%; left: 20%; padding-bottom: 30px;
  }
  .carousel-indicators {
    bottom: 20px;
  }
}
@media all and (transform-3d) (-webkit-transform-3d) {
  .carousel-inner>.item {
    -webkit-transition: -webkit-transform .6s ease-in-out; -o-transition: -o-transform .6s ease-in-out; transition: transform .6s ease-in-out; -webkit-backface-visibility: hidden; backface-visibility: hidden; -webkit-perspective: 1000px; perspective: 1000px;
  }
  .carousel-inner>.item.active.right {
    left: 0; -webkit-transform: translate3d(100%,0,0); transform: translate3d(100%,0,0);
  }
  .carousel-inner>.item.next {
    left: 0; -webkit-transform: translate3d(100%,0,0); transform: translate3d(100%,0,0);
  }
  .carousel-inner>.item.active.left {
    left: 0; -webkit-transform: translate3d(-100%,0,0); transform: translate3d(-100%,0,0);
  }
  .carousel-inner>.item.prev {
    left: 0; -webkit-transform: translate3d(-100%,0,0); transform: translate3d(-100%,0,0);
  }
  .carousel-inner>.item.active {
    left: 0; -webkit-transform: translate3d(0,0,0); transform: translate3d(0,0,0);
  }
  .carousel-inner>.item.next.left {
    left: 0; -webkit-transform: translate3d(0,0,0); transform: translate3d(0,0,0);
  }
  .carousel-inner>.item.prev.right {
    left: 0; -webkit-transform: translate3d(0,0,0); transform: translate3d(0,0,0);
  }
}
@media (min-width:768px) and (max-width:991px) {
  .visible-sm {
    display: block !important;
  }
  table.visible-sm {
    display: table !important;
  }
  tr.visible-sm {
    display: table-row !important;
  }
  td.visible-sm {
    display: table-cell !important;
  }
  th.visible-sm {
    display: table-cell !important;
  }
  .visible-sm-block {
    display: block !important;
  }
  .visible-sm-inline {
    display: inline !important;
  }
  .visible-sm-inline-block {
    display: inline-block !important;
  }
  .hidden-sm {
    display: none !important;
  }
}
@media (min-width:992px) and (max-width:1199px) {
  .visible-md {
    display: block !important;
  }
  table.visible-md {
    display: table !important;
  }
  tr.visible-md {
    display: table-row !important;
  }
  td.visible-md {
    display: table-cell !important;
  }
  th.visible-md {
    display: table-cell !important;
  }
  .visible-md-block {
    display: block !important;
  }
  .visible-md-inline {
    display: inline !important;
  }
  .visible-md-inline-block {
    display: inline-block !important;
  }
  .hidden-md {
    display: none !important;
  }
}
</style>
        <div class="container" style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;">
            <div class="row" style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: -15px; margin-left: -15px;">
                <div class="col-lg-12 text-left" style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px;" align="left">
                    <table class="table table-bordered table-condensed table-responsive" style="border-spacing: 0; border-collapse: collapse !important; color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width: 100%; max-width: 100%; margin-bottom: 20px; min-height: .01%; overflow-x: auto; background: transparent; border: 1px solid #ddd;" bgcolor="transparent">
<thead style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; display: table-header-group; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><tr style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; page-break-inside: avoid; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<th style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; line-height: 1.42857143; vertical-align: bottom; padding: 5px; border: 1px solid #ddd;" align="left" bgcolor="#fff !important" valign="bottom">Vendor Name</th>
                                <th style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; line-height: 1.42857143; vertical-align: bottom; padding: 5px; border: 1px solid #ddd;" align="left" bgcolor="#fff !important" valign="bottom">Operator</th>
                                <th style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; line-height: 1.42857143; vertical-align: bottom; padding: 5px; border: 1px solid #ddd;" align="left" bgcolor="#fff !important" valign="bottom">Order</th>
                                <th style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; line-height: 1.42857143; vertical-align: bottom; padding: 5px; border: 1px solid #ddd;" align="left" bgcolor="#fff !important" valign="bottom">Order Amount</th>
                                <th style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; line-height: 1.42857143; vertical-align: bottom; padding: 5px; border: 1px solid #ddd;" align="left" bgcolor="#fff !important" valign="bottom">UTR</th>
                                <th style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: left; line-height: 1.42857143; vertical-align: bottom; padding: 5px; border: 1px solid #ddd;" align="left" bgcolor="#fff !important" valign="bottom">A/C no</th>
                            </tr></thead>
<tbody style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><tr style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; page-break-inside: avoid; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<td style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; line-height: 1.42857143; vertical-align: top; padding: 5px; border: 1px solid #ddd;" bgcolor="#fff !important" valign="top">{name}</td>
                                <td style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; line-height: 1.42857143; vertical-align: top; padding: 5px; border: 1px solid #ddd;" bgcolor="#fff !important" valign="top">{operator}</td>
                                <td style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; line-height: 1.42857143; vertical-align: top; padding: 5px; border: 1px solid #ddd;" bgcolor="#fff !important" valign="top">{todaysorder}</td>
                                <td style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; line-height: 1.42857143; vertical-align: top; padding: 5px; border: 1px solid #ddd;" bgcolor="#fff !important" valign="top">{to_pay}</td>
                                <td style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; line-height: 1.42857143; vertical-align: top; padding: 5px; border: 1px solid #ddd;" bgcolor="#fff !important" valign="top">{txnid}</td>
                                <td style="color: #000 !important; text-shadow: none !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-color: #fff !important; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; line-height: 1.42857143; vertical-align: top; padding: 5px; border: 1px solid #ddd;" bgcolor="#fff !important" valign="top">{account_no}</td>
                            </tr></tbody>
</table>
</div>
            </div>
        </div>
    </body>
</html>
