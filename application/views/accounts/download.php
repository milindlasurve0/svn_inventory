<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script src="<?php echo base_url('public/js/accounts.js?'.time()); ?>"></script>
<div class="container">
    <div class="row">
    <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h3>Download Excel</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="download" method="get">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Select Batch</label>
                                    <?php $batchs=$this->config->item('batch'); ?>
                                    <select name="batch_type" id="batch_type"  style="width: 150px" >
                                        <option value="">Select</option>
                                        <option value="all"  <?php if($this->input->get('batch_type')=="all"):  echo "Selected";  endif;?>>All</option>
                                        <?php foreach($batchs as $b): ?>
                                        <option value="<?php echo $b; ?>" <?php if($this->input->get('batch_type')==$b):  echo "Selected";  endif;?>><?php echo $b; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label>Select Operator</label>
                                    <select name="operator_id" id="operator_id"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value="all"  <?php if($this->input->get('operator_id')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($operators as $operator): ?>
                                      <option value="<?php echo $operator->id;  ?>"  <?php if($this->input->get('operator_id')==$operator->id):  echo "Selected";  endif;?>><?php echo $operator->operator_name;  ?></option>
                                      <?php endforeach; ?>
                                  </select>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                    </div>
                    
                </div>



            </div>

        </div>
    
    <?php if(!empty($batches)):  ?>
    <div class="row">
            <div class="col-lg-12">
                 <div class="panel panel-default">
                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Orders</div></div>
                    <div class="panel-body">
                            <div class="col-lg-3 pull-left">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">From Bank</label>
                                        <div class="col-sm-8">
                                            <label class="radio-inline"> <input type="radio" name="frombank" id="frombank" value="icici"  autocomplete="off">Icici</label>
                                        <label class="radio-inline"> <input type="radio" name="frombank" id="frombank" value="axis" autocomplete="off" > Axis</label>
                                    </div>
                                   </div>
                            </div>
                        <div class="text-right bg-success accountsamountpayable fixme">
                            <h5><b>Amount payable : </b> <span class="orderamt" data-amt="0.00">0.00</span></h5>
                            <div style="background-color: white;"><label>Single : </label><input type="checkbox" name="is_single" id="is_single"  autocomplete="off"/>
                            <button class="btn btn-default btn-success btn-sm" id="downloadexcel">Download</button>
                            <button class="btn btn-default btn-danger btn-sm" id="disapproveexcel">Reject</button></div>
                        </div>
                             <?php foreach($batches as $key=>$batch):  ?>
                         <div class="batchclass batch<?php echo $key; ?>">
                             <h2><input autocomplete="off"  type="checkbox" name="checkallbatch" style="margin-left: 7px; margin-right: 10px;" value="<?php echo $key;  ?>" />Batch : <?php  echo $key; ?></h2>
                                        
                                        <?php foreach($batch as $frombank =>$bank): ?>
                                        <h3 class="text-center text-primary"><?php  echo strtoupper($banklist[$frombank]); ?></h3>
                                                 <?php foreach($bank  as $suppliername=>$supplier): $totalamount=0;$totaltopay=0;$i=0; ?>
                                                         <tr>
                                                             <td> <?php 
                                                                                $tobedone=array();
                                                                                foreach($supplier as $row):
                                                                                    if($row['is_downloaded']=='2'):
                                                                                            $tobedone[]=$row['order_id'];
                                                                                    endif;
                                                                                endforeach;
                                                                            ?>
                                                                         <h4>      
                                                                             <?php if(!empty($tobedone)):  $childarrstr=count($tobedone)>1?implode(",",$tobedone):$tobedone[0]; ?>
                                                                            <span><input autocomplete="off"  type='checkbox'  name='suppliername' value='<?php echo $childarrstr; ?>'  data-singlepayment="<?php echo $supplier[key($supplier)]['single_payment'];  ?>"   /></span> 
                                                                            <?php endif;  ?>
                                                                            <?php echo  ucwords(strtolower($suppliername)); ?>&nbsp; <?php if($supplier[key($supplier)]['single_payment']=="1"):  ?><i>(Single)<?php endif; ?></i>
                                                                         </h4>
                                                        </td>
                                                         </tr>
                                                        <table class="table bootstrap-admin-table-with-actions">
                                                                       <thead>
                                                                           <tr>
                                                                               <th>#</th>
                                                                               <th>Status</th>
                                                                                <th>Supplier</th>
                                                                                <th>Operator</th>
                                                                                <th>Bank/Ac</th>
                                                                                <th>Comment</th>
                                                                                <th>Pendings</th>    
                                                                                <th>Order</th>
                                                                                <th>To Pay</th>
                                                                           </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                     <?php foreach($supplier as $name=>$order): $totalamount+=$order['amount'];$totaltopay+=$order['to_pay'];$i++; ?>
                                                                                                        <tr class="info">
                                                                                                            <td id="order_<?php echo $order['order_id'];  ?>"><?php echo $order['order_id']; ?></td>
                                                                                                            <td>
                                                                                                                <?php if($order['is_downloaded']=='1' && $order['is_payment_done']!='1'): ?>
                                                                                                                 <span class="glyphicon glyphicon-ok"></span>
                                                                                                                <?php elseif($order['is_payment_done']=="1"): ?>
                                                                                                                   <span class="glyphicon glyphicon-ok text-success"></span>
                                                                                                                 <?php elseif($order['is_payment_done']=='0'): ?>  
                                                                                                                     <span class="glyphicon glyphicon-remove text-danger"></span>
                                                                                                                <?php endif;  ?>
                                                                                                            </td>
                                                                                                            <td><?php echo $order['supplier_name']; ?></td>
                                                                                                            <td><?php echo $order['operator_name']; ?></td>
                                                                                                            <td><?php echo $order['bank_name']; ?>/<?php echo $order['account_no']; ?></td>
                                                                                                             <td>
                                                                                                             <span><?php echo getLastComment($order['supplier_operator_id']);  ?></span>
                                                                                                             <a class="btn btn-info btn-xs" data-orderid="<?php echo$order['order_id'];  ?>" data-soid="<?php echo $order['supplier_operator_id']; ?>"  data-toggle="modal" href="#addcomment">View</a>
                                                                                                             </td>
                                                                                                              <td><?php echo $order['pending']; ?></td>
                                                                                                              <td><?php echo $order['amount']; ?></td>
                                                                                                              <td class="topay" data-tobeconsidered="<?php echo $order['is_downloaded']!='1'?"1":"0"; ?>" data-topay="<?php echo $order['to_pay']; ?>"><?php echo $order['to_pay']; ?></td>
                                                                                                        </tr>
                                                                                                  <?php endforeach; ?>
                                                                                                            <?php if($i>1): ?> <tr><th></th><th></th><th></th><th></th><th></th><th></th><th>Totals : </th><th><?php echo number_format($totalamount,2);;  ?></th><th><?php echo number_format($totaltopay,2);;  ?></th></tr> <?php endif; ?>
                                                                                                        </tbody>          
                                                                                            </table>
                                                                      <?php endforeach; ?>
                                                                         
                                        <?php endforeach; ?>
                            </div>            
                          <?php endforeach; ?>
                                        
                    </div>
                </div>
            </div>
    </div>
    
            <div class="modal" id="addcomment">
            <div class="modal-dialog" style="width: 300px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Comment</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12" id="loadcomments">

                            </div>
                        </div>
                         <input type="hidden" name="comment_so_id" id="comment_so_id" value="" />
                        <input type="hidden" name="comment_order_id" id="comment_order_id" value="" />
                        <textarea  name="comment" id="comment"></textarea>
                    </div>
                    <div class="modal-footer">
                        <div id="ordercommentmsg" style="text-align: left"></div>
                        <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Update</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
    <?php else: echo "No orders detected with following filters"; endif; ?>
    
<?php $this->load->view('common/footer');  ?>
