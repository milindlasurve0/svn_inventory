<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script src="<?php echo base_url('public/js/updatetxnid.js'); ?>"></script>
<div class="container">
    <div class="row">
    <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h3>Update UTR</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    
                    <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                    <?php endif;  ?> 
                    
                    <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                    <?php endif;  ?>    

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="updateTxnId" method="get">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Select Batch</label>
                                    <?php $batchs=$this->config->item('batch'); ?>
                                    <select name="batch_type" id="batch_type"  style="width: 150px" >
                                        <option value="">Select</option>
                                        <option value="all"  <?php if($this->input->get('batch_type')=="all"):  echo "Selected";  endif;?>>All</option>
                                        <?php foreach($batchs as $b): ?>
                                        <option value="<?php echo $b; ?>" <?php if($this->input->get('batch_type')==$b):  echo "Selected";  endif;?>><?php echo $b; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Select Operator</label>
                                    <select name="operator_id" id="operator_id"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value="all"  <?php if($this->input->get('operator_id')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($operators as $operator): ?>
                                      <option value="<?php echo $operator->id;  ?>"  <?php if($this->input->get('operator_id')==$operator->id):  echo "Selected";  endif;?>><?php echo $operator->operator_name;  ?></option>
                                      <?php endforeach; ?>
                                  </select>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                       
                    
                          <form action="uploadexcel" method="post" action="post" enctype="multipart/form-data" >
                        <div class="col-lg-5">
                            <div class="form-group">
                                <div class="col-lg-2"><label>Upload </label></div>
                                <div class="col-lg-6"><input type="file" name="utrfile" id="utrfile" /></div>
                                <div class="col-lg-2"><button type="submit" class="btn btn-default btn-primary btn-sm">Upload</button></div>
                            </div>
                        </div>
                        </form>
                        
                    </div>
                    
                </div>



            </div>

        </div>
    
    <?php if(!empty($batches)):  ?>
    <div class="row">
            <div class="col-lg-12">
                 <div class="panel panel-default">
                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Orders</div></div>
                    <div class="panel-body">
<!--                        <div class="text-right bg-success accountsamountpayable fixme">
                            <h5><b>Amount payable : </b> <span class="orderamt" data-amt="0.00">0.00</span></h5>
                            <button class="btn btn-default btn-success btn-sm" id="downloadexcel">Download</button>
                            <button class="btn btn-default btn-danger btn-sm" id="disapproveexcel">Reject</button>
                        </div>-->
                             <?php foreach($batches as $key=>$batch):  ?>
                         <div class="batch<?php echo $key; ?>">
                             <h2>Batch : <?php  echo $key; ?></h2>
                                        
                                        <?php foreach($batch as $frombank =>$bank): ?>
                                        <h3 class="text-center text-primary"><?php   echo strtoupper($banklist[$frombank]); ?></h3>
                                                        <table class="table bootstrap-admin-table-with-actions">
                                                                       <thead>
                                                                           <tr>
                                                                               <th>#</th>
                                                                                <th>Supplier</th>
                                                                                <th>Operator</th>
                                                                                <th>A/c name</th>
                                                                                <th>Bank</th>
                                                                                <th>Comment</th>
                                                                                <th>Pendings</th>    
                                                                                <th>Order</th>
                                                                                <th>To Pay</th>
                                                                                <th>Txn ID</th>
                                                                           </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                    <?php foreach($bank as $supplier): $totalamount=0;$totaltopay=0;$i=0; ?>
                                                                                                <?php foreach($supplier as $name=>$order): $totalamount+=$order['amount'];$totaltopay+=$order['to_pay'];$i++; ?>
                                                                                                          <tr class="info" id="tr_<?php echo $order['order_id'];  ?>">
                                                                                                            <td id="order_<?php echo $order['order_id'];  ?>">
                                                                                                                <?php if($order['is_downloaded']=="1" && $order['is_payment_done']!="1"): ?>
                                                                                                                <span class="glyphicon glyphicon-ok"></span>
                                                                                                                <?php elseif($order['is_payment_done']=="1"): ?>
                                                                                                                 <span class="glyphicon glyphicon-ok text-success"></span>
                                                                                                                <?php elseif($order['is_payment_done']=="0"): ?>
                                                                                                                 <span class="glyphicon glyphicon-remove text-danger"></span>
                                                                                                                <?php endif; ?>
                                                                                                            </td>
                                                                                                            <td><?php echo $order['supplier_name']; ?></td>
                                                                                                            <td><?php echo $order['operator_name']; ?></td>
                                                                                                            <td><?php echo $order['account_holder_name']; ?></td>
                                                                                                            <td><?php echo $order['bank_name']; ?>/<?php echo $order['account_no']; ?></td>
                                                                                                             <td>
                                                                                                             <span><?php echo getLastComment($order['supplier_operator_id']);  ?></span>
                                                                                                             <a class="btn btn-info btn-xs" data-orderid="<?php echo$order['order_id'];  ?>" data-soid="<?php echo $order['supplier_operator_id']; ?>"  data-toggle="modal" href="#addcomment">View</a>
                                                                                                             </td>
                                                                                                              <td><?php echo $order['pending']; ?></td>
                                                                                                              <td><?php echo $order['amount']; ?></td>
                                                                                                              <td class="topay"><?php echo $order['to_pay']; ?></td>
                                                                                                              <?php if($order['is_payment_done']=="1"): ?>
                                                                                                              <td><span><?php echo $order['txnid'];  ?></span>&nbsp;&nbsp;<!-- <button data-utr="<?php //echo $order['txnid'];  ?>" data-orderid="<?php //echo $order['order_id'];  ?>" class="btn btn-default btn-xs editutr"  data-toggle="modal" href="#editutrdiv">Edit</button>--></td>
                                                                                                              <?php else:  ?>
                                                                                                                      <?php if($order['is_downloaded']=="1"): ?>
                                                                                                                        <td class="edit_<?php echo $order['order_id'];  ?>">
                                                                                                                            <input type="text"  name="updatetxntext" placeholder="Transaction Id" />
                                                                                                                            <button class="btn btn-default btn-success btn-sm updatetxn" data-orderid="<?php echo $order['order_id'];  ?>">Update</button>
                                                                                                                            <button class="btn btn-default btn-danger btn-sm rejecttxn" data-orderid="<?php echo $order['order_id'];  ?>">Reject</button>
                                                                                                                        </td>
                                                                                                                        <?php elseif($order['is_payment_done']=="0"):  ?>
                                                                                                                        <td><b><?php echo "Rejected";  ?></b></td>
                                                                                                                        <?php endif; ?>
                                                                                                              <?php endif;  ?>
                                                                                                              
                                                                                                        </tr>
                                                                                                  <?php endforeach; ?>
                                                                                                            <?php if($i>1): ?> <tr><th></th><th></th><th></th><th></th><th></th><th>Totals : </th><th><?php echo number_format($totalamount,2);;  ?></th><th><?php echo number_format($totaltopay,2);;  ?></th></tr> <?php endif; ?>
                                                                                    <?php endforeach; ?>
                                                                          </tbody>
                                                        </table>
                                        <?php endforeach; ?>
                            </div>            
                          <?php endforeach; ?>
                                        
                    </div>
                </div>
            </div>
    </div>
    
            <div class="modal" id="addcomment">
            <div class="modal-dialog" style="width: 300px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Comment</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12" id="loadcomments">

                            </div>
                        </div>
                         <input type="hidden" name="comment_so_id" id="comment_so_id" value="" />
                        <input type="hidden" name="comment_order_id" id="comment_order_id" value="" />
                        <textarea  name="comment" id="comment"></textarea>
                    </div>
                    <div class="modal-footer">
                        <div id="ordercommentmsg" style="text-align: left"></div>
                        <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Update</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    
            <div class="modal" id="editutrdiv">
            <div class="modal-dialog" style="width: 300px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit UTR</h4>
                    </div>
                    <div class="modal-body">
                        <label>Old UTR : </label>  <span id="oldutr">45464</span>
                        <input type="hidden" name="utr_order_id" id="utr_order_id" value="" />
                        <input style="width: 80%;"  type="text" name="newutr" id="newutr" placeholder="Enter new utr" class="form-control"  />
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default btn-primary" id="updateutrbtn">Update</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php else: echo "No orders detected with following filters"; endif; ?>
    
<?php $this->load->view('common/footer');  ?>


