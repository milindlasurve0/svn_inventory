<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Add Supplier Operator Mapping</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Supplier</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('operators/addSupplierOperatorMapping'); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <input type="hidden" name="supplier_id" value="<?php echo $supplier->id;  ?>" />  
                                            
                                            <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-2 control-label">Supplier Name </label>
                                                <div class="col-lg-10">
                                                    <?php echo $supplier->name;  ?>
                                                 </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-2 control-label">Operators</label>
                                                <div class="col-lg-10">
                                                    <select name="operator_id" id="supplier_id"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                         <?php foreach($operators as $operator): ?>
                                                        <option value="<?php echo $operator->id;  ?>" <?php if($operator->id==set_value('operator_id')): echo "selected"; endif; ?>><?php echo $operator->operator_name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div class="form-group">
                                                <label for="capacity" class="col-lg-2 control-label">Total Capacity</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('capacity'); ?>"   id="capacity" name="capacity" class="form-control form-inp-nol" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="capacity_per_day" class="col-lg-2 control-label">Capacity per day</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('capacity_per_day'); ?>"   id="capacity_per_day" name="capacity_per_day" class="form-control form-inp-nol" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="capacity_per_month" class="col-lg-2 control-label">Capacity per month</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('capacity_per_month'); ?>"   id="capacity_per_month" name="capacity_per_month" class="form-control form-inp-nol" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="frequency" class="col-lg-2 control-label">Frequency</label>
                                                <div class="col-lg-10">
                                                    <select name="frequency" id="frequency"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                        <?php foreach($this->config->item('frequency') as $key=>$frequency):  ?>
                                                        <option value="<?php echo $key;  ?>" <?php if($key==set_value('frequency')): echo "selected"; endif; ?>><?php echo $frequency;  ?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="commission_type" class="col-lg-2 control-label">Commission Type</label>
                                                <div class="col-lg-10">
                                                    <select name="commission_type" id="commission_type"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                       <?php foreach($this->config->item('commission_type') as $key=>$commission_type):  ?>
                                                        <option value="<?php echo $key;  ?>" <?php if($key==set_value('commission_type')): echo "selected"; endif; ?>><?php echo $commission_type;  ?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="commission_type_formula" class="col-lg-2 control-label">Commission Type formula</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('commission_type_formula'); ?>"   id="commission_type_formula" name="commission_type_formula" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="riskfactor" class="col-lg-2 control-label">Risk Factor</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('riskfactor'); ?>"   id="riskfactor" name="riskfactor" class="form-control form-inp-no" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                            <a href="<?php echo base_url('suppliers/get/'.$supplier->id) ?>">Back to Supplier</a>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>