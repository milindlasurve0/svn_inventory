<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script type="text/javascript" src="<?php echo base_url('public/js/so.js'); ?>"></script>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Add Supplier Operator Mapping</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Edit Supplier Operator Mapping : #<?php  ?></div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('operators/editmapping/'.$supplier_operator->id); ?>" id="editsomapping">
                                        <fieldset>
                                            <legend></legend>
                                            
                                             <div class="col-lg-6">
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-4 control-label  label13px">Supplier</label>
                                                <div class="col-lg-8">
                                                    <select name="supplier_id" id="supplier_id"  style="width: 150px" required  disabled="true">
                                                        <option value="">Select</option>
                                                         <?php foreach($suppliers as $supplier): ?>
                                                        <option value="<?php echo $supplier->id;  ?>" <?php if($supplier->id==(set_value('supplier_id')?set_value('supplier_id'):$supplier_operator->supplier_id)): echo "selected='true'"; endif; ?>><?php echo $supplier->name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-4 control-label  label13px">Operators</label>
                                                <div class="col-lg-8">
                                                    <select name="operator_id" id="supplier_id"  style="width: 150px" required disabled="true">
                                                        <option value="">Select</option>
                                                         <?php foreach($operators as $operator): ?>
                                                        <option value="<?php echo $operator->id;  ?>"  <?php if($operator->id==(set_value('operator_id')?set_value('operator_id'):$supplier_operator->operator_id)): echo "selected='true'"; endif; ?>><?php echo $operator->operator_name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label for="capacity" class="col-lg-4 control-label  label13px">Max Order Capacity</label>
                                                <div class="col-lg-8">
                                                    <input type="text"value="<?php echo set_value('capacity')?set_value('capacity'):$supplier_operator->capacity; ?>"   id="capacity" name="capacity" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="min_capacity" class="col-lg-4 control-label  label13px">Min Order Amt</label>
                                                <div class="col-lg-8">
                                                    <input type="text"value="<?php echo set_value('min_capacity')?set_value('min_capacity'):$supplier_operator->min_capacity; ?>"   id="min_capacity" name="min_capacity" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="capacity_per_day" class="col-lg-4 control-label  label13px">Vendor total max Capacity per day</label>
                                                <div class="col-lg-8">
                                                    <input type="text"value="<?php echo set_value('capacity_per_day')?set_value('capacity_per_day'):$supplier_operator->capacity_per_day; ?>"   id="capacity_per_day" name="capacity_per_day" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="capacity_per_month" class="col-lg-4 control-label  label13px">Vendor Capacity per month</label>
                                                <div class="col-lg-8">
                                                    <input type="text"value="<?php echo set_value('capacity_per_month')?set_value('capacity_per_month'):$supplier_operator->capacity_per_month; ?>"   id="capacity_per_month" name="capacity_per_month" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="frequency" class="col-lg-4 control-label  label13px">Frequency</label>
                                                <div class="col-lg-8">
                                                    <select name="frequency" id="frequency"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                        <?php foreach($this->config->item('frequency') as $key=>$frequency):  ?>
                                                        <option value="<?php echo $key;  ?>"   <?php if($key==(set_value('frequency')?set_value('frequency'):$supplier_operator->frequency)): echo "selected='true'"; endif; ?>><?php echo $frequency;  ?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                                 
                                            <div class="form-group">
                                                      <label for="sim_bal_range" class="col-lg-4 control-label  label13px">Sim Bal Range</label>
                                                        <div class="col-lg-8">
                                                            <input type="text"  value="<?php echo set_value('sim_bal_range')?set_value('sim_bal_range'):$supplier_operator->sim_bal_range; ?>"   id="sim_bal_range" name="sim_bal_range" class="form-control form-inp" />
                                                            <span class="help-block">eg (100-200)</span>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                            </div>
                                            </div>
                                            <div class="col-lg-6">
                                            
                                                            <div class="form-group">
                                                                <label for="commission_type" class="col-lg-4 control-label  label13px">Commission Type</label>
                                                                <div class="col-lg-8">
                                                                    <select name="commission_type" id="commission_type"  style="width: 150px" required>
                                                                        <option value="">Select</option>
                                                                         <?php foreach($this->config->item('commission_type') as $key=>$commission_type):  ?>
                                                                        <option value="<?php echo $key;  ?>"   <?php if($key==(set_value('commission_type')?set_value('commission_type'):$supplier_operator->commission_type)): echo "selected='true'"; endif; ?>><?php echo $commission_type;  ?></option>
                                                                         <?php endforeach;?>
                                                                    </select>
                                                                    <span class="help-block with-errors"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="commission_type_formula" class="col-lg-4 control-label  label13px">Margin</label>
                                                                <div class="col-lg-8">
<!--                                                                    <input type="text" value="<?php //echo set_value('commission_type_formula')?set_value('commission_type_formula'):(!empty($supplier_operator->commission_type_expression)?$supplier_operator->commission_type_expression:$supplier_operator->commission_type_formula); ?>"  autocomplete="off"  id="commission_type_formula" name="commission_type_formula" class="form-control form-inp-no" data-error="This field is required" required>-->
                                                                    <input type="text" value="<?php echo $supplier_operator->commission_type_formula; ?>"  autocomplete="off"  id="commission_type_formula" name="commission_type_formula" class="form-control form-inp-no" data-error="This field is required" required>
                                                                    <span class="help-block with-errors"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="riskfactor" class="col-lg-4 control-label  label13px">Pending Riskfactor (%)</label>
                                                                <div class="col-lg-8">
                                                                    <input type="text"  value="<?php echo set_value('riskfactor')?set_value('riskfactor'):$supplier_operator->riskfactor; ?>"   id="riskfactor" name="riskfactor" class="form-control form-inp-no" data-error="This field is required" required>
                                                                    <span class="help-block with-errors"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="basesale" class="col-lg-4 control-label  label13px">Base Sale</label>
                                                                <div class="col-lg-8">
                                                                    <input type="text"  value="<?php echo $supplier_operator->basesale; ?>"   id="basesale" name="basesale" class="form-control form-inp flatclass" data-error="This field is required" required>
                                                                    <span class="help-block with-errors"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="batch" class="col-lg-4 control-label  label13px">Batch</label>
                                                                <div class="col-lg-8">
                                                                    <select name="batch" id="batch"><option value="">select</option>
                                                                    <?php for($i=1;$i<=8;$i++): ?>
                                                                        <option value="<?php echo $i; ?>"  <?php if($i==(set_value('batch')?set_value('batch'):$supplier_operator->batch)): echo "selected='true'"; endif; ?>><?php echo $i; ?></option>
                                                                    <?php endfor; ?>
                                                                     </select>
                                                                    <span class="help-block with-errors"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="disputeamt" class="col-lg-4 control-label  label13px">Dispute Amt</label>
                                                                <div class="col-lg-8">
                                                                    <input type="text"  value="<?php echo set_value('disputeamt')?set_value('disputeamt'):$supplier_operator->disputeamt; ?>"   id="disputeamt" name="disputeamt" class="form-control form-inp" data-error="This field is required" required>
                                                                    <span class="help-block with-errors"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="is_api" class="col-lg-4 control-label  label13px">isApi</label>
                                                                <div class="col-lg-8">
                                                                    <input type="checkbox" name="is_api" value="1"  id="is_api" <?php echo $supplier_operator->is_api?"checked":""; ?> />
                                                                </div>
                                                            </div>
                                                
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            
                                            <div class="row">
                                                <div class="col-md-4 pull-right">
                                                    <div class="form-group">
                                                    <label for="" class="col-lg-2 control-label  label13px"></label>
                                                    <button class="btn btn-primary btn-sm" type="button" id="btneditsomapping">Save changes</button>
                                                        <a href="<?php echo base_url('suppliers/get/'.$supplier_operator->supplier_id); ?>" class="btn btn-info btn-sm" role="button">Back to Supplier</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
            
   <div id="socommentmdl" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Margin/Base change detected</h4>
            </div>
            <div class="modal-body">
                <div class="row divmarginchange" style="display: none">
                    <div class="col-lg-3"><label class="label13px">Reason for margin change</label></div>
                    <div class="col-lg-6"><textarea id="txtmarginchangecomment"  name="txtmarginchangecomment"  style="height: 50px; width: 245px;"></textarea></div>
                </div>
                <div class="clearfix"></div>
                <div class="row divbasechange" style="display: none">
                    <div class="col-lg-3"><label class="label13px">Reason for base change</label></div>
                    <div class="col-lg-6"><textarea id="txtbasechangecomment" name="txtbasechangecomment" style="height: 50px; width: 245px;"></textarea></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnsobasecomment">Save changes</button>
            </div>
        </div>
    </div>
</div>
            
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>