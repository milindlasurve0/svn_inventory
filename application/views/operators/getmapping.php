<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

<div class="container">
    
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>View  Supplier vs Operator : # <?php echo $supplier->id; ?></h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                               
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>  
                                
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Supplier Info</div>
                                </div>
                                <div class="panel-body" style="height:60%">
                                    
                                    <div class="col-md-4">
                                    <ul style="list-style-type: none;line-height: 20px;">
                                        
                                        <li>
                                            <b>Name</b>
                                            <div><?php echo $supplier->name; ?></div>
                                        </li>
                                        <li>
                                            <b>Email</b>
                                            <div><?php echo $supplier->email_id; ?></div>
                                        </li>
                                        <li>
                                            <b>Contact</b>
                                            <div><?php echo $supplier->contact; ?></div>
                                        </li>
                                        <li>
                                            <b>Address</b>
                                            <div><?php echo $supplier->address; ?></div>
                                        </li>
                                    </ul>
                                    </div>
                                    <div class="col-md-4">
                                         <ul style="list-style-type: none;line-height: 20px;">
                                        <li>
                                            <b>Account No</b>
                                            <div><?php echo $supplier->account_no; ?></div>
                                        </li>
                                        <li>
                                            <b>Bank Name</b>
                                            <div><?php echo $supplier->bank_name; ?></div>
                                        </li>
                                        <li>
                                            <b>Risk Factor</b>
                                            <div><?php echo $supplier->riskfactor; ?> / 5.00 </div>
                                        </li>
                                        <li>
                                            <b>Location</b>
                                            <div><?php echo $supplier->location; ?></div>
                                        </li>
                                         </ul>
                                    </div>
                                        <div class="col-md-4"> 
                                        <ul style="list-style-type: none;line-height: 20px;">
                                         <li>
                                            <b>Rating</b>
                                            <div><?php echo $supplier->rating; ?></div>
                                        </li>
                                        <li>
                                            <b>Last Created</b>
                                            <div><?php echo $supplier->created_at; ?></div>
                                        </li>
                                        <li>
                                            <b>Last Updated</b>
                                            <div><?php echo $supplier->updated_at; ?></div>
                                        </li>
                                        </ul>
                                        </div>  
                                  
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <p><a href="<?php echo base_url('operators/addSupplierOperatorMapping/'.$supplier->id); ?>">Add more operators</a></p>
                </div>
            </div>
            
             <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                               
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Operators</div>
                                </div>
                                <div class="panel-body" >
                                   
                                   <table class="table bootstrap-admin-table-with-actions">
                                        <thead>
                                            <tr>
                                          
                                                <th>Operator Name</th>
                                                <th>Total Capacity</th>
                                                <th>Capacity/day</th>
                                                <th>Capacity/month</th>
                                                <th>Frequency</th>
                                                <th>Risk factor</th>
                                                <th>Commission Type</th>
                                                <th>Commission Type Formula</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($supplier_operators)):  ?>
                                            <?php foreach ($supplier_operators as $supplier_operator):  ?>
                                            <tr>
                                                <td><?php echo $supplier_operator->operator_name;  ?></td>
                                                <td><?php echo $supplier_operator->capacity;  ?></td>
                                                <td><?php echo $supplier_operator->capacity_per_day;  ?></td>
                                                <td><?php echo $supplier_operator->capacity_per_month;  ?></td>
                                                <td>
                                                    <?php 
                                                    $frequencys=$this->config->item('frequency');
                                                    echo $frequencys[$supplier_operator->frequency]; 
                                                    ?>
                                                </td>
                                                <td><?php echo $supplier_operator->riskfactor;  ?></td>
                                                <td>
                                                     <?php 
                                                    $commission_types=$this->config->item('commission_type');
                                                    echo $commission_types[$supplier_operator->commission_type]; 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo ($supplier_operator->commission_type==1)?$supplier_operator->commission_type_formula."% ":$supplier_operator->commission_type_formula;
                                                    ?>
                                                </td>
                                                <td class="actions">
                                                    
                                                    <a href="<?php echo base_url('operators/editmapping/'.$supplier_operator->id); ?>">
                                                        <button class="btn btn-xs btn-primary">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                            Edit
                                                        </button>
                                                    </a>
                                                     <a href="<?php echo base_url('operators/deletemapping/'.$supplier_operator->id); ?>" onClick="javascript:return confirm('Are you sure to Delete ?');">
                                                        <button class="btn btn-xs btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>
                                                    
                                                </td>
                                                
                                               
                                            </tr>
                                           <?php endforeach; ?> 
                                            
                                            <?php else: echo  "No records Found"; endif;?>
                                            
                                        </tbody>
                                    </table>
                                  
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
      
            
        </div>
    </div>
        
</div>
<?php $this->load->view('common/footer');  ?>