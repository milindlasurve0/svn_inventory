<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

 <script src="<?php echo base_url('public/js/typeahead.bundle.js'); ?>"></script>
 <script src="<?php echo base_url('public/js/supplier.js'); ?>"></script>
 
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Search Supplier</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Search</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
<!--                                    <form id="searchsupplier" class="form-horizontal" data-toggle="validator" method="get" action="<?php echo base_url('suppliers/get/'); ?>">-->
                                        <fieldset>
                                            <legend></legend>
                                            
                                            
                                             <div class="form-group">
                                                <label for="supplier_id" class="col-lg-2 control-label">Select Supplier</label>
                                                <div class="col-lg-5">
<!--                                                    <select name="supplier_id" id="supplier_id"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                         <?php /* foreach($suppliers as $supplier): ?>
                                                        <option value="<?php echo $supplier->id;  ?>" <?php if($supplier->id==set_value('supplier_id')): echo "selected"; endif; ?>><?php echo $supplier->name;  ?></option>
                                                        <?php endforeach; */?>
                                                    </select>-->
                                                 <input class="form-control typeahead" name="query" id="query" placeholder="Start typing something to search..." type="text" autocomplete="off" spellcheck="false" />              
<!--                                                    <span class="help-block with-errors"></span>-->
                                                </div>
                                            </div>
                                            
                                             
                                            
                                            
                                            
                                            <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="button" id="searchsupplier">View</button>
                                            </div>
                                            
                                        </fieldset>
<!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>