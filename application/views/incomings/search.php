<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/incomings.js'); ?>"></script>

<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search Incomings</h3>
           </div>
        </div>
    </div>
    
           <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="search" method="get">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-4"><label>Date</label></div>
                                    <div class="col-lg-7" id="sandbox-container">
                                  <input type="text" name="incomingdate" id="incomingdate" class="form-control input-md input-sm"  value="<?php echo $this->input->get('incomingdate'); ?>"  required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Operator</label></div>
                                   <div class="col-lg-7"> <select name="operator_id" id="operator_id"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value="all"  <?php if($this->input->get('operator_id')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($operators as $operator): ?>
                                      <option value="<?php echo $operator['id'];  ?>"  <?php if($this->input->get('operator_id')==$operator['id']):  echo "Selected";  endif;?>><?php echo $operator['name'];  ?></option>
                                      <?php endforeach; ?>
                                       </select></div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="col-lg-4"><label for="supplier_id" class="col-lg-5 control-label">Supplier</label></div>
                                     <div class="col-lg-6">
                                           <select name="supplier_id" id="supplier_id"  style="width: 150px" >
                                              <option value="">Select</option>
                                               <?php foreach($suppliers as $supplier): ?>
                                              <option value="<?php echo $supplier->id;  ?>"  <?php  if($this->input->get('supplier_id')==$supplier->id): echo "selected";  endif; ?>><?php echo $supplier->name;  ?></option>
                                              <?php endforeach; ?>
                                          </select>
                            </div>
                            </div>
                             <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Handled</label></div>
                                   <div class="col-lg-7"> <select name="handled_by" id="handled_by"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value=""  <?php if($this->input->get('handled_by')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($internals as $internal): ?>
                                      <option value="<?php echo $internal->id;  ?>"  <?php if($this->input->get('handled_by')==$internal->id):  echo "Selected";  endif;?>><?php echo $internal->name;  ?></option>
                                      <?php endforeach; ?>
                                       </select></div>
                                </div>
                            </div>
                             <div class="col-lg-3" style="margin-top: 25px;">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Modems</label></div>
                                   <div class="col-lg-7">  
                                       <select name="vendors[]" multiple="multiple" style="height: 111px; width: 128px;">
                                                       <?php foreach($vendors as $vendor): ?>
                                                        <option value="<?php echo $vendor->id;  ?>" <?php if($this->input->get('vendors')): if(in_array($vendor->id,$this->input->get('vendors'))):  echo "selected"; endif; endif; ?>  ><?php echo $vendor->vendor_name;  ?></option>
                                                        <?php endforeach; ?> 
                                      </select>
                                   </div>
                                </div>
                            </div>
                            <div class="col-lg-1"  style="float: right;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
 </div>
    
    <?php if(!empty($incomings)):  ?>
    
            <div class="row">
                <div class="col-lg-12">
                       <h3>Incomings for date : ( <?php echo $this->input->get('incomingdate'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Orders</div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                  
                                       <th>Incoming date</th>
                                       <th>Supplier</th>
                                       <th>Operator</th>
                                       <th>Modem diff</th>
                                       <th>Server diff</th>
                                       <th>Incoming</th>
                                       <th>Sims</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php $totalIncoming= 0;?>
                                   <?php foreach($incomings as $incoming): $totalIncoming+=($incoming->incoming?$incoming->incoming:0); ?>
                                   <tr>
                                       <td><?php echo $incoming->sync_date; ?></td>
                                       <td><?php echo $incoming->suppliername; ?></td>
                                       <td><?php echo $incoming->operatorname; ?></td>
                                       <?php 
                                       $vendorsids=$this->input->get('vendors');
                                       if(!empty($vendorsids)):
                                            if(count($vendorsids)>1):
                                              $vendorsids=urlencode(implode(',',$vendorsids));
                                            else:
                                                $vendorsids=$vendorsids[0];
                                            endif;
                                         else:
                                             $vendorsids=  urlencode($incoming->vendorids);
                                        endif;
                                       ?>
                                       <td>
                                           <?php 
                                           $modemdiff=0;
                                           if(strtotime($this->input->get('incomingdate'))==strtotime('today')):
                                           $modemdiff=-($incoming->opening-$incoming->currentbalance+$incoming->incoming-$incoming->sale+$incoming->inc);    
                                           else:
                                            $modemdiff=-($incoming->opening-$incoming->closing+$incoming->incoming-$incoming->sale+$incoming->inc);       
                                           endif; 
                                           echo number_format($modemdiff,2);
                                           ?>
                                       </td>
                                       <td><?php echo number_format($incoming->server_diff,2);  ?></td>
                                       <td><a href="/orders/viewlastdayincoming/<?php echo $incoming->supplier_operator_id; ?>/<?php echo $vendorsids; ?>/<?php echo $incoming->operator_id ?>/<?php echo $incoming->sync_date; ?>" target="_blank"><?php echo $incoming->incoming?$incoming->incoming:"NA"; ?></a></td>
                                       <td><?php echo $incoming->incomingsims; ?>/<?php echo $incoming->totalsims; ?></td>
                                   </tr>
                                   <?php endforeach;  ?>
                               </tbody>
                               <tfoot>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th>Total Incoming</th>
                               <th><?php echo number_format($totalIncoming,2); ?></th>
                               </tfoot>
                           </table>
                       </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($incomings) && $this->input->get()): echo "No Incomings Detected ";  endif; ?>
</div>


<?php $this->load->view('common/footer');  ?>
