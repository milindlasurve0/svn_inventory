<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js/addapincoming.js'); ?>"></script>
<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Add Api's Incomings for date (<?php echo $date; ?>)</h3>
                        <form>
                            <input type="text" name="incoming_date" id="incoming_date"  class="form-control form-inp-med" value="<?php echo $date; ?>" style="float: left; margin-right: 10px;"/>
                            <input type="submit" class="btn btn-default btn-sm" value="Submit" />
                        </form>
           </div>
        </div>
    </div>
    
    
        <div class="row" >
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>Api</th>
                            <th>Operator</th>
                            <th>Incoming</th>
                            <th>Todays total incoming</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                         
                        <tbody>
                           <?php foreach($incomings as $incoming): ?>
                            <tr id="so_<?php echo $incoming['supplier_operator_id'];  ?>">
                        <td><?php echo $incoming['supplier']; ?></td>
                        <td><?php echo $incoming['operator']; ?></td>
                        <td><input autocomplete="off" value="0.00" class="form-control form-inp" type="text" data-soid='<?php echo $incoming['supplier_operator_id'] ?>'   name="inpapiincoming[]" id="inpso_<?php echo $incoming['supplier_operator_id']; ?>" /></td>
                        <td data-oldincoming='<?php echo $incoming['incoming'];  ?>'><?php echo $incoming['incoming'];  ?></td>
                        <td><button class="btn btn-default btn-success btn-sm" id="btnsaveapicoming" data-soid='<?php echo $incoming['supplier_operator_id'] ?>'  data-sid='<?php echo $incoming['supplierid']; ?>'  data-oid='<?php echo $incoming['operator_id'];  ?>' data-vendorid='<?php echo $incoming['vendor_id']; ?>'>Save</button></td>
                         </tr>
                       <?php endforeach;?>
                         </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
<?php $this->load->view('common/footer');  ?>
