<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Edit Supplier : #<?php echo $supplier->id; ?></h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Supplier</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('suppliers/edit/'.$supplier->id); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <div class="form-group">
                                                <label for="suppl_name" class="col-lg-2 control-label">Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('suppl_name')?set_value('suppl_name'):$supplier->name; ?>"   id="suppl_name" name="suppl_name" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_email" class="col-lg-2 control-label">Email</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('suppl_email')?set_value('suppl_email'):$supplier->email_id; ?>"   id="suppl_email" name="suppl_email" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
<!--                                            <div class="form-group">
                                                <label for="suppl_contact" class="col-lg-2 control-label">Contact</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  pattern="[0-9]{1,15}" value="<?php echo set_value('suppl_contact')?set_value('suppl_contact'):$supplier->contact; ?>"   id="suppl_contact" name="suppl_contact" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>-->
                                           
                                       
                                            <div class="form-group">
                                                <label for="suppl_address" class="col-lg-2 control-label">Address</label>
                                                <div class="col-lg-10">
                                                    <textarea  name="suppl_address" class="form-control form-inp" style="width: 305px; height: 94px;" ><?php echo set_value('suppl_address')?set_value('suppl_address'):$supplier->address; ?></textarea>
<!--                                                    <span class="help-block with-errors"></span>-->
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-2 control-label">RM</label>
                                                <div class="col-lg-10">
                                                    <select name="suppl_rm_id" id="suppl_rm_id"  style="width: 150px" required  onchange="checkRm(this.value)">
                                                        <option value="">Select</option>
                                                         <?php foreach($rms as $rm): ?>
                                                        <option value="<?php echo $rm->id;  ?>" <?php if($rm->id==(set_value('suppl_rm_id')?set_value('suppl_rm_id'):$supplier->rm_id)): echo "selected='true'"; endif; ?>><?php echo $rm->name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>


                                          

                                             <div class="form-group" id="other_rm_div">
                                                      <label for="other_rm" class="col-lg-2 control-label">Other RM</label>
                                                      <div class="col-lg-10">
                                                          <input type="text" name="other_rm"  value="<?php echo set_value('other_rm')?set_value('other_rm'):$supplier->other_rm; ?>" <?php if($supplier->rm_id!="0"): echo 'disabled';  endif; ?> />
                                                      </div>
                                            </div>

                                          
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-2 control-label">Parent</label>
                                                <div class="col-lg-10">
                                                    <select name="parent_id" id="parent_id"  style="width: 150px" required>
                                                        <option value="0">None</option>
                                                         <?php foreach($parent_ids as $parent): ?>
                                                        <option value="<?php echo $parent->id;  ?>" <?php if($parent->id==(set_value('parent_id')?set_value('parent_id'):$supplier->parent_id)): echo "selected='true'"; endif; ?>><?php echo $parent->name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label for="frombank" class="col-lg-2 control-label">From Bank</label>
                                                <div class="col-lg-10">
                                                 <select name="frombank"  style="width:165px;" required="true">
                                                         <option value="">Select bank</option>    
                                                                <?php foreach($banks as $bank): ?>
                                                                 <option value="<?php echo $bank->id;  ?>"  <?php if($bank->id==(set_value('frombank')?set_value('frombank'):$supplier->frombank)): echo "selected='true'"; endif; ?>><?php echo $bank->bank_name;  ?></option>
                                                                 <?php endforeach; ?> 
                                                   </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="suppl_rating" class="col-lg-2 control-label">Rating</label>
                                                <div class="col-lg-10">
                                                    <select name="suppl_rating" id="suppl_rating"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                        <option value="1"  <?php if(1==(set_value('suppl_rating')?set_value('suppl_rating'):$supplier->rating)): echo "selected='true'"; endif; ?>>A</option>
                                                        <option value="2" <?php if(2==(set_value('suppl_rating')?set_value('suppl_rating'):$supplier->rating)): echo "selected='true'"; endif; ?>>B</option>
                                                        <option value="3" <?php if(3==(set_value('suppl_rating')?set_value('suppl_rating'):$supplier->rating)): echo "selected='true'"; endif; ?>>C</option>
                                                        <option value="4" <?php if(4==(set_value('suppl_rating')?set_value('suppl_rating'):$supplier->rating)): echo "selected='true'"; endif; ?>>D</option>
                                                        <option value="5" <?php if(5==(set_value('suppl_rating')?set_value('suppl_rating'):$supplier->rating)): echo "selected='true'"; endif; ?>>E</option>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_risk_factor" class="col-lg-2 control-label">Overall Pending Risk Factor</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('suppl_risk_factor')?set_value('suppl_risk_factor'):$supplier->riskfactor; ?>"   id="suppl_risk_factor" name="suppl_risk_factor" class="form-control form-inp-no" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_location" class="col-lg-2 control-label">Location</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('suppl_location')?set_value('suppl_location'):$supplier->location; ?>"   id="suppl_location" name="suppl_location" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="handled_by" class="col-lg-2 control-label">Handled By</label>
                                                <div class="col-lg-10">
                                                      <select name="handled_by" >
                                                           <option value="">None</option>
                                                       <?php foreach($internals as $internal): ?>
                                                        <option value="<?php echo $internal->id;  ?>"   <?php if($supplier->handled_by==(set_value('handled_by')?set_value('handled_by'):$internal->id)): echo "selected='true'"; endif; ?> ><?php echo $internal->name;  ?></option>
                                                        <?php endforeach; ?>
                                                      </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="single_payment" class="col-lg-2 control-label">Single Payment</label>
                                                <div class="col-lg-10">
                                                    <input type="checkbox" name="single_payment" value="1"  id="single_payment" <?php echo $supplier->single_payment?"checked":""; ?> />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="is_api" class="col-lg-2 control-label">isApi</label>
                                                <div class="col-lg-10">
                                                    <input type="checkbox" name="is_api" value="1"  id="is_api" <?php echo $supplier->is_api?"checked":""; ?> />
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="vendors" class="col-lg-2 control-label">Map Vendor</label>
                                                   <div class="col-lg-10">
                                                <select name="vendors[]" multiple="multiple" style="height: 111px; width: 128px;">
                                                       <?php foreach($vendors as $vendor): ?>
                                                        <option value="<?php echo $vendor->id;  ?>" <?php if(in_array($vendor->id,$vendorMappedToSupplier)):  echo "selected"; endif; ?>  ><?php echo $vendor->vendor_name;  ?></option>
                                                        <?php endforeach; ?> 
                                                </select>
                                                   </div>
                                            </div>
                                            

                                            <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                              <a href="<?php echo base_url('suppliers/get/'.$this->uri->segment(3)); ?>" class="btn btn-info btn-sm" role="button">Back to Supplier</a>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>