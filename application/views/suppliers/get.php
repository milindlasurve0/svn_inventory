<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

<div class="container">
    
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>View  Supplier : <a href="/suppliers/edit/<?php echo $supplier->id; ?>">#<?php echo $supplier->id; ?></a></h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-9">
                            <div class="panel panel-default">
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Supplier Info</div>
                                </div>
                           <div class="panel-body" style="height:60%">
                                    
                                    <div class="col-md-4">
                                    <ul style="list-style-type: none;line-height: 20px;">
                                        
                                        <li>
                                            <b>Name</b>
                                            <div><?php echo $supplier->name; ?></div>
                                        </li>
                                        <li>
                                            <b>Email</b>
                                            <div style="word-wrap: break-word;"><?php echo $supplier->email_id; ?></div>
                                        </li>
                                        <li>
                                            <b>Contact</b>
                                            <div>
                                                 <?php 
                                                 if(!empty($supplier_contacts)):
                                                 foreach($supplier_contacts as $cont): 
                                                        if($cont->to_send): 
                                                                echo $cont->name." / ". $cont->contact;
                                                                break;
                                                        endif;
                                                 endforeach;
                                                 endif;
                                                 ?>
                                                 <button data-toggle="modal" data-target="#contactsModal" class="btn btn-success btn-xs">View all</button>
                                            </div>
                                        </li>
                                        <li>
                                            <b>Address</b>
                                            <div><?php echo $supplier->address; ?></div>
                                        </li>
                                    </ul>
                                    </div>
                                    <div class="col-md-5">
                                         <ul style="list-style-type: none;line-height: 20px;">
                                        <li>
                                            <b>Risk Factor</b>
                                            <div><?php echo number_format($supplier->riskfactor,2); ?></div>
                                        </li>
                                        <li>
                                            <b>Location</b>
                                            <div><?php echo $supplier->location; ?></div>
                                        </li>
                                        
                                        <li>
                                            <b>Bank Details</b>
                                            <div> 
                                                    
                                                    <?php if(!empty($supplier_banks)):  foreach($supplier_banks as $defaultbank):   if($defaultbank->default_bank): ?>
                                                
                                                      <span><?php echo $defaultbank->bank_name; ?> / <?php echo $defaultbank->account_no; ?></span><br/>
                                                     
                                                      <?php endif; endforeach; ?>
                                                      
                                                        <button data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs"> View all</button>
                                                        
                                                      <?php else:  ?>
                                                        
                                                       <a href="<?php echo base_url('suppliers/addBankdetails/'.$supplier->id); ?>" class="btn btn-info btn-xs" role="button">Add Now</a>
                                                       
                                                      <?php endif;  ?>
                                                       
                                                      <br/>  
                                                      
                                                   
                                            </div>
                                        </li>
                                        
                                         </ul>
                                    </div>
                                        <div class="col-md-3"> 
                                        <ul style="list-style-type: none;line-height: 20px;">
                                         <li>
                                            <b>Rating</b>
                                            <div>
                                                 <?php 
                                                    $ratings=$this->config->item('rating');
                                                    echo $ratings[$supplier->rating]; 
                                                    ?>
                                            </div>
                                        </li>
                                        <li>
                                            <b>Last Created</b>
                                            <div><?php echo $supplier->created_at; ?></div>
                                        </li>
                                        <li>
                                            <b>Last Updated</b>
                                            <div><?php echo $supplier->updated_at; ?></div>
                                        </li>
                                        </ul>
                                        </div>  
                                  
                                    </div>
                            </div>
                        </div>
               
                        <div class="col-lg-3">
                            
                            <div class="panel panel-default">
                               
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">RM Info</div>
                                </div>
                                <div class="panel-body">
                                    
                                    <ul style="list-style-type: none;line-height: 20px;" >
                                        
                                        <li>
                                            <b>Name : </b>
                                            <?php echo $supplier->rm_name; ?>
                                        </li>
                                        <li>
                                            <b>Email : </b>
                                            <?php echo $supplier->rm_email; ?>
                                        </li>
                                        <li>
                                            <b>Contact : </b>
                                            <?php echo $supplier->rm_mobile; ?>
                                        </li>
                                        <li>
                                            <b>Address : </b>
                                            <?php echo $supplier->rm_address; ?>
                                        </li>
                                        <li>
                                            <b>Last Created : </b>
                                            <?php echo $supplier->rm_created_at; ?>
                                        </li>
                                        <li>
                                            <b>Last Updated : </b>
                                            <?php echo $supplier->rm_updated_at; ?>
                                        </li>
                                        
                                    </ul>
                                        
                                </div>
                            </div>
                    
                          <?php $this->load->view('suppliers/modems'); ?>  
                            
                       </div>
                
                    </div>
            
              <div class="row">
                <div class="col-lg-12">
                    <p><a href="<?php echo base_url('operators/addSupplierOperatorMapping/'.$supplier->id); ?>">Add more operators</a></p>
                </div>
            </div>
            
            <?php $this->load->view('suppliers/operators'); ?>
               
            
        </div>
    </div>
        
</div>

<?php $this->load->view('suppliers/banks'); ?>

<?php $this->load->view('suppliers/contacts'); ?>

<?php $this->load->view('common/footer');  ?>