<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog bankmodelwidth">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Bank Details</h4>
      </div>
      <div class="modal-body">
       <?php
   
       if(!empty($supplier_banks)):
       ?>
          <table class="table">
              <thead>
                  <tr>
                      <th>Default</th>
                      <th>A/C  name</th>
                      <th>Bank Name</th>
                      <th>ISFC Code</th>
                      <th>A/C no</th>
                      <th>Code</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                    <?php foreach($supplier_banks as $bank):  ?>
                  <tr>
                      <td style="width: 80px;">
<!--                          <input type="radio" <?php // if($bank->default_bank): echo "checked";  endif; ?>  name="default_bank" value="<?php //echo $bank->sb_id; ?>" onchange="setDefaultBank(this.value,'<?php //echo $this->uri->segment('3'); ?>');" />-->
                          <div  class="btn-group pull-left" data-toggle="buttons">
                         <label class="btn btn-default btn-sm  <?php if(!$bank->default_bank): echo "active btn-danger";  endif; ?> ">
                        <input type="radio" name="bank_onoff_flag" value="0"   data-sbid="<?php  echo $bank->sb_id; ?>"  <?php if($bank->default_bank): echo "checked"; endif; ?> />Off
                        </label> 
                        <label class="btn btn-default btn-sm <?php if($bank->default_bank): echo "active btn-success";  endif; ?> ">
                        <input type="radio"  name="bank_onoff_flag" value="1"  data-sbid="<?php  echo $bank->sb_id; ?>"  <?php if($bank->default_bank): echo "checked"; endif; ?> />On
                        </label> 
                          </div>
                      </td>
                      <td><?php echo $bank->account_holder_name; ?></td>
                      <td><?php echo $bank->bank_name; ?></td>
                      <td><?php echo $bank->isfc_code; ?></td>
                      <td><?php echo $bank->account_no; ?></td>
                      <td><?php echo $bank->beneficiary_code; ?></td>
                      <td class="actions">
                          
                          <a href="<?php echo base_url('suppliers/editbankDetails/' .$bank->sb_id); ?>">
                                      <button class="btn btn-xs btn-primary">
                                          <i class="glyphicon glyphicon-pencil"></i>
                                          Edit
                                      </button>
                                  </a>
                          
<!--                          <a onclick="return confirm('Are you sure?')" href="<?php //echo base_url('suppliers/deletebankDetails/' .$bank->sb_id.'/'.$bank->supplier_id); ?>">
                                      <button class="btn btn-xs btn-danger">
                                          <i class="glyphicon glyphicon-pencil"></i>
                                          Delete
                                      </button>
                                  </a>-->
                          
                      </td>
                  </tr>
                 <?php endforeach; ?>
              </tbody>
          </table>   
    
          
      <?php else:  ?>
          <a href="<?php echo base_url('suppliers/addBankdetails/'.$supplier->id); ?>" class="btn btn-info btn-sm" role="button">Add Now</a>
     <?php endif; ?>
      </div>
      <div class="modal-footer">
          <div id="div_success" class="alert alert-success" style="display: none;text-align: center;">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <strong>Success ! </strong>Updated
          </div>
          <div id="div_error" class="alert alert-danger" style="display: none;text-align: center;">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <p><strong>Error ! </strong>Error
          </div>
          
          <div class="btn-group pull-left" data-toggle="buttons" id="bankswitchmode">
              <span class="pull-left"> Bank Switch Mode : </span> 
              <label class="btn btn-default btn-sm  <?php if(!$supplier->bank_change_flag): echo "active";  endif; ?>">
                    <input type="radio" name="bank_change_flag" value="0"  data-supplier="<?php echo $supplier->id; ?>"   <?php if($supplier->bank_change_flag): echo "checked"; endif; ?> />Off
                </label> 
                <label class="btn btn-default btn-sm  <?php if($supplier->bank_change_flag): echo "active";  endif; ?> ">
                    <input type="radio"  name="bank_change_flag" value="1" data-supplier="<?php echo $supplier->id; ?>"  <?php if($supplier->bank_change_flag): echo "checked"; endif; ?> />On
                </label> 
          </div>
          
              <a href="<?php echo base_url('suppliers/addBankdetails/'.$supplier->id); ?>" class="btn btn-info btn-sm" role="button">Add More Banks</a>
<!--        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>