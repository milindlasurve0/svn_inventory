<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Contacts of  supplier #<?php echo $this->uri->segment(3) ?></h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Contacts</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('suppliers/addContacts/'.$this->uri->segment(3)); ?>">
                                        <fieldset>
                                            <legend></legend>
                                          
                                          
                                            
                                            <div class="col-xs-4">
                                                
                                                    <div class="form-group">
                                                    
                                                        <div class="col-lg-10">
                                                            <input type="text" placeholder="Contact Name"    id="name" name="name" class="form-control" data-error="This field is required" required>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                           
                                            <div class="col-xs-4">
                                                
                                                    <div class="form-group">
                                                 
                                                        <div class="col-lg-10">
                                                            <input type="number" placeholder="Contact no"    id="contact" name="contact" class="form-control" data-error="Enter valid contact no" required>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                          
                                          
                                            <div class="row">
                                                <div class="col-lg-10">
                                            <div class="form-group pull-right" >
                                            <label for="" class="col-lg-12 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                               <a href="<?php echo base_url('suppliers/get/'.$this->uri->segment(3)); ?>" class="btn btn-info btn-sm" role="button">Back to Supplier</a>
                                            </div>
                                                  </div>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>
