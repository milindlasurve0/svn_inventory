<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script src="<?php echo base_url('public/js/jquery.multiple.select.js'); ?>"></script>
<script src="<?php echo base_url('public/js/reports.js?'.time()); ?>"></script>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/multiple-select.css'); ?>" />
<style>body { font-size: 12px;}.ms-drop input[type="checkbox"] {vertical-align: text-bottom;}.pagination{float: right}</style>
<div class="container-fluid">
    
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>View All Suppliers</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filters</div></div>
                        <div class="panel-body">
                            <form role="form" class="form-horizontal" name="frmfiltersupplier">
                                <div class="form-group">
                                <div class="col-sm-3 autowidth">
                                    <label>Operator</label>
                                    <select   style="width: 150px" name="operator_id"  id="operator_id">
                                                       <option value="">Select</option>
                                                        <?php foreach($operators as $operator): ?>
                                                       <option value="<?php echo $operator->id;  ?>"  <?php if($this->input->get('operator_id')==$operator->id): echo "Selected"; endif; ?>><?php echo $operator->operator_name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 autowidth">
                                    <label>Modems</label>
                                    <select  style="width: 150px"  name="modems[]">
                                                        <?php $vendorids=$this->input->get('modems_ids');  $vendorids=  strpos($vendorids,",")?explode(",",$vendorids):array($vendorids);
                                                         foreach($vendors as $vendor): ?>
                                                       <option value="<?php echo $vendor->id;  ?>"  <?php if(in_array($vendor->id,$vendorids)): echo "Selected"; endif; ?>><?php echo $vendor->vendor_name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                </div>
                                 <div class="col-sm-3 autowidth">
                                    <label>RM</label>
                                    <select  style="width: 150px" name="rm"  id="rm">
                                                       <option value="">Select</option>
                                                        <?php foreach($rms as $rm): ?>
                                                       <option value="<?php echo $rm->id;  ?>"  <?php if($this->input->get('rm')==$rm->id): echo "Selected"; endif; ?>><?php echo $rm->name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                </div>
                                 <div class="col-sm-3 autowidth">
                                    <label>Handled by</label>
                                    <select  style="width: 150px" name="handled_by"  id="handled_by">
                                                       <option value="">Select</option>
                                                        <?php  foreach($internals as $internal): ?>
                                                       <option value="<?php echo $internal->id;  ?>"  <?php if($this->input->get('handled_by')==$internal->id  ): echo "Selected"; endif; ?>><?php echo $internal->name;  ?></option>
                                                       <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                     <div class="col-sm-5"><label>Isactive</label></div>
                                     <select  style="width: 70px" name="is_active" id="is_active" >
                                                       <option value="all"  <?php if($this->input->get('is_active')=="all"): echo "Selected"; endif; ?>>All</option>
                                                       <option value="no"  <?php if($this->input->get('is_active')=='no'  ): echo "Selected"; endif; ?>>Inactive</option>
                                                       <option value="yes"  <?php if($this->input->get('is_active')=='yes'  ): echo "Selected"; endif; ?>>Active</option>
                                    </select>
                                </div>
                                    <div class="clearfix">  </div>
                                    <div class="col-sm-3 ">
                                        <label>Show all</label>
                                        <input type="checkbox" name="showall" value="1" id="showall" style="vertical-align: sub;"  <?php if($this->input->get('showall')): echo "checked"; endif;  ?>  />
                                    </div>
                                   <div class="col-sm-1 pull-right">
                                        <input type="hidden" name="orderby" value=""  id="orderby"/>
                                        <input type="hidden" name="ordertype" value=""  id="ordertype"/>
                                        <button class="btn btn-default btn-info btn-sm">Filter</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <div class="container-fluid">
             <?php if(!empty($suppliers)):  ?>
            <div class="row" style="height: 30px;">
                    <div class="col-lg-2"><strong>Total Unique Suppliers : </strong><?php echo $totaluniquesuppliers;  ?></div>
                    <div class="col-lg-2"><strong>Total Rows : </strong> <?php echo $total_rows;  ?></div>
                    <?php if($this->input->get('operator_id')): ?>
                    <div class="col-lg-2"><strong>Total Avg Sale :  </strong> <?php echo number_format($avgOperatorSale+$avgApiSale,2);  ?></div>
                    <div class="col-lg-2"><strong>Total Baseale : </strong> <?php echo number_format($totalbasesale,2);  ?></div>
                    <div class="col-lg-2"><strong>Total Short : </strong> <?php echo number_format($totalbasesale-$avgOperatorSale-$avgApiSale,2);  ?></div>
                    <?php endif; ?>
            </div>
           <?php endif; ?>
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Listings</div>
                                </div>
                                <div class="bootstrap-admin-panel-content">
                                    <table class="table bootstrap-admin-table-with-actions">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><a href="<?php echo  getSortUrlByColumn('supplier'); ?>">Supplier</a>&nbsp;<?php if($this->input->get('orderby')=="supplier"): ?><i class="fa fa-sort"></i><?php endif; ?></th>
                                                <th>Modems</th>
                                                <th>Operator</th>
                                                 <th><a href="<?php echo  getSortUrlByColumn('commission_type_formula'); ?>">Margin</a>&nbsp;<?php if($this->input->get('orderby')=="commission_type_formula"): ?><i class="fa fa-sort"></i><?php endif; ?></th>
                                                <th><a href="<?php echo  getSortUrlByColumn('capacity_per_month'); ?>">Capacity</a>&nbsp;<?php if($this->input->get('orderby')=="capacity_per_month"): ?><i class="fa fa-sort"></i><?php endif; ?></th>
                                                <th><a href="<?php echo  getSortUrlByColumn('base_amount'); ?>">Basesale</a>&nbsp;<?php if($this->input->get('orderby')=="base_amount"): ?><i class="fa fa-sort"></i><?php endif; ?></th>
                                                <th>Sims</th>
                                                <th>Location</th>
                                                <th>Email</th>
                                                <th>RM</th>
                                                <th><a href="<?php echo  getSortUrlByColumn('created_at'); ?>">Created</a>&nbsp;<?php if($this->input->get('orderby')=="created_at"): ?><i class="fa fa-sort"></i><?php endif; ?></th>
                                                <th>Active</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($suppliers)):  ?>
                                            <?php foreach ($suppliers as $supplier):  ?>
                                            <tr>
                                                
                                                <td><?php echo $supplier['supplier_id'];  ?></td>
                                                <td class="supplierlink"><a  target="_blank" href="/suppliers/get/<?php echo $supplier['supplier_id'];  ?>"><?php echo $supplier['supplier'];  ?></a></td>
                                                  <td><?php echo $supplier['vendors'];  ?></td>
                                                <td><?php echo $supplier['operator'];  ?></td>
                                                 <td>
                                                     <?php echo $supplier['margin'];  ?>
                                                    <?php if($supplier['commission_type']=="1"): ?>
                                                    <span class="glyphicon glyphicon-arrow-down text-success"></span>
                                                    <?php else:  ?>
                                                    <span class="text-success glyphicon glyphicon-arrow-up"></span>
                                                    <?php endif;  ?>
                                                 </td>
                                                 <td><?php echo $supplier['capacity'];  ?></td>
                                                <td><?php echo $supplier['base_amount'];  ?></td>
                                                <td><?php echo $supplier['activesims']."/".$supplier['totalsims'];  ?></td>
                                                <td><?php echo $supplier['location'];  ?></td>
                                                <td title="<?php echo $supplier['email_id'];  ?>"><?php echo substr($supplier['email_id'],0,30);  ?></td>
                                                <td><?php echo $supplier['rmname'];  ?></td>
                                                <td><?php echo date("Y-m-d", strtotime($supplier['created_at']));  ?></td>
                                                <td>
                                                    <?php if($supplier['is_active']):   ?>
                                                    <span class="glyphicon glyphicon-ok-circle text-success font12"></span>
                                                     <?php else:   ?>
                                                    <span class="glyphicon glyphicon-remove-circle text-danger font12"></span>
                                                    <?php endif; ?>
                                                </td>
                                              
                                            </tr>
                                           <?php endforeach; ?> 
                                            
                                            <?php else: echo  "No records Found"; endif;?>
                                            
                                        </tbody>
                                    </table>
                                <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>

<?php $this->load->view('common/footer');  ?>