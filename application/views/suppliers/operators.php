<script src="<?php echo base_url('public/js/get.js'); ?>"></script>  
<div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                               
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Operators</div>
                                </div>
                                <div class="panel-body" >
                                   
                                   <table class="table bootstrap-admin-table-with-actions">
                                        <thead>
                                            <tr>
                                          
                                                <th>Operator</th>
                                                <th>Total Cap</th>
                                                <th>Cap/day</th>
                                                <th>Cap/month</th>
                                                <th>Basesale</th>
                                                <th>Freq</th>
                                                <th>Risk Fac</th>
                                                <th>Bound</th>
                                                <th>Margin</th>
                                                <th>Last Updated</th>
                                                <th>Active</th>
                                                <th>OnHold</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($supplier_operators)):  ?>
                                            <?php foreach ($supplier_operators as $supplier_operator):  ?>
                                            <tr>
                                                <td><?php echo $supplier_operator->operator_name;  ?></td>
                                                <td><?php echo number_format($supplier_operator->capacity,2);  ?></td>
                                                <td><?php echo number_format($supplier_operator->capacity_per_day,2);  ?></td>
                                                <td><?php echo number_format($supplier_operator->capacity_per_month,2);  ?></td>
                                                <td><?php echo number_format($supplier_operator->base_amount,2);  ?></td>
                                                <td>
                                                    <?php 
                                                    if(!empty($supplier_operator->frequency)):
                                                        $frequencys=$this->config->item('frequency');
                                                        echo $frequencys[$supplier_operator->frequency]; 
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><?php echo number_format($supplier_operator->riskfactor,2);  ?></td>
                                                <td>
                                                     <?php 
                                                    $commission_types=$this->config->item('commission_type');
                                                    echo $commission_types[$supplier_operator->commission_type]; 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo $supplier_operator->commission_type_formula."%";
                                                    
                                                    if($supplier_operator->commission_type==3):
                                                        echo " [".$supplier_operator->commission_type_expression."] ";
                                                    endif;
                                                    
                                                    ?>
                                                </td>
                                                <td><?php echo $supplier_operator->updated_at;  ?></td>
                                                <td>
                                                    <div class="bs-example">
                                                        <div class="btn-group" data-toggle="buttons">
                                                            <label class="btn btn-default btn-xs <?php echo $supplier_operator->is_active=="1"?"btn-success disabled":""; ?>">
                                                                <input type="radio" value='on' name="options"  data-soid="<?php echo $supplier_operator->id;  ?>" data-margin="<?php echo $supplier_operator->commission_type_formula;?>" data-baseamt="<?php echo $supplier_operator->base_amount;  ?>">On
                                                            </label>
                                                            <label class="btn btn-default btn-xs <?php echo $supplier_operator->is_active=="0"?"btn-danger disabled":""; ?>">
                                                                <input type="radio" value='off' name="options"  data-soid="<?php echo $supplier_operator->id;  ?>" data-margin="<?php echo $supplier_operator->commission_type_formula;?>" data-baseamt="<?php echo $supplier_operator->base_amount; ?>">Off
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="bs-example col-sm-2">
                                                    <input type="checkbox" name="is_onhold"  data-soid="<?php echo $supplier_operator->id;  ?>" data-margin="<?php echo $supplier_operator->commission_type_formula;?>" data-baseamt="<?php echo $supplier_operator->base_amount; ?>" <?php echo ($supplier_operator->is_onhold=='1')?"checked='checked'":""; ?> autocomplete="off"/>
                                                    </div>
                                                </td>
                                                <td class="actions">
                                                    
                                                    <a href="<?php echo base_url('operators/editmapping/'.$supplier_operator->id); ?>">
                                                        <button class="btn btn-xs btn-primary">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                            Edit
                                                        </button>
                                                    </a>
<!--                                                     <a href="<?php //echo base_url('operators/deletemapping/'.$supplier_operator->id); ?>" onClick="javascript:return confirm('Are you sure to Delete ?');">
                                                        <button class="btn btn-xs btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>-->
                                                    
                                                </td>
                                                
                                               
                                            </tr>
                                           <?php endforeach; ?> 
                                            
                                            <?php else: echo  "No records Found"; endif;?>
                                            
                                        </tbody>
                                    </table>
                                  
                                    </div>
                                  
                                </div>
                            </div>
                        </div>

<div class="modal" id="addcomment">
    <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Comment</h4>
            </div>
        <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12" id="loadcomments">

                    </div>
                </div>
                <input type="hidden" name="comment_so_id" id="comment_so_id" value="" />
                <input type="hidden" name="inpflag" id="inpflag"  autocomplete="off" />
                <input type="hidden" name="baseamt" id="baseamt"  autocomplete="off" />
                <input type="hidden" name="margin" id="margin"  autocomplete="off" />
                <textarea  name="comment" id="comment"></textarea>
        </div>
        <div class="modal-footer">
            <div id="commentmsg" style="text-align: left"></div>
            <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Add</button>
            <button type="button" class="btn btn-default btn-sm" id="resetbtn" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>

<div class="modal" id="onholdcomment">
    <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Comment</h4>
            </div>
        <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12" id="loadcomments">

                    </div>
                </div>
                <input type="hidden" name="comment_soid" id="comment_soid" value="" />
                <input type="hidden" name="flag" id="flag"  autocomplete="off" />
                <input type="hidden" name="baseamt" id="baseamt"  autocomplete="off" />
                <input type="hidden" name="margin" id="margin"  autocomplete="off" />
                <textarea  name="comment" id="comment"></textarea>
        </div>
        <div class="modal-footer">
            <div id="commentmsg" style="text-align: left"></div>
            <button class="btn btn-sm btn-default btn-primary" id="onholdcommentbtn">Add</button>
            <button type="button" class="btn btn-default btn-sm" id="reset" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
