<div class="modal fade" id="contactsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Contacts</h4>
      </div>
      <div class="modal-body">
          <?php if(!empty($supplier_contacts)): ?>
           <table class="table">
              <thead>
                  <tr>
                      <th>SMS</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                   <?php foreach($supplier_contacts as $contact):  ?>
                  <tr>
                    
                      <td><input type="checkbox" <?php if($contact->to_send): echo "checked";  endif; ?>  onclick="setSmsContact(this,this.value);"  value="<?php echo $contact->id; ?>"   /></td>
                      <td><?php echo $contact->name; ?></td>
                      <td><?php echo $contact->contact; ?></td>
                      <td class="actions">
                          
                          <a href="<?php echo base_url('suppliers/editContact/' .$contact->id); ?>">
                                      <button class="btn btn-xs btn-primary">
                                          <i class="glyphicon glyphicon-pencil"></i>
                                          Edit
                                      </button>
                                  </a>
                          
                      </td>
                  </tr>
                 <?php endforeach; ?>
              </tbody>
           </table>
          <?php else: ?>
          <a href="<?php echo base_url('suppliers/addContacts/'.$supplier->id); ?>" class="btn btn-info btn-sm" role="button">Add Contacts</a>
          <?php endif; ?>
      </div>
      <div class="modal-footer">
          <div id="div_success" class="alert alert-success" style="display: none;text-align: center;">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <strong>Success ! </strong>Updated
          </div>
          <div id="div_error" class="alert alert-danger" style="display: none;text-align: center;">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <p><strong>Error ! </strong>Error
          </div>
          <?php if(!empty($supplier_contacts)): ?>
          <a href="<?php echo base_url('suppliers/addContacts/'.$supplier->id); ?>" class="btn btn-info btn-sm" role="button">Add More Contacts</a>
          <?php endif;?>
<!--        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>