<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<!--<script>
$(function() {
    $('.selectize-select').selectize();
});
</script>-->
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Add Supplier</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Supplier</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('suppliers/add'); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <div class="form-group">
                                                <label for="suppl_name" class="col-lg-2 control-label">Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('suppl_name'); ?>"   id="suppl_name" name="suppl_name" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_email" class="col-lg-2 control-label">Email</label>
                                                <div class="col-lg-10">
                                                    <input type="email"value="<?php echo set_value('suppl_email'); ?>"   id="suppl_email" name="suppl_email" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_contact" class="col-lg-2 control-label">Contact</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  pattern="[0-9]{1,15}" value="<?php echo set_value('suppl_contact'); ?>"   id="suppl_contact" name="suppl_contact" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_bank_account_no" class="col-lg-2 control-label">Account No</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('suppl_bank_account_no'); ?>"   id="suppl_bank_account_no" name="suppl_bank_account_no" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_bank_name" class="col-lg-2 control-label">Bank Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('suppl_bank_name'); ?>"   id="suppl_bank_name" name="suppl_bank_name" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_address" class="col-lg-2 control-label">Address</label>
                                                <div class="col-lg-10">
                                                    <textarea  name="suppl_address" class="form-control form-inp" style="width: 305px; height: 94px;" required data-error="Enter a valid Address" ><?php echo set_value('suppl_address'); ?></textarea>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label for="suppl_parent_id" class="col-lg-2 control-label">Parent</label>
                                                <div class="col-lg-10">
                                                    <select name="suppl_parent_id" id="suppl_parent_id"  style="width: 150px" required>
                                                        <option value="0">No Parent</option>
                                                         <?php foreach($parent_ids as $parent_id): ?>
                                                        <option value="<?php echo $parent_id->id;  ?>" <?php if($parent_id->id==set_value('suppl_parent_id')): echo "selected"; endif; ?>><?php echo $parent_id->name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-2 control-label">RM</label>
                                                <div class="col-lg-10">
                                                    <select name="suppl_rm_id" id="suppl_rm_id"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                         <?php foreach($rms as $rm): ?>
                                                        <option value="<?php echo $rm->id;  ?>" <?php if($rm->id==set_value('suppl_rm_id')): echo "selected"; endif; ?>><?php echo $rm->name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_rating" class="col-lg-2 control-label">Rating</label>
                                                <div class="col-lg-10">
                                                    <select name="suppl_rating" id="suppl_rating"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                        <option value="1"  <?php if(1==set_value('suppl_rating')): echo "selected"; endif; ?>>A</option>
                                                        <option value="2" <?php if(2==set_value('suppl_rating')): echo "selected"; endif; ?>>B</option>
                                                        <option value="3" <?php if(3==set_value('suppl_rating')): echo "selected"; endif; ?>>C</option>
                                                        <option value="4" <?php if(4==set_value('suppl_rating')): echo "selected"; endif; ?>>D</option>
                                                        <option value="5" <?php if(5==set_value('suppl_rating')): echo "selected"; endif; ?>>E</option>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_risk_factor" class="col-lg-2 control-label">Risk Factor</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('suppl_risk_factor'); ?>"   id="suppl_risk_factor" name="suppl_risk_factor" class="form-control form-inp-no" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppl_location" class="col-lg-2 control-label">Location</label>
                                                <div class="col-lg-10">
                                                    <input type="text"  value="<?php echo set_value('suppl_location'); ?>"   id="suppl_location" name="suppl_location" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            

                                            <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                            <button class="btn btn-default btn-sm" type="reset">Reset</button>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>