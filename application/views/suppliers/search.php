<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script>$(document).ready(function(){$('#sandbox-container .input-daterange').datepicker({format: "yyyy-mm-dd",startDate: "-365d",endDate: "1d",todayHighlight: true,orientation: "top right"});});</script>

<div class="container-fluid">
  
   <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search Toggle History</h3>
           </div>
        </div>
    </div>
        
       <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <div class="row">
                        <form action="search" method="get">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Date range</label></div>
                                    <div class="col-lg-7" id="sandbox-container">
                                            <div class="input-daterange input-group" id="datepicker">
                                                <input type="text" class="input-sm form-control" name="start" value="<?php echo $this->input->get('start'); ?>" />
                                                <span class="input-group-addon">to</span>
                                                <input type="text" class="input-sm form-control" name="end" value="<?php echo $this->input->get('end'); ?>" />
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Operator</label></div>
                                   <div class="col-lg-7"> <select name="operator_id" id="operator_id"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value="all"  <?php if($this->input->get('operator_id')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($operators as $operator): ?>
                                      <option value="<?php echo $operator['id'];  ?>"  <?php if($this->input->get('operator_id')==$operator['id']):  echo "Selected";  endif;?>><?php echo $operator['name'];  ?></option>
                                      <?php endforeach; ?>
                                       </select></div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                <div class="col-lg-3"><label for="supplier_id" class="col-lg-5 control-label">Supplier</label></div>
                                     <div class="col-lg-7">
                                           <select name="supplier_id" id="supplier_id"  style="width: 150px" >
                                              <option value="">Select</option>
                                               <?php foreach($suppliers as $supplier): ?>
                                              <option value="<?php echo $supplier->id;  ?>"  <?php  if($this->input->get('supplier_id')==$supplier->id): echo "selected";  endif; ?>><?php echo $supplier->name;  ?></option>
                                              <?php endforeach; ?>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                     <div class="col-lg-3"><label>IsActive</label></div>
                                     <div class="col-lg-7">
                                     <select  style="width: 70px" name="is_active" id="is_active" >
                                                       <option value="">Select</option>
                                                       <option value="on"  <?php if($this->input->get('is_active')=="on"  ): echo "Selected"; endif; ?>>On</option>
                                                       <option value="off"  <?php if($this->input->get('is_active')=="off"  ): echo "Selected"; endif; ?>>Off</option>
                                    </select>
                                     </div>
                                </div>
                                </div>
                                <div class="col-lg-3">
                                <div class="form-group">
                                     <div class="col-lg-3"><label>IsOnhold</label></div>
                                     <div class="col-lg-7">
                                     <select  style="width: 70px" name="is_onhold" id="is_onhold" >
                                                       <option value="">Select</option>
                                                       <option value="yes"  <?php if($this->input->get('is_onhold')=="yes"  ): echo "Selected"; endif; ?>>Yes</option>
                                                       <option value="no"  <?php if($this->input->get('is_onhold')=="no"  ): echo "Selected"; endif; ?>>No</option>
                                    </select>
                                     </div>
                                </div>
                                </div>
                            
                                <div class="col-lg-1"  style="float: right;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                                </div>
                            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
 </div>
    
    <?php if(!empty($togglehistory)):  ?>
    
    <strong>Total rows : </strong><?php echo count($togglehistory); ?>
     
            <div class="row">
                <div class="col-lg-12">
                       <h3>History for date : ( <?php echo $this->input->get('start'); ?> - <?php echo $this->input->get('end'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Toggle History</div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions table-striped">
                               <thead>
                                   <tr>
                                        <th>Date</th>
                                       <th>Supplier</th>
                                       <th>Operator</th>
                                       <th>Basesale</th>
                                       <th>Margin</th>
                                       <th>Active</th>
                                       <th>OnHold</th>
                                       <th>Comments</th>
                                       <th>Username</th>
                                       <th>Updated Time</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php foreach ($togglehistory as $history):  ?>
                                   <tr>
                                       <td><?php echo $history['updated_date']; ?></td>
                                       <td><?php echo $history['supplier_name']; ?></td>
                                       <td><?php echo $history['operator_name']; ?></td>
                                       <td><?php echo $history['base_amount']; ?></td>
                                       <td><?php echo $history['margin']; ?></td>
                                       <td><?php echo (($history['is_active']=="1")?"On":(($history['is_active']=="0")?"Off":"NA")); ?></td>
                                       <td><?php echo (($history['is_onhold']=="1")?"Yes":(($history['is_onhold']=="0")?"No":"NA")); ?></td>
                                       <td><?php echo $history['comments']; ?></td>
                                       <td><?php echo $history['user_name']; ?></td>
                                       <td><?php echo $history['updated_time']; ?></td>
                                   </tr>
                                    <?php endforeach;?>
                                      </tbody>
                               <tfoot>
                                   
                               </tfoot>
                            
                           </table>
                       </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($togglehistory) && $this->input->get()): echo "No History Found ";  endif; ?>
        
<?php $this->load->view('common/footer');  ?>