<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Edit Bank Details #<?php echo $this->uri->segment(3) ?></h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Bank Details</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('suppliers/editbankDetails/'.$this->uri->segment(3)); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <div class="col-xs-3">
                                                
                                                    <div class="form-group">
                                                        <label>Account Holder Name</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" placeholder="A/c holder Name"  value="<?php echo $sb->account_holder_name; ?>"  id="account_holder_name" name="account_holder_name" class="form-control" data-error="This field is required" required>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                            <div class="col-xs-3">
                                                
                                                    <div class="form-group">
                                                  <label>Bank Name</label>
                                                        <div class="col-lg-10">
<!--                                                            <select name="bank_id"  style="width:165px;" required="true">
                                                         <option value="">Select bank</option>    
                                                                <?php foreach($banks as $bank): ?>
                                                                 <option value="<?php echo $bank->id;  ?>" <?php if($bank->id==$sb->bank_id): echo "selected"; endif; ?> ><?php echo $bank->bank_name;  ?></option>
                                                                 <?php endforeach; ?> 
                                                        </select>-->
                                                              <?php foreach($banks as $bank): ?>
                                                                  <?php if($bank->id==$sb->bank_id): ?> <?php echo $bank->bank_name;  ?><?php endif;  ?></option>
                                                                 <?php endforeach; ?> 
                                                           
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                            <div class="col-xs-3">
                                                
                                                    <div class="form-group">
                                                  <label>ISFC code</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" placeholder="IFSC code" value="<?php echo $sb->isfc_code; ?>"    id="isfc_code" name="isfc_code" class="form-control" data-error="This field is required" required>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                            <div class="col-xs-3">
                                                
                                                    <div class="form-group">
                                                     <label>A/C no</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" placeholder="Account No"  value="<?php echo $sb->account_no; ?>"   id="account_no" name="account_no" class="form-control" data-error="This field is required" required>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                            
                                            <div class="col-xs-3">
                                                
                                                    <div class="form-group">
                                                     <label>Beneficiary Code</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" placeholder="Beneficiary Code"  value="<?php echo $sb->beneficiary_code; ?>"   id="beneficiary_code" name="beneficiary_code" class="form-control" data-error="This field is required" required>
                                                            <span class="help-block with-errors"></span>
                                                        </div>
                                                    </div>
                                                
                                             </div>
                                          
                                            <div class="row">
                                                <div class="col-lg-12">
                                            <div class="form-group pull-right" >
                                            <label for="" class="col-lg-12 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Update</button>
                                            <a href="<?php echo base_url('suppliers/get/'.$sb->supplier_id); ?>" class="btn btn-info btn-sm" role="button">Back to Supplier</a>
                                            </div>
                                                  </div>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>
