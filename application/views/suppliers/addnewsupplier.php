<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script>
var compressedHTML=$('<div><div class="col-lg-12">\n<div class="col-lg-4">\n<div class="form-group">\n<label for="operator_id" class="col-lg-4 control-label-default">Operator</label>\n<div class="col-lg-4">\n<?php echo getOperatorDropdownHTML(); ?><span class="help-block with-errors"></span>\n</div>\n</div>\n<div class="form-group">\n<label for="total_capacity" class="col-lg-4 control-label-default">Total Order  Capacity</label>\n<div class="col-lg-4">\n<input type="text" name="total_capacity[]" class="form-control" data-error="This field is required" required>\n</div>\n</div>\n<div class="form-group">\n<label for="so_riskfactor" class="col-lg-4 control-label-default">Pending Risk Factor</label>\n<div class="col-lg-4">\n<input type="text" name="so_riskfactor[]" class="form-control" data-error="This field is required" required>\n</div>\n</div>\n<div class="form-group">\n<label for="so_basesale" class="col-lg-4 control-label-default">Base Sale</label>\n<div class="col-lg-4"><input type="text" name="basesale[]" class="form-control" required="" data-error="This field is required"></div>\n</div></div>\n<div class="col-lg-4">\n<div class="form-group">\n<label for="capacity_per_day" class="col-lg-5 control-label-default">Vendor Capacity Per Day</label>\n<div class="col-lg-4">\n<input type="text" name="capacity_per_day[]" class="form-control" data-error="This field is required" required>\n</div>\n</div>\n<div class="form-group">\n<label for="capacity_per_month" class="col-lg-5 control-label-default">Vendor Capacity Per Month</label>\n<div class="col-lg-4">\n<input type="text" name="capacity_per_month[]" class="form-control" data-error="This field is required" required>\n</div>\n</div>\n<div class="form-group">\n<label for="frequency" class="col-lg-5 control-label-default">Frequency</label>\n<div class="col-lg-4">\n<?php echo getFrequencyDropdownHTML(); ?>\n<span class="help-block with-errors"></div>\n</div>\n</div>\n<div class="col-lg-4">\n<div class="form-group">\n<label for="commission_type" class="col-lg-5 control-label-default">Commission Type</label>\n<div class="col-lg-4">\n<?php echo getCommissionTypeDropdownHTML(); ?>\n<span class="help-block with-errors"></div>\n</div>\n<div class="form-group">\n<label for="commission_type_formula" class="col-lg-5 control-label-default">Commission Type Formula</label>\n<div class="col-lg-7">\n<input type="text" name="commission_type_formula[]" class="form-control" data-error="This field is required" required><br/><span><i>If formula then 40000=50000</i> </span>\n</div>\n</div>\n<div class="form-group">\n<label for="operator_id" class="col-lg-4 control-label-default">Batch</label>\n<div class="col-lg-4">\n<?php echo getBatchDropdownHTML(); ?><span class="help-block with-errors"></span>\n</div>\n</div>\n</div>\n</div><p style="text-align:center">----------------------------------------------------------------------------------------------------------------</p>\n</div>')
var contactHTML =$( '<div><div class="row">\n<div class="col-lg-10">\n<div class="col-sm-5">\n<div class="form-group">\n<label for="contact_name" class="col-lg-4 control-label">Contact Name</label>\n<div class="col-lg-6">\n<input type="text" id="contact_name" name="contact_name[]" class="form-control" data-error="This field is required" required>\n<span class="help-block with-errors"></span>\n</div>\n</div>\n</div>\n<div class="col-sm-5">\n<div class="form-group">\n<label for="contact_no" class="col-lg-2 control-label">No</label>\n<div class="col-lg-6">\n<input type="text" id="contact_no" name="contact_no[]" class="form-control" data-error="This field is required" required>\n<span class="help-block with-errors"></span>\n</div>\n</div>\n</div>\n<div class="col-sm-2">\n</div>\n</div>\n</div></div>');
</script>
 <script src="<?php echo base_url('public/js/site.js'); ?>"></script>
 
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Add Supplier</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Supplier</div>
                                </div>
                                 <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>    
                                <div class="panel-body"> <!-- Start Panel Body -->
                                    
                                  <form class="form-horizontal"  data-toggle="validator" method="post" action="<?php echo base_url('suppliers/save'); ?>">
                                      <div class="suppl_form">
                                        <fieldset>
                                            
                                            <legend></legend>
                                            <div class="col-md-3">
                                                
                                            <div class="form-group">
                                                <label for="suppl_name" class="col-lg-3 control-label">Name</label>
                                                <div class="col-lg-9">
                                                    <input type="text" data-remote="/suppliers/CheckValidUsername"   value="<?php echo set_value('suppl_name'); ?>"   id="suppl_name" name="suppl_name" class="form-control" data-error="This field is required"  data-remote-error="Name already exists" required />
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                                
                                            <div class="form-group">
                                                <label for="suppl_email" class="col-lg-3 control-label">Email</label>
                                                <div class="col-lg-9">
<!--                                                    <input type="email"   data-remote="/suppliers/CheckValidEmail"  value="<?php echo set_value('suppl_email'); ?>"   id="suppl_email" name="suppl_email" class="form-control " data-error="This field is required" data-remote-error="Email already exists"  required>-->
                                                    <input type="email"   value="<?php echo set_value('suppl_email'); ?>"   id="suppl_email" name="suppl_email" class="form-control " data-error="This field is required"  required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                                
                                            <div class="form-group">
                                                <label for="suppl_contact" class="col-lg-3 control-label">Contact</label>
                                                <div class="col-lg-9">
                                                    <input type="text"  pattern="[0-9]{1,15}" value="<?php echo set_value('suppl_contact'); ?>"   id="suppl_contact" name="suppl_contact" class="form-control " data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                                
                                               
                                                
                                            </div>
                                   <div class="col-md-3">
                                       
                                       
                                                 <div class="form-group">
                                                <label for="suppl_location" class="col-lg-4 control-label">Location</label>
                                                <div class="col-lg-8">
                                                    <input type="text"  value="<?php echo set_value('suppl_location'); ?>"   id="suppl_location" name="suppl_location" class="form-control form-inp-med " data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                                </div>
                                       
                                            <div class="form-group">
                                                <label for="suppl_address" class="col-lg-4 control-label">Addr</label>
                                                <div class="col-lg-8">
                                                    <textarea  name="suppl_address" class="form-control form-inp-med " style="width: 175px; height: 84px;"  ><?php echo set_value('suppl_address'); ?></textarea>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                       
                                       
                                       <div class="form-group">
                                           <button class="btn btn-sm btn-default"  type="button" id="btn_addcontact"> (+)</button>
                                       </div>
                                       
                                  
                                       
                                       
                                       
                                  </div>
                                            
                                         <div class="col-md-4">
                                             
                                            <div class="form-group "  >
                                                <label for="suppl_parent_id" class="col-lg-4 control-label">Parent</label>
                                                <div class="col-lg-8">
                                                    <select name="suppl_parent_id" id="suppl_parent_id"  style="width: 150px" required>
                                                        <option value="0">No Parent</option>
                                                         <?php foreach($parent_ids as $parent_id): ?>
                                                        <option value="<?php echo $parent_id->id;  ?>" <?php if($parent_id->id==set_value('suppl_parent_id')): echo "selected"; endif; ?>><?php echo $parent_id->name;  ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label for="suppl_rm_id" class="col-lg-4 control-label">RM</label>
                                                <div class="col-lg-8">
                                                    <select name="suppl_rm_id" id="suppl_rm_id"  style="width: 150px" onchange="checkRm(this.value)"  required >
                                                        <option value="">Select</option>
                                                         <?php foreach($rms as $rm): ?>
                                                        <option value="<?php echo $rm->id;  ?>" <?php if($rm->id==set_value('suppl_rm_id')): echo "selected"; endif; ?>><?php echo $rm->name;  ?></option>
                                                        <?php endforeach; ?>
                                                     </select>
                                                       <span class="help-block with-errors"></span>
                                                </div>
                                               
                                                
                                            </div>
                                             
                                             <div class="form-group"  id="other_rm_div">
                                                   <label for="other_rm" class="col-lg-4 control-label">Others</label>
                                                  <div class="col-lg-8">
                                                       <input type="text" name="other_rm" required  data-error="Specify Other RM" disabled="" />
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                             </div>
                                                        
                                            <div class="form-group " >
                                                <label for="suppl_rating" class="col-lg-4 control-label">Rating</label>
                                                <div class="col-lg-8">
                                                    <select name="suppl_rating" id="suppl_rating"  style="width: 150px" required>
                                                        <option value="">Select</option>
                                                        <option value="1"  <?php if(1==set_value('suppl_rating')): echo "selected"; endif; ?>>A</option>
                                                        <option value="2" <?php if(2==set_value('suppl_rating')): echo "selected"; endif; ?>>B</option>
                                                        <option value="3" <?php if(3==set_value('suppl_rating')): echo "selected"; endif; ?>>C</option>
                                                        <option value="4" <?php if(4==set_value('suppl_rating')): echo "selected"; endif; ?>>D</option>
                                                        <option value="5" <?php if(5==set_value('suppl_rating')): echo "selected"; endif; ?>>E</option>
                                                    </select>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                                        
                                            <div class="form-group ">
                                                <label for="suppl_risk_factor" class="col-lg-7 control-label">Overall Pending Risk Factor</label>
                                                <div class="col-lg-5">
                                                    <input type="text"  value="<?php echo set_value('suppl_risk_factor'); ?>"   id="suppl_risk_factor" name="suppl_risk_factor" class="form-control" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                                        
                                            </div>
                                            
                                            <div class="col-md-2">
                                                <label>Map Vendor</label>
                                                <select name="vendors[]" multiple="multiple" style="height: 173px; width: 159px;" data-error="This field is required" required >
                                                       <?php foreach($vendors as $vendor): ?>
                                                        <option value="<?php echo $vendor->id;  ?>" ><?php echo $vendor->vendor_name;  ?></option>
                                                        <?php endforeach; ?> 
                                                </select>
                                            </div>
                                            
                                      

                                          
                                            
                                        </fieldset>
                                      </div>
                                      
                                      <div id="alternateContactDiv">
                                                    
                                          </div>
                                      
                                      
                                      <hr/> 
                                      <p><button  type="button" class="btn btn-sm btn-default" id="btn_map_operators">Add More Operators (+) </button></p>
                                
                                          <div id="mappingdiv">
                                              
                                              
                                             
                                              
                                          </div>    
                                          
                                          <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                            </div>
                                    </form>
                                    
                                    
                                </div> <!-- End Panel Body -->
                                
                                
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>