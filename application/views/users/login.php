<?php $this->load->view('common/header'); ?>
<div class="container">    
    <div id="loginbox" style="margin-top:50px;" class="col-lg-4 col-lg-offset-4">                    
        <div class="panel panel-info" >
            
            <div class="panel-heading">
                <div class="panel-title">Sign In</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >
                <?php if($this->session->flashdata('error')):  ?>
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?>
                </div>
               <?php endif;  ?> 

                <form id="loginform" class="form-horizontal" role="form" method="post" action="<?php echo base_url('users/login'); ?>">

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control input-sm" name="username" value="" placeholder="username">                                        
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control input-sm" name="password" placeholder="password">
                    </div>

                    <div style="margin-top:10px" class="form-group">
                        <div class="col-sm-12 controls">
                            <button type="submit" class="btn btn-primary btn-sm" >Login</button>
                        </div>
                    </div>


                </form>     

            </div>          
            
        </div>  
    </div>

</div>


<?php $this->load->view('common/footer'); ?>
