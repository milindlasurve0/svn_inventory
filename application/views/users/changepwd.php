<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Change Password</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Reset Password</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                                                  
                                    <form class="form-horizontal" method="post" action="<?php echo base_url('users/changepwd'); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <div class="form-group">
                                                <label for="old_pwd" class="col-lg-2 control-label">Old Password</label>
                                                <div class="col-lg-10">
                                                    <input type="password"value="<?php // echo set_value('old_pwd'); ?>"   id="old_pwd" name="old_pwd" class="form-control form-inp" data-error="This field is required" required>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="new_pwd" class="col-lg-2 control-label">New Password</label>
                                                <div class="col-lg-10">
                                                    <input type="password"value="<?php // echo set_value('new_pwd'); ?>"   id="new_pwd" name="new_pwd" class="form-control form-inp" data-error="This field is required" required>
                                                    <!--<span class="help-block with-errors"></span>-->
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="confirm_pwd" class="col-lg-2 control-label">Confirm Password</label>
                                                <div class="col-lg-10">
                                                    <input type="password"value="<?php // echo set_value('confirm_pwd'); ?>"   id="confirm_pwd" name="confirm_pwd" class="form-control form-inp" data-error="This field is required" required>
                                                    <!--<span class="help-block with-errors"></span>-->
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                            <label for="" class="col-lg-3 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button class="btn btn-default btn-sm" type="reset">Reset</button>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>