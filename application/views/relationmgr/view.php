<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

<div class="container">
    
    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>View All Relationship Manager</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Table with actions</div>
                                </div>
                                <div class="bootstrap-admin-panel-content">
                                    <table class="table bootstrap-admin-table-with-actions">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Address</th>
                                                <th>Email ID</th>
                                                <th>Created at</th>
                                                <th>Modified at</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($rms)):  ?>
                                            <?php foreach ($rms as $rm):  ?>
                                            <tr>
                                                <td><?php echo $rm->id;  ?></td>
                                                <td><?php echo $rm->name;  ?></td>
                                                <td><?php echo $rm->mobile;  ?></td>
                                                <td><?php echo $rm->address;  ?></td>
                                                <td><?php echo $rm->email_id;  ?></td>
                                                <td><?php echo $rm->created_at;  ?></td>
                                                <td><?php echo $rm->updated_at;  ?></td>
                                                
                                                <td class="actions">
                                                    <a href="<?php echo base_url('relationmgr/edit/'.$rm->id); ?>">
                                                        <button class="btn btn-xs btn-primary">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                            Edit
                                                        </button>
                                                    </a>
<!--                                                    <a href="#">
                                                        <button class="btn btn-sm btn-success">
                                                            <i class="glyphicon glyphicon-ok-sign"></i>
                                                            Confirm
                                                        </button>
                                                    </a>-->
<!--                                                    <a href="#">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="glyphicon glyphicon-bell"></i>
                                                            Notify
                                                        </button>
                                                    </a>-->
                                                    <a href="<?php echo base_url('relationmgr/delete/'.$rm->id); ?>" onClick="javascript:return confirm('Are you sure to Delete ?');">
                                                        <button class="btn btn-xs btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                           <?php endforeach; ?> 
                                            
                                            <?php else: echo  "No records Found"; endif;?>
                                            
                                        </tbody>
                                    </table>
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
        
</div>
<?php $this->load->view('common/footer');  ?>