<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script>$(document).ready(function(){$('#sandbox-container .input-daterange').datepicker({format: "yyyy-mm-dd",startDate: "-365d",endDate: "1d",todayHighlight: true,orientation: "top right"});});</script>
<style>.pagination{float: right}</style>
<div class="container-fluid">
  
   <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search Order</h3>
           </div>
        </div>
    </div>
    
    
       <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <div class="row">
                        <form action="search" method="get">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <div class="col-lg-4"><label>Date range</label></div>
                                    <div class="col-lg-7" id="sandbox-container">
                                            <div class="input-daterange input-group" id="datepicker">
                                                <input type="text" class="input-sm form-control" name="start" value="<?php echo $this->input->get('start'); ?>"  required=""/>
                                                <span class="input-group-addon">to</span>
                                                <input type="text" class="input-sm form-control" name="end" value="<?php echo $this->input->get('end'); ?>" required="" />
                                            </div>
                                       </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-3"><label>Operator</label></div>
                                   <div class="col-lg-7"> <select name="operator_id" id="operator_id"  style="width: 150px" >
                                      <option value="">Select</option>
                                       <option value="all"  <?php if($this->input->get('operator_id')=="all"):  echo "Selected";  endif;?>>All</option>
                                       <?php foreach($operators as $operator): ?>
                                      <option value="<?php echo $operator['id'];  ?>"  <?php if($this->input->get('operator_id')==$operator['id']):  echo "Selected";  endif;?>><?php echo $operator['name'];  ?></option>
                                      <?php endforeach; ?>
                                       </select></div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3">
                                  <div class="form-group">
                                <div class="col-lg-5"><label for="supplier_id" class="col-lg-5 control-label">Supplier</label></div>
                                     <div class="col-lg-6"><?php // echo count($suppliers); ?>
                                           <select name="supplier_id" id="supplier_id"  style="width: 150px" >
                                              <option value="">Select</option>
                                               <?php foreach($suppliers as $supplier): ?>
                                              <option value="<?php echo $supplier->id;  ?>"  <?php  if($this->input->get('supplier_id')==$supplier->id): echo "selected";  endif; ?>><?php echo $supplier->name;  ?></option>
                                              <?php endforeach; ?>
                                          </select>
                            </div>
                            </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            
                             <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-5"><label>Order exists</label></div>
                                    <div class="col-lg-7"> 
                                        <input type="checkbox" name="order_exists" id="order_exists"  value="1" <?php if($this->input->get('order_exists')): echo "checked"; endif; ?> />
                                   </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-1" style="float: right;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                        </div>
                    </div>
                </div>
            </div>
 </div>
    
        <?php if(!empty($orders)):  ?>
     <p>Total rows : <?php echo count($orders); ?></p>
            <div class="row">
                <div class="col-lg-12">
                       <h3>Order history for date : ( <?php echo $this->input->get('start'); ?> - <?php echo $this->input->get('end'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Orders</div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                       <th>#</th>
                                       <th>Date</th>
                                       <th>Order</th>
                                       <th>Supplier</th>
                                       <th>Operator</th>
                                       <th>Order</th>
                                       <th>bounds</th>
                                       <th>To pay</th>
                                       <th>UTR</th>
                                       <th>Incoming</th>
                                       <th>Pending</th>
                                       <th>P.Amt</th>
                                       <th>Comm</th>
                                   </tr>
                               </thead>
                               
                               <tbody>
                                   <?php $totalOrderAmt=0;$totalAmountbyBounds=0;$totalPaymentAmount=0;$totalIncoming=0;$totalPending=0;$totalpamt=0;$totalRIA=0;?>
                                   <?php foreach($orders as $order): ?>
                                       <?php
                                       $totalOrderAmt+=$order->amount;
                                       $totalAmountbyBounds+=$order->amount_by_bounds;
                                       $totalPaymentAmount+=$order->to_pay;
                                       $totalIncoming+=$order->incoming; 
                                       $totalPending+= $order->pending;
                                       ?>
                                   <tr>
                                       <td><?php echo $order->order_id?$order->order_id:"--"; ?></td>
                                       <td><?php echo $order->pending_date; ?></td>
                                       <td><?php if($order->is_approved=="1"): ?><span class="glyphicon glyphicon-ok text-success"></span><?php endif;  ?></td>
                                       <td><?php echo $order->suppliername; ?></td>
                                       <td><?php echo $order->operatorname; ?></td>
                                       <td><?php echo $order->amount; ?></td>
                                       <td><?php echo $order->amount_by_bounds; ?></td>
                                       <td><?php echo $order->to_pay; ?></td>
                                       <td><?php echo $order->txnid; ?></td>
                                       <td><a href="/orders/viewlastdayincoming/<?php echo $order->supplier_operator_id; ?>/<?php echo urlencode($order->vendorids); ?>/<?php echo $order->operator_id ?>/<?php echo $order->pending_date; ?>" target="_blank"><?php echo $order->incoming?$order->incoming:"NA"; ?></a></td>
                                       <td><a target="_blank"  href="/orderhistorys/pendinghistory/<?php echo $order->supplier_operator_id; ?>"><?php echo $order->pending?$order->pending:"NA"; ?></a></td>
                                       <td><?php  $pamt=  isUpperBoundH($order->supplier_operator_id)?($order->pending/((100+$order->margin)/100)):$order->pending*((100-$order->margin)/100);  echo round($pamt,2);  $totalpamt+=$pamt;  ?></td>
                                       <td><a target="_blank"  href="/orderhistorys/comments?soid=<?php echo $order->supplier_operator_id; ?>&date=<?php echo $order->pending_date; ?>&order_id=<?php echo $order->order_id; ?>">View</a></td>
                                       
                                   </tr>
                                   <?php endforeach;  ?>
                                      </tbody>
                               <tfoot>
                                   <tr>
                                       <td></td><td></td><td></td><td></td><td></td><th><?php echo number_format($totalOrderAmt,2) ?></th>
                                       <th><?php echo number_format($totalAmountbyBounds,2) ?></th>
                                       <th><?php echo number_format($totalPaymentAmount,2) ?></th>
                                       <th></th>
                                       <th><?php echo number_format($totalIncoming,2) ?></th>
                                       <th><?php echo number_format($totalPending,2); ?></th>
                                       <th><?php echo number_format($totalpamt,2); ?></th>
                                       <td></td>
                                       <!--<th><?php echo number_format($totalRIA,2); ?></th>-->
                                   </tr>
                               </tfoot>
                            
                           </table>
                        </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($orders) && $this->input->get()): echo "No Orders Detected ";  endif; ?>
    
</div>    
<?php $this->load->view('common/footer');  ?>