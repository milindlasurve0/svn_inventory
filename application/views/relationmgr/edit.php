<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Edit  Relationship Manager</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Relationship Manager</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                              
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                <?php echo validation_errors('<p>','</p>'); ?>    
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('relationmgr/edit/'.$rm->id); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <div class="form-group">
                                                <label for="rm_name" class="col-lg-2 control-label">Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('rm_name')?set_value('rm_name'):$rm->name; ?>"   id="rm_name" name="rm_name" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="rm_email" class="col-lg-2 control-label">Email</label>
                                                <div class="col-lg-10">
                                                    <input type="email" id="rm_email" value="<?php echo set_value('rm_email')?set_value('rm_email'):$rm->email_id; ?>"   name="rm_email" class="form-control form-inp" data-error="Enter a valid email" required>
                                                     <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="rm_contact" class="col-lg-2 control-label">Contact</label>
                                                <div class="col-lg-10">
                                                    <input type="text" pattern="[0-9]{1,15}" value="<?php echo set_value('rm_contact')?set_value('rm_contact'):$rm->mobile; ?>"    maxlength="15" id="rm_contact"  name="rm_contact" class="form-control form-inp" data-error="Enter a valid Contact"  required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="rm_address" class="col-lg-2 control-label">Address</label>
                                                <div class="col-lg-10">
                                                    <textarea  name="rm_address" class="form-control form-inp" style="width: 305px; height: 94px;" required data-error="Enter a valid Address" ><?php echo set_value('rm_address')?set_value('rm_address'):$rm->address; ?></textarea>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Update</button>
                                            
                                            <a href="<?php echo base_url('relationmgr/view'); ?>">Back</a>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>