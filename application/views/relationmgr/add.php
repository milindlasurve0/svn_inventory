<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h3>Add Relationship Manager</h3>
                            </div>
                        </div>
              </div>
            
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default bootstrap-admin-no-table-panel">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Relationship Manager</div>
                                </div>
                                <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                    
                                
                                    
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                                </div>
                                <?php endif;  ?> 
                                    
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                                </div>
                                <?php endif;  ?>    
                                    
                                <?php if(validation_errors()):  ?>
                                <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <p><strong>Errors ! </strong><?php echo validation_errors('<p>','</p>'); ?> </p>
                              
                                </div>
                                <?php endif;  ?>     
                                    
                                    <form class="form-horizontal" data-toggle="validator" method="post" action="<?php echo base_url('relationmgr/add'); ?>">
                                        <fieldset>
                                            <legend></legend>
                                            
                                            <div class="form-group">
                                                <label for="rm_name" class="col-lg-2 control-label">Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text"value="<?php echo set_value('rm_name'); ?>"   id="rm_name" name="rm_name" class="form-control form-inp" data-error="This field is required" required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="rm_email" class="col-lg-2 control-label">Email</label>
                                                <div class="col-lg-10">
                                                    <input type="email" id="rm_email" value="<?php echo set_value('rm_email'); ?>"   name="rm_email" class="form-control form-inp" data-error="Enter a valid email" required>
                                                     <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="rm_contact" class="col-lg-2 control-label">Contact</label>
                                                <div class="col-lg-10">
                                                    <input type="text" pattern="[0-9]{1,15}" value="<?php echo set_value('rm_contact'); ?>"    maxlength="15" id="rm_contact"  name="rm_contact" class="form-control form-inp" data-error="Enter a valid Contact"  required>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="rm_address" class="col-lg-2 control-label">Address</label>
                                                <div class="col-lg-10">
                                                    <textarea  name="rm_address" class="form-control form-inp" style="width: 305px; height: 94px;" required data-error="Enter a valid Address" ><?php echo set_value('rm_address'); ?></textarea>
                                                    <span class="help-block with-errors"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                            <label for="" class="col-lg-2 control-label"></label>
                                            <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                            <button class="btn btn-default btn-sm" type="reset">Reset</button>
                                            </div>
                                            
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
</div>
<?php $this->load->view('common/footer');  ?>