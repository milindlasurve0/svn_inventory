<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>

<script src="<?php echo base_url('public/js/order.js?'.time()); ?>"></script>

<div class="container-fluid">
    
     <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Generate Order</h2>
        </div>
    </div>
    
      <div class="row">
          <div class="col-lg-10 col-md-offset-2">
                    <div class="col-lg-3 text-center">
                        <div class="panel panel-default tile-header" id="tlda">
                               <h4 class="orderText">Last Day Sale</h4>
                               <span></span>
                          </div>
                     </div>
                    <div class="col-lg-3 text-center">
                         <div class="panel panel-default tile-header"  id="tldfp">
                                <h4 class="orderText">Failure %</h4>
                                <span>?</span>
                          </div>
                      </div>
                    <div class="col-lg-3 text-center">
                        <div class="panel panel-default tile-header" id="tes">
                                <h4 class="orderText">Expected Sale For the day</h4>
                                <span>?</span>
                          </div>
                      </div>
              
                    <div class="col-lg-3 text-center">
                         <div class="form-group">
                            <label for="order_days" class="col-lg-6 control-label">Order Days</label>
                            <div class="col-lg-6">
                                 <select name="order_days" id="order_days"  style="width: 75px" required>
                                    <option value="1.8">1.8</option>
                                    <option value="2.00">2.0</option>
                                    <option value="2.75">2.75</option>
                                     <option value="3.00">3.0</option>
                                     <option value="3.5">3.5</option>
                                    <option value="4.00">4.0</option>
                                </select>
                            </div>
                        </div>
                      </div>
              
          </div>
      </div>
    
    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="operator_id" class="col-lg-5 control-label">Select Operator</label>
                            <div class="col-lg-6">
                                 <select name="operator_id" id="operator_id"  style="width: 150px" required>
                                    <option value="">Select</option>
                                     <?php foreach($operators as $operator): ?>
                                    <option value="<?php echo $operator['id'];  ?>"><?php echo $operator['name'];  ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-1 loadingbar"></div>
                    
                    <div class="col-lg-1">
                        <div class="form-group">
                            <input type="button" class="btn btn-default btn-sm" value="submit"  id="getOrder" />
                        </div>
                    </div>
                    
                    <div class="col-lg-3" id="existsmessage">
                        
                    </div>
                    
                    <div id="orderpanel">
                        <div class="row">
                            <div class="col-lg-12">
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owlds" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Last day Sale</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owfp" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Failure</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owes" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Exp Sale</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owcb" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Current Bal</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owto" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Total Order</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owsftd" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Shrt fr  day</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owldsfa" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Api Sale</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owaas" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Avg. Api Sale</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 text-center notetiles">
                                    <div class="panel panel-default" id="owp" style="display:none;">
                                        <div class="panel-body">
                                            <h4 class="orderText">Pending</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-lg-12">
                                
                                
                                <div class="row" id="disapprovedsupplierlistdiv" style="display: none">
                                    <div class="col-lg-12">
                                        <h2>Disapproved</h2>    
                                        <table class="table table-bordered" id="disapprovedsupplierlist">
                                             <thead>
                                            <th>#</th>
                                            <th>Supplier</th>
                                            <th>Margin</th>
                                            <th>Base Sale</th>
                                            <th>L.day Inc</th>
                                            <th>Opening</th>
                                            <th>Pending</th>
                                            <th>Order</th>
                                            <th>Per Sim Sale</th>
                                            <th>Sims</th>
                                            <th>Last Day Sale</th>
                                            <th>Blocked Bal</th>
                                            <th>Comm</th>
                                            <th>Days</th>
                                            <th>OAR</th>
<!--                                            <th>OP</th>
                                            <th>TEO</th>-->
                                             <th>Topay</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                
                                
                                <div id="regularsupplierlist">
                                    <h2>Recommended/Approved</h2>    
                                <table class="table table-bordered" id="supplierlist">
                                    <thead>
                                            <tr id="recommededsuppliertr">
                                            <th>#</th>
                                            <th>Supplier</th>
                                            <th>Margin</th>
                                            <th>Base Sale</th>
                                            <th>L.day Inc</th>
                                            <th>Opening</th>
                                            <th>Pending</th>
                                            <th>Order</th>
                                            <th>Per Sim Sale</th>
                                            <th>Sims</th>
                                            <th>Last Day Sale</th>
                                            <th>Block Bal</th>
                                            <th>Comm</th>
                                            <th>Days</th>
                                            <th>OAR</th>
<!--                                            <th>OP</th>
                                            <th>TEO</th>-->
                                             <th>Topay</th>
                                            </tr>
                                    </thead>
                                    
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 text-right" style="padding-bottom: 10px;">
                                        <button class="btn btn-default btn-warning btn-sm btn-removefromorder pull-left">Remove from order</button>
                                        <div id="output_message" style="text-align: left;"></div>
                                        <button class="btn btn-default btn-success btn-sm" id="sendforapproval">Send for approval</button>
                                    </div>
                                </div>
                                
                          
                                
                                <div class="row" id="occasionalsupplierlistdiv" style="display: none">
                                    <div class="col-lg-12">
                                        <h2>Optional</h2>    
                                        <table class="table table-bordered" id="occasionalsupplierlist">
                                             <thead>
                                            <th>#</th>
                                            <th>Supplier</th>
                                            <th>Margin</th>
                                            <th>Base Sale</th>
                                            <th>L.day Inc</th>
                                            <th>Opening</th>
                                            <th>Pending</th>
                                            <th>Order</th>
                                            <th>Per Sim Sale</th>
                                            <th>Sims</th>
                                            <th>Last Day Sale</th>
                                            <th>Block Bal</th>
                                            <th>Comm</th>
                                            <th>Days</th>
                                            <th>OAR</th>
<!--                                            <th>OP</th>
                                            <th>TEO</th>-->
                                            <th>Topay</th>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    
                                     <div class="col-lg-12">
                                        <button class="btn btn-default btn-warning btn-sm btn-movetoorder">Move to Order</button>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>    
    
<div class="modal" id="neworderamount">
<div class="modal-dialog" style="width: 300px;">
<div class="modal-content">
  <div class="modal-header">
      <h4 class="modal-title">Update Order Amount</h4>
  </div>
  <div class="modal-body">
      <p>Old Order Amount :  <span class="oldorderamt"></span></p>
    <p>Enter New Order Amount</p>
    <input type="hidden" name="so_id" id="so_id" value="" />
    <input type="hidden" name="order_operator_id" id="order_operator_id" value="" />
    <input type="text" name="order_amount" value=""  class="form-control form-inp"/>
    <span class="words"></span>
  </div>
  <div class="modal-footer">
      <div id="orderamtmsg" style="text-align: left"></div>
      <button class="btn btn-sm btn-default btn-primary" id="updateorderamount">Update</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
<div class="modal" id="addcomment">
<div class="modal-dialog" style="width: 300px;">
<div class="modal-content">
  <div class="modal-header">
      <h4 class="modal-title">Add Comment</h4>
  </div>
  <div class="modal-body">
       <div class="row">
          <div class="col-lg-12" id="loadcomments">
              
          </div>
      </div>
      <input type="hidden" name="comment_so_id" id="comment_so_id" value="" />
        <input type="hidden" name="comment_order_operator_id" id="comment_order_operator_id" value="" />
      <textarea  name="comment" id="comment"></textarea>
  </div>
  <div class="modal-footer">
      <div id="ordercommentmsg" style="text-align: left"></div>
      <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Update</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
    
</div>


<?php $this->load->view('common/footer');  ?>


