<?php

if(!empty($incomings)):
    $totalIncoming=0;$totalBalance=0;$totalOpening=0;$totalSale=0;$totalClosing=0;
 ?>   
<h1>Supplier Info for date : <?php echo $this->uri->segment('6')?$this->uri->segment('6'):date('Y-m-d',strtotime('-1 day')); ?></h1>
<table border="1" style="font-size: 12px;text-align: right;">
    <tr>
    <th>supplier</th>
    <th>operator</th>
    <th>modemname</th>
    <th>mobile</th>
    <th>sync_date</th>
    <th>sync_timestamp</th>
    <th>inc</th>
    <th>diff</th>
    <th>Server diff</th>
    <th>balance</th>
    <th>opening</th>
    <th>incoming</th>
    <th>closing</th>
    <th>sale</th>
    <th>is Blocked</th>
    <th>is active</th>
    </tr>
    
   <?php foreach($incomings as $value): $totalIncoming+=$value->incoming;$totalBalance+=$value->balance;$totalOpening+=$value->opening;$totalSale+=$value->sale;$totalClosing+=$value->closing;  ?>
    <tr>
        <td><?php echo $value->supplier;  ?></td>
        <td><?php echo $value->operator;  ?></td>
        <td><?php echo $value->modemname;  ?></td>
        <td><?php echo $value->mobile;  ?></td>
        <td><?php echo $value->sync_date;  ?></td>
        <td><?php echo $value->sync_timestamp;  ?></td>
        <td><?php echo $value->inc;  ?></td>
        <td>
            <?php 
            $date=$this->uri->segment('6')?$this->uri->segment('6'):date('Y-m-d',strtotime('-1 day'));
            if(strtotime($date)==strtotime('today')):
                echo -($value->opening-$value->balance+$value->incoming-$value->sale+$value->inc);
            else:
                 echo -($value->opening-$value->closing+$value->incoming-$value->sale+$value->inc);
            endif;
            ?>
        </td>
        <td><?php echo $value->server_diff;  ?></td>
        <td><?php echo $value->balance;  ?></td>
        <td><?php echo $value->opening;  ?></td>
        <td><?php echo $value->incoming;  ?></td>
        <td><?php echo $value->closing;  ?></td>
        <td><?php echo $value->sale;  ?></td>
        <td><?php if($value->block=='1'): echo "yes"; else:  echo "No"; endif; ?></td>
        <td><?php if($value->active_flag=='1'): echo "yes"; else:  echo "No"; endif; ?></td>
    </tr>
    <?php endforeach; ?>
    
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?php echo number_format($totalBalance,2); ?></td>
        <td><?php echo number_format($totalOpening,2); ?></td>
        <th><?php echo number_format($totalIncoming,2); ?></th>
        <td><?php echo number_format($totalClosing,2); ?></td>
        <td><?php echo number_format($totalSale,2); ?></td>
    </tr>
<!--    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Last Day Order</td>
        <th>-<?php //echo number_format($lastdayorderamt,2); ?></th>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="2">Pending Amt : <?php //echo date('Y-m-d',strtotime('-2 days')); ?></td>
        <th><?php //echo number_format($lastavaliablepending,2); ?></th>
        <td></td>
    </tr>
    <tr style="background-color: yellow;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Final Pending</td>
        <th><?php //echo number_format($totalIncoming-$lastdayorderamt+$lastavaliablepending,2); ?></th>
        <td></td>
    </tr>-->
</table>    
<?php
else:
    echo "No data avaliable for previous date";
endif;
