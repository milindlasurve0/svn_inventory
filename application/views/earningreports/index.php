<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js/earningreport.js?'.time()); ?>"></script>
<style>
    #earningtablelevel1 th,td { text-align: center; }
    #earningtablelevel1 th { background-color: lightyellow; }
</style>

<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Earning Report</h3>
           </div>
         <form method="get" action="/earningreports/get">
                  <div class="col-lg-3" id="sandbox-container">
                
                                          <div class="input-daterange input-group" id="datepicker">
                                              <input type="text" class="form-control form-inp-med" placeholder="From" name="from_history_date"  id="from_history_date" value="<?php echo $from_date; ?>" />
                                              <span class="input-group-addon smallheight">to</span>
                                              <input type="text" class="form-control form-inp-med" placeholder="To"  name="to_history_date"  id="to_history_date"  value="<?php echo $to_date ; ?>"  />
                                          </div>

                  </div>
      
                <div class="col-lg-2"><button type="submit" class="btn btn-default btn-sm">Submit</button></div>
        </form>        
               <div class="clearfix"></div>
                <div class="col-lg-6 col-lg-offset-3" style="padding: 10px;"> <h3 style="margin:0px;">Earning report for date (<?php echo date('d/m/Y',  strtotime($from_date)); ?> - <?php echo date('d/m/Y',  strtotime($to_date)); ?>)</h3></div>
        
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-lg-12" style="font-size: 12px;">
            <table class="table table-bordered table-hover" id="earningtablelevel1">
                    <thead>
                             <th>Operator</th>
                             <th>Ideal Margin</th>
                             <th>Expected Margin</th>
                             <th>Actual Margin</th>
                             <th>Planning Loss</th>
                             <th>Operational Loss</th>
                             <th>Earning</th>
                             <th>Reasons</th>
                    </thead>
                    <tbody>
                        <?php $totalplanningloss=0;$totaloperationaloss=0;$totalPlanningNegative=0;$totalPlanningPositive=0;$totalOperationalNegative=0;$totalOperationalPositive=0; $totalSale=0;$totalEarning=0; ?>
                        <?php foreach($earningReports as $earning):
                            $totalplanningloss+=$earning['planning_loss'];$totaloperationaloss+=($earning['operational_loss']);
                            $totalPlanningNegative+=$earning['planning_loss']<0?$earning['planning_loss']:0;$totalPlanningPositive+=$earning['planning_loss']>0?$earning['planning_loss']:0; 
                            $totalOperationalNegative+=$earning['operational_loss']<0?$earning['operational_loss']:0;$totalOperationalPositive+=$earning['operational_loss']>0?$earning['operational_loss']:0; 
                            $totalSale += $earning['totalActualSale']>0?$earning['totalActualSale']:0;
                            $totalEarning += $earning['totalActualSale']*($earning['totalActualMargin']-$earning['operationalcost']);
                            ?>
                        <tr>
                            <td><?php if($rangedays==1): ?><a  target="_blank" href="/earningreports/get?operator_id=<?php echo $earning['operator_id'];  ?>&from_history_date=<?php echo $from_date; ?>&to_history_date=<?php echo $to_date;  ?>"><?php echo $earning['operator_name']; ?></a><?php else: echo $earning['operator_name'];  endif;  ?></td>
                            <td><?php echo number_format($earning['totalIdealMargin'],2) ?></td>
                            <td><?php echo number_format($earning['totalExpectedMargin'],2) ?></td>
                            <td><?php echo number_format($earning['totalActualMargin'],2) ?></td>
                            <td><?php echo number_format($earning['planning_loss'],2); ?></td>
                            <td><?php echo number_format($earning['operational_loss'],2); ?></td>
                            <td><?php echo number_format($earning['totalActualMargin']-$earning['operationalcost'],2); ?></td>
                            <td><?php if($rangedays==1): ?><a class="highlightitle" title="<?php echo $earning['messages']; ?>" data-type="2" data-soid="0" data-oprid="<?php echo $earning['operator_id']; ?>"  data-toggle='modal' href='#addnotesdiv'><i class="fa fa-comments-o fa-2"></i></a><?php else: echo "NA";  endif;  ?> <?php if($rangedays==1): echo  "(".$earning['messagecount'].")";  endif;  ?> </td>
                        </tr>
                        <?php endforeach;  ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>Totals</th>
                            <th>
                                ( <?php echo number_format($totalPlanningNegative,2); ?> )&nbsp;/&nbsp;( <?php echo number_format($totalPlanningPositive,2); ?> )
                                
                                <?php //echo number_format($totalplanningloss,2) ?>
                            </th>
                            <th><?php //echo number_format($totaloperationaloss,2) ?>
                               ( <?php echo number_format($totalOperationalNegative,2); ?> )&nbsp;/&nbsp;( <?php echo number_format($totalOperationalPositive,2); ?> )
                            </th>
                             <th><?php echo number_format($totalEarning/$totalSale,2) . " (". $totalSale. ")";?></th>
                             <th>&nbsp;</th>
                        </tr>
                    </tfoot>
            </table>
        </div>
    </div>
   
</div>
<?php  $this->load->view('common/notes');  ?>
<?php $this->load->view('common/footer');  ?>