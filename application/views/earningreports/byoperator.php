<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script src="<?php echo base_url('public/js/earningreport.js?'.time()); ?>"></script>
<style>
    .tblearning th,td { text-align: center; }
    .tblearning th { text-align: center;background-color: lightyellow; }
      #earningtablelevel1 th { background-color: lightyellow; }
</style>

<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Earning Report</h3>
           </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2" style="font-size: 12px;">
            <table class="table table-bordered tblearning" >
                <thead>
                    <tr>
                              <th>Operator</th>
                              <th>Ideal Margin</th>
                              <th>Expected Margin</th>
                              <th>Actual Margin</th>
                              <th>Planning Loss</th>
                              <th>Operational Loss</th>
                              <th>Earning</th>
                              <th>Reasons</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($earningReports as $earning):   ?>
                    <tr>
                            <td><?php echo $earning['operator_name']; ?></td>
                            <td><?php echo number_format($earning['totalIdealMargin'],2) ?></td>
                            <td><?php echo number_format($earning['totalExpectedMargin'],2) ?></td>
                            <td><?php echo number_format($earning['totalActualMargin'],2) ?></td>
                            <td><?php echo number_format($earning['planning_loss'],2); ?></td>
                            <td><?php echo number_format($earning['operational_loss'],2); ?></td>
                            <td><?php echo number_format($earning['totalActualMargin']-$earning['operationalcost'],2); ?></td>
                            <td><a class="highlightitle" title="<?php echo $earning['messages']; ?>"  data-type="2" data-soid="0" data-oprid="<?php echo $earning['operator_id']; ?>"  data-toggle='modal' href='#addnotesdiv'><i class="fa fa-comments-o fa-2"></i></a> (<?php echo $earning['messagecount'];  ?>)</td>
                    </tr>
                    <?php endforeach;  ?>
                </tbody>
            </table>
        </div>
    </div>
    
    
     <div class="row">
        <div class="col-lg-12 " style="font-size: 12px;">
            <table class="table table-bordered tblearning table-hover" >
                <thead>
                    <tr>
                              <th>Modem</th>
                              <th>Supplier</th>
                              <th>Margin</th>
                              <th>Ideal Margin</th>
                              <th>Expected Margin</th>
                              <th>Actual Margin</th>
                              <th>Expected Sale</th>
                              <th>Actual Sale</th>
                              <th>Difference</th>
                              <th>Margin Deviation</th>
                              <th>Reasons</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalChange=0;$totalIdealMargin=0;$totaltargetMargin=0;$totalactualWeight=0;$totalTargetSale=0;$totalActualSale=0; ?>
                    <?php foreach($results as $row): $totalIdealMargin+=$row['baseweight'];$totaltargetMargin+=$row['targetweight'];$totalactualWeight+=$row['actualweight'];$totalTargetSale+=$row['target'];$totalActualSale+=$row['sale'];   endforeach; ?>
                    <?php $increment = ($totalActualSale - $totalTargetSale)/$totalTargetSale;?>
                    
                    <?php foreach($results as $row):     ?>
                    <tr>
                            <td>
                                <?php 
                                if($row['is_api']=="0"):
                                     echo !empty($row['actualsalemodemnames'])?$row['actualsalemodemnames']:$row['vendoridsnames'];
                                else:
                                    echo $row['suppliername'];    
                                endif;
                                ?>
                                
                                <?php 
                                $expected_basedonsale = $row['target']*(1 + $increment);
                                $increment_margin = round(($row['margin'] - $totalactualWeight)*($row['sale'] - $expected_basedonsale)/$totalActualSale,3);
                                $totalChange += ($increment_margin < -0.005 || $increment_margin > 0.005)?$increment_margin:0;
                                ?>
                            </td>
                            <td><?php echo $row['suppliername'];  ?></td>
                            <td><?php echo $row['margin'];  ?></td>
                            <td><?php echo number_format($row['baseweight'],2) ?></td>
                            <td><?php echo number_format($row['targetweight'],2) ?></td>
                            <td><?php echo number_format($row['actualweight'],2) ?></td>
                            <td><?php echo number_format($row['target'],2) ?></td>
                            <td><a target="_blank"  href="/orders/viewlastdayincoming/<?php echo $row['supplier_operator_id']; ?>/<?php echo $row['vendorids']; ?>/<?php echo $row['operator_id']; ?>/<?php echo $this->input->get('from_history_date'); ?>"><?php echo number_format($row['sale'],2); ?></a></td>
                            <td><?php echo number_format($row['target']-$row['sale'],2) ?></td>
                            <td>
                                <?php
                                        if($increment_margin < 0 && $increment_margin < -0.005) echo "-<span style='color:red;font-size:10px'>".$increment_margin."%</span>";
                                         else if($increment_margin > 0 && $increment_margin > 0.005) echo "+<span style='color:green;font-size:10px'>".$increment_margin."%</span>";
                            ?>
                            </td>
                             <td><?php if($rangedays==1): ?><a class="highlightitle" title="<?php echo $row['messages']; ?>"  data-type="1" data-soid="<?php echo $row['supplier_operator_id']; ?>" data-oprid="<?php echo $row['operator_id']; ?>"  data-toggle='modal' href='#addnotesdiv'><i class="fa fa-comments-o fa-2"></i></a><?php else: echo "NA";  endif;  ?> (<?php echo $row['messagecount'];  ?>)</td>
                    </tr>
                    <?php endforeach;  ?>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th><?php echo number_format($totalIdealMargin,2) ?></th>
                            <th><?php echo number_format($totaltargetMargin,2) ?></th>
                            <th><?php echo number_format($totalactualWeight,2) ?></th>
                            <th><?php echo number_format($totalTargetSale,2) ?></th>
                            <th><?php echo number_format($totalActualSale,2) ?></th>
                            <th><?php echo number_format($totalTargetSale-$totalActualSale,2) ?></th>
                             <th><?php if($totalChange < 0) echo "-<span style='color:red;font-size:10px'>".$totalChange."%</span>";
                            else if($totalChange > 0) echo "+<span style='color:green;font-size:10px'>".$totalChange."%</span>";
                            ?></th>
                            
                             <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php  $this->load->view('common/notes');  ?>
<?php $this->load->view('common/footer');  ?>
    
    