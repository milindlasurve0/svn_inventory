<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js/pendingreport.js?'.time()); ?>"></script>
<div class="container">
    <div class="row">
        <form method="get" action="/reports/pendingreport">
        <div class="form-horizontal">
            <div class="form-group">

                <div class="col-lg-3" id="sandbox-container">
                
                                          <div class="input-daterange input-group" id="datepicker">
                                              <input type="text" class="form-control form-inp-med" name="from_history_date"  id="from_history_date" value="<?php echo $this->input->get('from_history_date')?$this->input->get('from_history_date'):"" ; ?>" />
                                              <span class="input-group-addon smallheight">to</span>
                                              <input type="text" class="form-control form-inp-med" name="to_history_date"  id="to_history_date"  value="<?php echo $this->input->get('to_history_date')?$this->input->get('to_history_date'):"" ; ?>"  />
                                          </div>

                  </div>
                
                <div class="col-lg-2"><button type="submit" class="btn btn-default btn-sm">Submit</button></div>
                <div class="clearfix"></div>
                <div class="col-lg-6 col-lg-offset-3"> <h3 style="margin:0px;">Pending report for date (<?php echo date('d/m/Y',  strtotime($from_date)); ?> - <?php echo date('d/m/Y',  strtotime($to_date)); ?>)</h3></div>
            </div>
         </div>
        </form>
    </div>
    <div class="row">
        
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Longterm</th>
                    <th>Shortterm</th>
                    <th>Goodbooks</th>
                    <th>Disputes</th>
                </tr>
            </thead>
            <tbody>
                    <?php foreach($ranges as $key=>$range): ?>
                <tr>
                    <td><a href="/reports/pendingreportbydate/<?php echo $key; ?>" target="_blank" ><?php  echo $key;?></a></td>
                    <td><?php echo number_format($range['lsum'],2); ?></td>
                    <td><?php echo number_format($range['ssum'],2); ?></td>
                    <td><?php echo number_format($range['gsum'],2); ?></td>
                    <td><?php echo number_format($range['dsum'],2); ?></td>
                </tr>
                    <?php endforeach; ?>
            </tbody>
        </table>
            
    </div>
</div>
<?php $this->load->view('common/footer');  ?>