<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('public/js/pendingreport.js?'.time()); ?>"></script>
<div class="container">
    <div class="row">
        <form method="get" action="/reports/pendingreport">
        <div class="form-horizontal">
            <div class="form-group">
             
<!--                <div class="col-lg-2"><input type="text" name="from_history_date" id="from_history_date" placeholder="Select from date" class="form-control form-inp-med smallheight"  style="margin-left: 16px;"  value="<?php echo $this->input->get('from_history_date')?$this->input->get('from_history_date'):"" ; ?>"/></div>
                <div class="col-lg-2"><input type="text" name="to_history_date" id="to_history_date" placeholder="Select to date" class="form-control form-inp-med smallheight"  style="margin-left: 16px;"  value="<?php echo $this->input->get('to_history_date')?$this->input->get('to_history_date'):"" ; ?>"/></div>
          -->
                <div class="col-lg-3" id="sandbox-container">
                <!--                                        <input type="text" name="orderdate" id="orderdate" class="form-control input-md input-sm"  value="<?php echo $this->input->get('orderdate'); ?>"  required />-->
                                          <div class="input-daterange input-group" id="datepicker">
                                              <input type="text" class="form-control form-inp-med" name="from_history_date"  id="from_history_date" value="<?php echo $this->input->get('from_history_date')?$this->input->get('from_history_date'):"" ; ?>" />
                                              <span class="input-group-addon smallheight">to</span>
                                              <input type="text" class="form-control form-inp-med" name="to_history_date"  id="to_history_date"  value="<?php echo $this->input->get('to_history_date')?$this->input->get('to_history_date'):"" ; ?>"  />
                                          </div>

                  </div>
                
                <div class="col-lg-2"><button type="submit" class="btn btn-default btn-sm">Submit</button></div>
                <div class="clearfix"></div>
                <div class="col-lg-6 col-lg-offset-3"> <h3 style="margin:0px;">Pending report for date (<?php echo date('d/m/Y',  strtotime($from_date)); ?> - <?php echo date('d/m/Y',  strtotime($to_date)); ?>)</h3></div>
            </div>
         </div>
        </form>
    </div>
    <div class="row">
        
        <div class="col-lg-4">
            <div class="panel panel-default">

                <div class="panel-heading">Long Term Pendings (<?php echo count($longtermsuppliers); ?>) <span class="pull-right">SUM : <?php echo number_format($lsum,2); ?></span></div>

                <div class="panel-body" style="max-height: 500px; overflow-y: scroll;">
                    <input type="text" id="filterlongterm" class="form-control input-sm" placeholder="Type to search within below records ..."  />
                    <table class="table" id="tbllongtermpending">
                            <thead>
                                <tr>
                                    <th>Supplier</th>
                                    <th>Operator</th>
                                    <th>C.Pending</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($longtermsuppliers as $supplier):  ?>
                                <tr>
                                    <td><a target="_blank" href="/orderhistorys/pendinghistory/<?php echo $supplier['supplier_operator_id']; ?>"><?php echo $supplier['supplier'] ?></a></td>
                                    <td><?php echo $supplier['operator'] ?></td>
                                    <td><?php echo $supplier['currentpending'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                       </table>
                </div>

            </div>
    </div>
        
        <div class="col-lg-4">
            <div class="panel panel-default">

                <div class="panel-heading">Short Term Pendings (<?php echo count($shorttermsuppliers); ?>)  <span class="pull-right">SUM : <?php echo number_format($ssum,2); ?></span></div>

                <div class="panel-body" style="max-height: 500px; overflow-y: scroll;">
                     <input type="text" id="filtershortterm" class="form-control input-sm" placeholder="Type to search within below records ..."  />
                      <table class="table" id="tblshorttermpending">
                            <thead>
                                <tr>
                                    <th>Supplier</th>
                                    <th>Operator</th>
                                    <th>C.Pending</th>
                                </tr>
                            </thead>
                              <tbody>
                                <?php foreach($shorttermsuppliers as $supplier):  ?>
                                <tr>
                                    <td><a target="_blank" href="/orderhistorys/pendinghistory/<?php echo $supplier['supplier_operator_id']; ?>"><?php echo $supplier['supplier'] ?></a></td>
                                    <td><?php echo $supplier['operator'] ?></td>
                                    <td><?php echo $supplier['currentpending'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                       </table>
                </div>

            </div>
    </div>
        
        <div class="col-lg-4">
            <div class="panel panel-default">

                <div class="panel-heading">Good Books (<?php echo count($goodbookssuppliers); ?>) <span class="pull-right">SUM : <?php echo number_format($gsum,2); ?></span></div>

                <div class="panel-body" style="max-height: 500px; overflow-y: scroll;">
                     <input type="text" id="inpgoodbooks" class="form-control input-sm" placeholder="Type to search within below records ..."  />
                      <table class="table" id="tblgoodbookspending">
                            <thead>
                                <tr>
                                    <th>Supplier</th>
                                    <th>Operator</th>
                                    <th>C.Pending</th>
                                </tr>
                            </thead>
                              <tbody>
                                <?php foreach($goodbookssuppliers as $supplier):  ?>
                                <tr>
                                  <td><a target="_blank" href="/orderhistorys/pendinghistory/<?php echo $supplier['supplier_operator_id']; ?>"><?php echo $supplier['supplier'] ?></a></td>
                                    <td><?php echo $supplier['operator'] ?></td>
                                    <td><?php echo $supplier['currentpending'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                       </table>
                </div>

            </div>
    </div>
        
        <div class="col-lg-4">
            <div class="panel panel-default">

                <div class="panel-heading">Disputes (<?php echo count($disputes); ?>) <span class="pull-right">SUM : <?php echo number_format($disputesum,2); ?></span></div>

                <div class="panel-body" style="max-height: 500px; overflow-y: scroll;">
                     <input type="text" id="inpdisputes" class="form-control input-sm" placeholder="Type to search within below records ..."  />
                      <table class="table" id="tbldisputes">
                            <thead>
                                <tr>
                                    <th>Supplier</th>
                                    <th>Operator</th>
                                    <th>Dispute</th>
                                </tr>
                            </thead>
                              <tbody>
                                <?php foreach($disputes as $supplier):  ?>
                                <tr>
                                  <td><?php echo $supplier['supplier'] ?></td>
                                    <td><?php echo $supplier['operator'] ?></td>
                                    <td><?php echo $supplier['disputeamt'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                       </table>
                </div>

            </div>
    </div>
        
    </div>
</div>
<?php $this->load->view('common/footer');  ?>