<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Eliminate Order</h3>
           </div>
        </div>
    </div>
    
      <div class="row">
            <div class="col-lg-12">

                            <?php if ($this->session->flashdata('success')): ?>
                            <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <?php endif; ?> 

                            <?php if ($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <p><strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?></p>
                            </div>
                            <?php endif; ?> 
                
                <div class="panel panel-default">
                        
                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Search</div></div>
                   
                    <div class="panel-body">
                        <form action="eliminateorders" method="post" class="form-horizontal" role="form">
                                  <div class="col-lg-6">
                                      
                                <div class="row">  
                                    <div class="form-group">
                                        <div class="col-lg-4"><label>Enter Order ID </label></div>
                                        <div class="col-lg-3" id="sandbox-container">
                                        <input type="text" name="orderid" id="orderid" class="form-control input-md input-sm"  value="<?php echo $this->input->get('orderid'); ?>"  required />
                                        </div>
                                     </div>
                                </div>    
                                      
                                <div class="row">  
                                    <div class="form-group">
                                      <div class="col-lg-4"><label>Comment</label></div>
                                      <div class="col-lg-3" id="sandbox-container">
                                          <textarea name="comment" id="comment" class="form-control input-md input-sm" style="width: 300px; height: 86px;" required></textarea>
                                      </div>
                                    </div>
                                </div>    
                                      
                                   <div class="row">     
                                    <div class="col-lg-2 col-lg-offset-4"  >
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default btn-primary btn-sm" style="margin-left: 10px;" onclick="return confirm('Are you sure?')" >Submit</button>
                                    </div>
                                    </div>
                                   </div>
                                    
                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
      </div>
    
</div>
<?php $this->load->view('common/footer');  ?>
