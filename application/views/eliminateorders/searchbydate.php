<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/incomings.js'); ?>"></script>

<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search canceled order</h3>
           </div>
        </div>
    </div>
    
           <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="searchbydate" method="get">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-lg-4"><label>Date</label></div>
                                    <div class="col-lg-7" id="sandbox-container">
                                  <input type="text" name="cancelation_date" id="cancelation_date" class="form-control input-md input-sm"  value="<?php echo $this->input->get('cancelation_date'); ?>"  required />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
 </div>
 
   <?php if(!empty($eliminatedorders)):  ?>
    
            <div class="row">
                <div class="col-lg-12">
                       <h3>Eliminated orders for date : ( <?php echo $this->input->get('cancelation_date'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Orders</div></div>
                       <div class="panel-body">
                           <table class="table bootstrap-admin-table-with-actions">
                               <thead>
                                   <tr>
                                  
                                       <th style="border:1px solid #ccc;">Order Id</th>
                                       <th style="border:1px solid #ccc;">Supplier Name</th>
                                       <th style="border:1px solid #ccc;">Operator Name</th>                                       
                                       <th style="border:1px solid #ccc;">Order Amount</th>
                                       <th style="border:1px solid #ccc;">To Pay</th>
                                       <th style="border:1px solid #ccc;">Comment</th>
                                                                              
                                    </tr>
                               </thead>
                               
                               <tbody>
                                    <?php foreach($eliminatedorders as $orders):  ?>
                                   <tr>
                                       <td style="border:1px solid #ccc;"><?php echo $orders->order_id; ?></td>
                                       <td style="border:1px solid #ccc;"><?php echo $orders->supp_name; ?></td>
                                       <td style="border:1px solid #ccc;"><?php echo $orders->opr_name; ?></td>
                                       <td style="border:1px solid #ccc;"><?php echo $orders->amount; ?></td>
                                       <td style="border:1px solid #ccc;"><?php echo $orders->to_pay; ?></td>
                                       <td style="border:1px solid #ccc;"><?php echo $orders->comment; ?></td>
                                    </tr>
                                   <?php endforeach;  ?>
                               </tbody>
                           </table>
                       </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($eliminatedorders) && $this->input->get()): echo "No Canceled Orders Detected ";  endif; ?>
</div>
    

<?php $this->load->view('common/footer');  ?>
