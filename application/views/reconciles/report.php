<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/incomings.js'); ?>"></script>
<script>$(document).ready(function(){$('#sandbox-container .input-daterange').datepicker({format: "yyyy-mm-dd",startDate: "-365d",endDate: "1d",multidate: false,autoclose: true,todayHighlight: true,orientation: "top right"});});</script>
<style>table th,td { text-align: center; } table th { background-color: lightyellow; }</style>
<div class="container">
    
    <div class="row">
    <div class="col-lg-12">
           <div class="page-header">
                        <h3>Search</h3>
           </div>
        </div>
    </div>
    
           <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Filter</div></div>
                    <div class="panel-body">
                        <form action="report" method="get">
                           <div class="col-lg-4" id="sandbox-container">
                                    <label class="col-lg-4">Date range</label>
                                          <div class="input-daterange input-group" id="datepicker">
                                              <input type="text" class="form-control form-inp-med" name="from_date"  id="from_date" value="<?php echo $this->input->get('from_date')?$this->input->get('from_date'):"" ; ?>" />
                                              <span class="input-group-addon smallheight">to</span>
                                              <input type="text" class="form-control form-inp-med" name="to_date"  id="to_date"  value="<?php echo $this->input->get('to_date')?$this->input->get('to_date'):"" ; ?>"  />
                                            </div>
                            </div>
                            
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-primary btn-sm">View</button>
                                </div>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
 </div>

   <?php if(!empty($reconcile)):     ?>
    
            <div class="row">
                <div class="col-lg-12">
                       <h3>Reconciliation  report for date : ( <?php echo $this->input->get('from_date'); ?> - <?php echo $this->input->get('to_date'); ?> )</h3>
                    <div class="panel panel-default">
                       <div class="panel-heading"><div class="text-muted bootstrap-admin-box-title">Reconciliation Details</div></div>
                       <div class="panel-body">
                           <table class="table table-bordered table-hover">
                               <thead>
                                   <tr>
                                  
                                       <th>Date</th>
                                       <th>Pending Closing</th>
                                       <th>Pending Closing Amount</th>
                                       <th>Pending Opening</th>                                       
                                       <th>Pending Opening Amount</th>
                                       <th>To Pay</th>
                                       <th>Adjustments</th>
                                       <th>In bank</th>
                                       <th>Incoming</th>
                                       <th>Incoming Invested</th>
                                                                              
                                    </tr>
                               </thead>
                               
                               <tbody>
                                    <?php foreach($reconcile as $k=>$v):     ?>
                                   <tr>
                                       <td><?php echo $k; ?></td>
                                       <td><?php echo number_format($v[0]->pendingclosing,2); ?></td>
                                       <td><?php echo number_format($v[0]->pendingclosingamt,2); ?></td>
                                       <td><?php echo number_format($v[0]->pendingopening,2); ?></td>
                                       <td><?php echo number_format($v[0]->pendingopeningamt,2); ?></td>
                                       <td><?php echo number_format($v[0]->to_pay,2); ?></td>
                                       <td><a  target="_blank" href="/reconciles/get?searchdate=<?php echo $k;  ?>"><?php echo number_format($v[0]->adjustments,2); ?></a></td>
                                       <td><a  target="_blank" href="/reconciles/get?searchdate=<?php echo $k;  ?>"><?php echo number_format($v[0]->inbank,2); ?></a></td>
                                       <td><?php echo number_format($v[0]->incoming,2); ?></td>
                                       <td><?php echo number_format($v[0]->incominginvested,2); ?></td>
                                    </tr>
                                   <?php endforeach;  ?>
                               </tbody>
                           </table>
                       </div>
                    </div>
                </div>
            </div>
    
        <?php endif; ?>
    
      <?php if(empty($reconcile) && $this->input->get()): echo "No Pending Records Detected ";  endif; ?>
</div>
    

<?php $this->load->view('common/footer');  ?>
