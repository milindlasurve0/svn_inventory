<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<style>body{font-size: 12px;}</style>
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/moment.js'); ?>"></script>
<script src="<?php echo base_url('public/js/planning.js?'.time()); ?>"></script>
<div class="container-fluid">
    
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Planning Sheet</h2>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    
                        <div class="form-group">
                            <div class="col-lg-3 autowidth">
                                <div class="col-lg-3"> <label>Date</label></div>
                                <div class="col-lg-8"><input type="text" name="target_date" class="form-control" id="target_date" value="<?php echo date('Y-m-d'); ?>" /></div>
                              </div>
                            <label for="operator_id" class="col-lg-1 control-label">Select</label>
                              <div class="col-lg-2">
                                <select name="operator_id" id="operator_id"  style="width: 150px" required>
                                    <option value="">Select</option>
                                     <?php foreach($operators as $operator): ?>
                                    <option value="<?php echo $operator->id;  ?>"><?php echo $operator->operator_name;  ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="col-lg-1 autowidth" id="loadsuppliermsg"></div>
                            <div class="col-lg-1">
                                <button class="btn btn-default btn-grey btn-sm" id="loadsuppliersforplanning">Load Suppliers</button>
                            </div>
                            <div class="sftdbs col-lg-3 autowidth" style="margin-top: 8px; float: left;">
                                
                            </div>
                            <div class="sftdta col-lg-3 autowidth" style="margin-top: 8px; float: right;">
                                
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="row" id="planningsheet" >
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <div class="row">
                        
                        <div class="col-lg-9" id="output_message">
                                
                        </div>    
                        
                        
                        
                    </div>
                 
                        
                    <table class="table " id="tblsetarget">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Margin</th>
                            <th>Avg</th>
                            <th>LDS</th>
                            <th>CS</th>
                            <th>Opening</th>
                            <th>Sims</th>
                            <th>Order</th>
                            <th>Pending</th>
                            <th>Base Amount</th>
                            <th>Target</th>
                            <th>Modem Targets</th>
                            <th>Max.Sale.Cap</th>
                            <th>Comm</th>
                            <th>Actions</th>
                           
                        </tr>
                        </thead>
                         
                        <tbody>
                           
                         </tbody>
                         <tfoot>
                             
                         </tfoot>   
                             
                    </table>
                  
                </div>
            </div>
        </div>
    </div>
    
<div class="modal" id="addnotesdiv">
  <div class="modal-dialog" style="width: 500px; height: auto;">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Add Notes  <span class="text-left pull-right" ><button data-soid=""  class="btn btn-default" id="btnrefreshcomments"><i  class="glyphicon glyphicon-refresh"></i></button></span></h4>
        
      </div>
      <div class="modal-body">
          <div id="notesthread" style="margin: 10px;">
              
          </div>
          <div>
              <input type="hidden" name="inpnotesoid" id="inpnotesoid"  autocomplete="off" />
              <input autocomplete="off"  type="text" name="addnotedate" id="addnotedate" class="form-control form-inp marginbt5" value="<?php echo date('Y-m-d'); ?>"/>
              <textarea  autocomplete="off" name="addnotemsg" id="addnotemsg" class="marginbt5 form-control" style="height: 50px; width: 350px;"></textarea>
              <button class="btn btn-sm btn-info btn-default" id="btnaddnote">Add</button>
          </div>
       </div>
      <div class="modal-footer">
          <div id="addnotemsg" style="text-align: left;float: left;width: 250px;"></div>
           <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
</div>

<?php $this->load->view('common/footer');  ?>
