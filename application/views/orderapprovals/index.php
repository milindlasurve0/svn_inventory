<?php $this->load->view('common/header');  ?>
<?php $this->load->view('common/menu');  ?>
<script src="<?php echo base_url('public/js/orderapproval.js?'.  time()); ?>"></script>
<script src="<?php echo base_url('public/js/regenerateorder.js?'.  time()); ?>"></script>
<div class="container">
  
     <div class="row">
        <div class="col-lg-12 text-left">
            <h2>View Order</h2>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            
        <div class="panel panel-default">
            <div class="panel-body">
              
                <form action="/orderapprovals" method="get">
                    
                        <div class="col-lg-4">
                            <label for="operator_id" class="col-lg-5 control-label">Select Operator</label>
                           <div class="col-lg-6">
                                           <select name="operator_id" id="operator_id"  style="width: 150px" required>
                                              <option value="">Select</option>
                                               <?php foreach($operators as $operator): ?>
                                              <option value="<?php echo $operator['id'];  ?>"  <?php  if($this->input->get('operator_id')==$operator['id']): echo "selected";  endif; ?>><?php echo $operator['name'];  ?></option>
                                              <?php endforeach; ?>
                                          </select>
                            </div>
                      </div>

                      <div class="col-lg-1">
                          <div class="form-group">
                              <input type="submit" class="btn btn-default btn-sm" value="Load"   />
                          </div>
                      </div>
                    
                    <div class="col-lg-2">
                        <h5><b>Last Day sale</b> </h5>
                        <span><?php echo number_format($tlds,2);  ?></span>
                    </div>
                    <div class="col-lg-2">
                        <h5><b>Failure %</b> </h5>
                        <span><?php echo number_format($tldfp,2);  ?></span>
                    </div>
                    <div class="col-lg-2">
                        <h5><b>Expected Sale</b> </h5>
                        <span><?php echo number_format($tes,2);  ?></span>
                    </div>
                    
              </form>        
                  
            </div>
        </div>
            
        </div>
    </div>
    
    <?php if(!empty($orders)): ?>
    <div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Sale Info</h1>
    </div>
            <div class="panel-body">
                <div class="col-lg-2 col-lg-new-2">
                    <h5>Opn Balance </h5>
                     <span><?php echo number_format($owcb,2); ?></span>
                </div>
                 <div class="col-lg-2 col-lg-new-2" >
                    <h5>Total Order</h5>
                    <span id="totalorderspan" data-totalorder="<?php echo $totalorder;  ?>"><?php echo number_format($totalorder,2);  ?></span>
                </div>
                <div class="col-lg-3 col-lg-new-2">
                    <h5>Shrt for the day</h5>
                    <span id="shortfortheday"><?php echo number_format($shortfortheday,2);  ?></span>
                </div>
                <div class="col-lg-2 col-lg-new-2">
                    <h5>Expected Sale</h5>
                    <span id="expectedsale" data-expectedsale="<?php echo $owes;  ?>"><?php echo ($owes>0)?number_format($owes,2):0.00;  ?></span>
                </div>
                <div class="col-lg-2 col-lg-new-2">
                    <h5>Failure </h5>
                    <span><?php echo number_format($owfp,2); ?></span>
                </div>
                  <div class="col-lg-2 col-lg-new-2">
                    <h5>Api Sale</h5>
                    <span><?php echo number_format($owldsfa,2);  ?></span>
                </div>
                <div class="col-lg-2 col-lg-new-2">
                    <h5> Last Day Sale </h5>
                    <span><?php echo number_format($owlds,2);  ?></span>
                </div>
                <div class="col-lg-2 col-lg-new-2">
                    <h5> Pending </h5>
                    <span><?php echo number_format($owp,2);  ?></span>
                </div>
            </div>
     </div>
    
    <div class="row">
        <div class="col-lg-12 text-right tile-header fixme">
            <div class="col-lg-8">
            <div id="approvemsg" style="text-align: left;"></div>
            </div>
             <div class="col-lg-4">
            <button class="btn btn-default btn-sm btn-success" id="approveorders">Approve</button>
            <button class="btn btn-default btn-sm btn-danger" id="disapproveorders">Reject</button>
            <button class="btn btn-default btn-sm btn-info" id="btnregenerateorder">Regenerate</button>
             </div>
        </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Orders [ No of days : <?php echo $no_of_days;  ?> ]</h1>
    </div>
        <div class="panel-body" id="orderdiv">
            <?php foreach($orders['batch'] as $batchno=>$batchdata):  ?>
            <h3>Batch : <?php echo $batchno?$batchno:" Not Defined";  ?></h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th><input type="checkbox" name="checkall" id="checkall" autocomplete="off"></th>
                        <th>ID</th>
                        <th>Status</th>
                        <th>Supplier Name</th>
                        <th>Operator</th>
                        <th>Margin</th>
                        <th>B.Sale</th>
                        <th>Opening</th>
                        <th>Pending</th>
                        <th>Order</th>
                        <th>To Pay</th>
                        <th>Bank</th>
                        <th>Comment</th>
                        <th>Batch</th>
                        <th>Days</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalBaseSale=0;$totalOrderAmt=0;$totalPending=0;$totalOpening=0;$totalToPay=0; ?>
                    <?php  foreach($batchdata as $order ): ?>
                    <?php $totalBaseSale+=$order->base_amount;$totalOrderAmt+=$order->amount;$totalPending+=$order->pending;$totalOpening+=$order->opening;$totalToPay+=$order->to_pay;  ?>
                    <tr>
                        <td>
                            <?php if($order->is_downloaded=="2"): ?>
                                    <input type='checkbox'  name='orderids[]' value='<?php echo $order->id; ?>'  autocomplete="off" />
                                        <?php if($order->is_payment_done==0):  ?><span class="glyphicon text-danger glyphicon-repeat"></span> <?php endif; ?>
                                        
                          <?php 
                          else:
                                        if($order->is_downloaded=="1"):
                                        $text= $order->is_payment_done==1?"text-success":"";  
                                   ?>
                                        <span class="glyphicon glyphicon-ok  <?php echo $text;  ?>"></span>     
                                   <?php
                                          endif;
                                    ?>              
                          <?php endif;  ?>
                        </td>
                        <td><?php echo $order->id; ?></td>
                        <td>
                            <?php $background=$order->is_approved=="1"?"#12eb50":(($order->is_approved=="2")?"#ccc":"red"); ?> 
                            <div style="background: <?php echo $background; ?>;width: 10px;height: 10px;border-radius: 50%;"></div> 
                       </td>
                       <td><a target="_blank" href="/suppliers/get/<?php echo $order->supplierid; ?>"><?php echo $order->name; ?></a><br/><div style="overflow:hidden;width:95px;" title="<?php echo $order->vendornames; ?>" class="btn btn-default btn-warning btn-xs"><?php echo $order->vendornames; ?></div></td>
                        <td><?php echo $order->operator_name; ?></td>
                        <td>
                            <?php echo $order->margin; ?>
                         <?php if($order->commission_type=="1"): ?>
                                           <span class="glyphicon glyphicon-arrow-down text-success"></span>
                                           <?php else:  ?>
                                           <span class="text-success glyphicon glyphicon-arrow-up"></span>
                         <?php endif;  ?>
                        </td>
                        <td><?php echo number_format($order->base_amount,2); ?></td>
                        <td><?php echo number_format($order->opening,2); ?></td>
                        <td><a target="_blank" href="/orderhistorys/pendinghistory/<?php echo $order->supplier_operator_id;  ?>"><?php echo number_format($order->pending,2); ?></a></td>
                        <td id="amount_<?php echo $order->id; ?>">
                            
                              <?php if($order->is_downloaded=="2"): ?>
                                    <p><?php echo number_format($order->amount,2); ?></p>
                                      <?php if(in_array($order->is_payment_done,array(0,2))): ?>  <a href="#neworderamount" data-toggle="modal"  data-soid="<?php echo $order->supplier_operator_id; ?>" data-batchid="<?php echo $batchno;  ?>" data-orderamount="<?php echo $order->amount;  ?>"   data-orderid="<?php echo $order->id; ?>">Edit</a> <?php endif; ?>
                            <?php else: ?>
                                     <?php echo number_format($order->amount,2); ?>
                            <?php endif; ?>
                                    
                        </td>
                        <td  data-topay="<?php echo $order->to_pay; ?>" id="topay_<?php echo $order->id; ?>"><?php echo $order->to_pay; ?></td>
                        <td>
                            <?php if($order->is_downloaded=="2"): ?>
                             <select  id="bank_<?php echo $order->id;  ?>" name="bank_id"  style="width:165px;" onchange="updatebank(this.value,'<?php echo $order->id; ?>','<?php echo $order->supplier_id; ?>')"  autocomplete="off" >
                                <option value="">Select Bank</option>
                            <?php foreach($order->supplier_selected_banks as $supplierbank): ?>
                                <option value="<?php echo $supplierbank['supplier_mapped_bank_id']; ?>"  <?php if(($supplierbank['supplier_mapped_bank_id']==$order->supplier_bank_id) || count($order->supplier_selected_banks)=="1"):  echo "selected"; endif; ?>><?php echo $banks[$supplierbank['bank_id']];   ?>/<?php echo $supplierbank['account_no']; ?></option>
                            <?php endforeach; ?>
                                  <option value="addmorebanks">----Map More Banks----</option>    
                            </select> 
                            <?php else: ?>
                                            <?php foreach($order->supplier_selected_banks as $supplierbank): 
                                                           if($order->supplier_bank_id==$supplierbank['supplier_mapped_bank_id']):  echo $banks[$supplierbank['bank_id']]."/".$supplierbank['account_no'];  endif;
                                                       endforeach;
                                            ?>
                           <?php endif; ?> 
                        </td>
                        <td>
                            <span><?php echo getLastComment($order->supplier_operator_id);  ?></span>
                            <a  data-orderid="<?php echo $order->id;  ?>" data-soid="<?php echo $order->supplier_operator_id; ?>"  data-operatorid="<?php echo $order->operator_id;  ?>" data-toggle="modal" href="#addcomment">View</a>
                        </td>
                        <td>
                             <?php if($order->is_downloaded=="2"): ?>
                                    <?php $isdisabled=($order->is_approved=="0")?"disabled='disabled'":""; ?>
                                    <select  <?php echo $isdisabled;  ?> id="batch_<?php echo $order->id; ?>" name="batch_id" onchange="updatebatch(this.value,'<?php echo $order->id; ?>')">
                                        <option value="0">Select</option>
                                        <?php for($i=1;$i<=8;$i++): ?>
                                        <option value="<?php echo $i; ?>"  <?php if($i==$order->batch ):  echo "selected"; endif; ?>><?php echo $i; ?></option>
                                        <?php endfor; ?>
                                    </select>
                               <?php else: ?>
                                        <?php echo $order->batch; ?>
                            <?php endif; ?>
                        
                        </td>
                        <td><?php echo $order->no_of_days;  ?></td>
                        <td><?php if($order->is_payment_done=='1'): ?><input autocomplete="off" type="checkbox" name="chkregenerate[]" data-soid="<?php echo $order->supplier_operator_id;  ?>"  value="<?php echo $order->id; ?>" /><?php endif;  ?></td>
                    </tr>
                    
                    <?php endforeach;  ?>
               </tbody>
                 <tfoot id="tfoot_batch_<?php echo $batchno;  ?>">
                    <tr>
                       <th></th> <th></th><th></th><th></th><th></th><th></th>
                        <th><?php echo number_format($totalBaseSale,2); ?></th>
                        <th><?php echo number_format($totalOpening,2); ?></th>
                        <th><?php echo number_format($totalPending,2); ?></th>
                        <th data-orderamt="<?php echo $totalOrderAmt;  ?>"><?php echo number_format($totalOrderAmt,2); ?></th>
                     
                        <th data-topay="<?php echo $totalToPay;  ?>"><?php echo number_format($totalToPay,2); ?></th>
                    </tr>
                </tfoot>
            </table>
            
               <?php endforeach;  ?>
        </div>
</div>
    <?php endif;  ?>
    
<div class="modal" id="neworderamount">
  <div class="modal-dialog" style="width: 300px;">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Update Order Amount</h4>
      </div>
      <div class="modal-body">
          <p>Old Order Amount :  <span></span></p>
       
        <p>Enter New Order Amount</p>
        <input type="hidden" name="order_id" id="order_id" value="" />
        <input type="hidden" name="batch_id" id="batch_id" value="" />
        <input type="hidden" name="so_id" id="so_id" value="" />
        <input type="text" id="order_amount" name="order_amount" value=""  class="form-control form-inp"/>
        <i class="words"></i>
      </div>
      <div class="modal-footer">
          <div id="orderamtmsg" style="text-align: left"></div>
          <button class="btn btn-sm btn-default btn-primary" id="updateorderamount">Update</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    

    
 <div class="modal" id="addcomment">
<div class="modal-dialog" style="width: 300px;">
<div class="modal-content">
  <div class="modal-header">
      <h4 class="modal-title">Add Comment</h4>
  </div>
  <div class="modal-body">
      <div class="row">
          <div class="col-lg-12" id="loadcomments">
              
          </div>
      </div>
      <input type="hidden" name="comment_order_id" id="comment_order_id" value="" />
      <input type="hidden" name="comment_so_id" id="comment_so_id" value="" />
      <textarea  name="comment" id="comment"></textarea>
  </div>
  <div class="modal-footer">
      <div id="ordercommentmsg" style="text-align: left"></div>
      <button class="btn btn-sm btn-default btn-primary" id="addcommentbtn">Update</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
    
    
    
 <div class="modal" id="divregenerateorder">
<div class="modal-dialog" style="width: 60%">
<div class="modal-content">
  <div class="modal-header">
      <h4 class="modal-title">Regenerate Order</h4>
  </div>
    
  <div class="modal-body">
      <div class="row">
          <div class="col-lg-12" id="regeneratehtml">
              
          </div>
      </div>
  </div>
    
  <div class="modal-footer">
      <div id="" style="text-align: left"></div>
      <button class="btn btn-sm btn-default btn-primary" id="btnconfirmregenerate" type="button">Regenerate</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
    
</div>

<?php $this->load->view('common/footer');  ?>
