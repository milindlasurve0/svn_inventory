<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['frequency']=array('0'=>'--NA--','1'=>'Daily','2'=>'Weekly','3'=>'Monthly','4'=>'[Tue,Thu,Sat]','5'=>'[Mon,Wed,Fri]','6'=>'[Mon-Fri]',
                                                    '7'=>'Mon',
                                                    '8'=>'Tue',
                                                    '9'=>'Wed',
                                                    '10'=>'Thu',
                                                    '11'=>'Fri',
                                                    '12'=>'Sat',
    );
$config['commission_type']=array('1'=>'[96-100]','2'=>'[100-104]','3'=>'Formula Based');
$config['rating']=array('1'=>'A','2'=>'B','3'=>'C','4'=>'D','5'=>'E');
$config['batch']=array('1','2','3','4','5','6','7','8');
$config['icici_debit_account_no']="054405006714";
$config['axis_debit_account_no']="123459563";
$config['ordersmstemplate']="Vendor Name:%s,Todays order:%d,Order amt:%d,UTR:%s,Operator:%s";
$config['orderemailtemplate']="Vendor Name:%s<br/>Todays order:%d<br/>Order amt:%d<br/>UTR:%s<br/>Operator:%s";

$config['invoperators']=array(
                                                        array('id'=>'9','name'=>'Tata Docomo'),
                                                        array('id'=>'15','name'=>'Vodafone'),
                                                        array('id'=>'17','name'=>'Big tv dth'),
                                                        array('id'=>'16','name'=>'Airtel dth'),
                                                        array('id'=>'11','name'=>'Uninor'),
                                                        array('id'=>'30','name'=>'MTNL'),
                                                        array('id'=>'8','name'=>'Reliance'),
                                                        array('id'=>'3','name'=>'BSNL'),
                                                        array('id'=>'20','name'=>'TATA sky'),
                                                        array('id'=>'18','name'=>'DISH TV DTH'),
                                                        array('id'=>'2','name'=>'Airtel'),
                                                        array('id'=>'1','name'=>'Aircel'),
                                                        array('id'=>'4','name'=>'Idea'),
                                                        array('id'=>'19','name'=>'Sun tv'),
                                                        array('id'=>'21','name'=>'Videocon DTH'),
                                                        array('id'=>'5','name'=>'Loop'),
                                                        array('id'=>'7','name'=>'Reliance CDMA'),
                                                        array('id'=>'54','name'=>'API OPERATOR'),
                                                        array('id'=>'12','name'=>'Videocon')
                                                      ); 

$config['operatorkeyvalue']=array(
                                                                  9=>'Tata Docomo',
                                                                  15=>'Vodafone',
                                                                  17=>'Big Tv Dth',
                                                                  16=>'Airtel Dth',
                                                                  11=>'Uninor',
                                                                  30=>'MTNL',
                                                                  8=>'Reliance',
                                                                  3=>'BSNL',
                                                                  20=>'TATA Sky',
                                                                  18=>'Dish Tv Dth',
                                                                  2=>'Airtel',
                                                                  1=>'Aircel',
                                                                  4=>'Idea',
                                                                  19=>'Sun tv',
                                                                  5=>'Loop',
                                                                  7=>'Reliance CDMA',
                                                                  21=>'Videocon DTH',
                                                                  12=>'Videocon',
                                                                  6=>'MTS',
                                                                  39=>'Idea Postpaid'
                                                               );


$config['notetype']=array(
                                                    '1'=>'Supplierwise From set target OR sale bifurcation OR earning report screen',
                                                    '2'=>'Operatorwise from earning report level1 screen'
                                                );


$config['inventoryOrderEmailCC']=array('deepti@mindsarray.com','pushpa@mindsarray.com','deepak.d@pay1.in','aamir@mindsarray.com','sudhir.yadav@pay1.in');

