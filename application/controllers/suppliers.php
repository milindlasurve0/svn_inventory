<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();

            if(!$this->session->userdata('isLoggedIn')):

                $this->session->set_flashdata('error','You need to be logged in to view that page');
                redirect('/');

            endif;
           
           $this->load->library('form_validation');
           
           $this->load->helper('supplier');
           
            $this->config->load('inventory');
            
            $this->load->model('operator');
            
            $this->load->model('supplier');
            $this->load->model('rm');
            $this->load->model('incoming');
            $this->load->model('report');
     }
     
     public function addold()
     {
         
         $this->load->model('rm');
         
          $data['rms']=$this->rm->getRMForDropdown();
          
          $data['parent_ids']=$this->supplier->getSuppliersForDropdown();
         
         if($this->input->post()):
             
            $this->form_validation->set_rules('suppl_name', 'Name', 'required|trim|xss_clean|max_length[250]|is_unique[inv_suppliers.name]');
            $this->form_validation->set_rules('suppl_email', 'Email', 'required|trim|xss_clean|valid_email|max_length[250]|is_unique[inv_suppliers.email_id]');
            $this->form_validation->set_rules('suppl_contact', 'Contact', 'required|trim|xss_clean|is_numeric|max_length[25]');
            $this->form_validation->set_rules('suppl_bank_account_no', 'Bank Account No', 'required|trim|xss_clean|max_length[100]');
            $this->form_validation->set_rules('suppl_bank_name', 'Bank Name', 'required|trim|xss_clean|max_length[100]');
            $this->form_validation->set_rules('suppl_address', 'Address', 'required|trim|xss_clean');
            $this->form_validation->set_rules('suppl_rm_id', 'inv_rm_id', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('suppl_rating', 'Rating', 'required|trim|xss_clean|callback_checkrating');
            $this->form_validation->set_rules('suppl_risk_factor', 'Risk Factor', 'required|trim|xss_clean|is_numeric|callback_checkriskfactor');
            $this->form_validation->set_rules('suppl_location', 'Location', 'required|trim|xss_clean|max_length[150]');
            
                  if ($this->form_validation->run() == FALSE): 
                        
                       $this->session->set_flashdata('error','There was error while inserting record');

                       $this->load->view('suppliers/add',$data);
                            
                  else:
                      
                            

                                if($this->supplier->saveSupplier($this->input->post())):

                                $this->session->set_flashdata('success','Record was successfully inserted');

                                redirect('suppliers/add');

                                else:

                                $this->session->set_flashdata('error','There was error while inserting record');

                                redirect('suppliers/add');

                                endif;
                       
                  endif;
                  
        else:

            $this->load->view('suppliers/add',$data);
         
         endif; 
     }
     
     public function checkrating($value)
     {
         $rating=array('1','2','3','4','5');
         
         if(!in_array($value,$rating)):
             
             $this->form_validation->set_message('checkrating', 'The %s field Should be within  A,B,C,D,E');  
             return false;
         
         endif;
         
         return true;
     }
     
     public function checkriskfactor($value)
     {
       
         
         if($value>0 && $value<=10):
             
             return true;
         
         else:
             
              $this->form_validation->set_message('checkriskfactor', 'The %s field Should be within 1 and 5 ');  
              return false;
              
         endif;
         
         return false;
     }
     
     public function view()
     {
        $this->load->library('pagination');
        
        $operator_id=  $this->input->get('operator_id')?$this->input->get('operator_id'):"";
        $rm_id=  $this->input->get('rm')?$this->input->get('rm'):"";
        $handled_by=  $this->input->get('handled_by')?$this->input->get('handled_by'):"";
        $is_active=  $this->input->get('is_active')?$this->input->get('is_active'):"all";
        $modems=  $this->input->get('modems_ids')?$this->input->get('modems_ids'):"";
        $orderby=  $this->input->get('orderby')?$this->input->get('orderby'):"";
        $ordertype=  $this->input->get('ordertype')?$this->input->get('ordertype'):"";
         
        $config['base_url'] = base_url('suppliers/view/?operator_id='.$operator_id.'&rm='.$rm_id.'&handled_by='.$handled_by.'&is_active='.$is_active.'&modems_ids='.$modems.'&orderby='.$orderby.'&ordertype='.$ordertype);
         $total_rows= $this->supplier->getTotalRows($this->input->get());
        $config['total_rows'] =$total_rows;
       
        $config['per_page']= $this->input->get('showall')?$total_rows:25;
        
        $pageno=$this->input->get('page')?$this->input->get('page'):1;
        
        $this->pagination->initialize($config); 
        
        $data['suppliers']=  $this->supplier->findAllSuppliers($this->input->get(),$pageno,$config['per_page']);
        $data['operators']=  $this->operator->getOperatorsForDropdown();
        $data['vendors']=$this->supplier->getVendorForDropdown();
        $data['rms']=$this->rm->getRMForDropdown();
        $data['internals']=  $this->incoming->getInternalsPersons();
        $data['total_rows']=$total_rows;
        $data['totaluniquesuppliers']=$this->supplier->getTotalUniqueSuppliers();
        $data['avgOperatorSale']=  $operator_id?$this->report->getAvgSaleByOperatorId($operator_id):0;
        $this->load->model('order');
        $data['avgApiSale']=  $operator_id?$this->order->getapiSale(combineOperator($operator_id),true):0;
        //$data['totalbasesale']=  $operator_id?$this->report->getTotalBaseSaleByOperatorid($operator_id):0;
        $totalBaseSale=0;
        if(!empty($data['suppliers'])):
                foreach($data['suppliers'] as $row):
                         $totalBaseSale+=$row['base_amount'];
                endforeach;
        endif;
         $data['totalbasesale']=$totalBaseSale;
        
        $this->load->view('suppliers/view',$data);
     }
     
     public function edit($id)
     {
         $this->load->model('rm');
         $this->load->model('incoming');
         
    
         
          $data['vendors']=$this->supplier->getVendorForDropdown();
          
           $data['rms']=$this->rm->getRMForDropdown();
         
           $data['supplier']=$this->supplier->findSupplierById($id);
           
           $data['parent_ids']=$this->supplier->getSuppliersForDropdown();
           
           $data['vendorMappedToSupplier']=$this->supplier->findVendorsSupplierById($id);
           
           $data['banks']=  $this->supplier->getBanks();
           
           $data['internals']=  $this->incoming->getInternalsPersons();

           if($this->input->post()):
             
                    $this->form_validation->set_rules('suppl_name', 'Name', "required|trim|xss_clean|max_length[250]|edit_unique[inv_suppliers.name.{$id}]");
                    //$this->form_validation->set_rules('suppl_email', 'Email', "required|trim|xss_clean|valid_email|max_length[250]|edit_unique[inv_suppliers.email_id.{$id}]");
                    $this->form_validation->set_rules('suppl_email', 'Email', "required|trim|xss_clean|max_length[250]");
                    //$this->form_validation->set_rules('suppl_contact', 'Contact', 'required|trim|xss_clean|is_numeric|max_length[25]');
                   // $this->form_validation->set_rules('suppl_address', 'Address', 'required|trim|xss_clean');
                   
                    $this->form_validation->set_rules('frombank', 'From bank', 'required|trim|xss_clean|is_numeric');
                    $this->form_validation->set_rules('suppl_rating', 'Rating', 'required|trim|xss_clean|callback_checkrating');
                    $this->form_validation->set_rules('suppl_risk_factor', 'Risk Factor', 'required|trim|xss_clean|is_numeric');
                    $this->form_validation->set_rules('suppl_location', 'Location', 'required|trim|xss_clean|max_length[150]');
                    $this->form_validation->set_rules('handled_by', 'Handled By', 'required|trim|xss_clean|is_numeric');
                    
                    if(!$this->input->post('is_api')):
                     $this->form_validation->set_rules('suppl_rm_id', 'inv_rm_id', 'required|trim|xss_clean|is_numeric');  
                     $this->form_validation->set_rules('vendors[]', 'Vendors', 'required|is_numeric');
                    endif;
                    
                    $this->form_validation->set_rules('other_rm','Other RM','trim|xss_clean|max_length[70]|callback_checkOtherRm');

                   $this->form_validation->set_message('edit_unique', 'The %s field with similar value  already exists'); 

                    if ($this->form_validation->run() == FALSE): 

                            $this->load->view('suppliers/edit',$data);
                            

                    else:
                               if($this->supplier->UpdateSupplier($id,$this->input->post())):

                                $this->session->set_flashdata('success','Record was Updated ');

                                redirect('suppliers/edit/'.$id);

                                else:

                                $this->session->set_flashdata('error','There was error while Updating record');

                                redirect('suppliers/edit/'.$id);

                                endif;
                                
                   endif;
         else:
             
          if(!$data['supplier']): exit("No Supplier Record Exists");  endif;
             
           $this->load->view('suppliers/edit',$data);
        
         endif;
     }
     
     
     public function checkOtherRm($value)
     {
         $value=  trim($value);
         
         if($this->input->post('suppl_rm_id')=="0"):
                
                if(!empty($value)):
                
                    return true;
                
                else:
                    
                     $this->form_validation->set_message('checkOtherRm', 'The %s field Should not be empty when others option is selected');  
                     return false;
                     
                endif;
             
         endif;
     }
     
     
//     public function delete($id)
//    {
//   
//        
//        if($this->supplier->deleteSupplier($id)):
//            
//                 $this->session->set_flashdata('success','Record Deleted');
//
//                  redirect('suppliers/view');
//                  
//        endif;
//        
//         $this->session->set_flashdata('error','Error in deleting Record');
//
//          redirect('suppliers/view');
//    }
    
    public function get($supplier_id=0)
     {
        if(!$supplier_id):
                            exit('No Supplier ID passed');
        endif;
        
     
         
         $this->load->model('operator');
        
        $data['supplier']=  $this->supplier->findSupplierById($supplier_id);
        
        $data['supplier_operators']=$this->operator->getOperatorsDetail($supplier_id);
        
        $data['supplier_banks']=$this->supplier->getBankDetailBySupplierID($supplier_id);
        
       $data['supplier_contacts']=  $this->supplier->getContactsBySupplierID($supplier_id);
       
       $data['supplier_vendors']=$this->supplier->getVendorsBySupplierID($supplier_id);
        
        $this->load->view('suppliers/get',$data);
     }
  
     public function add()
     {
         $this->load->model('rm');
         
     
         
          $data['rms']=$this->rm->getRMForDropdown();
          
          $data['vendors']=$this->supplier->getVendorForDropdown();
          
          $data['parent_ids']=$this->supplier->getSuppliersForDropdown();
          
          $this->load->view('suppliers/addnewsupplier',$data);
     }
     
     public function save()
     {
         $supplier_data=array('inv_rm_id'=>  $this->input->post('suppl_rm_id'),
                                                'email_id'=>  $this->input->post('suppl_email'),
                                                'name'=>  $this->input->post('suppl_name'),
                                                'contact'=>  $this->input->post('suppl_contact'),
                                                'location'=>  $this->input->post('suppl_location'),
                                                'address'=>  $this->input->post('suppl_address'),
                                                'parent_id'=>  $this->input->post('suppl_parent_id'),
                                                'rating'=>  $this->input->post('suppl_rating'),
                                                'riskfactor'=>  $this->input->post('suppl_risk_factor'),
                                                );
         
         if($this->input->post('other_rm')!="" && $this->input->post('suppl_rm_id')=="0"):
                $supplier_data['other_rm']=$this->input->post('other_rm');
         endif; 
         
         $OperatorsIds=$this->input->post('operators');
         $total_capacityIds=$this->input->post('total_capacity');
         $so_riskfactorIds=$this->input->post('so_riskfactor');
         $capacity_per_dayIds=$this->input->post('capacity_per_day');
         $capacity_per_monthIds=$this->input->post('capacity_per_month');
         $frequencyIds=$this->input->post('frequency');
         $commission_typeIds=$this->input->post('commission_type');
         $commission_type_formulaIds=$this->input->post('commission_type_formula');
         $batch_types=$this->input->post('batch_type');
         $basesales=$this->input->post('basesale');
         
         
         $supplier_operator_mapping_data=array();
         $i=0;
      
         foreach($OperatorsIds as $value):
             
            $percentage=$commission_type_formulaIds[$i];
             $expression="";
           // Percentage Increase is ((RHS-LHS)*100)/LHS
         
             if(strpos($commission_type_formulaIds[$i],'=') && $commission_typeIds[$i]=="3"):
                 $expression=$commission_type_formulaIds[$i];
                 $parts=  explode('=', $commission_type_formulaIds[$i]);
                 $percentage=(($parts[1]-$parts[0])*100)/$parts[0];
             endif;
             
             $supplier_operator_mapping_data[]=array(
                                                                                                'operator_id'=>$OperatorsIds[$i],
                                                                                                'capacity'=>$total_capacityIds[$i],
                                                                                                'capacity_per_day'=>$capacity_per_dayIds[$i],
                                                                                                'capacity_per_month'=>$capacity_per_monthIds[$i],
                                                                                                'frequency'=>$frequencyIds[$i],
                                                                                                'commission_type'=>$commission_typeIds[$i],
                                                                                                'commission_type_formula'=>$percentage,
                                                                                                'commission_type_expression'=>$expression,
                                                                                                'riskfactor'=>$so_riskfactorIds[$i],
                                                                                                'batch'=>$batch_types[$i],
                                                                                                'base_amount'=>$basesales[$i]
                                                                                            );
            
             
         
            $i++;
            
           endforeach;
         
       
           
           $data['supplier']=$supplier_data;
           $data['alternate_contact']=$this->__getAlternateContact($this->input->post('contact_name'),$this->input->post('contact_no'),
                                                                                                                                                array('name'=>$supplier_data['name'],'contact'=>$supplier_data['contact'])
                                                                                                                                              );
           $data['supplier_operator']=$supplier_operator_mapping_data;
           $data['vendors']=  $this->input->post('vendors');
           
           
           if($supplier_id=$this->supplier->save($data)):
               
                    $this->session->set_flashdata('success','Record was Inserted Successfully <a href="addBankdetails/'.$supplier_id.'">Add Bank details</a>');
           
                    redirect('suppliers/add');
           
           else:
               
                    $this->session->set_flashdata('error','Error inserting record');
           
                    redirect('suppliers/add');
                    
           endif;
         
     }
     
     
     
     public function __getAlternateContact($names,$no,$data)
     {
         $alternateContactArr=array();
         
         $i=0;
         
         if(!empty($names)):
             
                foreach($names as $value):

                           $alternateContactArr[]=array(
                                                                                   'name'=>$names[$i],
                                                                                   'contact'=>$no[$i]
                                                                                   );

                            $i++;

                endforeach;
                
           endif;
           
         $alternateContactArr[]=array('name'=>$data['name'],'contact'=>$data['contact']);  
         
         return $alternateContactArr;
         
       
         
     }
     /*
     public function syncVendors()
     {
         $this->load->model('supplier');
         
         $response=  file_get_contents("http://start.loc/start.php?query=getVendors");
         
         $vendors=  json_decode($response);
     
         $suppliers=array_map('trim',$vendors->vendors);
         
         if(!(empty($suppliers)) && ($vendors->modem_id>0)):
             
                       if($this->supplier->SyncVendorsFromShopsToInventory($suppliers,$vendors->modem_id)):
                           
                           echo json_encode(array('status'=>'true','message'=>'Vendor Synchronization Complete')); exit();
                           
                       else:
                           
                           echo json_encode(array('status'=>'true','message'=>'Vendor Synchronization Incomplete')); exit();
                       
                       endif;
         
         endif;
         
          echo json_encode(array('status'=>'true','message'=>'No Vendors Found'));
          
        exit();
         
      
          }
          
          */
          public function addBankdetails($id)
          {
         
              
              $data['banks']=  $this->supplier->getBanks();
              
            if($this->input->post()):

                if($this->supplier->saveBankDetails($this->uri->segment(3),$this->input->post())):
                    
                      $this->session->set_flashdata('success','Details Mapped Successfully');
           
                       redirect('suppliers/addBankdetails/'.$this->uri->segment(3));
                       
                    else:
                    
                        $this->session->set_flashdata('error','Error Mapping Details');
           
                        redirect('suppliers/addBankdetails/'.$this->uri->segment(3));
                        
                endif;
                
            else:
                
                   $this->load->view('suppliers/addBankdetails',$data);
            
            endif;
           
          }
          
          public function setDefaultBank()
          {
              
            
                
              if($this->supplier->updateDefaultBank($this->input->post())):
                           
                            echo  json_encode(array('status'=>true,'message'=>"success"));
                            exit();
              
              endif;
              
                echo  json_encode(array('status'=>true,'message'=>"error"));
                exit();
         }
         
         public function editbankDetails($id)
         {
            
              
              $data['banks']=  $this->supplier->getBanks();
              
              $data['sb']=  $this->supplier->getBankDetailsByID($id);
              
               if($this->input->post()):

                        if($this->supplier->updateBankDetails($this->uri->segment(3),$this->input->post())):

                              $this->session->set_flashdata('success','Bank Details Updated');

                               redirect('suppliers/editbankDetails/'.$this->uri->segment(3));

                            else:

                                $this->session->set_flashdata('error','Error Mapping Details');

                                redirect('suppliers/editbankDetails/'.$this->uri->segment(3));

                        endif;
                
            else:
                
                   $this->load->view('suppliers/editbankDetails',$data);
            
            endif;
            
         }
         
         public function deletebankDetails($sb_id,$supplier_id)
         {
            
             if($this->supplier->deleteBank($sb_id,$supplier_id)):
                   $this->session->set_flashdata('success','Bank Deleted Successfully');
                 redirect('suppliers/get/'.$supplier_id);
             else:
                   $this->session->set_flashdata('error','Error Deleting Bank');
                   redirect('suppliers/get/'.$supplier_id);
             endif;
             
             
         }
         
         public function vendorlist($id=1)
         {
          
             
             echo json_encode($this->supplier->getSuppliersForDropdown());
             
             exit();
             
         }
         
         public function addContacts($id)
         {
             
          

                if ($this->input->post()):

                            if ($this->supplier->saveContacts($this->uri->segment(3), $this->input->post())):

                            $this->session->set_flashdata('success', 'Contacts Mapped Successfully');

                            redirect('suppliers/addContacts/' . $this->uri->segment(3));

                            else:

                            $this->session->set_flashdata('error', 'Error Mapping Contacts');

                            redirect('suppliers/addContacts/' . $this->uri->segment(3));

                            endif;

                else:

                $this->load->view('suppliers/addContacts');

                endif;



         }
         
         public function setSmsContact()
         {
         
                
              if($this->supplier->updateSmsNo($this->input->post())):
                           
                            echo  json_encode(array('status'=>true,'message'=>"success"));
                            exit();
              
              endif;
              
                echo  json_encode(array('status'=>true,'message'=>"error"));
                exit();
          }
          
                public function editContact($id)
         {
         
              
               $data['supplier_contact']=  $this->supplier->getContactByID($id);
              
               if($this->input->post()):

                        if($this->supplier->updateContact($this->uri->segment(3),$this->input->post())):

                              $this->session->set_flashdata('success','Contact Updated');

                               redirect('suppliers/editContact/' . $this->uri->segment(3));

                            else:

                                $this->session->set_flashdata('error','Error Contact');

                               redirect('suppliers/editContact/' . $this->uri->segment(3));

                        endif;
                
            else:
                
                   $this->load->view('suppliers/editContact',$data);
            
            endif;
            
         }
         
           public function setBankSwitchMode()
         {
        
                
              if($this->supplier->updateBankSwitchMode($this->input->post())):
                           
                            echo  json_encode(array('status'=>true,'message'=>"success"));
                            exit();
              
              endif;
              
                echo  json_encode(array('status'=>true,'message'=>"error"));
                exit();
          }
          
          public function CheckValidUsername()
          {
         
                  
              if(!$this->supplier->checkIfSupplierAlreadyExists($this->input->get('suppl_name'))):
                  
                  echo json_encode(array('isValid'=>"true"));die;
                  
              endif;
              
                echo json_encode(array('isValid'=>"false"));
                
              die;
          }   
          public function CheckValidEmail()
          {
          
                  
              if(!$this->supplier->checkIfSupplierEmailAlreadyExists($this->input->get('suppl_email'))):
                  
                  echo json_encode(array('isValid'=>"true"));die;
                  
              endif;
              
                echo json_encode(array('isValid'=>"false"));
                
              die;
          }   
          
          //save comments on on-off toggle history by soid
        function saveComments()
        {
            $soid=  $this->input->post('soid');
            $basesale = $this->input->post('basesale');
            $margin = $this->input->post('margin');
            $flag=  $this->input->post('flag')=="on"?"1":"0";
            $comment=  $this->input->post('comment',true);

              if($this->supplier->saveToggleCommentsBySoID($soid,$basesale,$margin,$flag,$comment)):

                 echo json_encode(array('status'=>true,'type'=>true,'msg'=>"Commented Successfully"));exit();

            endif;

            echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
        }
        
        //show toggle history(i.e supp-opr's on-off history) by date
        public function search()
        {
            $start = $this->input->get('start');
            $end = $this->input->get('end');
            $is_active= $this->input->get('is_active');
            $is_onhold= $this->input->get('is_onhold');
            $operator_id=  $this->input->get('operator_id')?$this->input->get('operator_id'):"";
            $supplier_id=  $this->input->get('supplier_id')?$this->input->get('supplier_id'):"";
            
            $data = array();
            
            $data['operators']=$this->config->item('invoperators');
            $data['suppliers']=  $this->supplier->getSuppliersForDropdown();
            
            if($start && $end):
                $togglehistory = $this->supplier->getToggleHistoryByDate($start,$end,$is_active,$is_onhold,$operator_id,$supplier_id);
                $data['togglehistory'] = $togglehistory;
            endif;

            $this->load->view('suppliers/search',$data);            
        }
        
        public function saveHoldStatusComments()
        {
            $soid =  $this->input->post('soid');
            $basesale = $this->input->post('basesale');
            $margin = $this->input->post('margin');
            $flag =  $this->input->post('flag');
            $comment =  $this->input->post('comment',true);

            if($this->supplier->saveHoldStatusCommentsBySoID($soid,$basesale,$margin,$flag,$comment)):

                 echo json_encode(array('status'=>true,'type'=>true,'msg'=>"Commented Successfully"));exit();

            endif;

            echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
        }

        public function setOnholdFlag()
        {
            $data =  $this->input->post();
            if($this->supplier->setOnholdFlag($data)):
                     echo json_encode(array('status'=>true,'type'=>true,'message'=>'Flag set successfully')); exit();
            endif;
             
            echo json_encode(array('status'=>true,'type'=>false,'message'=>'No data received')); exit();
        }
        
        public function soHistory()
        {
            $start = $this->input->get('start');
            $end = $this->input->get('end');
            $operator_id=  $this->input->get('operator_id')?$this->input->get('operator_id'):"";
            $supplier_id=  $this->input->get('supplier_id')?$this->input->get('supplier_id'):"";
            
            $data = array();
            
            $data['operators']=$this->config->item('invoperators');
            $data['suppliers']=  $this->supplier->getSuppliersForDropdown();
            
            if($start && $end):
                $sohistory = $this->supplier->getSoHistoryByDate($start,$end,$operator_id,$supplier_id);
                $data['sohistory'] = $sohistory;
            endif;
            
            $this->load->view('suppliers/sohistory',$data);
        }
}

