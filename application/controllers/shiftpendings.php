<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Shiftpendings extends CI_Controller
{
      public function __construct()
      {
            parent::__construct();
            if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

            endif;
           
            $this->load->model('shiftpending');
               
      }
      
      public function getpendings()
        {
            $pendings=$this->shiftpending->getPendings($this->input->get());

            echo !empty($pendings)?json_encode(array('data'=>$pendings,'status'=>'success','type'=>true)):json_encode(array('status'=>'success','type'=>false,'data'=>''));
        }
                
        public function getOperatorListBySupplierIdJSON()
        {
            $data=$this->shiftpending->getOperatorWisePendingForSuppliers($this->input->get());

            echo !empty($data)?json_encode(array('operators'=>$data,'status'=>'success','type'=>true)):json_encode(array('status'=>'success','type'=>false));
        }

        public function adjustPending()
       {
            if(!in_array(getLoggedInUserId(), array(48347012,48636190))):echo json_encode(array('status'=>true,'type'=>false,'message'=>'You dont have permission'));exit();endif;
            
            $data=  $this->input->post('data');
           
           $data=  json_decode($data,true);

           if(!empty($data)):

                        $sourceData= $data['parentpendings'][0];
                        $targetData=  $data['targetpendings'];

                        //Adjust pending for existing operator
                        $this->shiftpending->adjustPendings($sourceData);
                                              
                        // Adjust Pending for other operators
                       foreach($data['targetpendings'] as $target):
                                         
                          //  $this->orderhistory->adjustOtherOperatorsPendings($target);
                            $this->shiftpending->adjustPendings($target);
                                        
                       endforeach;
                       
                       echo json_encode(array('status'=>true,'type'=>true,'message'=>'Successfully adjusted'));exit();
           endif;

                      echo json_encode(array('status'=>true,'type'=>false,'message'=>'Empty Set'));exit();
       }
}