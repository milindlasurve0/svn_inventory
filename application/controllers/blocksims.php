<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blocksims extends CI_Controller
{
     public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('isLoggedIn')):

          $this->session->set_flashdata('error','You need to be logged in to view that page');
          redirect('/');

        endif;
        $this->load->model('blocksim');
     }
     
     public function search()
     {
        $data=array();
        
        if($this->input->get()):
                $data['sims']=  $this->blocksim->getBlockSimsByDate($this->input->get());
        endif;
        
        $this->load->view('blocksims/search',$data);
     }
}