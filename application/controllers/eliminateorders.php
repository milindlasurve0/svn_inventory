<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Eliminateorders extends CI_Controller
{
    
      public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        $this->load->model('eliminateorder');
    }
    
    public function index()
    {
       if($this->input->post()):
           
                $orderId=  $this->input->post('orderid');
       
                if($orderId>0):
                                      //$this->load->model('eliminateorder');    
                                   
                                        $response=$this->eliminateorder->eliminateByOrderId($this->input->post());
                                        
                                        if($response['type']):
                                                    
                                                  $this->sendOrderCancelationEmail($response['info']);
                                                  $this->session->set_flashdata('success','Order was eliminated successful');
                                                  redirect('eliminateorders');
                                            
                                        else:
                                            
                                                $this->session->set_flashdata('error',$response['msg']);
                                                 redirect('eliminateorders');
                                                 
                                        endif;
                endif;
           
       else:
           
             $this->load->view('eliminateorders/index');
       
       endif;
       
        
    }
    
    public function sendOrderCancelationEmail($data)
    {
         $this->load->library('email');
         $this->email->initialize(array('mailtype'=>'html','protocol'=>'sendmail'));
         $this->email->from('orders@mindsarray.com', 'Pay1');
         $this->email->to($data['info']['email']); 
         $this->email->cc(array('deepti@mindsarray.com','pushpa@mindsarray.com','deepak.d@pay1.in','aamir@mindsarray.com','sunilr@pay1.in','yojana@mindsarray.com')); 
         $this->email->subject('Order Canceled');
         $message="Order with #{$data['id']} has been Canceled.Below are order details <br/>";
         $message.="Order Id : {$data['id']} <br/>";
         $message.="Supplier Name  : {$data['suppliername']} <br/>";
         $message.="Operator  : {$data['operator']} <br/>";
         $message.="Order Amount : {$data['amount']} <br/>";
         $message.="To Pay  : {$data['to_pay']} <br/>";
         $message.="Order date  : {$data['order_date']} <br/>";
         $message.="Order Canceled by  : ".  getLoggedInUsername();
         $this->email->message($message);
         $this->email->send();
    }
    
    public function searchbydate()
    {
        $data = array();

        if($this->input->get('cancelation_date')):
        $data['eliminatedorders']=  $this->eliminateorder->getCanceledOrderByDate($this->input->get('cancelation_date'));
        endif;
            
        $this->load->view('eliminateorders/searchbydate',$data);

    }
}