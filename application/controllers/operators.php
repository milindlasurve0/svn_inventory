<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Operators extends CI_Controller
{
    
    public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        
        $this->load->library('form_validation');
        
        $this->config->load('inventory');
    }
    
    public function addmapping()
    {
    
        $this->load->model('supplier');
        
        $this->load->model('operator');
        
        $data['suppliers']=$this->supplier->getSuppliersForDropdown();
        
        $data['operators']=$this->operator->getOperatorsForDropdown();
        
        if($this->input->post()):
            
            $this->form_validation->set_rules('supplier_id', 'Supplier', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('operator_id', 'Operator', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity', 'Capacity', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_day', 'Capacity per day', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_month', 'Capacity per month', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_month', 'Capacity per month', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('frequency', 'Frequency', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('commission_type', 'Commission Type', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('commission_type_formula', 'Commission Type Formula', 'required|trim|xss_clean|is_numeric|callback_checkvalidformula');
            
                    if ($this->form_validation->run() == FALSE):

                        $this->session->set_flashdata('error', 'There was error while inserting record');

                        $this->load->view('operators/addmapping', $data);

                    else:

                                if ($this->operator->saveOperatorMapping($this->input->post())):

                                    $this->session->set_flashdata('success', 'Mapping was successfully inserted');

                                    redirect('operators/addmapping');

                                else:

                                    $this->session->set_flashdata('error', 'There was error while inserting record');

                                    redirect('operators/addmapping');

                                endif;

                    endif;

         else:
            
            $this->load->view('operators/addmapping',$data); 
        
        endif;
        
        
     
    }
    public function addSupplierOperatorMapping($id=0)
    {
    
        $supplier_id=($id>0)?$id:$this->input->post('supplier_id');
        
        $this->load->model('supplier');
        
        $this->load->model('operator');
        
            $data['supplier']=$this->supplier->getSupplierNameById($supplier_id);
        
           $data['operators']=$this->operator->getOperatorsForDropdown();
       
        if($this->input->post()):
            
            $this->form_validation->set_rules('operator_id', 'Operator', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity', 'Capacity', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_day', 'Capacity per day', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_month', 'Capacity per month', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_month', 'Capacity per month', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('frequency', 'Frequency', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('commission_type', 'Commission Type', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('commission_type_formula', 'Commission Type Formula', 'required|trim|xss_clean|callback_checkvalidformula');
            $this->form_validation->set_rules('riskfactor', 'Risk Factor', 'required|trim|xss_clean|is_numeric');
            
                    if ($this->form_validation->run() == FALSE):

                        $this->session->set_flashdata('error', 'There was error while inserting record');

                        $this->load->view('operators/addSupplierOperatorMapping', $data);

                    else:

                        if(!$this->operator->checkifAlreadyExists($this->input->post('supplier_id'),$this->input->post('operator_id'))):
                                
                           
                                if ($this->operator->saveOperatorMapping($this->input->post())):

                                    $this->session->set_flashdata('success', 'Mapping was successfully inserted');

                                    redirect('operators/addSupplierOperatorMapping/'.$this->input->post('supplier_id'));

                                else:

                                    $this->session->set_flashdata('error', 'There was error while inserting record');

                                    $this->load->view('operators/addSupplierOperatorMapping', $data);

                                endif;
                                
                          else:
                              
                         
                                  $this->session->set_flashdata('error', 'Mapping already exists');

                                   redirect('operators/addSupplierOperatorMapping/'.$this->input->post('supplier_id'));
                              
                          endif;
                              
                              
                                

                    endif;

         else:
            
            $this->load->view('operators/addSupplierOperatorMapping',$data); 
        
        endif;
        
        
     
    }
    
    
    public function checkvalidformula($value)
    {
        if($this->input->post('commission_type')==1 || $this->input->post('commission_type')==2):
            
                if($value>0 && $value<=12):

                 return true;

                else:

                  $this->form_validation->set_message('checkvalidformula', 'The %s field Should be within 1 and 12 ');  
                  return false;

                endif;
         
        endif;
        
        if($this->input->post('commission_type')==3):
            
             if(strpos($value, "=")):
                        
                        return true;
                    
               endif;
        
        endif;
        
        return false;
    }
    

    
    public function viewmapping()
    {
        $this->load->model('supplier');
      
        $this->load->view('operators/viewmapping');
    }
    
    public function getmapping($id=0)
    {
        $supplier_id=$this->input->post('supplier_id')?$this->input->post('supplier_id'):$id;
        
        $this->load->model('supplier');
        
        $this->load->model('operator');
        
        $data['supplier']=  $this->supplier->findSupplierById($supplier_id);
        
        $data['supplier_operators']=$this->operator->getOperatorsDetail($supplier_id);
        
        $this->load->view('operators/getmapping',$data);
       
    }
    
    public function editmapping($id)
    {
         $this->load->model('supplier');
        
        $this->load->model('operator');
        
        $data['suppliers']=$this->supplier->getSuppliersForDropdown();

        $data['operators']=$this->operator->getOperatorsForDropdown();
        
        if($this->input->post()):
            
            $this->form_validation->set_rules('capacity', 'Capacity', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_day', 'Capacity per day', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_month', 'Capacity per month', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('capacity_per_month', 'Capacity per month', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('basesale', 'Base Sale', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('frequency', 'Frequency', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('commission_type', 'Commission Type', 'required|trim|xss_clean|is_numeric');
             if(!$this->input->post('is_api')):$this->form_validation->set_rules('commission_type_formula', 'Commission Type Formula', 'required|trim|xss_clean|callback_checkvalidformula');endif;
            $this->form_validation->set_rules('riskfactor', 'Risk Factor', 'required|trim|xss_clean|is_numeric|callback_checkriskfactor');
            $this->form_validation->set_rules('batch', 'Batch', 'required|trim|xss_clean|is_numeric');
            $this->form_validation->set_rules('disputeamt', 'Dispute Amount', 'trim|xss_clean|is_numeric');
            if($this->input->post('somargincomment',true)): $this->form_validation->set_rules('somargincomment', 'Margin change comment', 'trim|required|xss_clean');  endif;
            if($this->input->post('sobasecomment',true)): $this->form_validation->set_rules('sobasecomment', 'Basesale change comment', 'trim|required|xss_clean');  endif;
            
                if ($this->form_validation->run() == FALSE): 

                            $data['supplier_operator']=  $this->operator->getSupplierOperatorMappingById($id);

                            $this->load->view('operators/editmapping',$data);
                            

                    else:
                        if($this->operator->updateOperatorMapping($id,$this->input->post())):

                                $this->session->set_flashdata('success','Record was Updated ');

                                redirect('operators/editmapping/'.$id);

                                else:

                                $this->session->set_flashdata('error','There was error while Updating record');

                                redirect('operators/editmapping/'.$id);

                                endif;
                                
                   endif;
                   

        else:
            
            $data['supplier_operator']=  $this->operator->getSupplierOperatorMappingById($id);
        
            $this->load->view('operators/editmapping',$data);
            
        endif;
        
        
    }
    
    public function deletemapping($id)
    {
         $this->load->model('operator');
         
         $supplier_id=  $this->operator->getSupplierIdFromOperatorMapping($id);
        
        if($this->operator->deleteSupplierOperatorMapping($id)):
            
                 $this->session->set_flashdata('success','Record Deleted');

                  redirect('operators/getmapping/'.$supplier_id);
                  
        endif;
        
         $this->session->set_flashdata('error','Error in deleting Record');

          redirect('operators/getmapping/'.$supplier_id);
    }
    
         public function checkriskfactor($value)
     {
    
         if($value >= 0 && $value<100):
             
             return true;
         
         else:
             
              $this->form_validation->set_message('checkriskfactor', 'The %s field Should be within 0 and 99%');  
              return false;
              
         endif;
         
         return false;
     }
     
     
     public function setisactive()
     {
          $this->load->model('operator');
              
         $data=  $this->input->post();
         
         if($this->operator->setisactive($data)):
                     echo json_encode(array('status'=>true,'type'=>true,'message'=>'Flag set successfully')); exit();
         endif;
             
         echo json_encode(array('status'=>true,'type'=>false,'message'=>'No data received')); exit();
          
     }
}