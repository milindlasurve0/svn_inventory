<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Earningreports extends CI_Controller
{
    public $reportdate=null;
    
     public function __construct() 
    {
        parent::__construct();

         if(!$this->session->userdata('isLoggedIn') && $this->uri->segment(3)!="invcrn"):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        
        $this->load->model('order');
        $this->load->model('earningreport');
        
    }
    
    public function  __listall()
    {
       $finalOperators=  $this->config->item('operatorkeyvalue');
      
       $result=array();

       $dateParam =  $this->reportdate;
       
       foreach($finalOperators as $operator_id=>$name):
                        
                        $supplierInfo=  $this->earningreport->getOperatorInfo($operator_id,$dateParam);
                        
                        if(!empty($supplierInfo)):
                            $result[$operator_id]=$this->CalculateWieght($supplierInfo,$operator_id);
                       endif;
                        
        endforeach;
         
     
           $crondata['supplierwise']=$result;
                 
           $result=$this->groupByOperator($result);
            
           $result=$this->addOperationalComm($result);
           
           $crondata['operatorwise']=$result;
           
           return $crondata;
           
          
     
    }
    
   
   public function CalculateWieght($supplierInfo,$operator_id)
    {
                $totalBaseSum=0;
                $totalTargetSum=0;
                $totalActualSum=0;
                $resultarr=array();

                foreach($supplierInfo as $row):
                            $totalBaseSum+=$row['basesale'];
                            $totalTargetSum+=$row['target'];
                            $totalActualSum+=$row['sale'];
                endforeach;

               // $api_margins_arr=  $this->setApiMarginArray($operator_id); // From slabs same applies for base & target
              //  $api_actual_weight_arr=  $this->setApiActulaMarginArray($operator_id);  // From VA  applies only for actual
                
                foreach($supplierInfo as $key=>$row):

		$basePartPercentage=($row['basesale']*100)/$totalBaseSum;
		$supplierInfo[$key]['baseweight']=round(($row['margin']*$basePartPercentage)/100,2);


		$targetPartPercentage=($row['target']*100)/$totalTargetSum;
		$supplierInfo[$key]['targetweight']=round(($row['margin']*$targetPartPercentage)/100,2);
            
                           $actualPartPercentage=($row['sale']*100)/$totalActualSum;
                           $supplierInfo[$key]['actualweight']=round(($row['margin']*$actualPartPercentage)/100,2);
                         
                            
                          
                endforeach;

               return $supplierInfo;

    }
 
    public function calculateApiActualWeight($vendor_id,$operator_id,$api_actual_weight_arr,$totalActualSum)
    {
        if(isset($api_actual_weight_arr[$vendor_id."_".$operator_id])  && $totalActualSum>0):
                return $api_actual_weight_arr[$vendor_id."_".$operator_id]/$totalActualSum;
        endif;
        
        return 0;
    }
    
    public function setApiActulaMarginArray($operator_id)
    {
        $data=  $this->earningreport->getApiActualMarginArray($operator_id);
        
        $result=array();
        
        if(!empty($data)):
            foreach($data as $value):
                            $result[$value['vendor_id']."_".$operator_id]=$value['weight'];
            endforeach;
        endif;
        
        return $result;
    }
    
    public function setApiMarginArray($operator_id)
    {
        $data=$this->earningreport->getApiMarginsByOperatorId($operator_id);
        $result=array();
        
        foreach($data as $value):
                        $result[$value['vendor_id']."_".$operator_id]=$value['discount_commission'];
        endforeach;
        
        return $result;
        
    }
    public function groupByOperator($result)
    {
        $toBeReturned=array();
        $operators=  $this->config->item('operatorkeyvalue');
        
        foreach($result as $operator_id=>$value):
                        
                        $totalIdealMargin=0;
                        $totalExpectedMargin=0;
                        $totalActualMargin=0;
                        $totalBaseSale=0;
                        $totalTarget=0;
                        $totalActualsale=0;
                        
                        foreach($value as $row):
                                $totalIdealMargin+=$row['baseweight'];
                                $totalExpectedMargin+=$row['targetweight'];
                                $totalActualMargin+=$row['actualweight'];
                                $totalBaseSale+=$row['basesale'];
                                $totalTarget+=$row['target'];
                                $totalActualsale+=$row['sale'];
                        endforeach;
                        
                        $toBeReturned[$operator_id]['totalIdealMargin']=$totalIdealMargin;
                        $toBeReturned[$operator_id]['totalExpectedMargin']=$totalExpectedMargin;
                        $toBeReturned[$operator_id]['totalActualMargin']=$totalActualMargin;
                        $toBeReturned[$operator_id]['totalBasesale']=$totalBaseSale;
                        $toBeReturned[$operator_id]['totalTarget']=$totalTarget;
                        $toBeReturned[$operator_id]['totalActualSale']=$totalActualsale;
                        $toBeReturned[$operator_id]['operator_name']=$operators[$operator_id];
                        $toBeReturned[$operator_id]['operator_id']=$operator_id;
                        $toBeReturned[$operator_id]['planning_loss']=(($totalBaseSale*$totalIdealMargin)/100)-(($totalTarget*$totalExpectedMargin)/100);
                        $toBeReturned[$operator_id]['operational_loss']=(($totalActualsale*$totalExpectedMargin)/100)-(($totalActualsale*$totalActualMargin)/100);
                        
                        
        endforeach;
        
        
       return $toBeReturned;
       
        }
        
        public function setActualSaleModem($result,$date="")
        {
            foreach($result as $operator_id=>$value):
             
                 $actual_modemsale_arr=  $this->earningreport->getActualModemSaleVendors($operator_id,$date);
            
                        foreach($value as $key=>$row): 

                        $temp=  isset($actual_modemsale_arr[$row['supplier_operator_id']])?$actual_modemsale_arr[$row['supplier_operator_id']]:array();

                        if(!empty($temp)):
                            $result[$operator_id][$key]['actualsalemodemnames']=$temp['actualsalemodemnames'];
                            $result[$operator_id][$key]['actualsalemodemids']=$temp['actualsalemodemids'];
                        endif;
                        
                        endforeach;
              
           endforeach;
           
         return $result;
            
       }
       
       // this is what we give to retailer 
       public function addOperationalComm($data,$operator_id=0)
       {
           
           $operatnational_margin_arr=  $this->earningreport->getOperationalMargin($operator_id,$this->reportdate);
           
           $temp=array();
           
           foreach ($operatnational_margin_arr as $value):
                        $temp[$value['parent']]=$value['comm']/$value['tot'];
           endforeach;
           
           
          foreach($data as $key=>$row):
                        $data[$key]['operationalcost']=isset($temp[$row['operator_id']])?$temp[$row['operator_id']]:0;
          endforeach; 
          
          return $data;
          
      }

      public function saveEarningReport()
      {
        logerror('Started','saveEarningReport');
        
        $date=  $this->input->get('date');
        
        $this->reportdate = empty($date)?date('Y-m-d',  strtotime('-1 day')):$date;
        
        logerror('Cron running for date : '.$this->reportdate,'saveEarningReport');
          
        if(strtotime($this->reportdate)==strtotime('today')): exit('Not Allowed'); endif;
        
        $result=$this->__listall();
        
        $this->earningreport->updateSupplierwiseEarningReport($result['supplierwise'],$this->reportdate);

        $this->earningreport->dumpOperatorwiseEarningReport($result['operatorwise'],$this->reportdate);
        
         logerror('Finished','saveEarningReport');

      }
      
      
        public function sortbyname($a, $b)
        {
             return strcmp($a["operator_name"], $b["operator_name"]);
        }
        
        public function sortbymodemname($a, $b)
        {
             return strcmp($a["vendoridsnames"], $b["vendoridsnames"]);
        }
        
      public function get()
      {
          if($this->input->get('from_history_date')):
              
                   // Since we are not using between 
                   $days=  dateDiff($this->input->get('from_history_date'), $this->input->get('to_history_date'))+1;
          
                   $earninglists= $this->earningreport->getEarningReportByDateRange($this->input->get());
                   
                   $groupedByOperator=array();
                   
                   foreach($earninglists as $row):
                        $groupedByOperator[$row['operator_id']][]=$row;
                   endforeach;

                   // Supplierwise start
                     if($this->input->get('operator_id')):
                       $groupedByOperator=  $this->setActualSaleModem($groupedByOperator,$this->input->get('from_history_date'));   
                       $supplierlist=$groupedByOperator[$this->input->get('operator_id')];
                       $supplierlist=$this->addModemNames($supplierlist,$this->input->get('operator_id'));
                       if($days==1): $supplierlist=$this->addNotesforSOID($supplierlist,$this->input->get('from_history_date'),$this->input->get('operator_id')); endif;
                       usort($supplierlist, array($this,"sortbymodemname"));
                       $data['results']=$supplierlist;
                   endif;
                   //end
                   
                  $results=  $this->groupByOperator($groupedByOperator);
                  
                  usort($results, array($this,"sortbyname"));
                  
                  $results=  $this->addTotalOperationalComm($results,$this->input->get());
                   if($days==1): $results=  $this->addNotesforOpr($results,$this->input->get('from_history_date')); endif;
                  
                  foreach ($results as $key=>$value):
                        $results[$key]['totalIdealMargin']/=$days;
                        $results[$key]['totalExpectedMargin']/=$days;
                        $results[$key]['totalActualMargin']/=$days;
                        $results[$key]['totalBasesale']/=$days;
                        $results[$key]['totalTarget']/=$days;
                        $results[$key]['totalActualSale']/=$days;
                        $results[$key]['planning_loss']/=$days;
                        $results[$key]['operational_loss']/=$days;
                        $results[$key]['operationalcost']/=$days;
                  endforeach; 
                  
                  
                  $data['earningReports']=$results;
                  $data['rangedays']=$days;

                   $data['from_date']=$this->input->get('from_history_date');
                   $data['to_date']=$this->input->get('to_history_date');

              endif;
          
              if($this->input->get('operator_id')):
                  
                   $this->load->view('earningreports/byoperator',$data);
              else:
                   $this->load->view('earningreports/index',$data);
              endif;
               
      }
      
      function addTotalOperationalComm($result,$dates)
      {
          $operationalMargin_arr=  $this->earningreport->getOperationalMarginByDateRange($dates);
          
          foreach($result as $key=>$row):
                    $operator_id=$row['operator_id'];
                    $result[$key]['operationalcost']=isset($operationalMargin_arr[$operator_id])?$operationalMargin_arr[$operator_id]:0;
          endforeach;
          
          return $result;
        }
        
        function addModemNames($supplierlist,$operator_id)
        {
            $modem_arr=  $this->earningreport->getModemNamesByOperatorid($operator_id);
          

            foreach($supplierlist as $key=>$row):
              $supplierlist[$key]['vendoridsnames']=isset($modem_arr[$row['supplier_operator_id']])?$modem_arr[$row['supplier_operator_id']]['vendornames']:"";
              $supplierlist[$key]['vendorids']=isset($modem_arr[$row['supplier_operator_id']])?$modem_arr[$row['supplier_operator_id']]['vendorids']:"";
            endforeach;
            
            return $supplierlist;
        }
        
        function addNotesforSOID($supplierlist,$date,$operator_id)
        {
            
              $notes_arr=  $this->earningreport->getNotesCount($date,$operator_id,$type=1);
          
              foreach($supplierlist as $key=>$row):
                   $supplierlist[$key]['messagecount']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]['totalmessages']:0;
                   $supplierlist[$key]['messages']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]['messages']:'';
               endforeach;
               
               return $supplierlist;
        }
        
        function addNotesforOpr($result,$date)
        {
            foreach($result as $key=>$row):
                   $notes_arr=  $this->earningreport->getNotesCount($date,$row['operator_id'],$type=2);
                   $result[$key]['messagecount']=isset($notes_arr[$row['operator_id']])?$notes_arr[$row['operator_id']]['totalmessages']:0;
                   $result[$key]['messages']=isset($notes_arr[$row['operator_id']])?$notes_arr[$row['operator_id']]['messages']:'';
               endforeach;
               
               return $result;
        }
      
        
        public function checkEarningReportByDate()
        {
            $date=  $this->input->get('date');
            
            if(empty($date)):  exit('No Date Passed'); endif;
           
            $invalid_data=$this->earningreport->getInvalidData($date);
           
            logerror("Diff data : ".  json_encode($invalid_data),"checkEarningReportByDate");
            
            if(!empty($invalid_data)):
                
                foreach($invalid_data as $row):
                //   echo "Diff : ".$row['diff'];   echo "<br/>";
                   echo  $sql=" Update inv_set_target_history set currentsale='{$row['devices_sale']}' where supplier_operator_id='{$row['supplier_operator_id']}'  AND vendor_id='{$row['vendor_id']}' AND history_date='{$date}' ; ";
                   echo "<br/>";
                   echo "<br/>";
                endforeach;
                
            else:
                echo "Yeah ! ! ";
            endif;
            
        }
        
}
