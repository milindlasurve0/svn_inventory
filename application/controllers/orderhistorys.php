<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Orderhistorys extends CI_Controller
{
      public function __construct()
      {
            parent::__construct();
            if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

            endif;
           
            $this->load->model('operator');
            $this->load->model('supplier');
            $this->load->model('orderhistory');
               
      }
      
      public function get($date=null)
      {
           $this->load->model('incoming');
           
          $date=is_null($date)?date('Y-m-d'):$date;
          
          $datediff=dateDiff($this->input->get('start'),$this->input->get('end'));
          
          if($datediff > 30):   
                exit("Daterange can not exceed 30 days.");
          endif;
          
          $data['operators']=$this->config->item('invoperators');
          $data['suppliers']=  $this->supplier->getSuppliersForDropdown();
          $data['internals']=  $this->incoming->getInternalsPersons();
            
          if($this->input->get()):
                
              $orders=$this->orderhistory->getOrderHistoryByDate($this->input->get());
//              $orders=$this->setCurrentPending($orders);
              $data['orders']= $orders;
              
          endif;
          
          $this->load->view('orderhistorys/get',$data);
      }
      
      public function pendinghistory($so_id,$showall=false)
      {
          
          $data['pendings']=  $this->orderhistory->getPendinghistory($so_id,$showall);
          $data['soid']=$so_id;
           $this->load->view('orderhistorys/pendinghistory',$data);
           
      }
      
      public function comments()
      {
            $this->load->model('orderapproval');
            $soid=  $this->input->get('soid');
            $date= $this->input->get('date')?$this->input->get('date'):null;
           $order_id= $this->input->get('order_id')?$this->input->get('order_id'):0;
             
            $data['comments']=  $this->orderapproval->getCommentsBySOID($soid,$date,$order_id);
          
              $this->load->view('orderhistorys/commenthistory',$data);
      }
      
      
      public function getPendingBySoid()
      {
         $this->load->model('pending');
        
        $pendings=$this->pending->getPendingBySoid($this->input->get('soid'));
        
        if(!empty($pendings)):
            
              echo json_encode(array('status'=>true,'type'=>true,'data'=>$pendings));exit();
              
        endif;
        
         echo json_encode(array('status'=>true,'type'=>false,'data'=>""));exit();
      }
      
          public function getpendingbysupplierid()
    {
        $this->load->model('pending');
        
        $pendings=$this->pending->getPendingBySupplierId($this->input->get('supplierid'),false,$this->input->get('operatorid'));
       
        
        if(!empty($pendings)):
            
              echo json_encode(array('status'=>true,'type'=>true,'data'=>$pendings));exit();
              
        endif;
        
         echo json_encode(array('status'=>true,'type'=>false,'data'=>""));exit();
    }
    
    public function setCurrentPending($orders)
    {
        $this->load->model('order');
        
        if(!empty($orders)):
                    foreach($orders as $key=>$order):
                        $orders[$key]->current_pending=$this->order->getLastPending($order->supplier_operator_id);
                    endforeach;
        endif;
        
        return $orders;
    }
    
    public function setrefund()
    {
        $data=$this->input->post();

        // Check if negative on that date & entered pending amount should be <= that date pending
       if($this->orderhistory->isValidEntry($data)):
           
           if($this->checkUserId($data)):
           
                if($this->orderhistory->validateAmount($data)):

                     if($this->orderhistory->getrefund($data)):

                          echo json_encode(array('status'=>true,'type'=>true,'message'=>"Refund Successful"));

                             if($data['enteredamt'] >= 1000 || $data['enteredamt']<=-1000):

                                 $this->sendRefundAdjustmentEmail($data);
                          endif;
                          exit();

                     endif;

                else:
                    echo json_encode(array('status'=>true,'type'=>false,'message'=>"Entered amount exceeds maximum refund amount of 1 Lac for this supplier_operator"));
                endif;
                    
                endif;

       else:
                    echo json_encode(array('status'=>true,'type'=>false,'message'=>"Invalid Amt Or Avaliable amount exceeds entered amt"));exit();
       endif;
       
       
    }
    
public function checkUserId($data)
   {
       $enteredamt=$data['enteredamt']<0?$data['enteredamt']*(-1):$data['enteredamt'];
       
       if((getLoggedInUserId()==7883026 && $enteredamt > 100) || (getLoggedInUserId()==7883026 && $data['refundType']==1)): //inventory team can adjust amount till 100 only
              echo json_encode(array('status'=>true,'type'=>false,'message'=>"You can only adjust amount till 100rs"));exit();
       elseif(in_array(getLoggedInUserId(),array(48347012,48636190)) && $enteredamt > 100000 && $data['refundType']==2)://sunilr  can adjust amount till 1 Lac only
              echo json_encode(array('status'=>true,'type'=>false,'message'=>"You can adjust amount till 1 Lac only"));exit();
       elseif(!in_array(getLoggedInUserId(), array(7883026,48347012,48636190))):
              echo json_encode(array('status'=>true,'type'=>false,'message'=>"You dont have permission to make adjustments"));exit();
       else:
              return true;
       endif;
   }
    
    public function sendRefundAdjustmentEmail($data)
    {
                $maildata=$this->orderhistory->getEmailData($data);

                $this->load->library('email');
                $this->email->initialize(array('mailtype'=>'html','protocol'=>'sendmail'));
                $this->email->from('orders@mindsarray.com', 'Pay1');
                $this->email->to(array('deepti@mindsarray.com','pushpa@mindsarray.com','deepak.d@pay1.in','aamir@mindsarray.com','esmerald@mindsarray.com','sunilr@pay1.in','yojana@mindsarray.com','vibhas@mindsarray.com')); 
                $this->email->subject('Refund Adjustments');
                $message="<b>Supplier</b> : {$maildata['supplier_name']} <br/>";
                $message.="<b>Operator</b>  : {$maildata['operator_name']} <br/>";
                $message.="<b>Current Pending Amount</b>  : {$maildata['pending']} <br/>";
                $message.="<b>Adjusted Amount</b> : {$maildata['refund']} <br/>";
                $message.="<b>Comment</b>  : {$maildata['comment']} <br/>";
                $refund_type=$maildata['refund_type']=="1"?"Bank":"Adjustments";
                $message.="<b>Refund Type</b> : $refund_type <br/>";
                $message.="<b>Refund Adjusted by</b>  : ".  getLoggedInUsername()."<br/>";
                $message.="<b>Pending Date</b> :{$maildata['pending_date']} <br/>";
                $this->email->message($message);
                $this->email->send();
        }
}
