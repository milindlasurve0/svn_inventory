<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller
{
    public function __construct() {
        parent::__construct();
          if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

          endif;
          
          $this->load->model('order');
          
          $this->load->model('account');
          
          $this->load->model('supplier');
          $this->load->model('operator');
          
            $this->config->load('inventory');
            
            
            $this->load->helper('supplier');
    }
    
    public function download()
    {
        $data=array();
        $orders=array();
       
        if($this->input->get()):
          
            $orders=  $this->account->getOrder($this->input->get());
        
            if(!empty($orders)):
                    $orders=  $this->setPendings($orders);
                    $orders=  $this->createBatch($orders);
                    $orders=  $this->groupByBank($orders);
                    $orders=  $this->groupBySupplier($orders);
                    
                    ksort($orders);
                    
                    $data['batches']=$orders;
                    $data['banklist']= $this->getKeyValueBankPair();
            endif;
            
        endif;
        
        $data['operators']=$this->operator->getOperatorsForDropdown();
        
        $this->load->view('accounts/download',$data);
    }
    
    public function createBatch($orders)
    {
        $temp=array();
        
        foreach($orders as $order):
            $temp[$order['batch']][]=$order;
        endforeach;
        
        return $temp;
    }
    
    public function groupByBank($batches)
    {
          $temp=array();
          
           foreach($batches as $batchno=>$batch):
                    
                foreach($batch as $order):
               
                        $temp[$batchno][$order['frombank']][]=$order;
               
                endforeach;
               
           endforeach;
           
           return $temp;
    }
    public function groupBySupplier($batches)
    {
         $temp=array();
            
           foreach($batches as $batchno=>$batch):
             foreach($batch as $bankid=>$bank):
                foreach($bank as $order):
                        $temp[$batchno][$bankid][$order['supplier_name']][$order['order_id']]=$order;
                endforeach;
             endforeach;
         endforeach;   
         
         return $temp;
    }
    public function setPendings($orders)
    {
         foreach($orders as $key=>$value):
                    $orders[$key]['pending']= $this->order->getLastPending($value['supplier_operator_id']);
         endforeach;
         
         return $orders;
    }
    public function setDownloadStatus()
    {
        if($this->account->updateDownloadFlag($this->input->post())):
            
            echo json_encode(array('status'=>true,'type'=>true,'message'=>"Flag updated successfully"));exit();
            
        endif;
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
        
    }
    
    public function getKeyValueBankPair()
    {
        $temp=array();
        
        $banks=$this->supplier->getBanks();
        
        foreach($banks as $bank):
            $temp[$bank->id]=$bank->bank_name;
        endforeach;
        
        return $temp;
    }
    
    public function downloadexcel()
    {
        
        $ids=$this->input->get();

        logerror("URL : http://inv.pay1.in/accounts/downloadexcel/?orderids=".$ids['orderids']."&fb=".$ids['fb'],"downloadexcel");
        
       $orderdata=  $this->account->getExcelRelatedOrderData($ids['orderids'],$ids['fb']);
         
        $this->load->helper('csv');
        
        $date=date('Y-m-d');
        
        echo array_to_csv($orderdata,"excel_{$date}.csv");

             
      }
      
      public function updateTxnId()
      {
        $data=array();
        $orders=array();
       
        if($this->input->get()):
          
            $params=$this->input->get();
            $params['updateTxnId']=true;
            
            $orders=  $this->account->getOrder($params);
        
            if(!empty($orders)):
                    $orders=  $this->setPendings($orders);
                    $orders=  $this->createBatch($orders);
                    $orders=  $this->groupByBank($orders);
                    $orders=  $this->groupBySupplier($orders);
                    ksort($orders);
                    $data['batches']=$orders;
                    $data['banklist']= $this->getKeyValueBankPair();
            endif;
            
        endif;
        
        $data['operators']=$this->operator->getOperatorsForDropdown();
        
        $this->load->view('accounts/updatetxnid',$data);
      }
      
      public function savetxnid()
      {
          if($this->account->updateTxnid($this->input->post())):
              
              echo json_encode(array('status'=>true,'type'=>true,'message'=>"Transaction Updated Successfully"));exit();
              
          endif;
          
           echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
      }
      
      public function rejecttxnid()
      {
          if($this->account->rejectTxnid($this->input->post())):
              
              echo json_encode(array('status'=>true,'type'=>true,'message'=>"Transaction Rejected Successfully"));exit();
              
          endif;
          
           echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
      }
      
      public function uploadexcel()
      {
               if(empty($_FILES['utrfile']['name'])): exit('Kindly Upload your UTR excel'); endif;
               
                $config['upload_path'] = FCPATH.'uploads/';
                $config['allowed_types'] = 'csv';
                $config['max_size']     = '1024';
                $config['file_name']="excel_".date('Y-m-d H:i:s');
                
                $this->load->library('upload', $config);
                
                 $this->upload->initialize($config);
                 
                if ($this->upload->do_upload('utrfile')):
                    
                         $uploadData=$this->upload->data('file_name');
                        
                        if (($handle = fopen($uploadData['full_path'], "r")) !== FALSE):
                            
                             $row = 1;
                        
                                while (($data = fgetcsv($handle, 0, ",")) !== FALSE):
                            
                                             $num = count($data); // Get total Field count
                                             
                                              for ($c=0; $c < $num; $c++):
                                                  
                                                          $temp[$row][]=$data[$c];
                                              
                                              endfor;
                                              
                                              $row++;
                        
                                endwhile;
                        
                                   fclose($handle);
                                   
                        endif;
                        
                       array_shift($temp);   //Remove Header from excel
                       
                      if(!empty($temp)):
                        $response=$this->account->updateUtrFromExcel($temp);
                        $result=$this->formatResponse($response);
                        
                        if(!empty($result)):
                            $this->session->set_flashdata('error',"UTR of order id '{$result}' already updated !");
                            redirect('accounts/updateTxnId?batch_type=&operator_id=');
                        else:
                             $this->session->set_flashdata('success',"UTR  updated successfully!");
                             redirect('accounts/updateTxnId?batch_type=&operator_id=');
                        endif;
                        
                      else:
                         echo "Empty Excel Detected";   exit();
                      endif;
                        
                else:
                    
                        echo "<pre>";
                        print_r($this->upload->display_errors());
                        echo "</pre>";

                endif;
                
      }
      
      public function formatResponse($data)
      {
          $temp=array();
          
          foreach ($data as $val):
              if($val['is_utr_updated']==0):
                  $temp[]=$val['order_id'];
              endif;
          endforeach;  
          
         $response= count($temp)>1?implode(",",$temp):(!empty($temp)?$temp[0]:$temp);
          
          return $response;
        }

}
