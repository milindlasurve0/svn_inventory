<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plannings extends CI_Controller
{
    
    public function __construct()
    {
        
         parent::__construct();

         if($this->uri->segment(3)!="invcrn"):
            if(!$this->session->userdata('isLoggedIn')):

                $this->session->set_flashdata('error','You need to be logged in to view that page');
                redirect('/');

            endif;
          endif;  
            
            $this->load->model('operator');
            
            $this->load->model('supplier');
            
            $this->load->model('planning');
            
            $this->load->model('order');
    }
    
    public function configure()
    {
         $data['operators']=$this->operator->getOperatorsForDropdown();
          
         $this->load->view('planning/configure',$data);
    }
    
    public function getSupplierListByOperatorIdJSON($operator_id=0,$iscron=false)
    {
        $operator_id=$operator_id?$operator_id:$this->input->get('operator_id');
        $date=$this->input->get('target_date')?$this->input->get('target_date'):date('Y-m-d');
        
        if(strtotime($date)==strtotime('today')):
        
                            $this->load->library('Memcachedlib');

                            $data=$this->supplier->findSuppliersByOperatorId($operator_id);
                           
                            $last_day_api_sale_arr=array();   

                            $avg_day_sale_arr=  $this->planning->getLastdaySaleByOprId($operator_id,true);   

                            $current_day_sale_arr=  $this->planning->getLastdaySaleByOprId($operator_id,false,true);   

                            $multiplevendormappingarr=$this->planning->getMultipleVendorMapping($operator_id);

                            // Add Api vendors AVG and LDS in  $avg_day_sale_arr so that it can auto get mapped in isset logic .Also used [0] because in api only 1 vendor would be mapped
                           // Add Api CUR sale 
                          // Start  
                           $apiavgsale_arr=json_decode($this->memcachedlib->get('inv_avgapisale'),true);
                           $apildssale_arr=json_decode($this->memcachedlib->get('inv_ldsapisale'),true);
                           $apicursale_arr=  $this->planning->getApiCurSale($operator_id);

                           foreach($multiplevendormappingarr as $row):
                                if(isset($apiavgsale_arr[$row[0]['vendor_id']."_".$operator_id])):
                                                  $avg_day_sale_arr[$row[0]['supplier_operator_id']]=$apiavgsale_arr[$row[0]['vendor_id']."_".$operator_id];
                                              endif;
                                              if(isset($apildssale_arr[$row[0]['vendor_id']."_".$operator_id])):
                                                  $last_day_api_sale_arr[$row[0]['supplier_operator_id']]=$apildssale_arr[$row[0]['vendor_id']."_".$operator_id];
                                              endif;
                                              if(isset($apicursale_arr[$row[0]['vendor_id']])):
                                                  $current_day_sale_arr[$row[0]['supplier_operator_id']]=$apicursale_arr[$row[0]['vendor_id']];
                                              endif;
                            endforeach;
                         // End

                                  if(!empty($data)):

                                              foreach($data as $key=>$value):

                                              $data[$key]['avg_sale']=isset($avg_day_sale_arr[$value['supplier_operator_id']])?$avg_day_sale_arr[$value['supplier_operator_id']]:0.00  ;

                                              $data[$key]['current_sale']=isset($current_day_sale_arr[$value['supplier_operator_id']])?$current_day_sale_arr[$value['supplier_operator_id']]:0.00  ;

                                              $data[$key]['vendors']=isset($multiplevendormappingarr[$value['supplier_operator_id']])?$multiplevendormappingarr[$value['supplier_operator_id']]:0.00;

                                              // Set API LDS since Modem LDS is already fetched in query
                                              if(isset($last_day_api_sale_arr[$value['supplier_operator_id']])):
                                                  $data[$key]['last_day_sale']=$last_day_api_sale_arr[$value['supplier_operator_id']];
                                              endif;

                                              endforeach;

                                              $data=$this->addNotesCount($data,$operator_id,$date);

                                              // Sort Data by desc as per last day sale         
                                                usort($data,function($a,$b){return $a['margin']<$b['margin']?1:-1;});

                                      
                                              echo json_encode(array('suppliers'=>$data,'status'=>'success','type'=>true));

                                  else:
                                            
                                               echo json_encode(array('status'=>'success','type'=>false));
                                  
                                  endif;

              else:
                  
                        $data=  $this->planning->getSetTargetFromHistory($this->input->get());
                            
                         $data=$this->addNotesCount($data,$operator_id,$date);
                         
                          usort($data,function($a,$b){return $a['margin']<$b['margin']?1:-1;});

                        echo !empty($data)?json_encode(array('suppliers'=>$data,'status'=>'success','type'=>true)):json_encode(array('status'=>'success','type'=>false));
                  
              endif;

        }

        public function setTarget()
        {
            $targets=json_decode($this->input->post('targets'));

            if(!empty($targets)):

                if($this->planning->saveTarget($targets)):

                        echo json_encode(array('status'=>'success','type'=>true,'message'=>'Target Set Successfully'));

                else:

                        echo json_encode(array('status'=>'success','type'=>false,'message'=>'Unable to set Targets'));

               endif;

            endif;
        
        exit();
        
     }
     
     
     public function addNotesCount($data,$operator_id,$date)
     {
        
          $this->load->model('earningreport');
          $notes_arr=  $this->earningreport->getNotesCount($date,$operator_id,$type=1);
       
          foreach($data as $key=>$row):
                        $data[$key]['message']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]:array('totalmessages'=>'0','messages'=>'');
          endforeach;
          
          return $data;
          
       }
//     public function storeSetTargetToCron()
//     {
//         $totalOperators=$this->config->item('invoperators');
//         
//         $temp=array();
//         foreach($totalOperators as $row):
//                    $response=$this->getSupplierListByOperatorIdJSON($row['id'],true);
//                    if(!empty($response) && is_array($response)):
//                          $this->planning->storetargethistory($response);
//                    endif;
//         endforeach;
//         
//       
//          }
}

