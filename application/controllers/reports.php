<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller
{
     public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn') && $this->uri->segment(3)!="invcrn"):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        
        $this->load->model('report');
    }
    
    public function pendingreport($isCron=false)
    {
      
        $from_date= $this->input->get('from_history_date')?$this->input->get('from_history_date'):date('Y-m-d');
        $to_date= $this->input->get('to_history_date')?$this->input->get('to_history_date'):date('Y-m-d');
        
        
        // For current day
        if(strtotime($from_date)==strtotime($to_date) && strtotime($from_date)==strtotime('today')):
        
                                $pendingreport=$this->report->getPendingReport();

                                $data=$this->formulateTodaysPendingReport($pendingreport);
                                
                                $data['from_date']=$from_date;
                                $data['to_date']=$to_date;

                                if($isCron):
                                      return $data;
                                endif;
                                
                                $this->load->view('reports/pendingreport',$data);
                            
        else: // For Range
            
                                  $data=  $this->getPendingReportByRange($from_date,$to_date);
                                  
                                  $this->load->view('reports/pendingreportrange',$data);
            endif;
        
        }
        
        public function groupBydate($pendingreport)
        {
                $datesRange=array();
                 
                foreach($pendingreport as $row):
                                       $datesRange[$row['history_date']][]=$row;
                 endforeach;   
                 
                 return $datesRange;
        }
        
        public function formulateTodaysPendingReport($pendingreport)
        {
            
            $lsum=0;
            $ssum=0;
            $gsum=0;

            $longtermpendingsuppliers=array();
            $shorttermpendingsuppliers=array();
            $goodbookssuppliers=array();
            
                            foreach($pendingreport as $key=>$row):

                                        if(($row['factor']==0)):
                                            $longtermpendingsuppliers[]=$row;
                                            $lsum+=$row['currentpending'];

                                            $pendingreport[$key]['longterm']=1;
                                            $pendingreport[$key]['shorterm']=0;
                                            $pendingreport[$key]['goodbooks']=0;
                                        elseif($row['factor']!=1 || $row['factorstring']!="00000"):
                                            $shorttermpendingsuppliers[]=$row;
                                            $ssum+=$row['currentpending'];

                                            $pendingreport[$key]['longterm']=0;
                                            $pendingreport[$key]['shorterm']=1;
                                            $pendingreport[$key]['goodbooks']=0;
                                        elseif($row['factor']==1 && $row['factorstring']=="00000"):
                                            $goodbookssuppliers[]=$row;
                                            $gsum+=$row['currentpending'];

                                            $pendingreport[$key]['longterm']=0;
                                            $pendingreport[$key]['shorterm']=0;
                                            $pendingreport[$key]['goodbooks']=1;
                                        endif;

                            endforeach;

                                    $disputes=  $this->report->getDisputeAmt();

                            
                                    $data['disputesum']=array_sum(array_map(function($args) {return $args['disputeamt']; },$disputes));
                                    $data['disputes']=$disputes;
                                    
                                    $data['lsum']=$lsum;
                                    $data['ssum']=$ssum;
                                    $data['gsum']=$gsum;

                                    $data['longtermsuppliers']=$longtermpendingsuppliers;
                                    $data['shorttermsuppliers']=$shorttermpendingsuppliers;
                                    $data['goodbookssuppliers']=$goodbookssuppliers;
                                    
                             
                                    return $data;
                                    
        }
        
        public function pendingreportbydate($date)
        {
            $pendingreport=$this->report->getPendingHistory($date,$date);
            $lsum=0;  $ssum=0;$gsum=0;
            
            if(!empty($pendingreport)):
                                    
                                   foreach($pendingreport as $row):

                                          if($row['longterm']=="1"):
                                              $longtermpendingsuppliers[]=$row;
                                              $lsum+=$row['currentpending'];
                                           elseif($row['shorterm']=="1"):
                                               $shorttermpendingsuppliers[]=$row;
                                               $ssum+=$row['currentpending'];
                                           elseif($row['goodbooks']=="1"):
                                              $goodbookssuppliers[]=$row;
                                              $gsum+=$row['currentpending'];
                                          endif;

                                          if($row['disputeamt']<>0):
                                              $disputes[]=$row;
                                          endif;

                                   endforeach;
                                   
               endif;
               
             $data['disputesum']=array_sum(array_map(function($args) {return $args['disputeamt']; },$disputes));
             $data['disputes']=$disputes;
             
            $data['lsum']=$lsum;
            $data['ssum']=$ssum;
            $data['gsum']=$gsum;

            $data['longtermsuppliers']=$longtermpendingsuppliers;
            $data['shorttermsuppliers']=$shorttermpendingsuppliers;
            $data['goodbookssuppliers']=$goodbookssuppliers;
            
            
             $data['from_date']=$date;
             $data['to_date']=$date;
            
              $this->load->view('reports/pendingreport',$data);
        }
        
        public function getPendingReportByRange($from_date,$to_date)
        {
              $pendingreport=$this->report->getPendingHistory($from_date,$to_date);
                           
                                $pendingreport=  $this->groupBydate($pendingreport);
                                
                                $result=array();
                                 
                                foreach ($pendingreport as $date=>$value):
                                    
                                    $lsum=0; $ssum=0; $gsum=0; $dsum=0;
                                    
                                    foreach ($value as $row):
                                    
                                                        if($row['longterm']=="1"):
                                                              $lsum+=$row['currentpending'];
                                                        elseif($row['shorterm']=="1"):
                                                               $ssum+=$row['currentpending'];
                                                        elseif($row['goodbooks']=="1"):
                                                                $gsum+=$row['currentpending'];
                                                        endif;
                                                        
                                                        if($row['disputeamt']<>0):
                                                                $dsum+=$row['disputeamt'];
                                                        endif;
                                                        
                                                        
                                                        
                                    endforeach;
                                    
                                    $result[$date]=array('lsum'=>$lsum,'ssum'=>$ssum,'gsum'=>$gsum,'dsum'=>$dsum);
                                    
                                endforeach;
                                
                $data['ranges']=$result;
                $data['from_date']=$from_date;
                $data['to_date']=$to_date;
                                  
               return $data;
        }
        
        
        public function savePendingReport()
        {
            $data=  $this->pendingreport($isCron=true);
		
            $disparr=$this->report->getDisputeAmt();
            $disuptes=array();

	     foreach($data['longtermsuppliers'] as $row):
                     $row['longterm']=1;
                     $row['shorterm']=0;
                     $row['goodbooks']=0;
                    $temp[]=$row;
            endforeach;
            
            foreach($data['shorttermsuppliers'] as $row):
                     $row['longterm']=0;
                     $row['shorterm']=1;
                     $row['goodbooks']=0;
                    $temp[]=$row;
            endforeach;
            
            foreach($data['goodbookssuppliers'] as $row):
                     $row['longterm']=0;
                     $row['shorterm']=0;
                     $row['goodbooks']=1;
                    $temp[]=$row;
            endforeach;	
           
            // Convert disputes key-value pair
                 foreach($disparr as $row):
                        $disuptes[$row['supplier_operator_id']]=$row['disputeamt'];
                 endforeach;
                
            // Add Disputes
                 foreach($temp as $key=>$value):
                        $temp[$key]['disputeamt']=isset($disuptes[$value['supplier_operator_id']])?$disuptes[$value['supplier_operator_id']]:0.00;
                 endforeach;
          
             if(!empty($temp)):
              $this->report->setPendingReportToDb($temp);
            endif;
        }
        
}


