<?php

class Pendings extends CI_Controller
{
   public function __construct() {
       parent::__construct();
       $this->load->model('pending');
   }
   
   public function adjustPending()
   {
       $data=  $this->input->post('data');
       
       $data=  json_decode($data,true);
      
       if(!empty($data)):
           
                    $sourceData= $data['parentpendings'][0];
                    $targetData=  $data['targetpendings'];
           
                    $sourceOrderid=$this->pending->getOrderidByPendingId($sourceData['id']);
                    
                    /*
                     * --------------------------------Source Part------------------------------------------
                     * -----------------------------------Start----------------------------------------------
                     */
                    
                    // Create & Adjust Order 
                    if($sourceOrderid):
                                                   $this->pending->adjustOrder($sourceOrderid,$sourceData,$isParent=true);
                    else:
                                                   $sourceOrderid=$this->pending->createOrder($sourceData,$isParent=true);
                    endif;
                    
                    // Adjust Pending
                    $this->pending->adjustPendingTable($sourceOrderid,$sourceData);
             
                    /*
                     * --------------------------------Source Part------------------------------------------
                     * -----------------------------------END----------------------------------------------
                     */
                    
                    
                    $transactionDetails=  $this->pending->getTransactionDetailsByOrderid($sourceOrderid);
                    
                     /*
                     * --------------------------------Target  Part------------------------------------------
                     * -----------------------------------Start----------------------------------------------
                     */
                   $targetLog=array();
                   
                   foreach($data['targetpendings'] as $target):
                       
                                     $targetOrderid=$this->pending->getOrderidByPendingId($target['id']);
                   
                                     if($targetOrderid):
                                                 $this->pending->adjustOrder($targetOrderid,$target,$isParent=false);
                                     else:
                                                $targetOrderid=$this->pending->createOrder($target,$isParent=false);
                                     endif;
                       
                                     // Adjust Pending
                                    $this->pending->adjustPendingTable($targetOrderid,$target);
                                  
                                    $targetLog[]=$target['textboxsum']." (#{$targetOrderid}) ";
                                  
                                    // Set TargetLog
                                    $this->pending->setLog($targetOrderid,$target,array("{$sourceData['textboxsum']} (#{$sourceOrderid}) "),$isParent=false);
                                    
                   endforeach;
                    
                    /*
                    * --------------------------------Target Part------------------------------------------
                    * -----------------------------------End----------------------------------------------
                    */
                
                    // Set SourceLog
                    $this->pending->setLog($sourceOrderid,$sourceData,$targetLog,$isParent=true);
                   
                   
                    /*
                     * End
                     */
                   
                   echo json_encode(array('status'=>true,'type'=>true,'message'=>'Successfully adjusted'));exit();
       endif;
         
                  echo json_encode(array('status'=>true,'type'=>false,'message'=>'Empty Set'));exit();
   }
}

