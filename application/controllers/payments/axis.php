<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Axis extends CI_Controller
{
     public function __construct() {
          parent::__construct();
     }
     
     public function get()
     {
        logerror("START","axispayments") ;
        
        logerror("SERVER PARAMS ".json_encode($_SERVER),"axispayments");    
        
        logerror("POST PARAMS ". json_encode($this->input->post()),"axispayments");   
        
         logerror("END","axispayments") ;
         
         echo json_encode(array('status'=>'success','type'=>true,'pay1_tran_id'=>'SOME ID WITH TYPE INT','message'=>'SOME MESSAGE TYPE STRING'));
     }
}