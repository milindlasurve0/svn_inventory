<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reconciles extends CI_Controller
{
     public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('isLoggedIn')):

          $this->session->set_flashdata('error','You need to be logged in to view that page');
          redirect('/');

        endif;
        $this->load->model('reconcile');
     }
     
     public function report()
     {
         $data=array();
        
         if($this->input->get('from_date') && $this->input->get('to_date')):
             
                    $nofdays=dateDiff($this->input->get('from_date'), $this->input->get('to_date'));

                     if($nofdays > 15): exit("Date range should not exceed 15 days");endif;

                    $dates=GetDays($this->input->get('from_date'),$this->input->get('to_date'));

                    foreach($dates as $date):
                        
                             $data['reconcile'][$date]=  $this->reconcile->getReconcileReport($date);
                    
                    endforeach;
       
         endif;

       $this->load->view('reconciles/report',$data);
     }
     
     public function get()
     {
         $data=array();
         
         if($this->input->get('searchdate')):
             $data['refunds'] = $this->reconcile->getReconcileReportByRefundType($this->input->get('searchdate'));
         endif;

        $this->load->view('reconciles/refunds',$data);
     }
}