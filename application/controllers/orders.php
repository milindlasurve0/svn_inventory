<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Orders extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

        endif;
        
         $this->load->model('operator');
         
         $this->load->model('order');
         
        $this->load->helper('order');
         
         $this->load->helper('dates');
    }
    
    // 0:not in list 1:inlist 2: disapproved
    public function configure()
    {
//         echo "<pre>";
//        print_r($this->session->all_userdata());
//        echo "</pre>";
        
        $data['operators']=$this->config->item('invoperators');
          
        $this->load->view('orders/configure',$data);
    }
    
   
    
    public function getSuppliersForOrders()
    {
        $operator_id=$this->input->get('operator_id');
        $days=($this->input->get('order_days'))?$this->input->get('order_days'):2;
        $existsFlag=$this->input->get('existsFlag');
        
        //Get all supplier wrt operator id
        $supplierlist=$this->order->getSuppliers($operator_id);

        //$days=  $this->calculateDays($days);
          
        if($existsFlag):
                $existingOrderAmts=  $this->order->getexistingOrderAmt($operator_id);
                $existingOrderDays=  $this->order->getexistingOrderDays($operator_id);
        endif;


        // Get list of all Supplierwise sale for last date
        $supplierwiseLastDaySale=  $this->order->getSupplierWiseLastDaySale();
       
        $frequencylist=$this->config->item('frequency');
        
        // Iterate total supplierlist to generate order
        foreach($supplierlist as $key=>$value): //echo $value['name']; echo "[".$value['supplier_operator_id']."]";
                $inlist=0;
                $finaldays=$existsFlag?(isset($existingOrderDays[$value['supplier_operator_id']])?($existingOrderDays[$value['supplier_operator_id']]):$days):$this->input->get('order_days'); 
                $factor=  isUpperBoundH($value['supplier_operator_id'])?(100+$value['commission_type_formula'])/100:(100-$value['commission_type_formula'])/100;
                $frequencykey=!empty($value['frequency'])?$value['frequency']:"1";
                $data=$this->order->getSupplierWiseSimdata($value['supplier_operator_id']);
                $supplierlist[$key]['opening']= !is_null($data->closing)?$data->closing:0.00;
                $supplierlist[$key]['lastdayincoming']= !is_null($data->lastdayincoming)?$data->lastdayincoming:0.00;
                $supplierlist[$key]['totalSims']= $data->totalSims;
                $supplierlist[$key]['persimsale']= !is_null($data->persimsale)?$data->persimsale:0.00;
                $supplierlist[$key]['pending']=  $this->order->getLastPending($value['supplier_operator_id']);    
                $supplierlist[$key]['lastdaysale']=isset($supplierwiseLastDaySale[$value['supplier_operator_id']])?$supplierwiseLastDaySale[$value['supplier_operator_id']]:0.00;  
                $supplierlist[$key]['blockedBalance']= !is_null($data->blockedbalance)?$data->blockedbalance:0.00;
                $supplierlist[$key]['frequency']= $frequencylist[$frequencykey];
                $supplierlist[$key]['comments']="";
                $supplierlist[$key]['no_of_days']=$finaldays;
         
               $supplierlist[$key]['order_amount_till_now']=$this->order->getOrderAmountTillNow($value['supplier_operator_id']);
               $supplierlist[$key]['weekfactor']=((date("d") - date("w") - 1) / 7) + 1;
               
                // Set Freeze flag
                $supplierlist[$key]['is_freezed']=$this->setFreezeFlag($value['supplier_operator_id']);
       
                // Setting Inlist Flag 
                if(!$existsFlag):
                    $inlist=$this->setinListFlag($frequencylist[$frequencykey]);
                else:
                        if(array_key_exists($value['supplier_operator_id'],$existingOrderAmts['approved'])):
                             $inlist=1;
                        endif;
                        if(array_key_exists($value['supplier_operator_id'],$existingOrderAmts['disapproved'])):
                             $inlist=2;
                        endif;
                endif;                   
                
                // if base sale is not set update inlist flag to 0 but if order is generated for that supplier then check before setting to 0
                $inlist=($value['base_amount']>0)?$inlist:(isset($existingOrderAmts['approved'][$value['supplier_operator_id']])?$inlist:0);
                
                $supplierlist[$key]['in_list']=$inlist;
                
               $actualdays=$existsFlag?$finaldays:$this->setNoofDays($frequencylist[$frequencykey],$days);
            
               $supplierlist[$key]['no_of_days']=$actualdays;
            
               if(!$existsFlag):
                    
                      $order=$this->getOrderForSupplier($supplierlist[$key],
                                                                                                                                  $actualdays,
                                                                                                                                  $operator_id
                                                                                                                                 );
                
                else:
               
                    // if exists flag then set order amt that was their earlier irrespective of approved / disapproved
                     if(isset($existingOrderAmts['approved'][$value['supplier_operator_id']])):
                         $order=$existingOrderAmts['approved'][$value['supplier_operator_id']];
                     elseif(isset($existingOrderAmts['disapproved'][$value['supplier_operator_id']])):
                          $order=$existingOrderAmts['disapproved'][$value['supplier_operator_id']];
                     else:   
                          $order=$this->getOrderForSupplier($supplierlist[$key],$actualdays,$operator_id);
                     endif;
                     
                endif;
                
         $supplierlist[$key]['order']=$order;         
        
        $supplierlist[$key]['to_pay']=  isUpperBoundH($value['supplier_operator_id'])?($order):($order*$factor); 
                
        endforeach;

        echo json_encode(array('supplierlist'=>$supplierlist));
            
            }
        
        public function  getOrderForSupplier($value=array(),$days=2,$operator_id=0,$isAjax=0)
        {
            if($this->input->get('isAjax')=="1"):
                $value=  json_decode($this->input->get('params'),true);
                $days=  $this->input->get('days');
                $operator_id=  $this->input->get('operator_id');
            endif;

            if($value['pending']<0): // Pending
                
                 $order=($value['base_amount']*$days)-getRoundoff($value['opening'],$operator_id)+$value['pending'];
            
            else:                               // Excess
                
                 $order=($value['base_amount']*$days)-getRoundoff($value['opening'],$operator_id);
            
            endif;
            
            if($order<0): return 0; endif;
                
//         // Handling excess pending case becuase order value will come -ve so just process order without excess pending
//          if($order<0):
//                
//                    $order=($value['base_amount']*$days)-getRoundoff($value['opening'],$operator_id);
//          
//                 //Say excess case ,  When base amount is to small then opening so 50000*2-120000=-ve order so 
//                    if($order<0):
//                        $order=0;
//                        return $order;
//                    endif;
//                    
//            endif;
            
            
            $order=  $this->CheckCapacityperDay($value['supplier_operator_id'],$order,$days,$value['capacity_per_day']);
            
            $order=  $this->CheckCapacityperMonth($value['supplier_operator_id'],$order,$days,$value['capacity_per_month']);
            
            $order=  $this->CheckCapacityperOrder($value['supplier_operator_id'],$order,$days,$value['capacity']);
            
            $order=  $this->CheckMinOrderAmount($value['supplier_operator_id'],$order,$value['min_capacity']);
            
            $order=  $this->CheckRiskFactor($value['supplier_operator_id'],$order,$value);
          
            $order=  $this->CheckWeeklyOrder($order,$value);
             
              // ROUND off to multiple of 10k for other  & 2.5k for idea
            $order=getOrderRoundoff($order,$operator_id);
            
           if($order<0): return 0; endif;
            
           if($this->input->get('isAjax')=="1"):
                echo json_encode(array('status'=>true,'type'=>true,'order'=>$order));exit();
           endif;
           
            return $order;
         }
         
         public function CheckCapacityperDay($so_id,$order,$days,$perdaycap=0)
         {
            // $perdaycap=  $this->order->getPerDayCapacity($so_id);
             
             if($order>($perdaycap*$days)):
                 
                 return ($perdaycap*$days);
             
             endif;
             
             return $order;
             
         }
         
         public function CheckCapacityperMonth($so_id,$order,$days,$permonthcap=0)
         {
            //  $permonthcap=  $this->order->getPerDayMonth($so_id);
             
              $tillnowOrderedAmount=  $this->order->getOrderAmountTillNow($so_id);
              
             if($order>($permonthcap-$tillnowOrderedAmount)):
                 
                 return ($permonthcap-$tillnowOrderedAmount);
             
             endif;
             
             return $order;
         }
         
         public function CheckCapacityperOrder($so_id,$order,$days,$perordercap=0)
         {
             //$perordercap=  $this->order->getPerDayOrder($so_id);
             
             if($order>($perordercap)):
                 
                 return $perordercap;
             
             endif;
             
             return $order;
             
         }
         
        
         public function CheckMinOrderAmount($so_id,$order,$minorder=0)
         {
             if($order<$minorder):
                 return $minorder;
             endif;
             
             return $order;
         }
         
         public function CheckRiskFactor($so_id,$order,$params)
         {
             if($params['pending']<0 && $params['riskfactor']>0)
             {   
                $params['pending']*=-1;
                $allowedpendingamt=($params['riskfactor']/100)*$params['base_amount'];
                
                // if avaliable pending is greater than allowed pending return zero
                if($params['pending']>$allowedpendingamt):
                    return 0;
                else:
                    return $order;
                endif;
             }
             
             return $order;
         }
        
        public function setinListFlag($freq)
        {
            // Check if Alternate date
            if(strpos($freq,',')):
         
                $freq=  ltrim($freq,'[');
                $freq=  rtrim($freq,']');
                $freqarr=  explode(',', $freq);

                        foreach ($freqarr as $f):
                            if($f==date('D'))
                            {
                                return true;
                            }
                        endforeach;
                    
          endif;
            
         // Check if Daily 
            if($freq=="Daily"):
                
                  return true;
            
            endif;
            
            
            if($freq=="[Mon-Fri]"):
                    if(date('D')!="Sat"):
                        return true;
                    endif;
            endif;
            
           if(date('D')==$freq):
                        return true;
            endif;
                    
            return 0;
            
        }
        
        public function CheckWeeklyOrder($order,$data)
        {
            if($data['frequency']==date('D')):
                    return $data['capacity'];
            endif;
            
            return $order;
        }
        public function calculateDays($days)
        {
           
            
            $calculatedDays=0;
            
            switch ($days):
                
                case $days==2 :
                                            $calculatedDays=$days;
                                            break;
                case $days==3:
                                          // $calculatedDays=$days-0.1;
                                           $calculatedDays=$days;
                                            break;
                case $days==4:
                                            //$calculatedDays=$days-0.1-0.15;
                                            $calculatedDays=$days;
                                            break;
                case $days==5:
                                            //$calculatedDays=$days-0.1-0.15-0.2;
                                            $calculatedDays=$days;
                                            break;
                 default :    
                                            $calculatedDays=2;
            endswitch;
            
            return $calculatedDays;
        }
        
        public function view($operator_id,$date=null)
        {
             $date=  is_null($date)?date('Y-m-d'):$date;
            
             $this->load->view('orders/view');
            
        }
         
        public function saveOrder($disapprovedorder=0)
        {
            $operator_id=  $this->input->post('operator_id');
            $orders=  !$disapprovedorder?json_decode(json_decode($this->input->post('orders'),true),true):array(json_decode($this->input->post('orders'),true));
            
            
            if($this->order->saveOrder($orders,$operator_id)):
                    echo json_encode(array('success'=>true,'type'=>true,'message'=>'Order has been saved.')); exit();
            endif;
            
          echo json_encode(array('success'=>true,'type'=>false,'message'=>'Unable to save order .')); exit();
            
            
        }
        
        public function checkifOrderexists()
        {
            $data=$this->order->getOrder($this->input->get('operator_id'));
                    
            if($data['exists']):
                
                echo json_encode(array('success'=>true,'type'=>true,'message'=>'Order Already Exists')); exit();
                
            endif;
            
             echo json_encode(array('success'=>true,'type'=>false,'message'=>'Order does not exists')); exit();
        }
        
       
        
       public function getTotalLastDaySale()
       {
           $operator_id= ($this->input->get('operator_id')>0)?$this->input->get('operator_id'):0;
                      
           if($lastdaysale=$this->order->getTotalLastDaySale($operator_id)):
               
               echo json_encode(array('status'=>true,'type'=>true,'lastdaysale'=>$lastdaysale->total_last_day_sale));
               exit();
               
           endif;
           
           echo json_encode(array('status'=>true,'type'=>false));
           
           
       }
       
       
       public function getTotalFailurePercentage()
       {
           $failurepercentage=0;
           
           $operator_id= ($this->input->get('operator_id')>0)?$this->input->get('operator_id'):0;
           
           $operator_id= combineOperator($operator_id);
           
             if($failure=$this->order->getTotalFailurePercentage($operator_id)):
               
                 if($failure->Totalfailed > 0 && $failure->Totaltransactions > 0):
                     
                     $failurepercentage=($failure->Totalfailed*100)/($failure->Totaltransactions);
                 
                 endif;
                
             echo json_encode(array('status'=>true,'type'=>true,'failurepercentage'=>$failurepercentage));
             
             exit();
               
           endif;
           
           echo json_encode(array('status'=>true,'type'=>false));
       }
       
       
       public function getCurrentBalance()
       {
           $operator_id= ($this->input->get('operator_id')>0)?$this->input->get('operator_id'):0;
           
            $operator_id= combineOperator($operator_id);
            
           if($operator_id):
               
                        if($balance=  $this->order->getCurrentBalance($operator_id)):
                            
                             echo json_encode(array('status'=>true,'type'=>true,'balance'=>$balance));exit();
                        
                        endif;
           endif;
           
            echo json_encode(array('status'=>true,'type'=>false));
       }
       
       public function getTotalapiSale()
       {
            $operator_id= ($this->input->get('operator_id')>0)?$this->input->get('operator_id'):0;
            $avg=($this->input->get('avg')>0)?true:false;
            
             $operator_id= combineOperator($operator_id);
             
           if($operator_id):
               
                        if($totalAPIsale=  $this->order->getapiSale($operator_id,$avg)):
                            
                             echo json_encode(array('status'=>true,'type'=>true,'sale'=>$totalAPIsale));exit();
                        
                        endif;
           endif;
           
            echo json_encode(array('status'=>true,'type'=>false));
       }
       
       
       public function getExpectedSale()
       {
           error_reporting(0);
           
           $AB=  $this->order->getAB();
           
           $A=$AB[0]->avgsale;
           $B=$AB[1]->avgsale;
                
           $variance=array();
           
           $actualsales=  $this->order->getexpectedsalesbetweendates(date('Y-m-d',  strtotime('-6 day')),date('Y-m-d',  strtotime('-1 day')));
           
           $dates=  GetDays(date('Y-m-d',  strtotime('-6 day')), date('Y-m-d',  strtotime('-1 day')));
           
           foreach($dates as $date):
               
              $ES=(1+(($actualsales[$date]['10hr']-$A)/$A)) *$B;
           
               $variance[]=  ($actualsales[$date]['24hr']-$ES)/$ES;
              
              $ES=0;
              
           endforeach;
           
           $lastdaysale=$actualsales[date('Y-m-d',strtotime('-1 day'))]['24hr'];
           
           $avgVariance=  array_sum($variance)/count($variance);
          
           $X= $lastdaysale+(($avgVariance*$lastdaysale)/100);
            
            echo json_encode(array('status'=>true,'type'=>true,'variance_percentage'=>$avgVariance,'expectedSale'=>round($X,2)));
                        
       }
       
      
       public function reset($operator_id)
       {
           $this->order->deleteallorders($operator_id);
           
           redirect('orders/configure');
       }
       
       public function setFreezeFlag($so_id)
       {
            return $this->order->getFreezeFlag($so_id);
       }
       
       public function viewlastdayIncoming($so_id,$modemids,$operator_id,$dateparam=null)
       {
           
           $data['incomings']=  $this->order->getLastDayIncomingBySoidandModemids($so_id,$modemids,$operator_id,$dateparam);
           
          // $data['lastdayorderamt']=  $this->order->getLastdayOrderamtBySoid($so_id);
           
         //  $data['lastavaliablepending']=$this->order->getLastavaliablePendingExceptCurrent($so_id);
           
           $this->load->view('orders/viewlastdayIncoming',$data);     
       }
       
       public function saveComments()
       {
          $soid=  $this->input->post('soid');
          $comment=  $this->input->post('comment');
        
          if($this->order->saveCommentsBySOID($soid,$comment)):
            
             echo json_encode(array('status'=>true,'type'=>true,'msg'=>"Commented Successfully"));exit();
             
        endif;
        
          echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
       }
       
       public function setNoofDays($freq,$days)
       {
           switch ($freq):
                                case "Daily":
                                    
                                                return $days;
                                    
                                case "[Tue,Thu,Sat]":
                                    
                                                return 3;
                                 
                                case "[Mon,Wed,Fri]":
                                    
                                               if(date('D')=="Fri"):
                                                          return 3.5;
                                                endif;
                                                
                                                return 3;
                                    
                                case"[Mon-Fri]":
                                    
                                                if(date('D')=="Fri"):
                                                          return 3.5;
                                                endif;
                                                
                                                return $days;
                                                
                               default :           
                                                return 2;   
                              
           endswitch;      
           
     }
     
     public function operatorWiseTotalPending($operator_id=0)
     {
       $operator_id=  $this->input->get('operator_id')?$this->input->get('operator_id'):$operator_id;  
       echo  json_encode(array('status'=>true,'type'=>true,'pending'=>$this->order->getOperatorWiseTotalPending($operator_id)));
     }
     
     public function regenerateorder()
     {
         $orderids=$this->input->get('orderids');
         
         $data=$this->order->getRegeneratedOrderDetails($orderids);
         
         if(!empty($data)):
             echo json_encode(array('status'=>true,'type'=>true,'orders'=>$data));exit();
         else:
              echo json_encode(array('status'=>true,'type'=>false));exit();
         endif;
     }
     
     public function saveRegeneratedOrder()
     {
          $json=json_decode($this->input->post('data'),true);
          
          if(!empty($json) && is_array($json)):

                    $this->order->setRegenerateOrderAmt($json);
              
                        echo json_encode(array('status'=>true,'type'=>true));exit();
              
          endif;    
          
           echo json_encode(array('status'=>true,'type'=>false));exit();
         
     }
     
    public function getMaxCapPerOrder()
     {
         $soid=  $this->input->get('soid');
         
         if($soid):
             
                $maxCap=  $this->order->getMaxCapBySoid($soid);
                
               
                       echo json_encode(array('status'=>true,'type'=>true,'data'=>$maxCap));exit();
              
         endif;
         
         echo json_encode(array('status'=>true,'type'=>false));exit();
     }
     
     //check max capacity while regenerating orders
     public function isMaxCapExceeded()
     {
         $json=json_decode($this->input->post('data'),true);

         $soidarr=array();
         $temp=array();
         $jsonresp=array();
         $result=array();
         
         foreach($json as $val):
            $soid= explode("_", $val);
            $temp[]=$soid[0];
            $jsonresp[$soid[0]]=$soid[1];
         endforeach;
         
         $soidarr=  count($temp)>1?implode(",", $temp):(!empty($temp)?$temp[0]:$temp);
         $maxcap=$this->order->getCapacityBySOID($soidarr);

         foreach($jsonresp as $k=>$v):
            $info=$maxcap[$k];   
            if($v > $info['capacity'] ):
                $result[]=$info;
            endif;
        endforeach;
        
        if(!empty($result)):
            echo json_encode(array('status'=>true,'type'=>false,'data'=>$result));exit();
        else:
            echo json_encode(array('status'=>true,'type'=>true));exit(); 
        endif;
    }
     
}

