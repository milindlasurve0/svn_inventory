<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct() 
    {
       parent::__construct();

       $this->load->library('form_validation');
    }
   public function index()
   {
       $this->load->view('users/login');
   }
   
   
    public function login()
    {
     
        $username=$this->input->post('username',true);
        
        $password=$this->input->post('password',true);

        $this->load->library('Curl');
        
        $result=$this->curl->post("http://cc.pay1.in/users/authenticate",array('mobile'=>$username,'password'=>$password,'source'=>'2'));
        
        $result=  json_decode($result,true);
        
        if($result['status']=="success" && $result['type']==true && !empty($result['data'])): 
            
            $data=array('id'=>$result['data']['User']['id'],'username'=>$result['data']['User']['name'],'group_id'=>$result['data']['User']['group_id']);
            
            $this->session->set_userdata(array('user'=>(object)$data,'isLoggedIn'=>true));

            redirect('dashboard');
        
        else:
            
            $this->session->set_flashdata('error','Invalid Username/Password combination');
        
            redirect('/');
            
        endif;
        
    }
    
    public function logout()
    {
         $this->session->set_userdata('isLoggedIn' , false);
         
         redirect('/');
    }
    
    public function changepwd()
    {
        $data=$this->input->post();

            $this->load->helper(array('form'));

                    $this->form_validation->set_rules('old_pwd', 'Old Password', 'required|trim|xss_clean’');
                    $this->form_validation->set_rules('new_pwd', 'New Password', 'required|trim');
                    $this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'required|trim|matches[new_pwd]');

                    if ($this->form_validation->run() == FALSE): 

                            $this->session->set_flashdata('error','There was error while inserting record');

                            $this->load->view('users/changepwd');

                    else:

                            $this->load->model('user');
                            if($result=$this->user->change_password($data)): 
                                
                                $this->session->set_flashdata('success','Password Updated Successfully!');
                                redirect('users/changepwd');
        
                            else:

                                $this->session->set_flashdata('error','Old password does not match. Password not updated');
                                redirect('users/changepwd');

                            endif;
                    endif;
               
    }
    
    public function __getHashedPasswordFromCc($password)
    {
        $this->load->library('Curl');
        
        $result=$this->curl->post("http://cc.pay1.com/users/getHashedPassword",array('payload'=>$password));
        
        $result=  json_decode($result,true);
     
        $password=$result['data'];
        
        if(!empty($password)):
            return $password;
        endif;
        
        return FALSE;
        
     }
     
}