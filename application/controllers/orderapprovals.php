<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orderapprovals extends CI_Controller
{
   
    
     public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

        endif;
        
        $this->load->model('supplier');
        
        $this->load->model('operator');
        
        $this->load->model('orderapproval');
        
        $this->load->model('order');
        
         $this->load->helper('dates');
         
        $this->load->helper('order');
        
        $this->load->helper('orderapprovals');
        
        $this->load->helper('supplier');
        
         
        
    }
    
    public function index()
    {
         $data['operators']=$this->config->item('invoperators');
        $data['tlds']=  $this->gettotallastdaysale();
        $data['tldfp']=  $this->gettotalfailurepercentage();
        $data['tes']=  $this->getexpectedsale();
        $data['owp']=$this->order->getOperatorWiseTotalPending($this->input->get('operator_id'));
        
         if($this->input->get('operator_id')):
             
                // Order info
                $orders=  $this->getOrder($this->input->get('operator_id'));
               if(!empty($orders)):
               // Map Supplier selected banks to order array
                $orders=   $this->getSupplierBanks($orders);
               
                // Get no of days from any order index
                $data['no_of_days']=$orders[0]->no_of_days;
         
                $data['banks']=  $this->supplier->getBanks(true);
                
                //Sale Info
                $data['owlds']=  $this->getOperatorwiseLastDaySale($this->input->get('operator_id'));
                $data['owfp']=  $this->getOperatorwiseLastDayFailure($this->input->get('operator_id'));
                $data['owcb']=  $this->getOperatorwiseCurrentBalance($this->input->get('operator_id'));
                $data['owes']=  $this->getOperatorwiseExpectedSale($this->input->get('operator_id'));
                $data['owldsfa']=  $this->getOperatorwiseLastDaySaleFromapi($this->input->get('operator_id'));
                $data['totalorder']=  $this->getTotalOrder($orders);
                $data['shortfortheday']=$data['totalorder']+$data['owcb']-$data['owp']-($data['owes']*2);
             
               // generate Batch
               $orders=$this->createBatch($orders);
         
               ksort($orders['batch']);
               
               $data['orders']=$orders;
               else:
                   $data['orders']=array();
               endif;
                
           endif;
     
        $this->load->view('orderapprovals/index',$data);
    }
    
    
    public function bysupppliers()
    {
        $data['suppliers']=  $this->supplier->getSuppliersForDropdown();
        $data['tlds']=  $this->gettotallastdaysale();
        $data['tldfp']=  $this->gettotalfailurepercentage();
        $data['tes']=  $this->getexpectedsale();
        
         if($this->input->get('supplier_id')):
                
               // Order info
                $orders=  $this->getOrder(0,$this->input->get('supplier_id'));
                $data['banks']=  $this->supplier->getBanks(true);
                $data['orders']=$orders;
              
            endif;
 
           $this->load->view('orderapprovals/bysupppliers',$data); 
    }
    
    
    function createBatch($orders)
    {
        $tobeReturned=array();
        
        foreach($orders as $key=>$value):
                        
                        if($value->is_approved):
                              $tobeReturned['batch'][$value->batch][]=$orders[$key];
                        else:
                               $tobeReturned['batch']['Disapproved'][]=$orders[$key];
                        endif;
                      
        
        endforeach;
        
        return $tobeReturned;
    }
    
    public function getOrder($operator_id=0,$supplier_id=0)
    {
       // $this->load->model('pending');
        
        if($operator_id):
            
        
            if($orders=$this->orderapproval->getOrderByOperatorId($operator_id)):

                // Get  total closing array in format [soid]=[closingamt]
                $closings_arr=  $this->orderapproval->getClosingByOperatorID($operator_id);
            
                foreach($orders as $key=>$value):
                    
                    // Set Pendings
                   $orders[$key]->pending= $this->order->getLastPending($value->supplier_operator_id);
                
                    // Set Openings 
                    if(isset($closings_arr[$value->supplier_operator_id])):
                           $orders[$key]->opening=$closings_arr[$value->supplier_operator_id];
                    else:
                           $orders[$key]->opening=0.00;
                    endif;
  
                   // Add last avaliable pendings for exceptional cases supplier wise instead of soid wise
                    
                  //  $orders[$key]->exceptional_pending_amt=  $this->pending->getPendingBySupplierId($value->supplier_id,true,false);
               
                endforeach;

                return $orders;

            endif; 
         
         endif;
             
         
         
         if($supplier_id):     
         
             if($orders=$this->orderapproval->getOrderBySupplierId($supplier_id)):

                  // Get  total closing array in format [soid]=[closingamt]  
                $closings_arr=  $this->orderapproval->getClosingBySupplierID($supplier_id);
              
               // map 1 to many banks of suppliers  to orders array
                $orders=$this->getSupplierBanks($orders);
                
                foreach($orders as $key=>$value):
                    
                    // Set Pendings
                   $orders[$key]->pending= $this->order->getLastPending($value->supplier_operator_id);
                
                  // Set Openings 
                   if (isset($closings_arr[$value->supplier_operator_id])):
                        $orders[$key]->opening = $closings_arr[$value->supplier_operator_id];
                    else:
                        $orders[$key]->opening = 0.00;
                    endif;

                endforeach;
                
                return $orders;

            endif; 
        
         endif;
         
         
         return ;
    }
    public function gettotallastdaysale()
    {
          error_reporting(0);
          
        if(isset($_COOKIE['tlds'])):
            
            return $_COOKIE['tlds'];
        
        else:
            
          $sale= $this->order->getTotalLastDaySale();
        
         if($sale->total_last_day_sale>0){setcookie('tlds', $sale->total_last_day_sale, strtotime("tomorrow"));}
        
        return $sale;
        
        endif;
    }
    public function gettotalfailurepercentage()
    {
        error_reporting(0);
        
        if(isset($_COOKIE['tldfp'])):
            
            return $_COOKIE['tldfp'];
        
        else:
            
        $failure=$this->order->getTotalFailurePercentage();
        
        $failurepercentage=($failure->Totalfailed*100)/($failure->Totaltransactions);
         
        setcookie('tldfp', $failurepercentage, strtotime("tomorrow"));
        
        return $failurepercentage;
        
        endif;
    }
    public function getexpectedsale()
    {
        if(isset($_COOKIE['tes'])):
            
                return $_COOKIE['tes'];
        
        else:
            
            $data=getexpectedsalefortheday();

            $data=  json_decode($data);

            if($data->expectedSale>0):

                setcookie('tes', $data->expectedSale, strtotime("tomorrow"));
            
                return $data->expectedSale;

            endif;

            return ;
        
        endif;
    }
    
    public function getOperatorwiseLastDaySale($operator_id)
    {
         if(isset($_COOKIE['owlds_'.$operator_id])):
            
              return $_COOKIE['owlds_'.$operator_id];
        
        else:
            
            $data=$this->order->getTotalLastDaySale($operator_id);

            if(isset($data->total_last_day_sale)):

                setcookie('owlds_'.$operator_id, $data->total_last_day_sale, strtotime("tomorrow"));
            
                return $data->total_last_day_sale;

            endif;

            return ;
        
        endif;
        
    }
    
    public function getOperatorwiseLastDayFailure($operator_id)
    {
         if(isset($_COOKIE['owfp_'.$operator_id])):
            
              return $_COOKIE['owfp_'.$operator_id];
        
        else:
            
            $data=$this->order->getTotalFailurePercentage($operator_id);

            if(!empty($data)):

                  if($data->Totalfailed > 0 && $data->Totaltransactions > 0):
                     
                     $failurepercentage=($data->Totalfailed*100)/($data->Totaltransactions);
                 
                 endif;
                 
                setcookie('owfp_'.$operator_id, $failurepercentage, strtotime("tomorrow"));
            
                return $failurepercentage;

            endif;

            return ;
        
        endif;
    }
    
    public function getOperatorwiseCurrentBalance($operator_id)
    {
         if(isset($_COOKIE['owcb_'.$operator_id])):
            
              return $_COOKIE['owcb_'.$operator_id];
        
        else:
            
            $data=$this->order->getCurrentBalance($operator_id);

            if($data):

                setcookie('owcb_'.$operator_id,$data, strtotime("tomorrow"));
            
                return $data;

            endif;

            return ;
        
        endif;
        
    }
    
        public function getOperatorwiseExpectedSale($operator_id)
    {
         if(isset($_COOKIE['owes_'.$operator_id])):
            
              return $_COOKIE['owes_'.$operator_id];
        
        else:
            
           $data=getexpectedsalefortheday();
        
           $data=  json_decode($data);
           
           $variance=$data->variance_percentage;
           
           $sale=  $this->getOperatorwiseLastDaySale($operator_id);

           $expectedoperatorwisesale=$sale+(($variance*$sale)/100);
           
            if($expectedoperatorwisesale):

                setcookie('owes_'.$operator_id,$expectedoperatorwisesale, strtotime("tomorrow"));
            
                return $expectedoperatorwisesale;

            endif;

            return ;
        
        endif;
        
    }
    
    public function getOperatorwiseLastDaySaleFromapi($operator_id)
    {
        if(isset($_COOKIE['owldsfa'.$operator_id])):
            
              return $_COOKIE['owldsfa'.$operator_id];
        
        else:
        
            $data=$this->order->getapiSale(combineOperator($operator_id));

            if($data):

                setcookie('owldsfa_'.$operator_id,$data, strtotime("tomorrow"));
            
                return $data;

            endif;

            return ;
        
        endif;
    }
    
    function getTotalOrder($orders)
    {
      $sum=0;
      
        if(!empty($orders)):
            foreach($orders as $order):
                if($order->is_approved!="0"):
                    $sum+=$order->amount;
                endif;
            endforeach;
        endif;
        
        return $sum;
        
    }
    
    function updateorderamt()
    {
        if($data=$this->orderapproval->updateorderamt($this->input->post()))
        {
           echo json_encode(array('status'=>true,'type'=>true,'message'=>"Amount Updated","to_pay"=>$data));exit();
        }
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
    }
    
    function updateexcessamt()
    {
        if($data=$this->orderapproval->updateexcessamt($this->input->post()))
        {
           echo json_encode(array('status'=>true,'type'=>true,'message'=>"Excess Amount Released","to_pay"=>$data));exit();
        }
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
    }
    
    function updatebank()
    {
         if($this->orderapproval->updatebankid($this->input->post()))
        {
           echo json_encode(array('status'=>true,'type'=>true,'message'=>"Bank Updated"));exit();
        }
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
    }
    
    function updatebatch()
    {
         if($this->orderapproval->updatebatchid($this->input->post()))
        {
           echo json_encode(array('status'=>true,'type'=>true,'message'=>"Batch Updated"));exit();
        }
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
    }
    
    
    function approveorder()
    {
        
         if($this->orderapproval->approve($this->input->post()))
        {
           echo json_encode(array('status'=>true,'type'=>true,'message'=>"Order approved for selected supplier"));exit();
        }
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
         
    }
    function disapproveorder()
    {
        
         if($errormsg=$this->orderapproval->disapprove($this->input->post()))
        {
             echo json_encode(array('status'=>true,'type'=>true,'message'=>"Order disapproved",'error'=>$errormsg));exit();
        }
        
         echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
         
    }
    
    function getComments()
    {
        $soid=  $this->input->get('soid');
        $date= $this->input->get('date')?$this->input->get('date'):null;
        $order_id= $this->input->get('order_id')?$this->input->get('order_id'):null;
        
        if($soid>0):
            
                if($comments=$this->orderapproval->getCommentsBySOID($soid,$date,$order_id)):

                      echo json_encode(array('status'=>true,'type'=>true,'comments'=>$comments));exit();

                endif;
                
        endif;
        
          echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
    }
    
    function saveComments()
    {
        $order_id=  $this->input->post('orderid');
        $soid=  $this->input->post('soid');
        $comment=  $this->input->post('comment');
        
          if($this->orderapproval->saveCommentsByOrderID($order_id,$comment,$soid)):
            
             echo json_encode(array('status'=>true,'type'=>true,'msg'=>"Commented Successfully"));exit();
             
        endif;
        
          echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
    }
    
        public function getSupplierBanks($orders)
        {
             foreach($orders as $key=>$order):

                    $id=$order->parent_id>0?$order->parent_id:$order->supplier_id;
                    $supplierbankspair=  $this->orderapproval->getGroupedBanksBySupplierId($id);

                    $orders[$key]->supplier_selected_banks=$supplierbankspair;

             endforeach;

             return $orders;

        }
   }
    
