<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Searches extends CI_Controller
{
    
     public function __construct()
      {
            parent::__construct();
            if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

            endif;
      }
      
    public function searchSupplier()
    {
        $this->load->model('search');
        
        $data=  $this->search->getSupplierLike($this->input->get('q'));
        
        echo json_encode(array('results'=>$data));
        
    }
}