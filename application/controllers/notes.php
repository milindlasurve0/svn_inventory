<?php

class Notes extends CI_Controller
{
    public function __construct() {
        
        parent::__construct();
        
          if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

        endif;
        
        $this->load->model('note');
    }
    
    public function fetchNotesBySoid()
    {
        $notes=  $this->note->getNotesBySoid($this->input->get());
        
        if($this->input->is_ajax_request()):
              echo json_encode(array('status'=>true,'data'=>$notes)); exit();
        else:
                return $notes;
        endif;
    }
    
    public function save()
    {
        if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",  $this->input->post('note_date'))):
            echo json_encode(array('status'=>true,'error'=>true,'message'=>'Invalid Date format'));exit();
        endif;
        
        if(trim($this->input->post('message'))==""):
               echo json_encode(array('status'=>true,'error'=>true,'message'=>'Empty Message'));exit();
        endif;
        
        if($this->note->saveNote($this->input->post())):
                    echo json_encode(array('status'=>true,'type'=>true));exit();
       else:
                     echo json_encode(array('status'=>true,'type'=>false));exit();
       endif;
    }
}


