<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Alerts extends CI_Controller
{
       public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        
        $this->load->model('alert');
    }
    
    public function index()
    {
        $data['operators']=  $this->alert->getValidOperators();
        
        $data['alerts']=  $this->alert->getAlerts();
        
        $this->load->view('notifications/alerts',$data);
    }
}

