<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newblocksims extends CI_Controller
{
     public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('isLoggedIn')):

          $this->session->set_flashdata('error','You need to be logged in to view that page');
          redirect('/');

        endif;
        $this->load->model('newblocksim');
        $this->load->model('operator');
        $this->load->model('supplier');
        $this->load->model('incoming');
     }
     
     public function search()
     {
        $data=array();
        $modems=  $this->input->get('modems_ids')?$this->input->get('modems_ids'):"";
        $operator_id=  $this->input->get('operator_id')?$this->input->get('operator_id'):"";
        $is_blocked=$this->input->get('status')?$this->input->get('status'):"";
        $handled_by=$this->input->get('handled_by')?$this->input->get('handled_by'):"";
        $data['operators']=  $this->operator->getOperatorsForDropdown();
        $data['vendors']=$this->supplier->getVendorForDropdown();
        $data['blocktags']=$this->newblocksim->getBlockTagsForDropdown();
        $data['internals']=  $this->incoming->getInternalsPersons();

        if($this->input->get()):
                $data['simdata']=  $this->newblocksim->getBlockSimsByDate($this->input->get());                
        endif;
        
        $simid=array();
        $temp=array();
        $result=array();
        
        foreach($data['simdata'] as $sim):
            $simid[]=$sim['id'];
            $temp[$sim['id']]=$sim;
        endforeach;
        
        $blockid=count($simid)>1?implode(",", $simid):(!empty($simid)?$simid[0]:$simid);
        $msg_count=$this->newblocksim->getMsgCountByBlocksimId($blockid);
        
        foreach ($temp as $k=>$v):
            if(isset($msg_count[$k])):
                $v['messagecount']=$msg_count[$k]['totalmessages'];
            else:
                $v['messagecount']=0;
            endif;
            $data['sims'][]=$v;
        endforeach;

        $this->load->view('newblocksims/search',$data);
     }
     
     public function getBlockSimsComments()
     {
         $blockid=  $this->input->get('blockid');
         
         if($blockid>0):
            
                if($comments=$this->newblocksim->getCommentsByBlocksimId($blockid)):

                      echo json_encode(array('status'=>true,'type'=>true,'comments'=>$comments));exit();

                endif;
                
        endif;
        
          echo json_encode(array('status'=>true,'type'=>false,'message'=>"Error"));exit();
     }
     
     public function saveComments()
    {
        if($this->newblocksim->saveCommentsByBlocksimID($this->input->post())):
            
             echo json_encode(array('status'=>true,'type'=>true));exit();
             
        endif;
  
        echo json_encode(array('status'=>true,'type'=>false));exit();
    }
    
}