<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relationmgr extends CI_Controller
{
    public function __construct() 
    {
       parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        
        $this->load->library('form_validation');
           
        $this->config->load('inventory');

        $this->load->model('rm');
           
    }
    
    /*
     * Checks if valid record is being inserted using CI validation library also checks redundancy
     */
    public function add()
    {
        
        if($this->input->post()):
            
                    $this->load->helper(array('form'));

                    $this->form_validation->set_rules('rm_name', 'Name', 'required|trim|is_unique[inv_rm.name]');
                    $this->form_validation->set_rules('rm_email', 'Email', 'required|trim|is_unique[inv_rm.email_id]');
                    $this->form_validation->set_rules('rm_contact', 'Contact', 'required|trim|is_unique[inv_rm.mobile]');

                   $this->form_validation->set_message('is_unique', 'The %s field with similar value  already exists'); 

                    if ($this->form_validation->run() == FALSE): 

                            $this->session->set_flashdata('error','There was error while inserting record');

                            $this->load->view('relationmgr/add');

                    else:

                                if($this->rm->saveRM($this->input->post())):

                                $this->session->set_flashdata('success','Supplier was successfully inserted');

                                redirect('relationmgr/add');

                                else:

                                $this->session->set_flashdata('error','There was error while inserting record');

                                redirect('relationmgr/add');

                                endif;


                    endif;
            
          
        
        else:
            
            $this->load->view('relationmgr/add');
        
        endif;
        
        
        
    }
  
    public function view()
    {
        $this->load->library('pagination');
        
        $config['base_url'] = base_url('relationmgr/view/page/?');
        $config['total_rows'] = $this->rm->getTotalRows();
        $config['per_page'] = 5; 
        
        $pageno=$this->input->get('page')?$this->input->get('page'):1;
        $this->pagination->initialize($config); 
   
        $data['rms']=$this->rm->findAll($pageno);
        
        $this->load->view('relationmgr/view',$data);
    }
    
    public function edit($id)
    {
         if($this->input->post()):
             
                    $this->load->helper(array('form'));

                    $this->form_validation->set_rules('rm_name', 'Name', "required|trim|edit_unique[inv_rm.name.{$id}]");
                    $this->form_validation->set_rules('rm_email', 'Email', "required|trim|edit_unique[inv_rm.email_id.{$id}]");
                    $this->form_validation->set_rules('rm_contact', 'Contact',"required|trim|edit_unique[inv_rm.mobile.{$id}]");

                   $this->form_validation->set_message('edit_unique', 'The %s field with similar value  already exists'); 

                    if ($this->form_validation->run() == FALSE): 

                            $this->session->set_flashdata('error','There was error while updating record');

                             redirect('relationmgr/edit/'.$id);
                            

                    else:
                               if($this->rm->UpdateRM($id,$this->input->post())):

                                $this->session->set_flashdata('success','Record was Updated ');

                                redirect('relationmgr/edit/'.$id);

                                else:

                                $this->session->set_flashdata('error','There was error while Updating record');

                                redirect('relationmgr/edit/'.$id);

                                endif;
                                
                   endif;
         else:
             
           $data['rm']=$this->rm->findRmById($id);

           $this->load->view('relationmgr/edit',$data);
        
         endif;
        
    }
 
    public function delete($id)
    {  
        if($this->rm->deleteRM($id)):
            
                 $this->session->set_flashdata('success','Record Deleted');

                  redirect('relationmgr/view');
                  
        endif;
        
         $this->session->set_flashdata('error','Error in deleting Record');

          redirect('relationmgr/view');
    }
    
    public function dashboard()
    {
        $this->load->library('pagination');
        
        $this->load->model('operator');
        $this->load->model('supplier');
        $this->load->model('order');
        
        $is_rm= (getLoggedInUserKey('group_id')==8)?1:0;
        $rm_id= $is_rm?getLoggedInUserId():"";
        
        $operator_id=  $this->input->get('operator_id')?$this->input->get('operator_id'):"";
        $is_active=  $this->input->get('is_active')?$this->input->get('is_active'):"all";
        $modems=  $this->input->get('modems_ids')?$this->input->get('modems_ids'):"";
        $orderby=  $this->input->get('orderby')?$this->input->get('orderby'):"";
        $ordertype=  $this->input->get('ordertype')?$this->input->get('ordertype'):"";
         
        $config['base_url'] = base_url('relationmgr/dashboard/?operator_id='.$operator_id.'&is_active='.$is_active.'&modems_ids='.$modems.'&orderby='.$orderby.'&ordertype='.$ordertype);
         $total_rows= $this->rm->getTotalRowsByRmId($this->input->get(),$rm_id);
        $config['total_rows'] =$total_rows;
       
        $config['per_page']= $this->input->get('showall')?$total_rows:25;
        
        $pageno=$this->input->get('page')?$this->input->get('page'):1;
        
        $this->pagination->initialize($config); 
        
        $data['suppliers']=  $this->rm->findSuppliersByRmId($this->input->get(),$rm_id,$pageno,$config['per_page']);
        
        foreach($data['suppliers'] as $key=>$value):
            $data['suppliers'][$key]['order_amount_till_now']=$this->order->getOrderAmountTillNow($value['soid']);
            $data['suppliers'][$key]['remainingorderamt']=$value['capacity']-$data['suppliers'][$key]['order_amount_till_now'];
        endforeach;
        
        $data['operators']=  $this->operator->getOperatorsForDropdown();
        $data['vendors']=$this->supplier->getVendorForDropdown();
        $data['total_rows']=$total_rows;
        $data['totaluniquesuppliers']=$this->rm->getTotalUniqueSuppliers($rm_id);
        
        $this->load->view('relationmgr/dashboard',$data);
    }
    
    public function search()
    {
          $is_rm= (getLoggedInUserKey('group_id')==8)?1:0;
          $rm_id= $is_rm?getLoggedInUserId():"";
          $start=$this->input->get('start');
          $end=$this->input->get('end');
          
          $validate_date=(validateDate($start) && validateDate($end));
          
          if(!empty($start) && !empty($end) && !$validate_date):
              exit("Enter date in valid format");
          endif;
          
          $datediff=dateDiff($start,$end);
          
          if($datediff > 31):   
                exit("Daterange can not exceed 31 days.");
          endif;
                    
          $data['operators']=$this->config->item('invoperators');
          $data['suppliers']=  $this->rm->getSuppliersForDropdownByRmId($rm_id);
                      
          if($this->input->get()):
                
              $orders=$this->rm->getOrderHistoryByDate($this->input->get(),$rm_id);
              $data['orders']= $orders;
              
          endif;
          
          $this->load->view('relationmgr/search',$data);
    }
    
}
