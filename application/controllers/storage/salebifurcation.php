<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Salebifurcation extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        $this->load->model('planning');
        $this->load->library('Memcachedlib');
    }
    
    public function setSBtoMemcache()
    {
        
        $backenddashbaord = $this->planning->getBackendDashBoard();
         
        $backenddashbaord = $this->groupByOperator($backenddashbaord);
        
        $oprids = array_keys($backenddashbaord);
        
        $backenddashbaord = $this->addLDSandAVGS($backenddashbaord, $oprids);

        $backenddashbaord = $this->addPriority($backenddashbaord,$oprids);

       // $backenddashbaord = $this->addRequestpermin($backenddashbaord,$oprids);

        $backenddashbaord=  $this->addApisLDSandAVGS($backenddashbaord,$oprids);
        
        $backenddashbaord=  $this->addApiBalance($backenddashbaord,$oprids);
        
        $this->memcachedlib->set('salebiburcationreport',  json_encode($backenddashbaord),240);
   
        return $backenddashbaord;
    }
    
    public function groupByOperator($data)
        {
            $temp=array();
            foreach($data as $row):
                $temp[$row['operator_id']][]=$row;
            endforeach;
            return $temp;

        }
        
    public function addLDSandAVGS($data,$oprids)
        {
            foreach($oprids as $operator_id):

                         $last_day_sale_arr=  $this->planning->getLastdaySaleByOprId($operator_id,false,false,true);   
                         $avg_day_sale_arr=  $this->planning->getLastdaySaleByOprId($operator_id,true,false,true);

                        foreach($data[$operator_id] as $key=>$value):

                                    $data[$operator_id][$key]['last_day_sale']=isset($last_day_sale_arr[$value['supplier_operator_id']."_".$value['vendorid']])?$last_day_sale_arr[$value['supplier_operator_id']."_".$value['vendorid']]:0.00  ;
                                    $data[$operator_id][$key]['avg_sale']=isset($avg_day_sale_arr[$value['supplier_operator_id']."_".$value['vendorid']])?$avg_day_sale_arr[$value['supplier_operator_id']."_".$value['vendorid']]:0.00  ;

                         endforeach;

            endforeach;


            return $data;
        }    
        
    function addPriority($data,$oprids)
      {
           foreach($oprids as $operator_id):

                        $response=$this->memcachedlib->get('prod'.$operator_id);
                         $i=1;
                         $temp=array();
                         if(!empty($response)):
                         foreach($response['vendors'] as $row){  $temp[$row['vendor_id']]=$i++;}      
                         endif;

                        foreach($data[$operator_id] as $key=>$value):

                                    $data[$operator_id][$key]['priority']=isset($temp[$value['vendorid']])?$temp[$value['vendorid']]:0  ;

                         endforeach;

            endforeach;


            return $data;
      }
      
      
    public function addRequestpermin($data,$oprids)
  {
          foreach($oprids as $operator_id):
              
                    $requestData=  $this->planning->getRequestPerMin($operator_id);
                
                    if(!empty($requestData)):
                        
                            foreach($data[$operator_id] as $key=>$value):
                                             if($value['is_api']):  
                                                         if(isset($requestData['api_'.$value['vendorid']])):
                                                                    $data[$operator_id][$key]['last2minsale']=$requestData['api_'.$value['vendorid']]['last2minsale'];
                                                                    $data[$operator_id][$key]['totalrequest']=$requestData['api_'.$value['vendorid']]['totalreq'];
                                                                    $data[$operator_id][$key]['successreq']=$requestData['api_'.$value['vendorid']]['successreq'];
                                                                    $data[$operator_id][$key]['failurereq']=$requestData['api_'.$value['vendorid']]['failurereq'];
                                                         endif;  
                                                 else:
                                                           if(isset($requestData[$value['supplier_operator_id']."_".$value['vendorid']])):
                                                                    $data[$operator_id][$key]['last2minsale']=$requestData[$value['supplier_operator_id']."_".$value['vendorid']]['last2minsale'];
                                                                    $data[$operator_id][$key]['totalrequest']=$requestData[$value['supplier_operator_id']."_".$value['vendorid']]['totalreq'];
                                                                    $data[$operator_id][$key]['successreq']=$requestData[$value['supplier_operator_id']."_".$value['vendorid']]['successreq'];
                                                                    $data[$operator_id][$key]['failurereq']=$requestData[$value['supplier_operator_id']."_".$value['vendorid']]['failurereq'];
                                                           endif;
                                              endif;
                           endforeach;
                    
                    else:
                        
                            return $data;
                    
                    endif;
                    
          endforeach;
          
          return $data;
  }
  
//  public function setSBToDb()
//  {
//      $date=date('Y-m-d');
//      logerror('Started','salebifurcationhistory');
//      
//      $output=$this->setSBtoMemcache();
//      $data['backenddashboard']=$output;
//      
//      $vendors = getVendorsKeyValuePair();
//      $data['vendors']=$vendors;
//      
//      $view=  $this->load->view('dashboard/backenddashboard',$data,true);
//      
//      logerror("Setting whole Sale Bifurcation to database  for date : {$date}  with data : ".  json_encode($output),'salebifurcationhistory');
//      
//      $this->planning->saveSaleBifurcationHistory(array('json_data'=>  json_encode($output),'html_data'=>$view));
//  }
  
  public function addApisLDSandAVGS($data,$oprids)
  {
      $cachedapiAVGsale=$this->memcachedlib->get('inv_avgapisale');
      $cachedapiLDSsale=$this->memcachedlib->get('inv_ldsapisale');
      
      if(!empty($cachedapiAVGsale)):
            $apiavgSales=json_decode($cachedapiAVGsale,true);
      else:
             logerror("Setting AVG API sale  to Memcache","salebifurcationhistory");
            $apiavgSales=$this->planning->getApisAVGS();
            logerror("AVG Query Response : ". json_encode($apiavgSales),"salebifurcationhistory");
            $this->memcachedlib->set('inv_avgapisale',  json_encode($apiavgSales),strtotime('tomorrow') - time());
      endif;
      
      if(!empty($cachedapiLDSsale)):
            $apildsSales=json_decode($cachedapiLDSsale,true);
      else:
            logerror("Setting LDS  API sale  to Memcache","salebifurcationhistory");
            $apildsSales=$this->planning->getApisLDS();
            logerror("LDS Query Response : ". json_encode($apildsSales),"salebifurcationhistory");
            $this->memcachedlib->set('inv_ldsapisale',  json_encode($apildsSales),strtotime('tomorrow') - time());
      endif;
      

       foreach($oprids as $operator_id):
                         foreach($data[$operator_id] as $key=>$value):
           
                                if($value['is_api']=="1"):
                                    
                                        $data[$operator_id][$key]['avg_sale']=isset($apiavgSales[$value['vendorid']."_".$value['operator_id']])?$apiavgSales[$value['vendorid']."_".$value['operator_id']]:0;
                                        $data[$operator_id][$key]['last_day_sale']=isset($apildsSales[$value['vendorid']."_".$value['operator_id']])?$apildsSales[$value['vendorid']."_".$value['operator_id']]:0;
                          
                                endif;
                                
                         endforeach;
       endforeach;
       
       return $data;
       
  }
  
  public function addApiBalance($data,$oprids)
  {
      foreach($oprids as $operator_id):
                foreach($data[$operator_id] as $key=>$value):
                           
                             if($value['is_api']=="1"):
                                 
                                $apibal=0;
                                $apires=$this->memcachedlib->get('balance_'.$value['vendorid']);
                                
                                        if(isset($apires['xml']['Wallet'])):  
                                            $apibal=$apires['xml']['Wallet'];  
                                        elseif(isset($apires['Data']['Balance'])):
                                             $apibal=$apires['Data']['Balance'];  
                                        elseif(isset($apires['ERROR']) && isset($apires['REST'])>=0 && count($apires)>1 ): 
                                               $apibal=$apires['REST'];  
                                        elseif($apires>=0 && !is_array($apires) && strpos($apires,"|")===false ): 
                                             $apibal=$apires; 
                                        elseif(isset($apires[1])>0 && is_array($apires) && is_numeric($apires[1])):
                                              $apibal=$apires[1];
                                        elseif(isset($apires['_ApiResponse']['availableBalance'])):  
                                               $apibal=$apires['_ApiResponse']['availableBalance'];  
                                        elseif(isset($apires['GetBalanceResponse']['REMAININGAMOUNT'])):
                                              $apibal=$apires['GetBalanceResponse']['REMAININGAMOUNT'];
                                         elseif($apires!="" && !is_array($apires) && strpos($apires,"|") ):  
                                                $temp=explode("|",$apires);
                                                $apibal=  isset($temp[1])?$temp[1]:0;    
                                        else:  $apibal=0;endif;

                                         
                                  $data[$operator_id][$key]['currentstock']=$apibal;
                                  
                            endif;
                            
                            
                endforeach;
      endforeach;
   
      return $data;
    }
  
    public function setSBtoDb()
    {
      
        logerror('Started','salebifurcationhistory');
        
        $data=array();
        $data=$this->setSBtoMemcache();
        
        logerror(json_encode($data),'salebifurcationhistory');
           
        //$data=json_decode($this->memcachedlib->get('salebiburcationreport'),true);
         $this->load->model('sb');
        foreach ($data as $operator_id=>$rows):
                        if(!empty($rows)):
                        $this->sb->setSbToDb($rows);
                        endif;
        endforeach;
        
         logerror('Completed','salebifurcationhistory');
    }
}

