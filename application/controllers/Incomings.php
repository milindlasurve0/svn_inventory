<?php

class Incomings extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        
        $this->load->model('operator');
        $this->load->model('supplier');
        $this->load->model('incoming');
       
    }
    
    public function search()
    {
      
        $data['operators']=$this->config->item('invoperators');
        $data['suppliers']=  $this->supplier->getSuppliersForDropdown();
        $data['internals']=  $this->incoming->getInternalsPersons();
        $data['vendors']=$this->supplier->getVendorForDropdown();
           
        if($this->input->get()):
            $data['incomings']=  $this->incoming->getIncomingByDate($this->input->get());
        endif;
        
          $this->load->view('incomings/search',$data);
    }
    
    public function modemwise()
    {
        $data['vendors']=$this->supplier->getVendorForDropdown();
        $data['operators']=$this->config->item('invoperators');
        
         if($this->input->get()):
            $data['incomings']=  $this->incoming->getModemwiseIncomingByDate($this->input->get());
        endif;
        
        $this->load->view('incomings/modemwise',$data);
    }
    
    public  function addapincoming()
    {
        $date=  $this->input->get('incoming_date')? $this->input->get('incoming_date'):date('Y-m-d');
        
       if(strtotime($date)<strtotime('today -3 days')):exit('Not Allowed'); endif;
           
        $data['date']=$date;
        
        $data['incomings']=$this->incoming->getAllapivendorswithoperators($date);
        
        $this->load->view('incomings/addapincoming',$data);
    }
    
    public function saveincoming()
    {
        if($this->incoming->setApiIncoming($this->input->post())):
                            echo json_encode(array('status'=>true,'type'=>true));exit();
        endif;
        
            echo json_encode(array('status'=>true,'type'=>false));exit();
    }
}

