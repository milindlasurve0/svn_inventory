<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Uploads extends CI_Controller
{
    public function __construct()
    {
         parent::__construct();
         $this->load->helper(array('form', 'url'));
         $this->load->library('csvreader');
         $this->load->helper('form');
    }
    
    public function index()
    {
       
        $this->load->view('uploads/index');
    }
    
    public function do_upload()
    {
             $config['upload_path'] = APPPATH.'../uploads';
             $config['allowed_types'] = 'csv';
             $this->load->library('upload', $config);
             if( $this->upload->do_upload('csvfile')):
                 echo "<pre>";
                 print_r($this->upload->data());
                 echo "</pre>";
                 
                 $result=$this->csvreader->parse_file('/var/www/inventory.pay1.com/uploads/unfilleddata.csv',true);
                 echo "<pre>";
                 print_r($result);
                 echo "</pre>";



             else:
                 echo "<pre>";
                 print_r( $this->upload->display_errors());
                 echo "</pre>";
                echo "ERROR : file upload failed";
             endif;
     }
     
     public function read()
     {
           $this->load->model('upload');
           $this->csvreader->separator=",";
           $result=$this->csvreader->parse_file('/var/www/inventory.pay1.com/uploads/final.csv');
           $temp=array();
            
         

           if(!empty($result)):
                                        foreach($result as $key=>$value):
               
                                                    if($value['Vendor_Closed']=="1"):
                                                                $temp=array(
                                                                                                'supplier_id'=>$value['supplier_id'],
                                                                                                'operator_id'=>$value['operator_id'],
                                                                                                'capacity'=>$value['Total_Order_Capacity'],
                                                                                                'capacity_per_day'=>$value['Vendor_total_Max_Capacity_Perday'],
                                                                                                'capacity_per_month'=>$value['Vendor_Capacity_per_month'],
                                                                                                'frequency'=>$value['frequency']?$value['frequency']:0,
                                                                                                'commission_type'=>$value['commissiontype'],
                                                                                               'commission_type_formula'=>$value['commissionpercent'],
                                                                                                'riskfactor'=>$value['riskfactor'],
                                                                                                'batch'=>$value['batch'],
                                                                                                'updated_at'=>date('Y-m-d H:is')
                                                                                           );

                                                                $this->upload->saveexceldatatoDB($temp,$value['supplier_operator_id'],$value['basesale']);
                                                    endif;  
                                                    
                                        endforeach;
           endif; 
           
     }
    
}
