<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();

        if(!$this->session->userdata('isLoggedIn')):
            
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        
        endif;
        
        $this->load->model('dashboardscreen');
        $this->load->model('supplier');
        $this->load->model('incoming');
        $this->load->model('operator');
         $this->load->model('planning');
        $this->load->library('Memcachedlib');
        
        
    }
    
    public function index()
    {
        $data=array();
        $data['operators'] = $this->config->item('operatorkeyvalue');
        $data['vendors']=getallVendorsKeyValuePair();
        
        switch(getLoggedInUserKey('group_id')):
        
          case 13 || 14 || 10 || 12 :
                              $data['todaysordersummary']=  $this->dashboardscreen->getTodaysOrdersummary();
          case 2:
                             
          default :    
            
        endswitch;
      
        $this->load->view('dashboard/index',$data);
    }
    
    public function viewtodaysorder()
    {
        $data['vendors']=$this->supplier->getVendorForDropdown();
        $data['internals']=  $this->incoming->getInternalsPersons();
        $data['operators']=$this->operator->getOperatorsForDropdown();
        $data['suppliers']=  $this->supplier->getSuppliersForDropdown();  
        
        $orders=$this->dashboardscreen->getTodaysOrdersdetails($this->input->get());
        
        $orders=$this->setNotesForTodaysOrder($orders);

        $data['todaysordersdetails']=$orders;
        
        $this->load->view('dashboard/todaysordersdetails',$data);
    }
    
    public function sendsms()
    {
        $this->load->library('Curl');
        $this->load->config('inventory');
        $temp=1;
        $data=$this->dashboardscreen->getsmsRelatedData($this->input->post());
        
        if(!empty($data['mobile'])):
                
                    foreach($data['mobile'] as $mobile):
            
                            if($mobile!=""):
                                 $sms=  sprintf($this->config->item('ordersmstemplate'),$data['info']['name'],$data['info']['todaysorder'],$data['info']['to_pay'],$data['info']['txnid'],$data['info']['operator']);   
                                 $this->curl->get("http://www.smstadka.com/redis/insertInQsms",array('mobile'=>$mobile['contact'],'root'=>'tata','sms'=>$sms));
                                 $this->dashboardscreen->setSmsEmailFlag($this->input->post('paymentid'),true);
                                 $temp*=1;
                             else:
                                 $temp*=0;
                             endif;
                    
                    endforeach;
                    
                    if(!$temp):
                        echo json_encode(array('status'=>true,'type'=>false,'message'=>"No Mobile Detected"));exit();
                    else:
                        echo json_encode(array('status'=>true,'type'=>true,'message'=>"Done"));exit();
                    endif;
                    
                    exit();
                    
        endif;
        
           echo json_encode(array('status'=>true,'type'=>false,'message'=>"Empty Mobile"));exit();

    }
    
    public function sendEmail()
    {
        $data=$this->dashboardscreen->getsmsRelatedData($this->input->post());
        
        if($data['info']['email']!=""):
        $this->load->library('email');
         $this->load->library('parser');
         
        $this->email->initialize(array('mailtype'=>'html','protocol'=>'sendmail'));
        
        $maildata=array('name'=>$data['info']['name'],'operator'=>$data['info']['operator'],'todaysorder'=>number_format($data['info']['todaysorder'],2),'to_pay'=>number_format($data['info']['to_pay'],2),'txnid'=>$data['info']['txnid']);  
      
        $this->email->from('orders@mindsarray.com', 'Pay1');
        $this->email->to($data['info']['email']); 
        $this->email->cc(array('deepti@mindsarray.com','pushpa@mindsarray.com','deepak.d@pay1.in','aamir@mindsarray.com')); 
        
        $this->email->subject('Todays Order');
        
        $message = $this->parser->parse('templates/ordermailtosupplier', $maildata, TRUE);
        $this->email->message($message);
        
        $this->email->send();
        
         $this->dashboardscreen->setSmsEmailFlag($this->input->post('paymentid'),false,true);
         
        echo json_encode(array('status'=>true,'type'=>true,'message'=>"Done"));exit();
        
        else:
            
              echo json_encode(array('status'=>true,'type'=>false,'message'=>"No emailid detected"));exit();
              
        endif;
    }
    
   public function getBackendDashBoard()
   {
    
        $operators=$this->input->get('oprids');
        $sbdate=$this->input->get('sb_sync_date')?$this->input->get('sb_sync_date'):date('Y-m-d');
        $backenddashbaord=array();
        
       $operators=!empty($operators)?array_flip($operators):$this->config->item('operatorkeyvalue');

       // $apiVendors=getApiVendorsKeyValuePair();
     
        if(strtotime($sbdate)==strtotime('today')):
        
        $memcacheData=$this->memcachedlib->get('salebiburcationreport');
        
                if(!empty($memcacheData)):
                     $backenddashbaord=  json_decode($memcacheData,true);
                else:

                        $i=1;
                        while($i<=3):
                                    $memcacheData=$this->memcachedlib->get('salebiburcationreport');
                                    if(!empty($memcacheData)):
                                        $backenddashbaord=  json_decode($memcacheData,true);
                                        break;
                                    else:
                                        sleep(10);
                                        $i++;
                                    endif;
                        endwhile;


                endif;
                
       elseif(strtotime($sbdate)<strtotime('today')):
            
              $oprs=$this->input->get('oprids');
              $vendorids=$this->input->get('vendorids');
              
              $oprs=$oprs?count($oprs)>1?implode(",",$oprs):$oprs[0]:0;
              $vendorids=$vendorids?count($vendorids)>1?implode(",",$vendorids):$vendorids[0]:0;
              
              $data=$this->planning->getSBhistoryByDate($sbdate,$oprs,$vendorids);
              
              $backenddashbaord= !empty($data)?$this->groupByOperator($data):"";
              
        endif;
        
        
                if(!empty($backenddashbaord)):

                           $data['vendors']=getallVendorsKeyValuePair();
                
                           $backenddashbaord=array_intersect_key($backenddashbaord,$operators);
                         
                            // Add Notes count to each records
                            $backenddashbaord=  $this->addNotesforSOID($backenddashbaord,$sbdate);
                            
                             // Filter data by vendorid
                              if($this->input->get('vendorids')): 
                                  $backenddashbaord=$this->filterDataByVendor($backenddashbaord,$this->input->get('vendorids')); 
                              endif;
                            
                            $data['backenddashboard']=$backenddashbaord;

                            $view=  $this->load->view('dashboard/backenddashboard',$data,true);

                        echo json_encode(array('status'=>true,'type'=>true,'data'=>$view));

                else:

                         echo json_encode(array('status'=>true,'type'=>true,'data'=>""));

                endif;
        
         
       
   }
   
     public function groupByOperator($data)
        {
            $temp=array();
            foreach($data as $row):
                $temp[$row['operator_id']][]=$row;
            endforeach;
            return $temp;

        }
   
        public function addNotesforSOID($listings,$date)
        {
            $this->load->model('earningreport');
            
            foreach ($listings as $operator_id=>$value):
                        $notes_arr=  $this->earningreport->getNotesCount($date,$operator_id,$type=1);
                        foreach($value as $key=>$row):
                                        $listings[$operator_id][$key]['messagecount']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]['totalmessages']:0;
                                        $listings[$operator_id][$key]['messages']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]['messages']:'';
                        endforeach;
            endforeach;
            
            return $listings;
         }
         
         function setNotesForTodaysOrder($orders)
         {
             if(empty($orders)): return $orders; endif;
              
             $this->load->model('earningreport');
              
             $notes_arr=  $this->earningreport->getNotesCount($date=date('Y-m-d'),$operator_id=0,$type=1);
             
              foreach($orders as $key=>$row):
                                        $orders[$key]['messagecount']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]['totalmessages']:0;
                                        $orders[$key]['messages']=isset($notes_arr[$row['supplier_operator_id']])?$notes_arr[$row['supplier_operator_id']]['messages']:'';
             endforeach;
             
             return $orders;
             
         }
         
         function filterDataByVendor($data,$vendorids)
         {
             foreach ($data as $operator_id=>$value):
                            foreach($value as $key=>$row):
                                        if(!in_array($row['vendorid'],$vendorids)):
                                            unset($data[$operator_id][$key]);
                                        endif;
                            endforeach;
             endforeach;
             
             return $data;
         }
  
}
