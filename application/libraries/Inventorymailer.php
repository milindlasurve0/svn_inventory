<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once APPPATH."third_party/phpmailer/PHPMailerAutoload.php";


class Inventorymailer
{
     var $ci;
     
    public function __construct() 
    {
        $this->ci =& get_instance();
        $this->ci->config->load('config');
       
    }
    public function sendMail($params)
    {
            $from=isset($params['from'])?$params['from']:"";
            $replyTo=isset($params['replyto'])?$params['replyto']:"";
            $to=isset($params['to'])?(is_array($params['to'])?$params['to']:(strpos($params['to'],",")?explode(",",$params['to']):array($params['to']))):"";
            $ccs=  isset($params['cc'])?$params['cc']:"";
            $body=$params['body'];
            $subject=$params['subject'];
         
            if(!empty($from) && !empty($to) && !empty($subject) && !empty($body)):
                
                $obj=new PHPMailer;
                $obj->isSMTP(); 
                $obj->Host=$this->ci->config->item('smtp_host');
                $obj->Username=$this->ci->config->item('smtp_user');
                $obj->Password=$this->ci->config->item('smtp_pass');
                $obj->SMTPDebug=0;
                $obj->SMTPAuth=true;
                $obj->setFrom($from,'Pay1');

                if(!empty($replyTo)): $obj->addReplyTo($replyTo); endif;
                
                logerror(json_encode($to),"inventorymailer");
                // Splitting array to get only first 3 elements for safe due to multiple to issues
                $final_to=array_splice($to,0,3);
                logerror(json_encode($final_to),"inventorymailer");
                foreach ($final_to as $value):  $obj->addAddress($value); endforeach;
                
                if(!empty($ccs)) : foreach ($ccs as $value): $obj->AddCC($value); endforeach; endif;

                $obj->Subject=$params['subject'];
                $obj->msgHTML($body);

                if(!$obj->send()):
                      logerror($obj->ErrorInfo,"inventorymailer");   
                endif;
            
            else:
                
                 logerror("Something seems empty","inventorymailer");   
          
            endif;
            
            logerror("END","inventorymailer");
            
    }
}



