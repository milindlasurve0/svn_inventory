<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once APPPATH."third_party/predis/Autoloader.php";

class Modemrequest
{
    var $redisObj;
    var $ci;
    
    public function __construct() 
    {
        $this->ci =& get_instance();
        $this->ci->config->load('config');
        $this->ci->load->library('curl');
    }
    
    public function redisConnect()
    {
        Predis\Autoloader::register();
       
        $host=  $this->ci->config->item('redis_host');
        $port=  $this->ci->config->item('redis_port');
        $password=  $this->ci->config->item('redis_password');
        
        try
        {
             $this->redisObj = new Predis\Client(array(
                                                                                            'host' =>$host,
                                                                                             'port' =>$port,
                                                                                             'password'=>$password
                                                                                           ));
        } 
        catch (Exception $e) 
        {
            $this->redisObj = false;
            echo $e->getMessage();
            echo "Couldn't connected to Redis";
            logerror($e->getMessage(),"redis");
        }
        
        
        return $this->redisObj;
       
    }
    
    public function get($query,$data=null,$timeout=30,$local=false)
    {
        
//        if($local===true):
//                    return array('status'=>'success','data'=>  $this->ci->curl->get("http://start.loc/start.php?{$query}")); 
//        endif;
        
        if(!is_null($data)):
        
             $modem_id = $data->id;

             $uuid = $modem_id . "_" . time() . rand(1, 99999);

             $query .= "&vendor_id=$modem_id&uuid=$uuid&reqtime=" . time();

             $vendor_q = $modem_id . "_" . $data->machine_id;
             
             $response = "";
              
             $max_limit = 20;
             
            $this->redisConnect();

                        if($this->redisObj==FALSE):

                            logerror("Unable to Connect to redis","redis");

                            return ;

                        endif;
                
             logerror("Checking Queue Length  for modem ID {$modem_id} ","redismaxthreshold");  
             
                    if($this->redisObj->llen($vendor_q) >= $max_limit):

                          logerror("max thresh limit reached  for modem ID {$modem_id}","redismaxthreshold");   
                    
                          return array('status'=>'failure','error'=>'max thresh limit reached');

                    endif;
              
             logerror("Inserting Data in queue  for modem ID {$modem_id}  : {$vendor_q} | data : {$query} ","redis");         
             
            $this->redisObj->set($uuid,"1");
            
            $this->redisObj->expire($uuid,20);       
            
            $this->redisObj->lpush($vendor_q,$query);
            
            
           logerror("Waiting for response of modem ID {$modem_id} ","redis");  
           
           $counter = 1;
             
           while(!$this->redisObj->hexists('dynamic_service',$uuid)):
               
                   if($counter > $timeout*5):
                     
                            break;
                   
                   endif;
                   
                 usleep(200000);
                   
                 $response_status = $this->redisObj->hexists('dynamic_service',$uuid);         
                 
                   $counter++;
                 
          endwhile;
          
        if($response_status):

            $response =  $this->redisObj->hget('dynamic_service',$uuid);
            
            logerror("Removing key from hash | key  for modem ID {$modem_id} : {$uuid} ","redis");
            
            $this->redisObj->hdel('dynamic_service',$uuid);
            
        endif;
            
        logerror("Response for modem ID {$modem_id} :  {$response}","redis");
        
        return array('status'=>'success','data'=>$response);
                        
        endif;
        
    }

    public function test()
    {
        logerror("Hello This is errodasdasdsa","redis");
//        $this->redisConnect();
//        
//        return $this->redisObj->get('mykey');
    }
    
    
   public function redisConnectSms($type='')
    {
        Predis\Autoloader::register();
       
        $redis_servers = $this->ci->config->item('redis_servers');
        
        $host=  $redis_servers[$type.'redis_host'];
        $port=  $redis_servers[$type.'redis_port'];
        if(in_array($type.'redis_password', $redis_servers))
        $password=  $redis_servers[$type.'redis_password'];
        
        try
        {
            $redis_config = array(
                                    'host' =>$host,
                                     'port' =>$port
                                   );
            if(in_array($type.'redis_password', $redis_servers))
              $redis_config['password']=$password;
             $this->redisObj = new Predis\Client($redis_config);
            
        } 
        catch (Exception $e) 
        {
            $this->redisObj = false;
            echo $e->getMessage();
            echo "Couldn't connected to Redis";
            logerror($e->getMessage(),"redis");
        }
        
        
        return $this->redisObj;
       
    }
     
    public function getRedisObj($type=''){
        return $this->redisConnectSms($type);
    }
}
